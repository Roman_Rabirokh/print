<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <div id="content">




    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCkLIJf6k9VGC7YgeGg6EbVXdz1EMFBeAQ"></script>

<h2>Options</h2>
<form>
<p><strong>Step 1</strong> : Radius
<input name="tb_radius" type="text" id="tb_radius" value="" size="4" maxlength="4" onchange="tb_radius_changed(this.value);">
km (maximum = 500km)
<strong>OR</strong>
<input name="tb_radius_miles" type="text" id="tb_radius_miles" value="" size="4" maxlength="4" onchange="tb_radius_miles_changed(this.value);">
miles (maximum = 310 miles)</p>

<p><strong>Step 2</strong> : Click on map <strong>OR</strong> Place radius by location name or post code :
<input name="goto" id="goto" type="text" size="25" maxlength="80">
</p>

<p><strong>Step 3</strong> : Remove duplicate postcodes? :
<input name="cb_removeduplicates" type="checkbox" id="cb_removeduplicates" checked="checked">
</p>

<center>
<p class="fmtbutton" onclick="javascript:usePointFromPostcode(document.getElementById('goto').value, placeMarkerAtPoint)">&nbsp;Draw Radius&nbsp;</p>
<p class="fmtbutton" onclick="fsmap();">&nbsp;Full Screen&nbsp;</p>
<p class="fmtbutton" onclick="ftn_resetmap();">&nbsp;Clear Map&nbsp;</p>
</center>

<h2>Output</h2>

<center>
	<div id="output" name="output">Done (404 unique postcodes found)</div>
	<p>Post Codes:</p>
    <p class="fmtbutton" onclick="ftn_togglecsv('tb_output');">&nbsp;Toggle CSV or New line&nbsp;</p><br>
	<textarea cols="50" rows="4" id="tb_output" name="tb_output" readonly=""></textarea>
    <p>Suburbs:</p>
    <p class="fmtbutton" onclick="ftn_togglecsv('tb_outputsuburbs');">&nbsp;Toggle CSV or New line&nbsp;</p><br>
	<textarea cols="50" rows="4" id="tb_outputsuburbs" name="tb_outputsuburbs" readonly=""></textarea>
</center>

<h3>Combined</h3>
<p>Post Code,Suburbs,Distance(<span id="span_unitssetting">km</span>)</p>

<center>
	<textarea cols="50" rows="4" id="tb_output_combined" name="tb_output_combined" readonly=""></textarea>
</center>




	</form>
	</div>

  </body>
</html>


<script type="text/javascript">
var map;
var geocoder = null;
var bool_clickmode=0;
var bounds;
var circle;
var centremarker;
var routeMarkers=new Array(0);

function GUnload(){}
function Gload()
{
var latlng = new google.maps.LatLng(-24.5,132.8);
var myOptions = {zoom:4,center:latlng,mapTypeId:google.maps.MapTypeId.ROADMAP,draggableCursor:'crosshair',mapTypeControlOptions:{style:google.maps.MapTypeControlStyle.DROPDOWN_MENU}};
map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);

document.getElementById("tb_output").value="";
document.getElementById("tb_outputsuburbs").value="";
document.getElementById("tb_radius").value=250;

tb_radius_changed(250);

google.maps.event.addListener(map, 'click', ftn_mapclick);

document.getElementById("span_unitssetting").innerHTML=settings_unit_handler.label;

ftn_pageloaded();
}


function ftn_mapclick(event)
{
if (bool_clickmode==0)
{
  startdraw(event.latLng);
}
}

function startdraw(point)
{
document.getElementById("output").innerHTML="Drawing Radius";

ftn_resetmap();

routeMarkers=new Array(0);


var givenRad = document.getElementById("tb_radius").value;

circle=DrawCircle(point,givenRad);
circle.setMap(map);

document.getElementById("output").innerHTML="<p><img src='images/loading.gif' />Searching...</p>";
document.getElementById("tb_output").value="";

showallwithfilter(givenRad,point.lat(),point.lng());

FMTradiuscentre=point;
FMTradiuskm=givenRad;
FMTkmlcoordinates=1;
}

function placeCentreMarker(point, text)
{
var image = {url: FMTmarkerurl,size: new google.maps.Size(20, 34),origin: new google.maps.Point(0,0),anchor: new google.maps.Point(10*FMTmarkersizefactor,34*FMTmarkersizefactor), scaledSize: new google.maps.Size(20*FMTmarkersizefactor, 34*FMTmarkersizefactor)};

var marker = new google.maps.Marker({position:point,map:map,icon:image,title:text,draggable:true,opacity:FMTmarkeropacity});

google.maps.event.addListener(marker, 'dragend', function()
{
  marker.setMap(null);
  startdraw(marker.getPosition());
});

return marker;
}

function ftn_resetmap()
{
if (circle)
{
  circle.setMap(null);
}

if (routeMarkers)
{
  for (i in routeMarkers)
  {
    routeMarkers[i].setMap(null);
  }
}

if (centremarker)
{
  centremarker.setMap(null);
}

document.getElementById("tb_output").value="";
document.getElementById("tb_outputsuburbs").value="";
bool_clickmode=0;

document.getElementById("tb_output_combined").value="";
}

function DrawCircle(point,rad) {

  rad *= 1000; // convert to meters if in miles
  draw_circle = new google.maps.Circle({
      center: point,
      radius: rad,
      strokeColor: "#FF0000",
      strokeOpacity: 0.8,
      strokeWeight: 1,
      fillColor: "#0000FF",
      fillOpacity: 0.25,
      map: null
  });
return draw_circle;
}

function showallwithfilter(givenRad,lat,lng)
{
bounds = new google.maps.LatLngBounds();


  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
  }
  catch (e)
  {
    // Internet Explorer
    try
      {
        xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      }
  catch (e)
  {
    try
    {
      xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
      catch (e)
    {
      alert("Your browser does not support AJAX!");
      return false;
    }
      }
  }
  xmlHttp.onreadystatechange=function()
{
      if(xmlHttp.readyState==4)
      {
    var xml = xmlHttp.responseXML;

    var output="";
    var suburb=""
    var count=0;

    var extra="";
    bool_clickmode=1;
    var output_combined = new Array();



    document.getElementById("tb_outputsuburbs").value=suburb.substring(0,suburb.length-1);

    document.getElementById("tb_output_combined").value = output_combined.join("\n");

    if (document.getElementById('cb_removeduplicates').checked==true)
    {
      document.getElementById("tb_output").value=removeduplicatesfromcsv(output.substring(0,output.length-1));
    }
    else
    {
      document.getElementById("tb_output").value=output.substring(0,output.length-1);
    }

    document.getElementById("output").innerHTML="Re-zoom...";
    if (count !=0)
    {
      map.setCenter(bounds.getCenter());
      map.fitBounds(bounds);
    }

    var commas;
    if (document.getElementById('cb_removeduplicates').checked==true)
    {
      commas=removeduplicatesfromcsv(output.substring(0,output.length-1)).CountCommas();
    }
    else
    {
      commas=output.substring(0,output.length-1).CountCommas();
    }
    commas=removeduplicatesfromcsv(output.substring(0,output.length-1)).CountCommas();
    var cplusone=commas+1;

    document.getElementById("output").innerHTML="Done ("+ cplusone +" unique postcodes found)" + extra;




    //showallwithfilter_suburb(givenRad,lat,lng);

  }
};
var rn=Math.floor(Math.random()*9999);
var urltext="ajax/get-all-australia-postcodes-inside.php?radius="+givenRad+"&lat="+lat+"&lng="+lng+"&rn="+rn;
//console.log(urltext);
  xmlHttp.open("GET",urltext,true);
  xmlHttp.send(null);

}

function placeMarker(id, point, outcode)
{
var image = {url: FMTmarkerurl,size: new google.maps.Size(20, 34),origin: new google.maps.Point(0,0),anchor: new google.maps.Point(10*FMTmarkersizefactor,34*FMTmarkersizefactor), scaledSize: new google.maps.Size(20*FMTmarkersizefactor, 34*FMTmarkersizefactor)};

var marker = new google.maps.Marker({position:point,icon:image,map:map,title:outcode,draggable:false,opacity:FMTmarkeropacity});

google.maps.event.addListener(marker, 'click', function()
{
  marker.setMap(null);

});

return marker;
}

/*
function showallwithfilter_suburb(givenRad,lat,lng)
{
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
  }
  catch (e)
  {
    // Internet Explorer
    try
      {
        xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      }
  catch (e)
  {
    try
    {
      xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
      catch (e)
    {
      alert("Your browser does not support AJAX!");
      return false;
    }
      }
  }
  xmlHttp.onreadystatechange=function()
{
      if(xmlHttp.readyState==4)
      {
    var xml = xmlHttp.responseXML;
    //alert (xmlHttp.responseText);
    var output="";
    var count=0;
    var markers = xml.documentElement.getElementsByTagName("marker");
    var extra="";

    if  (markers.length>3000)
    {
      for (var i = 0; i < markers.length; i++)
      {
        var outcode = markers[i].getAttribute("suburb");
        output+=outcode+",";
        count++;
      }
    }
    else
    {
      for (var i = 0; i < markers.length; i++)
      {
        var outcode = markers[i].getAttribute("suburb");
        output+=outcode+",";
      }
    }

    document.getElementById("tb_outputsuburbs").value=output.substring(0,output.length-1);

  }
};
var rn=Math.floor(Math.random()*9999);
var urltext="ajax/get-all-australia-suburbs-inside.php?radius="+givenRad+"&lat="+lat+"&lng="+lng+"&rn="+rn;
  xmlHttp.open("GET",urltext,true);
  xmlHttp.send(null);

}
*/

function removeduplicatesfromcsv(input)
{
var splitted = input.split(',');
var collector = {};
for (i = 0; i < splitted.length; i++) {
   key = splitted[i].replace(/^\s*/, "").replace(/\s*$/, "");
   collector[key] = true;
}
var out = [];
for (var key in collector) {
   out.push(key);
}
return out.join(',');
}


function tb_radius_changed(inp)
{
if (inp>500)
{
  document.getElementById("tb_radius").value=500;
  inp=500;
}
document.getElementById("tb_radius_miles").value=(inp*(1/1.609344)).toFixed(2);
}

function tb_radius_miles_changed(inp)
{
if (inp>310)
{
  document.getElementById("tb_radius_miles").value=310;
  inp=310;
}
document.getElementById("tb_radius").value=(inp*1.609344).toFixed(2);
}

function usePointFromPostcode(place , callbackFunction)
{
geocoder = new google.maps.Geocoder();
geocoder.geocode( { 'address': place + ", Australia"}, function(results, status)
{
  if (status == google.maps.GeocoderStatus.OK)
  {
    var point = results[0].geometry.location;
    var resultLat = point.lat;
    var resultLng = point.lng;
    callbackFunction(point);
  }
  else
  {
        alert("Location not found!");
      }
  });
}

function placeMarkerAtPoint(pointin)
{
var str_lat=pointin.lat();
var str_lng=pointin.lng();

var point=new google.maps.LatLng(parseFloat(str_lat),parseFloat(str_lng));
startdraw(point);
}

String.prototype.CountCommas = function(){
return (this.split(",")).length-1;
};
</script>
