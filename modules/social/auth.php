<?php
session_start();

$url = $_SERVER['DOCUMENT_ROOT'];

include_once($url."/api/config.php");
include_once($url."/api/db.php");

include_once($url."/api/functions.php");
include_once($url."/api/image.php");
include_once($url."/api/user.php");
include_once($url."/api/application.php");
include_once($url."/api/page.php");




if(isset($info) && isset($info['id']) && !empty($info['id']))
{
  $first_name = '';
  $last_name = '';
  $email='';
  $city ='';
  $country=''; // некуда вставлять)
  $phone= '';
  
  if(!empty($info['first_name']))
  {
    $first_name = $info['first_name'];
  }
  if(!empty($info['last_name']))
  {
    $last_name = $info['last_name'];
  }

  if(!empty($info['email']))
  {
      $email=$info['email'];
      $isValid= dbGetOne('SELECT id FROM #__users WHERE email = :email', ['email' => $email]);
      if(!empty($isValid))
          $email='';
  }
  if(!empty($info['city']))
  {
      $city =$info['city'];
  }
  
  if(!empty($info['country']))
  {
      $country =$info['country'];
  }
  
  if(!empty($info['phone']))
  {
      $phone =$info['phone'];
  }
  
  
 global $user;
 $user = new User;
  $id = dbGetOne("SELECT id FROM #__users WHERE social_code = :code",array(":code" => $info['id']) );

  if(!empty($id))
  {


     $user->AuthorizeByID($id);
      header('location:https://www.yashaprint.com/');

  } else {

  $id = dbNonQuery("INSERT INTO #__users(created,social_code,social, email) VALUES(NOW(),:code,:social, :email)",array(":code" => $info['id'],":social" => $info['socialservice'], ':email' => $email),true);
  dbNonQuery("INSERT INTO #__profiles(created,userid,active,first_name,last_name) VALUES(NOW(),:id,1,:first_name,:last_name)",array(
    ':first_name' => $first_name,
    ':last_name' => $last_name,
    ':id' => $id));
  dbNonQuery('INSERT INTO #__user_info (fio, userid, email, city, phone) VALUES(:fio, :userid, :email, :city, :phone)', 
        [
            ':fio'      => $first_name.' '.$last_name,
            ':userid'   => $id,
            ':email'    => $email,
            ':city'     => $city,
            ':phone'    => $phone,
        ]);

   $user->AuthorizeByID($id);
    header('location:https://www.yashaprint.com/');
  }

  if($info['socialservice'] == 'facebook' || $info['socialservice'] == 'twitter' || $info['socialservice'] == 'ok' || $info['socialservice'] = 'mailru' || $info['socialservice'] == 'google' || $info['socialservice'] == 'vk')
  {
    if(isset($img) && !empty($img) && isset($_SESSION['CURRENT_USER']))
    {
      $_SESSION["CURRENT_USER"]['avatar'] = $img;
    }
  }



 }
