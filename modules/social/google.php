<?php

$client_id = '923569619710-3uaqskanfuakgk0h8hk14vosbegp8utd.apps.googleusercontent.com'; // Client ID
$client_secret = 'ZbvX7-JLVKn6MTA2-FY7bKoh'; // Client secret
$redirect_uri = 'https://www.yashaprint.com/modules/social/google.php'; // Redirect URIs

$url = 'https://accounts.google.com/o/oauth2/auth';

$params = array(
    'redirect_uri'  => $redirect_uri,
    'response_type' => 'code',
    'client_id'     => $client_id,
    'scope'         => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'
);



if (isset($_GET['code'])) {
    $result = false;

    $params = array(
        'client_id'     => $client_id,
        'client_secret' => $client_secret,
        'redirect_uri'  => $redirect_uri,
        'grant_type'    => 'authorization_code',
        'code'          => $_GET['code']
    );

    $url = 'https://accounts.google.com/o/oauth2/token';

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($curl);
    curl_close($curl);
    $tokenInfo = json_decode($result, true);

    if (isset($tokenInfo['access_token'])) {
        $params['access_token'] = $tokenInfo['access_token'];

        $userInfo = json_decode(file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo' . '?' . urldecode(http_build_query($params))), true);
        if (isset($userInfo['id'])) {
            $userInfo = $userInfo;
            $result = true;
        }
    }
}


if ($result) {
  $img = '';
  $info = array('id' => $userInfo['id'],
                'socialservice' => 'google',
                'last_name' => $userInfo['family_name'],
                'first_name' => $userInfo['given_name'],
                'email' => $userInfo['email']);
                if(isset($userInfo['picture']) && !empty($userInfo['picture']))
                {
                  $img = $userInfo['picture'];
                }
                require_once("auth.php");
}
?>
