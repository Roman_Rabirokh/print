<?php if(!defined("_APP_START")) { exit(); }
header('Access-Control-Allow-Origin: *');
include  "orders.php";


$server='https://'.$_SERVER['SERVER_NAME'];

$app->routes[] = array("pattern"=>'/upload\/order\/$/',"action"=>"_upload_order");

$app->routes[] = array("pattern"=>'/social\/cron\/$/',"action"=>"_social_cron");

$app->routes[] = array("pattern"=>'/item\/image\/upload\/(.*)\//',"action"=>"_images_temp_upload");

$app->routes[] = array("pattern"=>'/del\/preview\/$/',"action"=>"_del_preview");

$app->routes[] = array("pattern"=>'/del\/previewmy\/$/',"action"=>"_del_previewmy");
$app->routes[] = array("pattern"=>'/del\/imgprop\/$/',"action"=>"_del_imgprop");

$app->routes[] = array("pattern"=>'/save\/tmp\/$/',"action"=>"_save_tmp");

$app->routes[] = array("pattern"=>'/save\/properties\/$/',"action"=>"_save_properties");

$app->routes[] = array("pattern"=>'/duplicate\/image\/$/',"action"=>"_duplicate_image");


$app->routes[] = array("pattern"=>'/sorting\/all\/$/',"action"=>"_sorting_all");

$app->routes[] = array("pattern"=>'/save\/ordercost\/$/',"action"=>"_save_ordercost");


$app->routes[] = array("pattern"=>'/get\/cities\/$/',"action"=>"_get_cities");

$app->routes[] = array("pattern"=>'/get\/demoimg\/$/',"action"=>"_get_demoimg");

$app->routes[] = array("pattern"=>'/save\/profileinfo\/$/',"action"=>"_save_profileinfo");

$app->routes[] = array("pattern"=>'/save\/imageprofile\/$/',"action"=>"_save_imageprofile");

$app->routes[] = array("pattern"=>'/get\/npcities\/$/',"action"=>"_get_npcities");

$app->routes[] = array("pattern"=>'/get\/npoffice\/$/',"action"=>"_get_npoffice");

$app->routes[] = array("pattern"=>'/update\/orderadressnp\/$/',"action"=>"_update_orderadressnp");

$app->routes[] = array("pattern"=>'/update\/orderadresscurier\/$/',"action"=>"_update_orderadresscurier");

$app->routes[] = array("pattern"=>'/save\/callback\/$/',"action"=>"_save_callback");

$app->routes[] = array("pattern"=>'/paging\/all\/$/',"action"=>"_paging_all");

$app->routes[] = array("pattern"=>'/new\/paging\/([0-9]+)/',"action"=>"_new_paging");

$app->routes[] = array("pattern"=>'/save\/all\/properties\/$/',"action"=>"_save_all_properties");

$app->routes[] = array("pattern"=>'/delete\/all\/images\/$/',"action"=>"_delete_all_images");

$app->routes[] = array("pattern"=>'/get\/price\/np\/delivery\/$/',"action"=>"_getPriceNpDelivery");

$app->routes[] = array("pattern"=>'/liqpay\/pay\/$/',"action"=>"_liqpayPay");

//admin/ direct
$app->routes[] = array("pattern"=>'/admin\/delete-image-from-order\/$/',"action"=>"_directDeleteImageFromOrder");

function _directDeleteImageFromOrder()
{
    global $app;
    $app->setStatus(TRUE,FALSE);
    
    $id=!empty($_POST['id']) ? $_POST['id'] :  0;
    if($id)
    {
        $idImage= dbGetOne('SELECT imageid FROM #__photo_orders_rows  WHERE id= :id', [':id' => $id]);
        if(!empty($idImage))
        {
            dbNonQuery('DELETE FROM  #__photo_orders_rows WHERE id= :id', [':id'=> $id]);
            rmImageById($idImage);
            exit('success');
        }
        
    }
    exit('error');
}

function _liqpayPay()
{
    global $app;
    $app->setStatus(TRUE,FALSE);
    $pay= new Payments();
    $pay->setPay();
}

function _getPriceNpDelivery()
{
    include_once(_DIR . "modules/photosite/novaposhta.php");
    $id=!empty($_POST['idorder']) ? $_POST['idorder'] : 0;
    $city=!empty($_POST['city']) ? $_POST['city'] : 0;
    $home=isset($_POST['home']) ? $_POST['home'] : 0;
    if($id && $city)
    {
        $np= new Novaposhta();
        $price=$np->getPrice($city, $id,['toHome'=>$home]);
    }
    exit(json_encode($price));
}


function _delete_all_images()
{
 global $user;
 global $app;
 $orederid= !empty($_POST['id']) ? $_POST['id'] : 0 ;
 $userid=$user->getID();
 
 if($orederid)
 {
     $issetOrder= dbGetOne('SELECT created  FROM #__photo_orders WHERE id = :id AND userid = :user', [':id'=> $orederid, ':user' => $userid]);
     if($issetOrder)
     {
         $delete_rows= dbQuery('SELECT d.*, i.filename FROM #__photo_orders_rows AS d INNER JOIN #__images AS i ON(i.id=d.imageid) WHERE d.orderid= :id ', [':id' => $orederid]);
         foreach($delete_rows as $row)
         {
                $path = $_SERVER['DOCUMENT_ROOT'].'/files/images/'.$row['filename'];
                unlink($path);
                // del from db
                dbQuery('DELETE FROM  #__photo_orders_rows WHERE id = :imageid',array(
                  ':imageid'=>$row['id']
                ));
                dbQuery('DELETE FROM  #__images WHERE parentid = :imageid',array(
                  ':imageid'=>$row['id']
                ));
         }
         // удалим стоимость заказа
         dbNonQuery('UPDATE #__photo_orders SET cost=0 WHERE id = :id', [':id' => $orederid]);
     }
 }
 exit();
    
}

function _save_all_properties()
{
    global $user;
    global $app;
    $orderid=$_POST['idorder'];
    unset($_POST['idorder']);
    if(Orders::update_all_image_parametrs($_POST, $orderid, $user->getID()))
        echo'save';
    else
        echo 'not save';
        die;
}

function _save_imageprofile()
{
    global $user;
    $user->rmProfileImage($user->getID());
    global $app;

    $app->setStatus(TRUE,FALSE);
    $main_image=( isset($_FILES['file'])&& $_FILES['file']['size']>0) ? $_FILES['file'] : 0;
    $new_image=$user->saveProfileImage($user->getID(), $main_image);
    
    
    
    echo json_encode($new_image);
    
}


function _new_paging($page){


$_SESSION["page"]=$page['1'];
redirect ($_SERVER['HTTP_REFERER']);



}


function _paging_all(){

$_SESSION['paging']=$_POST['paging'];
$_SESSION["page"]="1";
 die;


}


function _save_callback () {
  Orders::add_callback ($_POST);
  //print_r($_POST);
  die;
}

function _update_orderadresscurier () {


    Orders::update_orderadresscurier ($_POST);

    die;

}

function _update_orderadressnp () {

    Orders::update_orderadressnp ($_POST);

    die;

}


function _get_npcities() {

  //print_r($_POST['ref']); die;

  //get cities
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  // только для https
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_URL,
      $server='https://'.$_SERVER['SERVER_NAME'].'/np/np.php?action=cities&ref='.$_POST['ref']
  );
  $content = curl_exec($ch);

  $np_cities=json_decode($content);
  $i=0;
  foreach ($np_cities as $np_city) {
    $line = get_object_vars($np_city);


    foreach ($line as $key => $value) {

      if ($key=="DescriptionRu")
      {
        $desc=$value;
        $rt[$i]['desc']=$desc;
      }
      if ($key=="Ref")
      {
        $ref=$value;
        $rt[$i]['ref']=$ref;

      }
      if ($key=="Area")
      {

        $area=$value;
        $rt[$i]['area']=$area;

      }

    }
    $i=$i+1;

  }
//print_r($rt); die;
  //отбираем нужное
  foreach ($rt as $key) {
//print_r($key); die;

    if ($key['area']==$_POST['ref']){
      $cities[]=$key;

    }

  }


//print_r($cities);
$option="<option value=''>Город</option></br>";
foreach ($cities as $city) {
  $option=$option.'<option value="'.$city["ref"].'">'.$city['desc'].'</option></br>';
}
print_r($option);

die;
}
function _get_npoffice() {



  //get offices
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  // только для https
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_URL,
      $server='https://'.$_SERVER['SERVER_NAME'].'/np/np.php?action=points&ref='.$_POST['ref']
  );
  $content = curl_exec($ch);



  $np_ofs=json_decode($content);
  $i=0;
  foreach ($np_ofs as $np_of) {
    $line = get_object_vars($np_of);



    foreach ($line as $key => $value) {

      if ($key=="DescriptionRu")
      {
        $desc=$value;
        $rt[$i]['desc']=$desc;
      }
      if ($key=="Ref")
      {
        $ref=$value;
        $rt[$i]['ref']=$ref;

      }
      if ($key=="CityRef")
      {
        $cref=$value;
        $rt[$i]['cityref']=$cref;

      }

    }
    $i=$i+1;

  }

  //отбираем нужное
  foreach ($rt as $key) {
//print_r($key); die;

    if ($key['cityref']==$_POST['ref']){
      $offices[]=$key;

    }

  }


//print_r($cities);
$option="<option value=''>Отделение</option></br>";
foreach ($offices as $office) {
  $option=$option.'<option value="'.$office["ref"].'">'.$office['desc'].'</option></br>';
}
print_r($option);

die;
}
function _save_profileinfo (){

  Orders::save_profileinfo($_POST);

  echo "Изменения сохранены"; die;
}

function _get_demoimg () {
  $folder=$_POST['folder'];

  $dir    = $_SERVER['DOCUMENT_ROOT'].'/files/tmp/'.$folder;
  if ($handle = @opendir($dir))
  {

    while (false !== ($file = readdir($handle))) {
      if (strpos($file, 'jpg') OR strpos($file, 'png') OR strpos($file, 'jpeg')OR strpos($file, 'gif')) {
        $image= new Images($dir.'/'.$file,'0');
        $image=$image->getInfo();
        $image['dpi']= Images::DPI($image['resolution']['width'], $image['resolution']['height'], 10, 15);
        $image['name']=$file;
        exit (json_encode([$image]));
        //print_r ($file); die;
      }
    }
    closedir($handle);
  }
die;
  //удаляем файлы из временной папки

//  foreach (glob($_SERVER['DOCUMENT_ROOT'].'/files/tmp/'.$folder.'/*') as $filed)
//  print_r($filed); die;

  //получаем имя файла


}

function _get_cities () {

$cities = Orders::get_cities($_POST['region_id']);
$result="";
foreach ($cities as $city) {
  $result=$result.'<option value="'.$city['city_id'].'">'.$city['name'].'</option>';
}

  print_r($result); die;

}



function _save_ordercost () {

  Orders::save_ordercost($_POST);
  die;
}

function _sorting_all(){

$_SESSION['sorting']=$order=$_POST['order'];
 die;


}

function _duplicate_image (){
          //запись в базу и перемещение файлов
          $file=$_POST['filename'];
          $order_id=$_POST['orderid'];
          //загрузка изображения
          $source=10;

          $url = $_SERVER['DOCUMENT_ROOT']."/files/images/".$file;

          $data = @getimagesize($url);

          $width=$data['0'];
          $height=$data['1'];
          $mime = explode("/",$data['mime']);
          $mime=$mime['1'];

          if(($mime != NULL)&&($width)&&($height))
          {
            //создаем пустой photo_orders_rows и берем его ИД в $parentid
            $order_row_id=Orders::create_new_order_row($order_id);

            $filename='copy_'.$file;
            $dir = $_SERVER['DOCUMENT_ROOT'] . "/files/images"; // Full Path
            copy($url, $dir . DIRECTORY_SEPARATOR . $filename);

              $new_image_id=dbNonQuery("INSERT INTO " . _DB_TABLE_PREFIX . "images (source,parentid,title,description,originalname,filename,width,height,mime) VALUES(:source,:parentid,:title,:description,:originalname,:filename,:width,:height,:mime
              )",array(
                ":source"=>$source,
                ":parentid"=>$order_row_id,
                ":title"=>'',
                ":description"=>'',
                ":originalname"=>$file,
                ":filename"=>$filename,
                ":width"=>$width,
                ":height"=>$height,
                ":mime"=>$mime
              ),TRUE);
              //возвращаем ИД изображения в photo_orders_rows
              //$new_image_id=Orders::get_last_image_id();
              //дописали ИД изображения в photo_orders_rows
              Orders::update_row($order_row_id,$new_image_id);
              
              echo 'Копия создана'; die;

      }
}


function _save_properties ()
{

  //сохраняем параметры изображения
  Orders::update_image_parametrs($_POST);
  echo'save';
    die;
}

function _save_tmp ($folder,$userid,$order_id,$isDelete=TRUE){


 if (isset($folder) && (!empty($folder)))
    {
        //удаляем файлы из временной папки
        if ($isDelete)
        {
            if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/files/tmp/' . $folder . '/thumbnail'))
                foreach (glob($_SERVER['DOCUMENT_ROOT'] . '/files/tmp/' . $folder . '/thumbnail/*') as $filed)
                    unlink($filed);
            if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/files/tmp/' . $folder . '/thumbnail'))
                rmdir($_SERVER['DOCUMENT_ROOT'] . '/files/tmp/' . $folder . '/thumbnail');
        }

        //записываем файлы в базу и в папку images



        $dir1 = $_SERVER['DOCUMENT_ROOT'] . '/files/tmp/' . $folder;
        if ($handle = @opendir($dir1))
        {
            $i = 1;
            while (false !== ($file = readdir($handle)))
            {
                //запись в базу и перемещение файлов
                $source = 10;

                $url = $dir1 . '/' . $file;

                $data = @getimagesize($url);

                $width = $data['0'];
                $height = $data['1'];
                $mime = explode(".", $file);
                if (!empty($mime['1']))
                    $mime = $mime['1'];

                //if(isset($mime)&&!empty($mime)&&($width)&&($height))
                if (strpos($file, 'jpg') OR strpos($file, 'png') OR strpos($file, 'jpeg')OR strpos($file, 'gif'))
                {




                    //создаем пустой photo_orders_rows и берем его ИД в $parentid
                    $order_row_id = Orders::create_new_order_row($order_id);

                    $filename = $order_row_id . '_' . $i . '_image.' . $mime;
                    $dir = $_SERVER['DOCUMENT_ROOT'] . "/files/images"; // Full Path


                    copy($url, $dir . DIRECTORY_SEPARATOR . $filename);
                    
                    $new_image_id = dbNonQuery("INSERT INTO " . _DB_TABLE_PREFIX . "images (source,parentid,title,description,originalname,filename,width,height,mime) VALUES(:source,:parentid,:title,:description,:originalname,:filename,:width,:height,:mime
              )", array(
                        ":source" => $source,
                        ":parentid" => $order_row_id,
                        ":title" => '',
                        ":description" => '',
                        ":originalname" => $file,
                        ":filename" => $filename,
                        ":width" => $width,
                        ":height" => $height,
                        ":mime" => $mime
                            ), $is_insert = true);
                    //возвращаем ИД изображения в photo_orders_rows
                    //    $new_image_id=Orders::get_last_image_id();
                    //  echo $new_image_id;
                    //дописали ИД изображения в photo_orders_rows
                    Orders::update_row($order_row_id, $new_image_id);

                }




                $i = $i + 1;
            }
            closedir($handle);
        }

        //удаляем файлы из временной папки
  if($isDelete){
  if (file_exists($_SERVER['DOCUMENT_ROOT'].'/files/tmp/'.$folder))
  foreach (glob($_SERVER['DOCUMENT_ROOT'].'/files/tmp/'.$folder.'/*') as $filed)
  unlink($filed);
  if (file_exists($_SERVER['DOCUMENT_ROOT'].'/files/tmp/'.$folder))
  rmdir($_SERVER['DOCUMENT_ROOT'].'/files/tmp/'.$folder);
  }

 }

}
function _delete_tmp_images($folder)
{
if (file_exists($_SERVER['DOCUMENT_ROOT'].'/files/tmp/'.$folder.'/thumbnail'))
  foreach (glob($_SERVER['DOCUMENT_ROOT'].'/files/tmp/'.$folder.'/thumbnail/*') as $filed)
  unlink($filed);
  if (file_exists($_SERVER['DOCUMENT_ROOT'].'/files/tmp/'.$folder.'/thumbnail'))
  rmdir($_SERVER['DOCUMENT_ROOT'].'/files/tmp/'.$folder.'/thumbnail');
  
  if (file_exists($_SERVER['DOCUMENT_ROOT'].'/files/tmp/'.$folder))
  foreach (glob($_SERVER['DOCUMENT_ROOT'].'/files/tmp/'.$folder.'/*') as $filed)
  unlink($filed);
  if (file_exists($_SERVER['DOCUMENT_ROOT'].'/files/tmp/'.$folder))
  rmdir($_SERVER['DOCUMENT_ROOT'].'/files/tmp/'.$folder);
  
}
function _del_preview ()
{

//  print_r ($_POST); die;
  $url = $_SERVER['DOCUMENT_ROOT']."/files/tmp/".$_POST['token']."/".$_POST['filename'];
  // del from folder
  unlink ($url);
  //удалить стоимость заказа
  if (!empty($_POST['id']))
        $orderid = $_POST['id'];
    else if (!empty($_SESSION['order_id']))
        $orderid = $_SESSION['order_id'];
    else
    {
        $orderid = Orders::order_exist();
        $orderid = $orderid['0']['id'];
    }
  dbNonQuery('UPDATE #__photo_orders SET cost=0 WHERE id = :id', [':id' => $orderid]);
  echo 'Изображение удалено';
  die;

}
function _del_previewmy ()
{

  //print_r ($_POST); die;
//get order id
//print_R($_POST); die;


  $dir    = $_SERVER['DOCUMENT_ROOT']."/files/images";

  $url = $_SERVER['DOCUMENT_ROOT']."/files/images/".$_POST['filename'];
  // del from folder
  unlink ($url);

  // удалим стоимость заказа
  $orderid= dbGetOne('SELECT orderid FROM #__photo_orders_rows WHERE imageid = :imageid',array(
    ':imageid'=>$_POST['imageid']));
    
   dbNonQuery('UPDATE #__photo_orders SET cost=0 WHERE id = :id', [':id' => $orderid]);
  
  // del from db
  dbQuery('DELETE FROM  #__photo_orders_rows WHERE imageid = :imageid',array(
    ':imageid'=>$_POST['imageid']
  ));
  dbQuery('DELETE FROM  #__images WHERE id = :imageid',array(
    ':imageid'=>$_POST['imageid']
  ));

  echo 'Изображение удалено';


  die;

}

function _del_imgprop ()
{
  //print_r ($_POST); die;
    $orderid= dbGetOne('SELECT orderid FROM #__photo_orders_rows WHERE id = :imageid',array(
    ':imageid'=>$_POST['imageid']));
    // удалим стоимость заказа
    dbNonQuery('UPDATE #__photo_orders SET cost=0 WHERE id = :id', [':id' => $orderid]);
    
  $path = $_SERVER['DOCUMENT_ROOT'].'/files/images/'.$_POST['filename'];
  unlink($path);
  // del from db
  dbQuery('DELETE FROM  #__photo_orders_rows WHERE id = :imageid',array(
    ':imageid'=>$_POST['imageid']
  ));
  dbQuery('DELETE FROM  #__images WHERE parentid = :imageid',array(
    ':imageid'=>$_POST['imageid']
  ));

 
  echo "Изображение удалено"; die;

}

function _images_temp_upload($params)
{

    global $app;
    $app->actionResult = TRUE;
    $app->loadTemplate = FALSE;
    global $user;
    $userId=$user->getID();
    if (!empty($_GET['id']))
        $orderid = $_GET['id'];
    else if (!empty($_SESSION['order_id']))
        $orderid = $_SESSION['order_id'];
    else
    {
        $orderid = Orders::order_exist();
        $orderid = $orderid['0']['id'];
    }
        require('UploadHandler.php');
        if ($app->checkToken('eslibmishkibylipchelami137', $params[1]) && !isset($_GET['file']))
        {


            $upload_handler = new UploadHandler(array(
                'upload_dir' => _TMP_PATH . $params[1] . '/'.$userId.'/'.$orderid.'/',
                'upload_url' => _FILES_URL . 'tmp/' . $params[1] . '/'.$userId.'/'.$orderid.'/',
                'script_url' => '/item/image/upload/' . $params[1] . '/'
            ));

  }
  else if($app->checkToken('eslibmishkibylipchelami137',$params[1]) && isset($_GET['file']))
        {

            $upload_handler = new UploadHandler(array(
            'upload_dir' => _TMP_PATH . $params[1] . '/'.$userId.'/'.$orderid.'/',
            'upload_url' => _FILES_URL . 'tmp/' . $params[1] . '/'.$userId.'/'.$orderid.'/',
            'script_url' => '/item/image/upload/' . $params[1] . '/'
            ));
            /*  header('Content-Type: application/json');
              $upload_handler->delete(); */
        } else
        {
            $app->actionResult = FALSE;
        }

    }


function _social_cron($params)
{
  global $app;
  global $user;
  $sendmail=array();
  $s_userid=array();
      $app->setStatus(TRUE,FALSE);
    // получаем таблицу для записи с соцсетей
    $imagelines=Orders::get_cron_line();



    if (isset($imagelines) &&(!empty($imagelines))){
      foreach ($imagelines as $line) {

        //проверяем вариант сохранения по sourceid
        // vk,fb,google,url
        if ($line['sourceid']=='1' or $line['sourceid']=='2' or $line['sourceid']=='3' or $line['sourceid']=='5')
        {

          //если у пользователя есть order c status=0
          if ($order=Orders::cron_order_exist($line['userid']))
          //add files to exist order
          {
            $order_id=$order['0']['id'];
          }
          else{
            //создаем новый order
            Orders::create_new_order();
            $result=Orders::order_exist();
            $order_id=$result['0']['id'];

          }




                      //создаем пустой photo_orders_rows и берем его ИД в $parentid
                      $order_row_id=Orders::create_new_order_row($order_id);



                      //загрузка изображения
                      $source=10;

                      $url = $line['url'];
                      $imageinfo = @getimagesize($url);
                      if($imageinfo != NULL)
                			{

                        $mime = explode("/",$imageinfo["mime"]);
                        $filename=$line['id'].'_image.'.$mime[1];
                        $dir = $_SERVER['DOCUMENT_ROOT'] . "/files/images"; // Full Path
                        copy($url, $dir . DIRECTORY_SEPARATOR . $filename);

                        $exif = exif_read_data($dir . DIRECTORY_SEPARATOR . $filename, 0, true);
                        if($exif) $exif=serialize($exif);




                				  $new_image_id = 	dbNonQuery("INSERT INTO " . _DB_TABLE_PREFIX . "images (source,parentid,title,description,originalname,filename,width,height,mime, exif_data) VALUES(:source,:parentid,:title,:description,:originalname,:filename,:width,:height,:mime, :exif
                					)",array(
                						":source"=>$source,
                						":parentid"=>$order_row_id,
                						":title"=>'',
                						":description"=>'',
                						":originalname"=>$filename,
                						":filename"=>$filename,
                						":width"=>$imageinfo[0],
                						":height"=>$imageinfo[1],
                						":mime"=>$mime[1],
                            ":exif"=>$exif
                					),$is_insert = true);



                          //возвращаем ИД изображения в photo_orders_rows
                      //    $new_image_id=Orders::get_last_image_id();
                          //дописали ИД изображения в photo_orders_rows
                          Orders::update_row($order_row_id,$new_image_id);
                          //удалили строку с которой записали
                          Orders::cron_delete_line($line['id']);

                          //добавляем ИД юзеров, которым надо отправить письмо о загрузке
                          $s_userid[]=$line['userid'];
                			}

        }
        // yandex, dropbox
        else{
            //yandex
            if ($line['sourceid']=='4')
            {
              //качаем по токену файлы в папку photo

              // include  $_SERVER['DOCUMENT_ROOT'].'/modules/photosite/social-api/get_yandex_folder.php';
              // get_folder_y ($line['token'],$line['url']);
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              // только для https
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_URL,
                  $server='https://'.$_SERVER['SERVER_NAME'].'/modules/photosite/social-api/get_yandex_folder.php?url='.$line['url'].'&token='.$line['token']
              );
              $content = curl_exec($ch);


              //если у пользователя есть order c status=0
              if ($order=Orders::cron_order_exist($line['userid']))
              //add files to exist order
              {
                $order_id=$order['0']['id'];
              }
              else{
                //создаем новый order
                Orders::create_new_order();
                $result=Orders::order_exist();
                $order_id=$result['0']['id'];

              }


              //получаем все файлы из photos


              $dir    = $_SERVER['DOCUMENT_ROOT'].'/modules/photosite/social-api/photos';
              //$files1 = scandir($dir);
              if ($handle = opendir($_SERVER['DOCUMENT_ROOT'].'/modules/photosite/social-api/photos')) {
                $i=1;
                while (false !== ($file = readdir($handle))) {



                      //запись в базу и перемещение файлов

                      //загрузка изображения
                      $source=10;

                      $url = $_SERVER['DOCUMENT_ROOT']."/modules/photosite/social-api/photos/".$file;
                      //echo $url.'</br>';

                      $data = @getimagesize($url);
                      if(empty($data))
                      {
                        continue;
                      }
                      $width=$data['0'];
                      $height=$data['1'];
                      $mime = explode("/",$data['mime']);
                      $mime=$mime['1'];

                      if(($mime != NULL)&&($width)&&($height))
                      {
                        //создаем пустой photo_orders_rows и берем его ИД в $parentid
                        $order_row_id=Orders::create_new_order_row($order_id);

                        $filename=$line['id'].'_'.$i.'_image.'.$mime;
                        $dir = $_SERVER['DOCUMENT_ROOT'] . "/files/images"; // Full Path
                        copy($url, $dir . DIRECTORY_SEPARATOR . $filename);

                        $exif = exif_read_data($dir . DIRECTORY_SEPARATOR . $filename, 0, true);
                        if($exif) $exif=serialize($exif);

                          $new_image_id = dbNonQuery("INSERT INTO " . _DB_TABLE_PREFIX . "images (source,parentid,title,description,originalname,filename,width,height,mime, exif_data) VALUES(:source,:parentid,:title,:description,:originalname,:filename,:width,:height,:mime,:exif
                          )",array(
                            ":source"=>$source,
                            ":parentid"=>$order_row_id,
                            ":title"=>'',
                            ":description"=>'',
                            ":originalname"=>$file,
                            ":filename"=>$filename,
                            ":width"=>$width,
                            ":height"=>$height,
                            ":mime"=>$mime,
                            ":exif"=>$exif
                          ),$is_insert = true);
                          //возвращаем ИД изображения в photo_orders_rows
                      //    $new_image_id=Orders::get_last_image_id();
                          //дописали ИД изображения в photo_orders_rows
                          Orders::update_row($order_row_id,$new_image_id);
                          //удалили строку с которой записали
                          Orders::cron_delete_line($line['id']);

                          //удаляем файл

                          //unlink($url);
                          $i=$i+1;
                      }





                }
                closedir($handle);
              }
              //удаляем файлы из временной папки photosite
              if (file_exists($_SERVER['DOCUMENT_ROOT'].'/modules/photosite/social-api/photos'))
              foreach (glob($_SERVER['DOCUMENT_ROOT'].'/modules/photosite/social-api/photos/*') as $filed)
              unlink($filed);

              $c_i=$i-1;
              //отправка отчета
              echo "Загружено с Yandex Disk ".$c_i." изображений  для пользователя с ID=".$line['userid']."</br>";


              //добавляем ИД юзеров, которым надо отправить письмо о загрузке
              $s_userid[]=$line['userid'];

            }
            //Dropbox
            if ($line['sourceid']=='7')
            {

              // ini_set('error_reporting', E_ALL);
              // ini_set('display_errors', 1);
              // ini_set('display_startup_errors', 1);
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              // только для https
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_URL,
                  $server='https://'.$_SERVER['SERVER_NAME'].'/modules/photosite/social-api/get_dropbox_folder.php?url='.$line['url'].'&token='.$line['token']
              );
              $content = curl_exec($ch);







              //если у пользователя есть order c status=0
              if ($order=Orders::cron_order_exist($line['userid']))
              //add files to exist order
              {
                $order_id=$order['0']['id'];
              }
              else{
                //создаем новый order
                Orders::create_new_order();
                $result=Orders::order_exist();
                $order_id=$result['0']['id'];

              }


              //получаем все файлы из photos


              $dir    = $_SERVER['DOCUMENT_ROOT'].'/modules/photosite/social-api/photos';
            //  $files1 = scandir($dir);


              if ($handle = opendir($_SERVER['DOCUMENT_ROOT'].'/modules/photosite/social-api/photos')) {
                $i=1;
                while (false !== ($file = readdir($handle))) {



                      //запись в базу и перемещение файлов

                      //загрузка изображения
                      $source=10;

                      $url = $_SERVER['DOCUMENT_ROOT']."/modules/photosite/social-api/photos/".$file;



                      $data = @getimagesize($url);
                      if(!$data)
                      {
                        continue;
                      }
                      echo $url.'</br>';

                      $width=$data['0'];
                      $height=$data['1'];
                      $mime = explode("/",$data['mime']);
                      $mime=$mime['1'];

                      if(($mime != NULL)&&($width)&&($height))
                      {

                        //создаем пустой photo_orders_rows и берем его ИД в $parentid
                        $order_row_id=Orders::create_new_order_row($order_id);

                        $filename=$line['id'].'_'.$i.'_image.'.$mime;
                        $dir = $_SERVER['DOCUMENT_ROOT'] . "/files/images"; // Full Path
                        copy($url, $dir . DIRECTORY_SEPARATOR . $filename);
                        $exif = exif_read_data($dir . DIRECTORY_SEPARATOR . $filename, 0, true);
                        if($exif) $exif=serialize($exif);


                          dbNonQuery("INSERT INTO " . _DB_TABLE_PREFIX . "images (source,parentid,title,description,originalname,filename,width,height,mime, exif_data) VALUES(:source,:parentid,:title,:description,:originalname,:filename,:width,:height,:mime, :exif
                          )",array(
                            ":source"=>$source,
                            ":parentid"=>$order_row_id,
                            ":title"=>'',
                            ":description"=>'',
                            ":originalname"=>$file,
                            ":filename"=>$filename,
                            ":width"=>$width,
                            ":height"=>$height,
                            ":mime"=>$mime,
                            ":exif"=>$exif
                          ));
                          //возвращаем ИД изображения в photo_orders_rows
                          $new_image_id=Orders::get_last_image_id();
                          $new_image_id = $new_image_id[0]['id'] - 1;

                          //дописали ИД изображения в photo_orders_rows
                          Orders::update_row($order_row_id,$new_image_id);
                          //удалили строку с которой записали
                          Orders::cron_delete_line($line['id']);

                          //удаляем файл

                          //unlink($url);
                          $i=$i+1;
                      }





                }

                closedir($handle);

              }
              //удаляем файлы из временной папки photosite
              if (file_exists($_SERVER['DOCUMENT_ROOT'].'/modules/photosite/social-api/photos'))
              foreach (glob($_SERVER['DOCUMENT_ROOT'].'/modules/photosite/social-api/photos/*') as $filed)
              unlink($filed);

              $c_i=$i-1;
              //отправка отчета
              echo "Загружено с Dropbox".$c_i." изображений  для пользователя с ID=".$line['userid']."</br>";

              //добавляем ИД юзеров, которым надо отправить письмо о загрузке
              $s_userid[]=$line['userid'];

            }

        }

      }


       //получаем почту у кого есть для отправки уведомления
       $s_userid=array_unique($s_userid);
      
       //тут вставить обновления статуса "Есть файлы в очереди"
      
       foreach ($s_userid as $userid) {
         $mail = dbQuery('SELECT email FROM  #__users WHERE id= :userid',array(
       			':userid'=>$userid
       		));
         if(isset($mail)&&!empty($mail)){
           $sendmail[]=$mail;
         }
       }
      
       foreach ($sendmail as $email) {
      
         //отправляем отчет
       //  echo "Загружены изображения на сайте "._SITE ." </br>";
      
         $to      = trim($email['0']['email']);
         $subject = 'Загрузка файлов для печати на сайте '._SITE .' </br>';
         $message="Загружены файлы пользователя с логином: ".$email['0']['email']."</br> Просмотреть можно по ссылке <a href='https://"._SITE ."/personal/new-order' >Просмотреть </a>";
        // $headers = 'From: noreply@'._SITE . "\r\n" .
         $headers='MIME-Version: 1.0'."\r\n".'Content-type:  text/html; charset="utf-8"'."\r\n".'From: noreply@yashaprint.com'."\r\n";
         mail($to, $subject, $message, $headers);
       }



      echo 'Все загрузки завершены.'; die;
    }
    else {
      echo 'Очередь на запись пуста';
    }

}



function _upload_order($params)
{
  global $app;
  global $user;
  /*authorized*/
  if($user->Authorized())
  {
    $app->setStatus(TRUE,FALSE);

    //token check
    if ($app->checkToken('eslibmishkibylipchelami137',$_POST['token']))
    {
      //check odrer with status 0 exist
      if ($order_id=Orders::order_exist())
      //add files to exist order
      {
        $order_id=$order_id['0']['id'];

        //change view
        ?><script>
          window.parent.$('.upload-photo-preloader').css('display','none');
          window.parent.$( "div" ).removeClass( "upload-photo-loading" );
        </script><?php



        //upload images

        foreach ($_FILES["files"]["error"] as $key => $error) {
            if ($error == UPLOAD_ERR_OK) {
                $image['tmp_name'] = $_FILES["files"]["tmp_name"][$key];
                $image['name'] = $_FILES["files"]["name"][$key];

                //создаем пустой photo_orders_rows и берем его ИД в $parentid
                $order_row_id=Orders::create_new_order_row($order_id);
                //загрузка изображения
                $source=10;
                uploadImage($source,$order_row_id,$image,$title = "",$description = "");
                //возвращаем ИД изображения в photo_orders_rows
                $new_image_id=Orders::get_last_image_id();
                Orders::update_row($order_row_id,$new_image_id);

            }
        }
        //обновляем страницу
        ?><script>
        window.parent.location.reload();

        </script><?php

      }

      else {
        //create new order

        Orders::create_new_order();
        $result=Orders::order_exist();
        $order_id=$result['0']['id'];

        //change view
        ?><script>
          window.parent.$('.upload-photo-preloader').css('display','none');
          window.parent.$( "div" ).removeClass( "upload-photo-loading" );
        </script><?php



        //upload images

        foreach ($_FILES["files"]["error"] as $key => $error) {
            if ($error == UPLOAD_ERR_OK) {
                $image['tmp_name'] = $_FILES["files"]["tmp_name"][$key];
                $image['name'] = $_FILES["files"]["name"][$key];

                //создаем пустой photo_orders_rows и берем его ИД в $parentid
                $order_row_id=Orders::create_new_order_row($order_id);
                //загрузка изображения
                $source=10;
                uploadImage($source,$order_row_id,$image,$title = "",$description = "");
                //возвращаем ИД изображения в photo_orders_rows
                $new_image_id=Orders::get_last_image_id();
                Orders::update_row($order_row_id,$new_image_id);

            }
        }
        //обновляем страницу
        ?><script>
        window.parent.location.reload();

        </script><?php
      }

    }
      else {
        echo 'wrong token'; die;
      }


  }
  /*authorized*/
}
