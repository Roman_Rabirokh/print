<?php

class Orders
{

function get_user_payments ($userid){

	$exist = dbQuery('SELECT * FROM  #__photo_payments WHERE userid= :userid  ORDER BY #__photo_payments.id DESC',array(
			':userid'=>$userid
		));

	return $exist;
}

function add_callback ($data) {
	date_default_timezone_set('UTC');
	$created = date("Y:m:d H:i:s");
	$result= dbQuery('INSERT INTO #__content (userid,created,content_type,detail_text,tags,content_date) VALUES (:userid,:created,:content_type,:detail_text, :tags,:content_date)',array(
		':userid'=>$data['userid'],':created'=>$created,':content_type'=>'2',':detail_text'=>$data['text'],':tags'=>'отзыв',':content_date'=>$created
	));
}

function update_orderadresscurier ($data) {

	$userid=$data['userid'];
	date_default_timezone_set('UTC');
	$ordermake = date("Y:m:d H:i:s");
	$exist = dbQuery('SELECT * FROM  #__photo_orders WHERE userid= :userid AND status =0 ',array(
			':userid'=>$userid
		));

// МЕНЯЕТСЯ СТАТУС ЗАКАЗА !!!!!
	if($exist){

		$result=dbQuery('UPDATE #__photo_orders SET region = :region, city= :city, street= :street,house=:house,flat=:flat,  cost= :cost,ordermake =:ordermake, novaposhta =0, courier = 1, status =1, phone_number= :number WHERE  id = :orderid',
		array(':region'=>$data['region'],':city'=>$data['city'],':street'=>$data['street'],':house'=>$data['house'],':flat'=>$data['flat'], ':orderid'=>$data['orderid'],':cost'=>$data['cost'],':ordermake'=>$ordermake,
                    ':number'=> $data['phone'],



		));
        //self::sendMailOrderPay($data['orderid']);
	}
	else{

		echo "Нет доступа к данному ордеру"; die;

	}

}

public function  sendMailOrderPay($idOrder)
{
    global $app;
    $dataDelivery= dbGetRow('SELECT region, city, street, userid, house, flat, npoffice, phone_number, fio  FROM #__photo_orders WHERE id = :id', ['id' => $idOrder]);
    
    $email= dbGetOne('SELECT IFNULL(u.email, i.email) AS email  FROM #__users AS u '
            . ' INNER JOIN #__user_info AS i ON (i.userid=u.id) '
            . ' WHERE u.id= :id AND i.order_paid=1', [':id' => $dataDelivery['userid']]);
    if(!empty($email))
    {
        $app->sendEmailTemplate(_SITE,_DEFAULT_EMAIL_FROM,"",$email,"
				Уведомление об оплате заказа " . _SITE,"_paid.tpl",array(
                    "#ORDER#"=>$idOrder,
                    "#SITE#"  =>_SITE,
                    '#REGION#'=>$dataDelivery['region'], 
                    '#CITY#'  => $dataDelivery['city'], 
                    '#STREET' => $dataDelivery['street'],
                    '#HOUSE#' => $dataDelivery['house'],
                    '#FLAT#'  => $dataDelivery['flat'],
                    '#NUMBER#'  => $dataDelivery['phone_number'],
                    '#FIO#'  => $dataDelivery['fio'],
                    '#NOF#' => $dataDelivery['npoffice']));
    }
    
}

function update_orderadressnp ($data) {

	$userid=$data['userid'];
	date_default_timezone_set('UTC');
	$ordermake = date("Y:m:d H:i:s");
	$exist = dbQuery('SELECT * FROM  #__photo_orders WHERE userid= :userid AND status =0 ',array(
			':userid'=>$userid
		));
                
// МЕНЯЕТСЯ СТАТУС ЗАКАЗА !!!!!
	if($exist){

        $courier=0;
        $data['street']= !empty($data['street']) ? $data['street'] : '';
        $data['house']= !empty($data['house']) ? $data['house'] : '';
        $data['flat']= !empty($data['flat']) ? $data['flat'] : '';
        print_r('<pre>');
        print_r('OLOLO');
        print_r('</pre>');
        if(!empty($data['street']) || !empty($data['house']) || !empty($data['flat']) )
        {
            $courier=1;
        }
		$result=dbQuery('UPDATE #__photo_orders SET region = :region, city= :city, npoffice= :npoffice, cost= :cost,ordermake=:ordermake, novaposhta =1, courier = :courier, status =1,fio= :fio, phone_number= :number, street = :street, house= :house, flat= :flat WHERE id = :orderid',
		array(':region'=>$data['region'],':city'=>$data['city'],':npoffice'=>$data['npoffice'],':orderid'=>$data['orderid'],':cost'=>$data['cost'],':ordermake'=>$ordermake,
                    ':fio' => $data['fio'],
                    ':number' => $data['phone'],
                    ':street' => $data['street'],
                    ':house' => $data['house'],
                    ':flat' => $data['flat'],
                    ':courier' =>$courier,


		));
          //self::sendMailOrderPay($data['orderid']);

	}
	else{

		echo "Нет доступа к данному ордеру"; die;

	}

}

function get_email($id) {
	$result = dbQuery('SELECT #__users.email FROM  #__users WHERE id= :id ',array(
			':id'=>$id
		));
	$result=$result['0']['email'];
		return $result;
}

function save_profileinfo($data) {

	global $app;
	global $user;
	$userid=$user->getid();

	//проверка на существование информации
	$exist = dbQuery('SELECT * FROM  #__user_info WHERE userid= :userid ',array(
			':userid'=>$userid
		));


	if($exist){

		$result=dbQuery('UPDATE #__user_info SET fio = :fio, adress= :adress, phone= :phone, email= :email,
			order_done=:order_done, order_in_work =:order_in_work, 	client_get_order=:client_get_order,
			order_in_transport= :order_in_transport, drop_files_notify = :drop_files_notify,
			region=:region, city =:city, street = :street, house=:house, flat=:flat, download_done= :download_done,order_paid= :order_paid
			 WHERE userid = :userid',array(
			':userid'=>$userid,':fio'=>trim($data['fio']),':adress'=>trim($data['adress']),':phone'=>trim($data['phone'])
			,':email'=>trim($data['email']),':order_done'=>trim($data['order_done']),':order_in_work'=>trim($data['order_in_work'])             
            ,':download_done'=> $data['download_done']
            ,':order_paid'=> $data['order_paid']    
			,':client_get_order'=>trim($data['client_get_order']),':order_in_transport'=>trim($data['order_in_transport'])
			,':drop_files_notify'=>trim($data['drop_files_notify'])
			,':region'=>trim($data['region']),':city'=>trim($data['city']),':street'=>trim($data['street']),':house'=>trim($data['house']),':flat'=>trim($data['flat'])));
                        
                        $email= dbGetOne('SELECT id FROM #__users WHERE email= :email', [':email' => trim($data['email']) ]);
                        if(empty($email))
                            dbNonQuery('UPDATE #__users  SET email = :email WHERE id= :id', [':email' =>  trim($data['email']), ':id' => $userid ]);          

		

	}
	else{

		$result=dbQuery('INSERT INTO #__user_info (userid,fio, adress,phone,email,order_done,order_in_work,client_get_order,order_in_transport,drop_files_notify,region,city,street,house,flat,download_done,order_paid) VALUES (:userid,:fio,:adress,:phone,:email,:order_done,:order_in_work, :client_get_order, :order_in_transport,:drop_files_notify,:region,:city,:street,:house,:flat,:download_done, :order_paid)',array(
			':userid'=>$userid,':fio'=>trim($data['fio']),':adress'=>trim($data['adress']),':phone'=>trim($data['phone'])
			,':email'=>trim($data['email']),':order_done'=>trim($data['order_done']),':order_in_work'=>trim($data['order_in_work'])
			,':client_get_order'=>trim($data['client_get_order']),':order_in_transport'=>trim($data['order_in_transport'])
			,':drop_files_notify'=>trim($data['drop_files_notify'])
			,':region'=>trim($data['region'])
			,':city'=>trim($data['city'])
			,':street'=>trim($data['street'])
			,':house'=>trim($data['house'])
			,':flat'=>trim($data['flat'])
            ,':download_done'=> $data['download_done']
            ,':order_paid'=> $data['order_paid']    
		));

	}




}

function get_user_info ($userid){
	$result = dbQuery('SELECT * FROM  #__user_info WHERE userid= :userid ',array(
      ':userid'=>$userid
    ));
	if($result){
		$result=$result['0'];
	}
        $result['email']= dbGetOne('SELECT email FROM #__users WHERE id = :id', [':id'=> $userid]);


	return $result;
}

function get_download_count($userid, $idOrder=false){
        if(!$idOrder){
	$result = dbQuery('SELECT *  FROM  #__photo_orders_rows	join #__photo_orders
		ON #__photo_orders_rows.orderid = #__photo_orders.id
		WHERE #__photo_orders.userid = :userid AND  #__photo_orders_rows.imageid !=0
		',array(':userid'=>$userid	));

		$result=count($result);
        }
        else if($idOrder)
        {
            $result = dbGetOne('SELECT COUNT(r.id) AS  count  FROM  #__photo_orders_rows AS r	
                JOIN #__photo_orders ON (r.orderid = #__photo_orders.id)
		WHERE #__photo_orders.userid = :userid AND r.orderid= :id  r.imageid !=0
		',array(':userid'=>$userid,  ':id' => $idOrder));
        }


	return $result;
}

function get_cities($region_id) {
	$result = dbQuery('SELECT name, city_id FROM  city WHERE region_id= :region_id ',array(
      ':region_id'=>$region_id
    ));

		  return $result;


}

function get_regions($id){
	$result = dbQuery('SELECT name, region_id FROM  region WHERE country_id= :country_id ',array(
      ':country_id'=>$id
    ));
		  return $result;
}

static	function order_exist() {
		global $app;
		global $user;

		$userid=$user->getid();
	  $result = dbQuery('SELECT id,cost FROM  #__photo_orders WHERE userid = :userid and status = 0 ',array(
	    ':userid'=>$userid
	  ));

	  return $result;
	}

	static	function order_exist_edit($orderid) {
			global $app;
			global $user;

			$userid=$user->getid();
		  $result = dbQuery('SELECT id,cost FROM  #__photo_orders WHERE userid = :userid and id = :orderid ',array(
		    ':userid'=>$userid, ':orderid'=>$orderid
		  ));

		  return $result;
		}

	static	function send_order_exist() {
			global $app;
			global $user;

			$userid=$user->getid();
	//	  $result = dbQuery('SELECT * FROM  #__photo_orders WHERE userid = :userid and status >0 ',array(
		  $result = dbQuery('SELECT * FROM  #__photo_orders WHERE userid = :userid  ORDER BY created DESC',array(
		    ':userid'=>$userid
		  ));

		  return $result;
		}

static function create_new_order ()
	{
		global $app;
		global $user;
		$userid=$user->getid();
		date_default_timezone_set('UTC');
		$created = date("Y:m:d H:i:s");

		$result=dbQuery('INSERT INTO #__photo_orders (userid,created) VALUES (:userid,:created)',array(
	    ':userid'=>$userid,':created'=>$created
	  ));


		return $result;
	}

	static function create_new_order_row ($orderid)
		{


			global $app;
			global $user;

			$result=dbNonQuery('INSERT INTO #__photo_orders_rows (orderid) VALUES(:orderid)',array(':orderid'=>$orderid),$is_insert = true);

			return $result;
		}

	static function get_last_image_id()
	{
		global $app;
		global $user;

		$result=dbQuery('SELECT id FROM  #__photo_orders_rows WHERE imageid = 0' ,array(),$is_insert = true);

		return $result;

	}

	static function update_row($order_row_id,$new_image_id) {


		if(!empty($new_image_id))
        {
            $result=dbQuery('UPDATE #__photo_orders_rows SET imageid = :imageid WHERE id = :id',array(
            ':id'=>$order_row_id,':imageid'=>$new_image_id));
            self::photo_orders_rows_log($order_row_id, TRUE);
            self::save_info_image($order_row_id,$new_image_id);
        }
        else {
            dbNonQuery('DELETE FROM #__photo_orders_rows WHERE  id = :id', [':id'=>$order_row_id]);
        }


	}
    
    static function save_info_image($order_row_id,$new_image_id)
    {
        $dir = $_SERVER['DOCUMENT_ROOT'] . "/files/images".DIRECTORY_SEPARATOR; // Full Path
        if(!empty($order_row_id) && !empty($new_image_id))
        {
            $order_row_id=(int) dbGetOne('SELECT id FROM #__photo_orders_rows WHERE id= :id', [':id' => $order_row_id]);
            if(!empty($order_row_id))
            {
                $fileName= dbGetOne('SELECT filename FROM #__images WHERE id= :id', [':id' => $new_image_id]);
                if(!empty($fileName))
                {
                    $imageObj= new Images($dir.$fileName,$order_row_id);
                    $imageObj->saveInfo();
                    
                }
            }
        }
    }
    static function photo_orders_rows_log($order_row_id, $success)
    {
        $id_order= dbGetOne('SELECT orderid FROM #__photo_orders_rows WHERE id= :id', ['id' => $order_row_id ]);
        $id= dbGetOne('SELECT id FROM #__photo_orders_rows_log WHERE id_order= :id', ['id' => $id_order]);
        if(!$success)
        {
            if(!empty($id))
            {
                dbNonQuery('UPDATE #__photo_orders_rows_log SET c_all=c_all+1 WHERE id_order = :id', [ ':id' => $id_order ]);
            }
            else
            {
                dbNonQuery('INSERT INTO #__photo_orders_rows_log (c_all,id_order) VALUES (1, :id)', [':id' => $id_order]);

            }
        }
        else if($success)
        {
                dbNonQuery('UPDATE #__photo_orders_rows_log SET c_success=c_success+1 WHERE id_order = :id', [ ':id' => $id_order]);
        }
    } 
	static function get_images($order_id) {

	global $app;
	global $user;
	$result = dbQuery('SELECT #__images.filename,#__images.id  FROM  #__photo_orders_rows	join #__images ON #__photo_orders_rows.imageid = #__images.id	WHERE #__photo_orders_rows.orderid = :order_id ',array(':order_id'=>$order_id	));

	return $result;
}

static function get_images_with_parametrs($order_id, $sorting) {

global $app;
global $user;

//print_R($_SESSION); die;
	//LIMIT 0,2

	$result = dbQuery('SELECT #__images.filename,#__images.id, #__photo_orders_rows.*  FROM  #__photo_orders_rows	join #__images
	ON #__photo_orders_rows.imageid = #__images.id
	WHERE #__photo_orders_rows.orderid = :order_id
	ORDER BY #__photo_orders_rows.id DESC ' ,array(':order_id'=>$order_id	));

if ($sorting=='1') {
	$result = dbQuery('SELECT #__images.filename,#__images.id, #__photo_orders_rows.*  FROM  #__photo_orders_rows	join #__images
		ON #__photo_orders_rows.imageid = #__images.id
		WHERE #__photo_orders_rows.orderid = :order_id
		ORDER BY #__photo_orders_rows.paper_size  ASC',array(':order_id'=>$order_id	));
};
if ($sorting=='2') {
	$result = dbQuery('SELECT #__images.filename,#__images.id, #__photo_orders_rows.*  FROM  #__photo_orders_rows	join #__images
		ON #__photo_orders_rows.imageid = #__images.id
		WHERE #__photo_orders_rows.orderid = :order_id
		ORDER BY #__images.filename ASC ',array(':order_id'=>$order_id	));
}
if ($sorting=='3') {
	$result = dbQuery('SELECT #__images.filename,#__images.id, #__photo_orders_rows.*  FROM  #__photo_orders_rows	join #__images
		ON #__photo_orders_rows.imageid = #__images.id
		WHERE #__photo_orders_rows.orderid = :order_id
		ORDER BY #__photo_orders_rows.id DESC',array(':order_id'=>$order_id	));
};




return $result;
}

static function get_images_with_parametrs_with_paging($order_id, $sorting,$pagesize,$start,$allcount) {
// $start - offset,  $pagesize - limit
global $app;
global $user;
if(empty($start)) {$start=0;}
if(empty($pagesize)) {$pagesize=10;}


$result = dbQuery('
select t2.filename,t2.id, t1.* from
(select * from #__photo_orders_rows WHERE orderid = :order_id ORDER BY id DESC  limit '.$start.','.$pagesize.' ) t1
join #__images t2 on t2.id = t1.imageid ' ,array(':order_id'=>$order_id	));

if ($sorting=='1') {
	$result = dbQuery('
	select t2.filename,t2.id, t1.* from
	(select * from #__photo_orders_rows WHERE orderid = :order_id ORDER BY paper_size  ASC limit '.$start.','.$pagesize.' ) t1
	join #__images t2 on t2.id = t1.imageid' ,array(':order_id'=>$order_id	));
};
if ($sorting=='2') {
	$result = dbQuery('
	select t2.filename,t2.id, t1.* from
	(select * from #__photo_orders_rows WHERE orderid = :order_id  limit '.$start.','.$pagesize.') t1
	join #__images t2 on t2.id = t1.imageid 	ORDER BY t2.filename ASC' ,array(':order_id'=>$order_id	));
}
if ($sorting=='3') {
	$result = dbQuery('
	select t2.filename,t2.id, t1.* from
	(select * from #__photo_orders_rows WHERE orderid = :order_id ORDER BY id DESC limit '.$start.','.$pagesize.') t1
	join #__images t2 on t2.id = t1.imageid' ,array(':order_id'=>$order_id	));
};


/*
	$result = dbQuery('SELECT #__images.filename,#__images.id, #__photo_orders_rows.*   FROM  #__photo_orders_rows	join #__images
	ON #__photo_orders_rows.imageid = #__images.id
	WHERE #__photo_orders_rows.orderid = :order_id
	ORDER BY #__photo_orders_rows.id DESC  ' ,array(':order_id'=>$order_id	));

if ($sorting=='1') {
	$result = dbQuery('SELECT #__images.filename,#__images.id, #__photo_orders_rows.*  FROM  #__photo_orders_rows	join #__images
		ON #__photo_orders_rows.imageid = #__images.id
		WHERE #__photo_orders_rows.orderid = :order_id
		ORDER BY #__photo_orders_rows.paper_size  ASC',array(':order_id'=>$order_id	));
};
if ($sorting=='2') {
	$result = dbQuery('SELECT #__images.filename,#__images.id, #__photo_orders_rows.*  FROM  #__photo_orders_rows	join #__images
		ON #__photo_orders_rows.imageid = #__images.id
		WHERE #__photo_orders_rows.orderid = :order_id
		ORDER BY #__images.filename ASC ',array(':order_id'=>$order_id	));
}
if ($sorting=='3') {
	$result = dbQuery('SELECT #__images.filename,#__images.id, #__photo_orders_rows.*  FROM  #__photo_orders_rows	join #__images
		ON #__photo_orders_rows.imageid = #__images.id
		WHERE #__photo_orders_rows.orderid = :order_id
		ORDER BY #__photo_orders_rows.id DESC',array(':order_id'=>$order_id	));
};


*/
foreach($result as &$image)
{
    $image['info']= dbGetRow('SELECT * FROM #__photo_orders_rows_info WHERE id= :id', [':id'=> $image['id']]);
}
unset($image);
return $result;

}

	static function save_to_db($userid,$created, $orderid, $sourseid, $url, $token) {

		global $app;
		global $user;
		$result=dbQuery('INSERT INTO #__photo_upload_queue (userid, created,orderid,sourceid,url, token) VALUES(:userid, now(), :orderid,:sourceid, :url, :token)',array(':userid'=>$userid,':orderid'=>$orderid,':sourceid'=>$sourseid,':url'=>$url,':token'=>$token));



}
        
        static function set_status_order($orderid, $status)
        {
            dbNonQuery('UPDATE #__photo_orders SET status= :status WHERE id = :id', [':id'=>$orderid, ':status' => $status ]);
        }
	static function get_cron_line() {

		global $app;
		global $user;
		$result = dbQuery('SELECT *  FROM  #__photo_upload_queue',array());


	return $result;
	}
        
        static function set_pay_order($id)
        {
            dbQuery('UPDATE #__photo_orders SET orderpay=NOW(), status=2 WHERE id= :id', [':id'=> $id]);
            self::sendMailOrderPay($id);
        }
        
        static function get_info_oreder($id)
        {
            return dbGetRow('SELECT * FROM #__photo_orders WHERE id= :id', ['id'=> $id]);
        }
	static	function cron_order_exist($userid) {
			global $app;
			global $user;

			$result = dbQuery('SELECT id FROM  #__photo_orders WHERE userid = :userid and status = 0 ',array(
		    ':userid'=>$userid
		  ));

		  return $result;
		}
		static	function cron_delete_line($id) {
				global $app;
				global $user;

				$result = dbQuery('DELETE FROM  #__photo_upload_queue WHERE id = :id ',array(
			    ':id'=>$id
			  ));

			  return $result;
			}
			static function save_to_db_token($userid,$created, $orderid, $sourseid, $url, $token) {

				global $app;
				global $user;

			  if(empty($orderid))
				{
					$orderid = 0;
				}

				$result=dbQuery('INSERT INTO #__photo_upload_queue (userid, created,orderid,sourceid,url, token) VALUES(:userid, now(), :orderid,:sourceid, :url, :token)',array(':userid'=>$userid,':orderid'=>$orderid,':sourceid'=>$sourseid,':url'=>$url,':token'=>$token));



		}
		static	function get_new_order() {
				global $app;
				global $user;

				$userid=$user->getid();
			  $result = dbQuery('SELECT id FROM  #__photo_orders WHERE userid = :userid and status = 0',array(
			    ':userid'=>$userid
			  ));
				$id=$result['0']['id'];
			  return $id;
			}

		static function get_page_parametrs() {
				$result = dbQuery('SELECT *  FROM  #__photo_parer_properties',array());
				foreach ($result as $value){
					$new[]=$value;
				}
				return $new;
		}

		static function update_image_parametrs($data) {


			$result=dbQuery('UPDATE #__photo_orders_rows SET paper_size = :paper_size, paper_type= :paper_type,
				count = :count, filds = :filds, description = :description, font = :font, font_size = :font_size,
				font_b = :input_font_b, font_i= :input_font_i, font_u = :input_font_u, write_date = :write_date,
				date_stam = :date_stam, write_coordinates = :write_coordinates, is_print = :is_print, color_text= :color_text,  text_orientation= :text_orientation, autocorrect= :autocorrect, field_width= :field_width, framing= :framing, photo_orientation= :photo_orientation, back_side_text= :back_side_text
				 WHERE id = :imageid',array(
		    ':imageid'=>$data['imageid'],':paper_size'=>$data['paper_size'],':paper_type'=>$data['paper_type'],':count'=>$data['count'],
				':filds'=>$data['filds'], ':description'=>$data['description'], ':font'=>$data['font'], ':font_size'=>$data['font_size'],
				':input_font_b'=>$data['input_font_b'],':input_font_i'=>$data['input_font_i'],':input_font_u'=>$data['input_font_u'],
				':write_date'=>$data['write_date'],':date_stam'=>$data['date_stam'],':write_coordinates'=>$data['write_coordinates'],
				':is_print'=>$data['is_print'],
                ':color_text' => $data['color_text'],     
                ':text_orientation' => $data['text_orientation'],
                ':autocorrect' => $data['autocorrect'],
                ':field_width' => $data['field_width'],
                ':framing' => $data['framing'],
                ':photo_orientation'=>   $data['photo_orientation'],
                ':back_side_text' => $data['back_side_text']     
			));


		}
                
                // обновляем параметры печати для всех фотографий в заказе
                static  function update_all_image_parametrs ($data, $orderid, $userid)
                {
                    if(empty($data) || empty($orderid))
                        return FALSE;
                    // проверочка на существования заказа
                    $issetOrder= dbGetOne('SELECT created  FROM #__photo_orders WHERE id = :id AND userid = :user', [':id'=> $orderid, ':user' => $userid]);
                    
                    if(empty($issetOrder))
                        return FALSE;
                    
                    $values=array();
                    $set='';
                    // выборочка только тех параметров которые пришли
                    foreach($data as $key => $item)
                    {
                        if(isset($item))
                        {
                            if(empty($set))
                                $set.=' '.$key.' = :'.$key;
                            else
                                $set.=', '.$key.' = :'.$key;
                            $values[$key] = $item;
                        }
                    }
                    
                    
                    if($set==''|| empty($values))
                       return FALSE;
                    // обновление данных
                    $values[':orderid']=$orderid;
                    dbNonQuery('UPDATE #__photo_orders_rows SET '.$set. ' WHERE  orderid= :orderid', $values);
                    return TRUE;
                   
                }
                static function get_check_images_count ($orderid)
		{
			$result = dbQuery('SELECT *  FROM  #__photo_orders_rows WHERE is_print = 1 and orderid = :orderid',array(':orderid'=>$orderid));
			return $result;

		}

		static function save_ordercost($data) {


			$result=dbQuery('UPDATE #__photo_orders SET cost = :cost
				 WHERE id = :orderid',array(
				':cost'=>$data['cost'],
				':orderid'=>$data['orderid']
			));


		}
}
