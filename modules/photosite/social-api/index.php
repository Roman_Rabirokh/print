<?php


require_once("functions.php");
session_start();

$_SESSION['sourceid']=$_POST['sourceid'];
$_SESSION['order_id']=$_POST['order_id'];
date_default_timezone_set('UTC');
$_SESSION['created']=date("Y:m:d H:i:s");
$_SESSION['userid']=$_POST['user_id'];


header('Content-Type: text/html; charset=utf-8');

$authUrl = getAuthorizationUrl("", "");


header('Location: '.$authUrl);



?>

<!DOCTYPE html>
<html lang="fi">
<head>
	<title>Сохраняем файлы из диска Гугл</title>
	<meta charset="UTF-8">
	<style>
		body {
	    color: #1f2639 !important;
		}
		html {
	    background: #1f2639;
		}
		h1 {
			color: darkblue;
			text-align: center;
		}
		.wrap{
			margin-top:100px;
			background: white;
			width:100%;
			height: 200px;
		}
		.cont{
			margin: 0 auto;
			width: 200px;
		}
	</style>
</head>
<body>
	<div class="wrap">
		<h1>Сохраняем файлы из диска Гугл</h1>
		<div class="cont">
			<a href=<?php echo "'" . $authUrl . "'" ?>>Авторизация</a>
		</div>
</div>
</body>
</html>
