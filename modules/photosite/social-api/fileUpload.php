<?php
require_once("functions.php");
session_start();

header('Content-Type: text/html; charset=utf-8');




global $CLIENT_ID, $CLIENT_SECRET, $REDIRECT_URI;
$client = new Google_Client();
$client->setClientId($CLIENT_ID);
$client->setClientSecret($CLIENT_SECRET);
$client->setRedirectUri($REDIRECT_URI);
$client->setScopes('email');

$authUrl = $client->createAuthUrl();

//print_r($authUrl); echo "</br>";
//print_r($_GET); die;
try
{
getCredentials($_GET['code'], $authUrl);
}
 catch(Exception $e)
 {
    header('/personal/orders/');
 }
$userName = $_SESSION["userInfo"]["name"];
$userEmail = $_SESSION["userInfo"]["email"];


?>

<!DOCTYPE html>
<html lang="fi">
<head>
	<title>Logged in</title>
	<meta charset="UTF-8">
	<style>
		body {
			color: #1f2639 !important;
		}
		html {
			background: #1f2639;
		}
		h1 {
			color: darkblue;
			text-align: center;
		}
		.wrap{
			margin-top:100px;
			background: white;
			width:100%;
			height: 400px;
		}
		.cont{
			margin: 0 auto;
			width: 800px;
		}
		.c_inp {
			margin-top: 20px;
			margin-bottom: 20px;
		}
		.c_button{
			cursor: pointer;
			margin-bottom: 50px;
			padding: 13px 66px;
			height: 25px;
			font-size: 1.5625rem;
			line-height: 0.3333;
			box-shadow: 0 5px 10px rgba(0,0,2,.35);
			-webkit-transition: box-shadow,.15s,ease;
			transition: box-shadow,.15s,ease;

			display: inline-block;
			vertical-align: middle;
			max-width: 310px;
			max-height: 55px;
			font-weight: 700;
			white-space: nowrap;
			color: #1f2639;
			background: #ffde01;

			box-sizing: inherit;

			text-align: center;

			text-decoration: none;



		}
	</style>
</head>
<body>

	<div class="wrap">



	<!--
			<input type="file" name="file" required>
			<br><br>
			<label for="folderName">Folder name, this either uploads file to an existing folder or creates a new one based on the name</label>
			<br>
			<input type="text" name="folderName" placeholder="My cat Whiskers">
			<br><br>
			-->
			<h1>Введите ссылку на папку Google Disk</h1>
			<div class="cont">
				<form enctype="multipart/form-data" action="formAction.php" method="POST">
					<label for="album_id">например https://drive.google.com/drive/folders/0B0yvFz7TPHo2YmFsOGJ6eXRSZU0 </br></br>( для папки должен быть включен "доступ по ссылке" - правой кнопкой мыши на папке и выбор "Включить доступ по ссылке")</label>

					<div class="c_inp">
						<input type="text" name="album_id" placeholder="Введите ссылку на папку ">
					</div>

					<input type="submit" name="submit" value="Закачать" class="c_button">
				</form>
			</div>
	</div>
</body>


</html>
