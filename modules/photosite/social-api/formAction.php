<?php
$server='https://'.$_SERVER['SERVER_NAME'];



require_once $_SERVER['DOCUMENT_ROOT'].'/modules/photosite/social-api/google-api-php-client/src/Google/Client.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/modules/photosite/social-api/google-api-php-client/src/Google/Service/Oauth2.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/modules/photosite/social-api/google-api-php-client/src/Google/Service/Drive.php';
session_start();

header('Content-Type: text/html; charset=utf-8');

// Init the variables
$driveInfo = "";
$folderName = "";
$folderDesc = "";

// Get the file path from the variable
//$file_tmp_name = $_FILES["file"]["tmp_name"];

// Get the client Google credentials
$credentials = $_COOKIE["credentials"];

// Get your app info from JSON downloaded from google dev console
$json = json_decode(file_get_contents("client_secret_508189151410-d9d7epsobleo7mnvi6b7bhhf2ool5k94.apps.googleusercontent.com.json"), true);
$CLIENT_ID = $json['web']['client_id'];
$CLIENT_SECRET = $json['web']['client_secret'];
$REDIRECT_URI = $json['web']['redirect_uris'][0];

//print_r($CLIENT_ID); die;

// Create a new Client
$client = new Google_Client();
$client->setClientId($CLIENT_ID);
$client->setClientSecret($CLIENT_SECRET);
$client->setRedirectUri($REDIRECT_URI);
$client->addScope(
	"https://www.googleapis.com/auth/drive",
	"https://www.googleapis.com/auth/drive.appfolder",
"https://www.googleapis.com/auth/drive.file",
'https://www.googleapis.com/auth/userinfo.email',
'https://www.googleapis.com/auth/userinfo.profile');


// Refresh the user token and grand the privileges
$client->setAccessToken($credentials);
$service = new Google_Service_Drive($client);

//проверка на правильность альбома
if (strpos($_POST['album_id'], '/drive.google.com/drive/folders'))
{


	//get folder id
	$folder= explode("/", $_POST['album_id']);
	$folderId=$folder['5'];



	// get file list
	$files = $service->files->listFiles([
	    "q" => "'$folderId' in parents and trashed=false"
	]);


	$i=0;
	$titles = [];
	foreach ($files->getItems() as $file) {
	    	$titles[] = $file->title;
				$fileMimeType= $file->getMimeType();

				$downloadUrl = $file->getWebContentLink();
				$downloadUrl=str_replace ( '&export=download', '', $downloadUrl);
				//echo $downloadUrl.'</br>';
				$_SESSION['new_images'][]=$downloadUrl;
				$i=$i+1;
	}
	$_SESSION['send_status']='1';
	//обновляем страницу
	?><script>
	window.location = "<?php echo $server ?>/personal/new-order?id=<?=$_SESSION['order_id']?>&edit=1";
	window.alert('Изображения добавлены в очередь для загрузки.');
	</script><?php


}
else {
	echo 'Альбом введен не правильно. <a href="'.$server.'/personal/new-order?id='.$_SESSION['order_id'].'&edit=1" >Назад</a>';
}



?>
