<?php

	session_start();

	/* Code By Qassim Hassan, wp-time.com */

	if( isset($_SESSION["oauth_token_secret"]) and isset($_SESSION["oauth_token"]) ){ // if user is logged in, redirect user to index.php
		header("location: index.php"); // redirect user to home page
		return false;
	}

	include "Qassim_HTTP.php"; // Qassim_HTTP() Function
	include "config.php";

	$Header = array("Authorization: OAuth oauth_version=\"1.0\", oauth_signature_method=\"PLAINTEXT\", oauth_consumer_key=\"$App_Key\", oauth_signature=\"$App_Secret&\"\r\n");

	$Data = 0; // data = 0 because we do not have data

	$JSON = 0; // json = 0 because we do not want json for this operation

	$Method = 1; // method = 1 because we want POST

	$API = Qassim_HTTP($Method, "https://api.dropbox.com/1/oauth/request_token", $Header, $Data, $JSON);

	$Result = explode("&", $API); // convert result to array

	$oauth_token_secret = str_replace("oauth_token_secret=", "", $Result[0]); // user oauth token secret

	$oauth_token = str_replace("oauth_token=", "", $Result[1]); // user oauth token

	$_SESSION["oauth_token_secret"] = $oauth_token_secret; // user oauth token secret save in session

	$_SESSION["oauth_token"] = $oauth_token; // user oauth token save in session

	$callback_url = $callback; // your callback link

	$authorize_url = "https://www.dropbox.com/1/oauth/authorize?oauth_token=$oauth_token&oauth_callback=$callback_url";

	header("location: $authorize_url"); // redirect user to dropbox authorize page

?>