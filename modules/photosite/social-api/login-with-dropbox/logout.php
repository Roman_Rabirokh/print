<?php

	session_start();

	/* Code By Qassim Hassan, wp-time.com */
	
	// if user is logged in, destroy all dropbox sessions
	if( isset($_SESSION["oauth_token"]) or isset($_SESSION["oauth_token_secret"]) or isset($_SESSION["access_token"]) or isset($_SESSION["access_token_secret"]) or isset($_SESSION["dropbox_user_info"]) ){
		unset( $_SESSION["oauth_token"] ); // destroy
		unset( $_SESSION["oauth_token_secret"] ); // destroy
		unset( $_SESSION["access_token"] ); // destroy
		unset( $_SESSION["access_token_secret"] ); // destroy
		unset($_SESSION["dropbox_user_info"]); // destroy
		header("location: index.php"); // redirect user to home page
	}

	else{ // if user is not logged in
		header("location: index.php"); // redirect user to home page
	}

?>