<?php


	session_start();

	/* Code By Qassim Hassan, wp-time.com */

	if( isset($_SESSION["access_token"]) and isset($_SESSION["access_token_secret"]) ){ // if user is logged in

		$access_token_secret = $_SESSION["access_token_secret"]; // user access token secret saved in session

		$access_token = $_SESSION["access_token"]; // user access token saved in session

		include "Qassim_HTTP.php"; // Qassim_HTTP() Function
		include "config.php";

		if( !isset($_SESSION["dropbox_user_info"]) ){
			$Header = array("Authorization: OAuth oauth_version=\"1.0\", oauth_signature_method=\"PLAINTEXT\", oauth_consumer_key=\"$App_Key\", oauth_token=\"$access_token\", oauth_signature=\"$App_Secret&$access_token_secret\"\r\n");

			$Data = 0; // data = 0 because we do not have data

			$JSON = 1; // json = 1 because we want json for this operation

			$Method = 0; // method = 0 because we want GET

			$API = Qassim_HTTP($Method, "https://api.dropbox.com/1/account/info", $Header, $Data, $JSON);

			$_SESSION["dropbox_user_info"] = $API; // save user info in session

			$Result = $_SESSION["dropbox_user_info"]; // user info in saved session

			$User_Email = $Result["email"]; // get user email

			$User_Name = $Result["display_name"]; // get user full name
		}

		else{
			$Result = $_SESSION["dropbox_user_info"]; // user info in saved session
			$User_Email = $Result["email"]; // get user email
			$User_Name = $Result["display_name"]; // get user full name
		}

		?>
			<h3>Welcome <?php echo $User_Name; ?> !</h3>
			<p>Your email is <?php echo $User_Email; ?> - <a href="logout.php">Logout</a></p>
		<?php

	}

	else{ // if user is not logged in
		echo '<a href="login.php">Login With Dropbox</a>';
	}

?>
