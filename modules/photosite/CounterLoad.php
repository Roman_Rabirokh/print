<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CounterLoad
 *
 * @author Roman Rabirokh
 */
class CounterLoad
{
    public function addNew($order,$all)
    {
        if($this->issetCounter($order))
        {
            dbNonQuery('UPDATE #__counter_load SET allImage = :all WHERE orderId= :order ', [':order' => $order, ':all' => $all]);
            $id= dbGetOne('SELECT id FROM  #__counter_load   WHERE orderId= :order ', [':order' => $order,]);
        }
        else
        {
           $id= dbNonQuery('INSERT INTO #__counter_load (allImage, orderId) VALUES (:all, :order) ', [':all' => (int)$all, ':order' => (int)$order], TRUE); 
        }
        return $id;
    }
    private function issetCounter($order)
    {
        $id=dbGetOne('SELECT c.id FROM #__counter_load AS c WHERE   c.orderId = :order ', [':order' => $order]);
        if(!empty($id))
            return true;
        else
            return false;
    }
    public function increment($order)
    {
        if($this->issetCounter($order))
        {
            dbNonQuery('UPDATE #__counter_load SET downloaded = downloaded + 1 WHERE orderId= :order ', [':order' => $order]);
        }
    }
    public function getInfo($order)
    {
        $info=FALSE;
         if($this->issetCounter($order))
         {
             $info= dbGetRow('SELECT c.* FROM  #__counter_load AS c WHERE c.orderId = :order ', [':order' => $order]);
         }
         return $info;
             
    }
    public function loadingComplete($order)
    {
        if($this->issetCounter($order))
        {
            dbNonQuery('UPDATE #__counter_load SET downloaded = 0, allImage= 0 WHERE orderId= :order ', [':order' => $order]);
        }
    }

}
