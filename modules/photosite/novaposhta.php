<?php if(!defined("_APP_START")) { exit(); }

class Novaposhta
{
    
  private $fromCityCode='db5c88d0-391c-11dd-90d9-001a92567626';
    public function getPrice($to,$idOrder,$params)
    {
        $weight=0.01125;
        $count= Orders::get_images($idOrder);
        $count=count($count);
        $ServiceType='';
        $weight=$weight*$count;
        if($weight<0.01)
            $weight=0.01;
        if(isset($params['toHome']))
        {
            if($params['toHome']==1)
                $ServiceType='WarehouseDoors';
            else
                $ServiceType='WarehouseWarehouse';
        }
        $cost=Orders::order_exist_edit($idOrder);
        $dataSend=[
            'modelName'=>'InternetDocument',
            'calledMethod' => 'getDocumentPrice',
            'methodProperties' => [
                'CitySender' => $this->fromCityCode,
                'CityRecipient'=> $to,
                'CargoType' => 'Cargo',
                'SeatsAmount' => 1,
                'ServiceType' =>$ServiceType,
                'Weight' => $weight,
            ]
            
        ];
        $data=$this->getData($dataSend);
        $data= json_decode($data);
        $price=$data->data[0]->Cost;
        return $price;
    }  
  public function get_regions(){

    //get regions
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // только для https
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_URL,
        $server='https://'.$_SERVER['SERVER_NAME'].'/np/np.php?action=areas'
    );
    $content = curl_exec($ch);
    $np_regions=json_decode($content);
    $i=0;
    foreach ($np_regions as $np_region) {
      $line = get_object_vars($np_region);

      foreach ($line as $key => $value) {

        if ($key=="Description")
        {
          $desc=$value;
          $rt[$i]['desc']=$desc;
        }
        if ($key=="Ref")
        {
          $ref=$value;
          $rt[$i]['ref']=$ref;

        }

      }
      $i=$i+1;

    }

    return $rt;
  }
  private function getData($data )
{


	$data_string = json_encode($data);

    $ch = curl_init('https://api.novaposhta.ua/v2.0/json/');


    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );

    $result = curl_exec($ch);

	return $result;
}

}
