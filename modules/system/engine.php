<?php if(!defined("_APP_START")) { exit(); }

$app->routes[] = array("pattern"=>'/component\/load\/(.*)/',"action"=>"loadComponent","contentType"=>"component");
$app->routes[] = array("pattern"=>'[(.*)]',"action"=>"loadContent","contentType"=>"content");


function loadContent()
{
	Content::loadContent();
}
function loadComponent($params)
{
	global $app;
	$app->loadTemplate = FALSE;
	$app->actionResult = TRUE;
	if(!empty($params[1]))
	{
		$vars = explode("?",$params[1],2);
		$cParams = array("IS_REQUEST"=>TRUE);
		if(!empty($vars[1]))
		{
			$get = explode("&",urldecode($vars[1]));
			foreach($get as $param)
			{
				$value = explode("=",$param,2);
				$cParams[$value[0]] = !empty($value[1]) ? $value[1] : "";
			}
		}
		foreach($_POST as $key=>$value)
		{
			$cParams[$key] = $value;
		}
		$app->IncludeComponent($vars[0],$cParams);
	}
}
