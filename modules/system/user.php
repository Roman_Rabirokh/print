<?php if(!defined("_APP_START")) { exit(); }

$app->routes[] = array("pattern"=>'/login\/form/',"action"=>"_showLoginFormAjax","contentType"=>"component");
$app->routes[] = array("pattern"=>'/registration\/form/',"action"=>"_showRegistrationFormAjax","contentType"=>"component");
$app->routes[] = array("pattern"=>'/login\/$/',"action"=>"_showLoginForm");
$app->routes[] = array("pattern"=>'/logout\/$/',"action"=>"_Logout");
$app->routes[] = array("pattern"=>'/registration\/$/',"action"=>"_showRegistrationForm");
$app->routes[] = array("pattern"=>'/personal\/$/',"action"=>"_showPersonalPage");
$app->routes[] = array("pattern"=>'/registration\/create/',"action"=>"_registrationCreate");

function _showLoginForm()
{
	 global $app;
	 global $page;
	 $app->actionResult = TRUE;
	 $page->meta_title = "Авторизация";
	 $app->IncludeComponent("system/auth");
}
function _showLoginFormAjax()
{
	global $app;
	$app->loadTemplate = FALSE;
	$app->actionResult = TRUE;
	$app->IncludeComponent("system/auth");
}
function _showRegistrationForm()
{
	 global $app;
	 global $page;
	 $app->actionResult = TRUE;
	 $page->meta_title = "Регистрация";
	 $app->IncludeComponent("system/register");
}
function _showRegistrationFormAjax()
{
	global $app;
	$app->loadTemplate = FALSE;
	$app->actionResult = TRUE;
	$app->IncludeComponent("system/register");
}
function _showPersonalPage()
{
	 global $app;
	 global $page;
	 $app->actionResult = TRUE;
	 $page->meta_title = "Персональный кабинет";
	 include(_TEMPL . "personal.php");

}
function _registrationCreate()
{
	$app->loadTemplate = FALSE;
	$app->actionResult = TRUE;
}
function _Logout()
{
	global $user;
	$user->Logout();
	//redirect(APP::getServerProtocol() .  "://" . _SITE);
	redirect("https://" . _SITE);
}
