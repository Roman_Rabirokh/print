<nav class="navbar navbar-default navbar-fixed-top" role="navigation"> <div class="container-fluid">
 <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" target="blank" href="http://<?php echo $_SERVER["SERVER_NAME"];?>"><span class="glyphicon glyphicon-globe"></span></a>
    </div>
 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">	
<ul  class="nav navbar-nav">  
  <?php
 
  function showmenu($parent = 0)
  {
	global $currentuser;
	$html = "";
	 
	 $menuid = $currentuser->Menu != "-1" ? $currentuser->Menu : dbGetOne("SELECT menuid FROM " . _DB_TABLE_PREFIX . "direct_usersgroup WHERE id = '$currentuser->Group'");
	 
	 $sql = dbQuery("SELECT * FROM " . _DB_TABLE_PREFIX . "direct_menuitem WHERE menuid = '$menuid' AND parentid = '$parent' ORDER BY showorder");
	 
	 if($parent != "0" && count($sql) > 0)
	 {
		$html .= "<ul class='dropdown-menu' role='menu'>";
	 }
	  foreach($sql as $r)
	  {
			if($r["childsql"] != "" && $r["linktemplate"] != "")
			{
				
				$subsql = dbQuery($r["childsql"]);
				foreach($subsql as $row)
				{
					$link = $r["linktemplate"];
					foreach(array_keys($row) as $key)
					{
						$link = str_replace("[$key]",$row[$key],$link);
					}
				
					
				$submenu = showmenu($r["id"]);	
				$html .= "
				<li ".($submenu != "" ? "class='dropdown'" : "").">$link";
				$html .=  $submenu;
				$html .= "</li>";
				}
			}
			else
			{
				$submenu = showmenu($r["id"]);
				$html .= "
				<li ".($submenu != "" ? "class='dropdown'" : "")."><a ".($submenu != "" ? "class='dropdown-toggle' data-toggle='dropdown'" : "")." href='$r[link]'>$r[title]".( $submenu != "" ? " <span class='caret'></span>" : "")."</a>";
				$html .= $submenu;
				$html .= "</li>";
			}	
	  }
	   if($parent != "0" && count($sql) > 0)
		 {
			$html .= "</ul>";
		 }
		 
		return $html; 
  }
	print showmenu(0);
  ?>
  

</ul>
<ul  class="nav navbar-nav pull-right">  
  <li><a href="login.php?logout"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Выход</a></li>
</ul>
</div>
</div></nav>
