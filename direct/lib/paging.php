<?php

$pagecount = ceil($count/$pagesize);
$pager_size = 10;
$addGet='';
if(!empty($_GET['user']))
{
    $addGet='&user='.$_GET['user'];
}
if(!empty($_GET['idorder']))
{
    $addGet='&idorder='.$_GET['idorder'];
}

$pager = "";
		global $pagesize_array;
		
	/*$pager .= "Показывать по:&nbsp;&nbsp;";
	foreach($pagesize_array as $item)
	{
		
		$pager .= "<a class='pl' ".($pagesize == $item ? "style='font-weight:bold;'" : "")." href='javascript:void(0);' onclick='set_page_size($item)'>$item</a>&nbsp;&nbsp;";
	
	}
	
	if(isset($_SESSION["pagesize_".$_GET["t"]."_all"]))
	{
		$pager .= "<a class='pl' style='cursor:pointer;' onclick=\"$.post('udf/ajax.php',{action:'show_pager',t:'".$_GET["t"]."'},reload_page)\" >Пейджинг</a>&nbsp;&nbsp;&nbsp;";
	}
	else
	{
		if($pagecount > 1)
		{		
		$pager .= "<a class='pl' style='cursor:pointer;' onclick=\"$.post('udf/ajax.php',{action:'show_all',t:'".$_GET["t"]."'},reload_page)\" >Все</a>&nbsp;&nbsp;&nbsp;";
		}
}
*/
$pager .= "<ul class='pagination pagination-sm pull-right'>";

$intervals = ceil($pagecount/$pager_size);
for($i = 0;$i < $intervals;$i++)
{
	
	$finish = ($i+1)*$pagesize*$pager_size;
	$start = $finish - ($pagesize*$pager_size) + 1;
	if(($current_page*$pagesize ) >= $start && $current_page*$pagesize <= $finish)
	{
		for($ind = 0; $ind < $pager_size;$ind++)
		{
			$end_value = $start+($ind*$pagesize) + $pagesize - 1;
			$page = ceil($end_value/$pagesize);
			if($i > 0 && $ind == 0)
			{
				$pager .= "<li><a class='pl' href='http://".get_url(2,$this->config)."&page=".($page - 1).$addGet."'>...</a></li>";
			}
			
			if($end_value >= $count)
			{
				$end_value = $count;
			}
			if($page == $current_page)
			{
				$class = "cpl";
				//$pager .= "<label class='$class'>".($start+($ind*$pagesize))."...$end_value</label>";
				$pager .= "<li class='active'><a href='#'>".($page)."</a></li>";
			}
			else
			{
				$class = "pl";
				//$pager .= "<a class='$class' href='http://".get_url(2,$this->config)."&page=$page'>".($start+($ind*$pagesize))."...$end_value</a>";
				$pager .= "<li><a class='$class' href='http://".get_url(2,$this->config)."&page=$page".$addGet."'>".($page)."</a></li>";
			}
			$pager .= "";
			if($end_value >= $count)
			{
				break;
			}
			if((($pager_size - $ind) == 1) && (($intervals - $i) > 1))
			{
				$pager .= "<li><a class='pl' href='http://".get_url(2,$this->config)."&page=".($page + 1).$addGet."'>...</a></li>";
			}
		}
	}
}
$pager .= "</ul>";

$pager .= '<div class="form-inline pull-right" >
  <div class="form-group">
    <label >Показывать по:&nbsp;</label>
   <select class="form-control psize" >';
   foreach($pagesize_array as $item)
	{
		
		$pager .= "<option ".($pagesize == $item ? "selected='selected'" : "")." value='$item'  >$item</option>";
	
	}
   
   $pager .='</select>
  </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>';
	
?>