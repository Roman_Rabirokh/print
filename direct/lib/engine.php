<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/direct/function/init.php");
$currentuser = $_SESSION["user"];
$currentuser = unserialize($currentuser);
initConnectionParams();


function db_action($config)
{
	
	if(isset($_POST["delete"]))
	{
		$table = new Table($config);
		if(!empty($_POST["delete"]))
		{
			if(is_array($_POST["delete"]))
			{
				foreach($_POST["delete"] as $id)
				{
					if($id == 'on') { continue; }
					$table->delete_row($id);
				}
			}
			else
			{
				$table->delete_row($_POST["delete"]);
			}
		}
	}
	if(isset($_GET["save"]))
	{
		$db = new DB($config,$_GET["id"],$_GET["t"]);
		if(isset($_POST["is_close"]) && strcmp($_POST["is_close"],"1") == 0)
		{
			$db->close_form = true;
		}
		$db->execute();
	}
	if(isset($_POST["copy"]))
	{
		$db = new DB($config,$_POST["copy"],$_GET["t"]);
		$db->copy();
		exit();
	}
	if(isset($_GET["simple_save"]))
	{
		$db = new DB($config,"",$_GET["t"]);
		$db->update_miltiple();
	}
}

function get_footer_scripts($config)
{
	global $version_info;
	?>
	<div id="modalWindow" class="modal fade"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title"></h4></div><div class="filter modal-body"></div><div class="modal-footer"> <button type="button" class="btn btn-default" class="close" data-dismiss="modal" aria-label="Close"><?php echo _BUTTON_CLOSE; ?></button></div></div></div></div>
	
	<div id="modalWindowGeneral" class="modal fade"><div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title"></h4></div><div id="modalContentBody" class="modal-body"></div><div class="modal-footer"> <button type="button" class="btn btn-default" class="close" data-dismiss="modal" aria-label="Close"><?php echo _BUTTON_CLOSE; ?></button></div></div></div></div>
	
	<script type="text/javascript" src="js/bootstrap/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/moment.js"></script>	
	<script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>	
	<script type="text/javascript" src="js/engine.js?v=<?php echo $version_info; ?>"></script>
	<?php foreach($config->scripts as $script) {  
		echo '<script type="text/javascript" src="' . $script . '"></script>'; 
	} 
	if(isset($_GET['autoheight']))
	{
		?><script>$(window).load(function(){ window.parent.setHeight('<?php echo $_GET['autoheight']; ?>',$('body').outerHeight()); } );</script><?php
	}
	
}

function get_header($config)
{
	
	global $config_title;
	
	if(!isset($_GET["noheader"]))
	{
		include("inc/header.php"); 
	}
	else
	{
		include("inc/head.php");
	}
	
}
function get_footer($config)
{
	if(!isset($_GET["nofooter"]))
	{
		include("inc/footer.php"); 
	}
	else
	{
	get_footer_scripts($config);
	?></body></html><?php
	}
}

function get_content($config)
{
	
	if(!empty($_GET["t"]))
	{
		get_header($config);
	
        if(!empty($_GET["id"]))
        {
            if(!empty($config->edit_page))
            {
                
				require($config->edit_page);
            }
            else
            {
                $row = new Row($config,$_GET["id"]);
                $row->id = $_GET["id"];
                $row->edit();
            }

        }
        else
        {
            if(!empty($config->table_page))
            {
                if(function_exists($config->table_page))
				{
					call_user_func($config->table_page,$this);	
				}
				else
				{
					include($config->table_page);
				}	
            }
            else
            {
                $table = new Table($config);
                if(isset($_GET["sort"]))
                {
                    $table->sort = $_GET["sort"];
                }
                $table->create_table();
            }
        }

		get_footer($config);		
				
	}
	else
	{
		get_header($config);get_footer();
	}
	
	
}
