<?php 
class Table
{
	var $config = "";
	var $sort = "";
	var $fast_edit_mode = false;
	var $fast_edit_array = array();
	
	function Table($config)
	{
		$this->config  = $config;
	}
	
	function check_edit_mode($id)
	{
		if($this->config->table_edit_mode)
		{
			return true;
		}
		else
		{	
			if($this->fast_edit_mode && in_array($id,$this->fast_edit_array))
			{
				
				return true;
			}
			else
			{
				return false;
			}
		}
		return true;
	}
	
	function create_table()
	{
		global $engine_db;
		global $db;
		global $titles;
		
		
		if(!empty($_GET["fast_edit"]))
		{
			$this->fast_edit_array = explode(",",$_GET["fast_edit"]);
			$this->fast_edit_mode = true;
		}
		
		$checkbox_array = array();
        $hide_array = array();
		$width_array = array();
        $farray = array();
        if(isset($_COOKIE[$_GET["t"]."_hide"]))
        {
            $hide_array = explode(",",$_COOKIE[$_GET["t"]."_hide"]);
        }
		 if(isset($_COOKIE[$_GET["t"]."_width"]))
        {
            $temp_width_array = explode(";",urldecode($_COOKIE[$_GET["t"]."_width"]));
			foreach( $temp_width_array as $current)
			{
				if($current != "")
				{
					$item = explode(":",$current);
					$width_array[$item[0]] = $item[1];
				}
			}
			
        }
		if(empty($title) && isset($titles[$_GET["t"]]))
		{
			$this->config->title = $titles[$_GET["t"]];
		}
		$use_udf = false;
		if(file_exists("udf/".$this->config->table.".php"))
		{
			$use_udf = true;
			require_once("udf/".$this->config->table.".php");
		}
		if(isset($_GET["sql"]) && !empty($_GET["sql"]))
		{
			$this->config->source = stripcslashes(urldecode($_GET["sql"]));
		}
		if(empty($this->config->source))
		{
			echo "Internal error: empty source.";
			return;
		}
		
		$formId = uniqid();
		
		?>	
	
		
		<div class="container-fluid">
		<?php
			if($this->config->table_content_top_1 != "")
			{
				include($this->config->table_content_top_1);
			}
		
		?>
		<div id="buttonsPanel" class="panel panel-default">
		  <div class="panel-body">
		<div class="page_title pull-right"><h2><?php echo $this->config->title; ?></h2></div>	
		<div class="pull-left">
		
		<?php if($this->config->top_links[0]){?>	
		<a  class="btn btn-success" href="http://<?php echo get_url(1,$this->config); ?>&id=-1"  /><?php echo $this->config->top_buttons_titles["ADD"]; ?></a>
		<?php } ?>
		<?php if(isset($this->config->filters) && count($this->config->filters) > 0) { ?>
		<input type="button"  class="btn btn-primary" onclick="show_filter(this)" value="<?php echo _FILTER; ?>" />
		<?php } ?>
		<?php if($this->config->top_links[1]){?>	
		<button  class="btn btn-info" onclick="fast_edit('<?php echo get_url(1,$this->config); ?>')" ><span class="glyphicon glyphicon-edit"></span></button>
		<?php } ?>
		<?php if($this->config->top_links[2]){?>
		<button class="btn btn-danger"  onclick="delete_rows('<?php echo $_GET["t"] ?>')"  /><span class="glyphicon glyphicon-trash"></span></button>
		<?php } ?>
        <?php if(isset($this->config->menu_html)) {echo $this->config->menu_html; } 
        
        if(!empty($_GET['t']) && $_GET['t'] == 'orders')
                {
                    if( !empty($_GET['user']))
                    {    
                        $name= dbGetOne('SELECT fio FROM #__user_info WHERE userid= :id', [':id' => $_GET['user']]);
                        if(empty($name))
                        {
                            $name='не указано';
                        }
                        echo '<div class="count_no_read_message_to_moderator" style="font-size:14px;display:inline-block;margin-bottom: -5px;margin-left: 20px;">Заказы пользователя: <b>'.$name.'</b> </div>';
                    }
                }    
        ?>
			</div>
		  </div>
		</div>
		</div>
		
		<?php if(!empty($this->config->breadcrumbs)) {?>
		<div class="container-fluid">
			<div  class="panel panel-default">
				<div class="panel-body">	
					<ol style="margin-bottom:0;" class="breadcrumb">
						<?php foreach($this->config->breadcrumbs as $current) {?>
						<li><a href="<?php echo $current['LINK']; ?>"><?php echo $current['TEXT']; ?></a></li>
						<?php } ?>
					</ol>
				</div>
			</div>	
		</div>
		<?php } ?>
		
			  <iframe style="display:none;" name="f<?php echo $formId; ?>"></iframe>
			<form target="f<?php echo $formId; ?>" method="post" action="db.php?t=<?php echo $_GET["t"]; ?>&simple_save" enctype="multipart/form-data" >
				<input type="hidden" name="token" value="<?php echo  createFormToken("t" . md5($this->config->table)); ?>" />
			<input type="hidden" name="base_url" id="base_url" value="http://<?php echo get_url(1,$this->config); ?>" />
		<?php
		if(!isset($this->config->pagesize))
		{
			$pagesize = 10;
		}
		else
		{
			$pagesize = $this->config->pagesize;
		}
		if(isset($_SESSION["pagesize_".$_GET["t"]."_all"]))
		{
			$pagesize = 10000000;
		}


		if(isset($_COOKIE["pagesize"]))
		{
			$pagesize = $_COOKIE["pagesize"];
		}

		$limit = "";
		
		$current_page = isset($_GET["page"]) ? $_GET["page"] : 1;

		$limit = "LIMIT ".($pagesize*($current_page-1)).",$pagesize";
		
		$this->config->source = $this->config->source." ".$limit;
	
		
		if($this->sort != "")
		{
			$this->sort = str_replace("+"," ",$this->sort);
			if(strpos($this->config->source,"ORDER BY") === false)
			{
				$source_array = explode("LIMIT",$this->config->source);
				
			    $first_part = array_merge($source_array);
				
				unset($first_part[count($first_part) - 1]);
				
				$this->config->source = implode(" LIMIT ",$first_part) . " ORDER BY $this->sort " . (isset($source_array[count($source_array) - 1]) ? " LIMIT " . $source_array[count($source_array) - 1] : "");
			}
			else
			{
				
			}
		}
		
		//print $this->config->source;
		
		
		$count = 0;
		$coln = 0;
		$columns = array();
		$result = dbQuery($this->config->source,array(),TRUE,$count,$coln,$columns);
		$footer_array = array();
		
		include("lib/paging.php");	
		$show_checkbox = $this->config->show_checkboxes;
		$links_array = $this->config->edit_buttons;

		?>
		<div class="container-fluid">
		<table width="100%" border="0"  class="ct">
			<tr>
				<?php if(!empty($this->config->table_left_content)) { ?>
				<td  class="ltd"><?php include($this->config->table_left_content); ?></td>
				<?php } ?>
				<td class="ctd" >			
						<?php
			if($this->config->table_content_top_2 != "")
			{
				include($this->config->table_content_top_2);
			}

		/* //if($pagecount > 1) { ?>
					<table width="100%">
<tr>
<td><?
	if((isset($this->config->table_edit_mode) && $this->config->table_edit_mode == "1")  || $this->fast_edit_mode)
						{
							?>
							<p><input type="button" class="btn btn-default" onclick="simple_save_form('<?=get_url(1,$this->config)?>');" value="<?=_BUTTON_SAVE?>" />
							&nbsp;&nbsp;
							<input type="reset" class="btn"  value="<?=_BUTTON_CANCEL?>" />?>
							</p>
							<?
						} else {?><p>&nbsp;</p><?} ?></td><td>
					<div class="paging"><?=$pager?></div></td></tr>
					</table>
					<? //} */?>
					
					
					<div class="panel panel-default">
					<div class="panel-body">
					<table class='t table table-hover' <?php echo (($pagecount <= 1) ? "style='margin-top:0px;'" : ""); ?> >
					<tr >
					<?php  if($show_checkbox) { ?>
					<th style="width:1%;" class="td0"  ><input class="cb" onclick='check_all(this)' type='checkbox'  /></th>
					<?php 
					$footer_array[1] = 0;
					}
					if(isset($this->config->edit_buttons) && ($this->config->edit_buttons[0] ) && !isset($_GET["noaction"]) )
					{ ?>
					<th  class="td0"  >&nbsp;</th>
					<?php
						$footer_array[2] = 0;
					}
					if(isset($_GET["parentcontrol"]) && !empty($_GET["parentcontrol"]))
					{
						?>
						<th  class="td0"  width="1%" >&nbsp;</th>
					<?php
					$footer_array[3] = 0;
					}
					
					$field_array = array();
					//create table title
					
					for ($i = 0; $i < $coln; $i++)
					{
						
						$field_name =  $columns[$i]["name"];
						$table_name =  $columns[$i]["table"];
						
						
                        if(in_array($field_name,$this->config->exclude_fields))
						{
							continue;
						}
                        array_push($farray,$field_name);
                        if(in_array($field_name,$hide_array))
						{
							continue;
						}
						?><th <?php echo (isset($width_array[$field_name]) ? "style='width:".$width_array[$field_name]."'" : "")?> nowrap >
						<?php
						
						$sort_field = ($this->config->sort_tablename ? $table_name."." : "").$field_name;
						if(isset($this->config->sort_changes[$field_name]))
						{
							$sort_field = $this->config->sort_changes[$field_name];
						}
						$this_sort = false;
						$sort_direction = "asc";
						if(isset($_GET["sort"]) && str_ireplace(array(" DESC"," ASC"),"",$_GET["sort"]) == $sort_field)
						{
							$this_sort = true;
							$sort_direction = strtolower(trim(str_ireplace($sort_field,"",$_GET["sort"])));
							
						}
						if(isset($this->config->unsorted_fields) && !in_array($field_name,$this->config->unsorted_fields))
						
						{
							?>
							<a href="http://<?php echo get_url(0,$this->config);?>&sort=<?php echo $sort_field."+".($sort_direction == "asc" ? "desc" : "asc"); ?>">
							<?php
						}
						$field_array[$field_name] = true;
						if(isset($this->config->title_fields[$field_name]))
						{
							echo $this->config->title_fields[$field_name];
						}
						else
						{
							echo $field_name;
						}
						if(isset($this->config->unsorted_fields) && !in_array($field_name,$this->config->unsorted_fields))
						
						{
							?></a>&nbsp;&nbsp;<a href="http://<?php echo get_url(0,$this->config)?>&sort=<?php echo $sort_field."+".($sort_direction == "asc" ? "desc" : "asc")?>" ><img alt="" src="images/<?php echo $sort_direction?>.gif" /></a>
							<?php
						}
						
						echo '</th>';
					}
						
					
					echo '</tr>';
					//end table title
					//create table row
					$item_index = 1;
					foreach($result as $row)
					{
						
						if($item_index == 1)
						{
							$item_index = 0;
						}
						else
						{
							$item_index = 1;
						}
						echo "<tr ";
						if($use_udf && function_exists("row_create"))
						{
							row_create($row,$this->config);
						}	
						echo " class='".($item_index == 1 ? "a": "")."tr'>";
						if($show_checkbox)
						{
						echo "<td style='width:1%;' class='td0'  ><input class='cb' type='checkbox' id='cb_".row($this->config->source_key_field,$row)."' name='cb_".row($this->config->source_key_field,$row)."' value='".row($this->config->source_key_field,$row)."' /></td>";
						}
						if(isset($_GET["parentcontrol"]) && !empty($_GET["parentcontrol"]))
						{
							?>
							<td class='td0' ><img alt=""  onclick="apply_value('<?=$_GET["parentcontrol"]?>','<?=row($_GET["keyfield"],$row)?>','<?=htmlspecialchars(row($_GET["textfield"],$row))?>',<?=$_GET["rtype"]?>)" src="images/check.gif" /></td>
						<?php
						}
						if(isset($this->config->edit_buttons) && ($this->config->edit_buttons[0] || $this->config->edit_buttons[1]) && !isset($_GET["noaction"]) )
						{
							echo "<td class='td0' >";
							
							?>
							<div class="btn-group">
								<button type="button" class="btn btn-default row-btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									 <span class="glyphicon glyphicon-menu-hamburger">&nbsp;</span><span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<?if($this->config->edit_buttons[0])
									{
									echo "<li><a href='http://".get_url(1,$this->config)."&id=".row($this->config->source_key_field,$row)."'><span class='glyphicon glyphicon-edit'></span>&nbsp;&nbsp;" . _EDIT . "</a></li>";
									}?>
									<li><a href="#" onclick="javascript:return copy_row('<?php echo row($this->config->source_key_field,$row); ?>','<?php echo $_GET["t"]; ?>');"  ><span class='glyphicon glyphicon-duplicate'></span>&nbsp;&nbsp;<?php echo _COPY; ?></a></li>
									<?php if(!empty($this->config->row_actions))
									{
										echo '<li role="separator" class="divider"></li>';
										foreach($this->config->row_actions as $current)
										{
											echo '<li><a href="' . str_replace('[ID]',row($this->config->source_key_field,$row),$current['LINK']) . '">' . $current['TEXT'] . '</a></li>';	
										}
									}
									?>
									<li role="separator" class="divider"></li>
									<li><a href="#" onclick="delete_row('<?php echo row($this->config->source_key_field,$row)."','". $_GET["t"] ."'" ?>)" ><span class='glyphicon glyphicon-trash'></span>&nbsp;&nbsp;<?php echo _DELETE; ?></a></li>
								</ul>
							</div>
							<?
							echo "</td>";
						}
						if((isset($this->config->table_edit_mode) && $this->config->table_edit_mode == "1") || $this->fast_edit_mode)
						{
							$edit_mode_row = dbQuery("SELECT ".$this->config->select_fields." FROM ".$this->config->table." WHERE ".$this->config->source_key_field." = '".$row[$this->config->source_key_field]."'");
                            
						}
						foreach(array_keys($field_array) as $key)
						{
							
							if(in_array($key,$this->config->sum_fields))
							{
								if(!isset($footer_array[$key]))
								{
									$footer_array[$key] = 0;
								}
								$footer_array[$key] = $footer_array[$key] + $row[$key];
							}
							else
							{
								$footer_array[$key] = 0;
							}
							echo "<td ".((isset($this->config->table_edit_mode) && $this->config->table_edit_mode == "1") || $this->fast_edit_mode ? " nowrap " : "")." >";
							
							if(
							((isset($this->config->table_edit_mode) && $this->config->table_edit_mode == "1") ||   $this->fast_edit_mode   ) 
							&& !in_array($key,array_keys($this->config->template_fields)) 
							&& !in_array($key,array_keys($this->config->eval_fields))
							&& $this->check_edit_mode($row[$this->config->source_key_field])
							)
							{
								if(isset($this->config->fast_edit_array_changes[$key]))
								{
									$key = $this->config->fast_edit_array_changes[$key];
								}
								
								if(!isset($edit_mode_row[$key]))
								{
									$edit_mode_row[$key] = $row[$key];
								}
								if(isset($this->config->controls[$key]))
								{
									$text = $this->config->controls[$key];
									$text->id = "rf_".$row[$this->config->source_key_field]."_".$key;
									$text->name = "fields[".$row[$this->config->source_key_field]."][".$key . ']';
									if($text->type == "list" || ($text->type == "longlist" && $text->showselect = true))
									{
										$text->css_style = $this->config->controls[$key]->css_style."width:80px;";
									}
									$text->value = $edit_mode_row[$key];
									$text->current_row = $edit_mode_row;
									$text->create();
									if($this->config->controls[$key]->type == "checkbox")
									{
										array_push($checkbox_array,$this->config->controls[$key]->name);
									}
								}
								else
								{
									$text = new Control("rf_".$row[$this->config->source_key_field]."_".$key,"text","");
									$text->name = "fields[".$row[$this->config->source_key_field]."][".$key . ']';
									$text->css_style = "width:100%;";
									$text->current_row = $edit_mode_row;
									$text->value = $edit_mode_row[$key];
									if(in_array($key,$this->config->number_fields))
									{
										$text->isNumber = TRUE;
									}
									if(in_array($key,$this->config->required_fields))
									{
										$text->required = true;
									}
									$text->create();
								}
							}
							else
							{
								if(isset($this->config->template_fields[$key]))
								{
									include($this->config->template_fields[$key]);
								}
								else if(isset($this->config->eval_fields) && isset($this->config->eval_fields[$key]))
								{
									eval($this->config->eval_fields[$key]);
								}
								else
								{
									echo $row[$key];
								}	
							}
							echo "</td>";
						}
						echo "</tr>";
					}
					//end table row
					if( count($footer_array) > 2) 
					{
						//create footer
						
						echo "<tr>";
						foreach($footer_array as $key=>$value)
						{
							if(in_array($key,$this->config->sum_fields))
							{
								?><th><?=$value?></th><?php
							}
							else
							{
								echo '<th>&nbsp;</th>';
							}
						}

						echo '</tr>';
					}
					//create footer
					?>
					</table>
					</div></div><?php
						if(count($checkbox_array) > 0)
						{			
							$checkbox_array_string = "";
							foreach($checkbox_array as $current)
							{
								$checkbox_array_string .= $current.",";
								
							}
							?>
							<input type="hidden" name="checkbox_array" value="<?php echo $checkbox_array_string; ?>" />
							<?php
						}
										if((isset($this->config->table_edit_mode) && $this->config->table_edit_mode == "1") || $this->fast_edit_mode)
						{
							?>
							<p><input  type="submit" class="btn btn-success" onclick="simple_save_form('<?php echo get_url(1,$this->config); ?>');" value="<?php echo _BUTTON_SAVE; ?>" />
							<?php /*&nbsp;&nbsp;
							<input type="reset" class="buttons"  value="<?=_BUTTON_CANCEL?>" />*/ ?>
							</p>
							<?php
						}
						?>
					</td>
					<?php if(!empty($this->config->table_right_content)) { ?>
				<td class="rtd">
				<?php include($this->config->table_right_content); ?>
				</td>
				<?php } ?>
				</tr>
			</table>
			<div class="panel panel-default">
				<div class="panel-body"><div class="paging"><?php echo $pager; ?></div></div>
			</div>
			</div>
			<?php
				
		if($this->config->table_content_bottom_1 != "")
		{
			include($this->config->table_content_bottom_1);
		}
		
	
		?>
		</form>
		
		<?php
			
		if(isset($this->config->filters))
		{
			$filters = new Filter($this->config->filters);
			$filters->init();
		}
		//if(isset($this->config->table_edit_mode) && $this->config->table_edit_mode == "1")
		//{
			?>
			<div id="dlonglist" >
            <iframe id="ilonglist" frameborder="0" style=""></iframe>
            <input onclick="$('#dlonglist').fadeOut();" class="buttons" type="button" value="Закрыть" />
            </div>
			<?php
		//}
        //?>
        <?php
	}
	function delete_row($id)
	{
		if(function_exists("before_delete"))
		{
			before_delete($id);
		}
		if($this->config->delete_mode == 0)
		{
			dbNonQuery("DELETE FROM ".$this->config->table." WHERE ".$this->config->source_key_field." = :id",array(":id"=>$id));
		}
		else
		{
			dbNonQuery("UPDATE " . $this->config->table . " SET deleted = 1 WHERE ".$this->config->source_key_field." = :id",array(":id"=>$id));
		}
		
		if(function_exists("after_delete"))
		{
			after_delete($id);
		}
		//header("Location: http://".get_url(1,$this->config));
	}
}