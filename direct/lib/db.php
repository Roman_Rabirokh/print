<?php
class DB
{
	var $config = "";
	var $id = "-1";
	var $object_name = "";
	var $close_form = false;
	function return_filed_value($id)
	{
		?>
		<script language="JavaScript" type="text/javascript" src="js/jquery.js"></script>	
		<script language="JavaScript" type="text/javascript" src="js/global_function.js"></script>
		<script language="JavaScript" type="text/javascript">
		apply_value('<?=$_GET["parentcontrol"]?>','<?=$id?>','<?=$_POST[$_GET["textfield"]]?>','<?=$_GET["rtype"]?>');
		</script>
		<?php
	}
	function check_unique($array,$table,$edit_title_fields)
	{
		global $engine_db;
		$i = 0;
		$res = _MESSAGE_UNIQUEERROR."\\n";
		foreach($array as $current)
		{
			$count = dbGetOne("SELECT count(*) FROM $table WHERE $current = :value",array(":value"=>$_POST[$current]));
			if($count > 0)
			{
				$i++;
				$res .= $edit_title_fields[$current]."\\n";
			}
		}
		if($i > 0)
		{
			$_SESSION["last_post"] = $_POST;
			$_SESSION["rowerror"] = $res;
			header("Location: http://".$_SESSION["back_row_url"]);
			exit;
		}
	}
	function DB($config,$id,$object_name)
	{
		$this->config  = $config;
		$this->id  = $id;
		$this->object_name  = $object_name;
		
	}
	function update_miltiple()
	{
		$fieldsData = !empty($_POST["fields"]) ? $_POST["fields"] : array();
		foreach($fieldsData as $key=>$value)
		{
			$this->id = $key;	
			$this->update($value,TRUE);
		}	
		?><script>window.parent.document.location.reload();</script><?php	
	}
	function insert()
	{
		
		$fieldsData = !empty($_POST["fields"]) ? $_POST["fields"] : array();
		
		$this->check_unique(isset($this->config->unique_fields) ? $this->config->unique_fields : array(),$this->config->table,$this->config->edit_title_fields);

		if(function_exists("before_insert"))
		{
			before_insert($fieldsData);
		}
		//print_r($fieldsData);
		$coln = 0;
		$columns = array();
		$result = dbGetRow("SELECT * FROM ".$this->config->table." WHERE ".$this->config->source_key_field." = :key",array(":key"=>$this->id),$coln,$columns);
		$sql = "INSERT INTO ".$this->config->table." ";
		$field = array();
		$value = "";
		$checkboxes = array();
		if(isset($_POST["checkbox_array"]))
		{
			$checkboxes = explode(",",$_POST["checkbox_array"]);
		}
		$values = array();
		for ($i = 0; $i < $coln; $i++)
		{
			$field_name = $columns[$i]["name"];
			if($field_name == $this->config->source_key_field) { continue; }	
			if(in_array($field_name,$checkboxes))
			{
				$field[] = $field_name;
				if(isset($fieldsData[$field_name]))
				{
					$values[":" . $field_name] = 1;
				}
				else
				{
					$values[":" . $field_name] = 0;
				}
			}
			else
			{
				if(isset($fieldsData[$field_name]))
				{
					$field[] = $field_name;
					$values[":" . $field_name] = htmlspecialchars($fieldsData[$field_name],ENT_QUOTES);
				}
			}
		}
		$field = "(".implode(",",$field).")";
		$value = " VALUES (".implode(",",array_keys($values)).")";
		
		$sql .= $field.$value;
		$lastid = dbNonQuery($sql,$values,TRUE);
		
		
		
		//echo $sql;
		//exit();
		if(function_exists("after_insert"))
		{
			after_insert($lastid,$fieldsData);
		}
		
		if(isset($_GET["return"]))
		{
			$this->return_filed_value($lastid);
			exit();
		}
		
		if($this->close_form)
		{
			
			?><script>window.parent.closeForm();</script><?php
		}
		else
		{
			?><script>window.parent.afterUpdate(<?php echo $lastid; ?>,1);</script><?php
		}	
	}
	function update($fieldsData = array(),$only_execute = false)
	{
		//$this->check_unique(isset($unique_fields) ? $unique_fields : array(),$table,$edit_title_fields);
		if(empty($fieldsData))
		{
			$fieldsData = !empty($_POST["fields"]) ? $_POST["fields"] : array();	
		}
		$coln = 0;
		$columns = array();
		$result = dbGetRow("SELECT * FROM ".$this->config->table." WHERE ".$this->config->source_key_field." = :key",array(":key"=>$this->id),$coln,$columns);
		
		if(function_exists("before_update"))
		{
			before_update($this->id,$result,$fieldsData);
		}
	
		$sql = "UPDATE ".$this->config->table." SET ";
		$sqlFields = array();
		$checkboxes = array();
		if(isset($_POST["checkbox_array"]))
		{
			$checkboxes = explode(",",$_POST["checkbox_array"]);
		}
		$values = array();
		for ($i = 0; $i < $coln; $i++)
		{
			$field_name = $columns[$i]["name"];
			if($field_name == $this->config->source_key_field) { continue; }
				
			
				//$sqlFields[] = $field_name . "= '" . htmlspecialchars($fieldsData[$field_name],ENT_QUOTES) . "'" ;
				
				if(in_array($field_name,$checkboxes))
				{
					if(isset($fieldsData[$field_name]))
					{
						$values[":" . $field_name] = 1;
					}
					else
					{
						$values[":" . $field_name] = 0;
					}
					$sqlFields[] = $field_name . "=:" . $field_name ;
				}
				else
				{
					if(isset($fieldsData[$field_name]))
					{
						$values[":" . $field_name] = htmlspecialchars($fieldsData[$field_name],ENT_QUOTES);
						$sqlFields[] = $field_name . "=:" . $field_name ;
					}
				}
		}	
		$sql .= implode(',',$sqlFields);
		$sql .= " WHERE ".$this->config->source_key_field." = :key";
		$values[":key"] = $this->id;

		
		
		dbNonQuery($sql,$values);

		if(function_exists("after_update"))
		{
			after_update($this->id,$result,$fieldsData);
		}
		if(isset($_GET["return"]))
		{
			$this->return_filed_value($this->id);
			exit();
		}

		//exit();
		if(!$only_execute)
		{
			if($this->close_form)
			{
				
				?><script>window.parent.closeForm();</script><?php
			}
			else
			{
				?><script>window.parent.afterUpdate(<?php echo $this->id; ?>,1);</script><?php
			}	
		}
		
	}
	function copy()
	{
		if(function_exists("before_copy"))
		{
			before_copy($this->id);
		}
		
		
		$row = dbGetRow("SELECT * FROM ".$this->config->table." WHERE ".$this->config->source_key_field." = :id",array(":id"=>$this->id));
		
		$fields = array();
		$values = array();
		foreach($row as $key=>$value)
		{
			if($key == $this->config->source_key_field) { continue; }
			$fields[] = $key;
			$values[":" . $key] = $value;	
		}

		$sql = "INSERT INTO ".$this->config->table." (" . implode(",",$fields) . ") VALUES(" . implode(",",array_keys($values)) . ")";
		
		$lastId = dbNonQuery($sql,$values,TRUE);
		
		
		if(function_exists("after_copy"))
		{
			after_copy($this->id,$lastId);
		}
		echo  $lastId;
	}
	function execute()
	{
		if($this->id == "-1")
		{
			$this->insert();
		}
		else
		{
			$this->update();
		}
	}
	
}

?>