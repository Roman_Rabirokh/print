<?php 
if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/api/config.php"))
{
	include($_SERVER["DOCUMENT_ROOT"] . "/api/config.php");
	define("_CMS",TRUE);
}
else
{
	define("_DB_TABLE_PREFIX","rs_");
}

include_once("config/global.php");
include_once("language/$language/$language.php");
include_once("config/db.php");
include_once("lib/user.php");
include_once("function/global.php");
include_once("function/db.pdo.php");

if(isset($_SESSION["user"]))
{
	include_once("lib/config.php");
	include_once("lib/table.php");
	include_once("lib/row.php");
	include_once("lib/control.php");
	include_once("lib/db.php");
	//include_once("lib/simple_table.php");
	include_once("lib/filter.php");
	include_once("lib/tabs.php");
}
