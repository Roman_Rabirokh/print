var waitingDialog = waitingDialog || (function ($) {
    'use strict';
	var $dialog = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
			'<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body">' +
				'<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
			'</div>' +
		'</div></div></div>');
	return {
		show: function (message, options) {
			if (typeof options === 'undefined') {
				options = {};
			}
			if (typeof message === 'undefined') {
				message = 'Loading';
			}
			var settings = $.extend({
				dialogSize: 'm',
				progressType: '',
				onHide: null
			}, options);

			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
			$dialog.find('.progress-bar').attr('class', 'progress-bar');
			if (settings.progressType) {
				$dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
			}
			$dialog.find('h3').text(message);
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);
				});
			}
			$dialog.modal();
		},
		hide: function () {
			$dialog.modal('hide');
		}
	};

})(jQuery);
function afterUpdate(id,is_insert)
{
	waitingDialog.hide();
	$("#btnSave").removeAttr("disabled");
	$("#btnSaveClose").removeAttr("disabled");
	if(is_insert == 1)
	{
		document.location.href = $("#base_url").val() + "&id=" + id;
	}
	
}
function closeForm()
{
	document.location.href = $("#base_url").val();
}
$(function(){
	$(".t tbody .cb").click(
		function()
		{
			$(this).parents("tr").toggleClass("success");
		}
	);	
	//$(".chosen-select").chosen();
	$('.control_date').datetimepicker({locale: 'ru', showClose: true, format: "YYYY-MM-DD HH:mm:ss",sideBySide:false,allowInputToggle:true});
	
	$("#btnSaveClose").click(function(){
		$("#is_close").val(1);
	});
	
	$(".form-edit").submit(function(){
		
		$("#btnSave").attr("disabled","disabled");
		$("#btnSaveClose").attr("disabled","disabled");
		waitingDialog.show("Сохранение...");
		return true;
		
	});
	
	
});

function copy_row(id,t)
{
	waitingDialog.show("Копируем...");
	$.post("db.php?t=" + t,{copy:id},function(id){  document.location.href = $("#base_url").val() + "&id=" + id; });
}

function delete_row(id,t)
{
	if(confirm(delete_message))
	{
		waitingDialog.show("Удаление...");
		$.post("db.php?t=" + t,{delete:id},function(){ 	waitingDialog.hide("Удаление..."); document.location.reload(); });
			
	}
	return false;
}
function delete_rows(t)
{
	if(confirm(delete_message))
	{
		var selected = [];
		$('.cb:checked').each(function() {
			selected.push($(this).val());
		});
		if(selected.length > 0)
		{
			waitingDialog.show("Удаление...");
			$.post("db.php?t=" + t,{delete:selected},function(){ 	waitingDialog.hide("Удаление..."); 
			document.location.reload(); 
			},"html");
		}
	}
	return false;
}

function simple_save_form(path)
{
	waitingDialog.show("Сохранение...");
}

function openWindow(title,content)
{
	$("#modalWindowGeneral").find(".modal-body").html(content);
	$("#modalWindowGeneral").find(".modal-title").html(title);
	$("#modalWindowGeneral").modal();
}

function edit_longlist(url,width,height,obj,parent)
{
	   if(parent != "")
    {
        var parentid = get_select_value(parent);
        if(parentid != "-1")
        {
            url = url + "&parent="+parentid;
        }
    }
	
	$("#modalContentBody").html('<iframe frameborder="0" border="0" style="width:100%;height:600px;" src="' + url + '"></iframe>');
	$("#modalWindowGeneral").modal();
	
}
function apply_value(control,id,name,type)
{
	if(type == 0)
	{
		window.parent.document.getElementById(control).value = id;
		window.parent.document.getElementById("t_"+control).innerHTML = name;
	}
	else
	{
		if(window.parent.$("#"+control).containsOption(id))
		{
			window.parent.$("#"+control).selectOptions(id, true);
		}
		else
		{
			window.parent.$("#"+control).addOption(id, name);
			window.parent.$("#"+control).selectOptions(id, true);
		}
	}	
	window.parent.$("#modalWindowGeneral").modal('hide');
}
function setHeight(id,h)
{
	$("#" + id).height(h);
}
function getUniqueID()
{
	return '_' + Math.random().toString(36).substr(2, 9);
}


function deleteImageFromOrder(obj)
{
    var idImage = $(obj).attr('idi');
    $.ajax({
        url: '/admin/delete-image-from-order/',
        type: 'POST',
        data: {id: idImage},
        success: function (data) {
           location.reload()


        },
    });
}