<?php 

$title_fields["name"] = "Наименование";
$title_fields["id"] = "ID";
$title_fields["showorder"] = "Порядок";
$title_fields["code"] = "Код";

$controls['showorder'] = new Control('showorder','number','Порядок');
$controls['showorder']->default_value = 500;

$controls['name'] = new Control('name','text','Наименование');
$controls['name']->required = TRUE;

