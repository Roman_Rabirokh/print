<?php


function getOpenContentTree($id,&$arr)
{
	$data = dbGetRow('SELECT name,parentid FROM #__content WHERE id = :id LIMIT 0,1',array(':id'=>intval($id)));
	$arr[$id] = $data['name'];
	if($data['parentid'] > 0)
	{
		getOpenContentTree($data['parentid'],$arr);
	}
}


function show_active($row)
{
	if(strcmp($row["active"],"1") == 0)
	{
		?><input type="checkbox" class="checkbox cb_active" data-id="<?php echo $row["id"]; ?>" value="1" checked /><?php
	}
	else
	{
		?><input type="checkbox" class="checkbox cb_active" data-id="<?php echo $row["id"]; ?>"  value="1" /><?php
	}
}

function uploadFile($id,$originalname,$filename,$value)
{
	if(empty($filename)) {  return; }

	$filesize = filesize($filename);
	$format = explode(".",$originalname);
	$strFormat = $format[count($format) - 1];
	$newName = $id . '_' . uniqid() . '.' . $strFormat;

	if(move_uploaded_file($filename,_FILES_PATH . $newName
	))
	{
		dbNonQuery(
		'INSERT INTO #__files (source,parentid,title,description,originalname,filename,filesize,showorder)
		VALUES(:source,:parentid,:title,:description,:originalname,:filename,:filesize,:showorder
		)',
		array(
		":source"=>1,
		":parentid"=>$id,
		":title"=>$value["title"],
		":description"=>$value["description"],
		":originalname"=>$originalname,
		":filename"=>$newName,
		':filesize'=>$filesize,
		':showorder'=>$value["showorder"]
		)
		);

	}

}

function rmFile($id)
{
	$fileInfo = dbGetRow('SELECT * FROM #__files WHERE id = :id',array(':id'=>$id));
	if(!empty($fileInfo))
	{
		unlink(_FILES_PATH . $fileInfo['filename']);
		dbNonQuery('DELETE FROM #__files WHERE id = :id',array(':id'=>$id));
	}
}
function rmImage($id)
{
	$fileInfo = dbGetRow('SELECT * FROM #__images WHERE id = :id',array(':id'=>$id));
	if(!empty($fileInfo))
	{
		unlink(_IMAGES_PATH . $fileInfo['filename']);
		dbNonQuery('DELETE FROM #__images WHERE id = :id',array(':id'=>$id));
		dbNonQuery('UPDATE #__content SET mainimage = 0 WHERE mainimage = :id',array(':id'=>$id));
	}
}

function uploadImage($id,$originalname,$filename,$value,$setMain = FALSE)
{
	if(empty($filename)) {  return; }

	$format = explode(".",$originalname);
	$strFormat = $format[count($format) - 1];
	$newName = $id . '_' . uniqid() . '.' . $strFormat;
	$size = getimagesize($filename);
	$width = $size[0];
	$height = $size[1];
	$format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
	$filesize = filesize($filename);

	if(move_uploaded_file($filename,_IMAGES_PATH . $newName
	))
	{
		$imgID = dbNonQuery(
		'INSERT INTO #__images (source,parentid,title,description,originalname,filename,width,height,mime,filesize,showorder)
		VALUES(:source,:parentid,:title,:description,:originalname,:filename,:width,:height,:mime,:filesize,:showorder
		)',
		array(
		":source"=>1,
		":parentid"=>$id,
		":title"=>$value["title"],
		":description"=>$value["description"],
		":originalname"=>$originalname,
		":filename"=>$newName,
		":width"=>$width,
		":height"=>$height,
		":mime"=>$format,
		':filesize'=>$filesize,
		':showorder'=>$value["showorder"]
		),TRUE
		);

		if($setMain)
		{
			dbNonQuery('UPDATE #__content SET mainimage = :imgID WHERE id = :id',array(
			':imgID'=>$imgID,
			':id'=>$id
			));
		}
	}
}



function saveHandler($id,&$data)
{
	if(!empty($_POST["images"]))
	{
		foreach($_POST["images"] as $key=>$value)
		{
			if(!empty($_FILES["images"]["tmp_name"][$key]["file"]))
			{
				uploadImage($id,$_FILES["images"]["name"][$key]["file"],$_FILES["images"]["tmp_name"][$key]["file"],$value);
			}
		}
	}

	if(!empty($_POST["files"]))
	{
		foreach($_POST["files"] as $key=>$value)
		{
			if(!empty($_FILES["files"]["tmp_name"][$key]["file"]))
			{
				uploadFile($id,$_FILES["files"]["name"][$key]["file"],$_FILES["files"]["tmp_name"][$key]["file"],$value);
			}
		}
	}

	if(isset($_FILES['mainimage']))
	{
		uploadImage($id,$_FILES['mainimage']["name"],$_FILES['mainimage']['tmp_name'],array('title'=>'','description'=>'','showorder'=>500),TRUE);
	}

	//create url
	$contentType = dbGetRow('SELECT * FROM #__content_types WHERE id = :id',array(':id'=>$data['content_type']));
	$url = array();
	if(!empty($contentType) && !empty($contentType['urlname']))
	{
		$url[] = $contentType['urlname'];
	}

	$url[] = $data['urlname'];

	dbNonQuery('UPDATE #__content SET url = :url WHERE id = :id',array(':url'=>implode('/',$url),':id'=>$id));


	if(function_exists('saveHandlerExt'))
	{
		saveHandlerExt($id,$data);
	}


}
function before_insert(&$data)
{
	$data["created"] = date("Y-m-d H:i:s");
	$data["updated"] = date("Y-m-d H:i:s");
	if(empty($data["urlname"]))
	{
		$data["urlname"] = create_urlname($data["name"]);
	}
}
function before_update($id,$old,&$data)
{
	$data["updated"] = date("Y-m-d H:i:s");
	if(empty($data["urlname"]))
	{
		$data["urlname"] = create_urlname($data["name"]);
	}
}

function after_insert($id,&$data)
{
	updateContentFields($id,$data,0);
	saveHandler($id,$data);

}

function after_update($id,$data,$fieldsData)
{
	saveHandler($id,$fieldsData);
	updateContentFields($id,$fieldsData,1);
}

function before_delete($id)
{
	$contentType = dbGetOne('SELECT content_type FROM #__content WHERE id = :id',array(':id'=>$id));
	dbNonQuery('DELETE FROM #__content_data_' . $contentType . ' WHERE id = :id',array(':id'=>$id));
}

function after_delete($id)
{

	$images = dbQuery('SELECT id FROM #__images WHERE parentid = :id AND source = 1',array(':id'=>$id));
	foreach($images as $file)
	{
		rmImage($file['id']);
	}

	$files = dbQuery('SELECT id FROM #__files WHERE parentid = :id AND source = 1',array(':id'=>$id));
	foreach($files as $file)
	{
		rmFile($file['id']);
	}
	updateContentFields($id,array(),2);

}
