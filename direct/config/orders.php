<?php
$title = 'Заказы пользователя';

$table = '#__photo_orders';
$where='';
if(!empty($_GET['user']))
    $where='WHERE o.userid ='.$_GET['user'];
else
    $where='';
//filters
$filters=array();
$numberOrderF = new Control('name','number','Номер заказа');

$filters[] = $numberOrderF;

$numberOrderV = filters_get_value($numberOrderF);

if(!empty($numberOrderV))
{
    if(empty($where))
    {
        $where='WHERE o.id ='.$numberOrderV;
    }
    else
        $where.=' AND o.id ='.$numberOrderV;
}



$SumF = new Control('sum','number','Сумма');

$filters[]= $SumF;

$SumV = filters_get_value($SumF);

if(!empty($SumV))
{
    if(empty($where))
    {
        $where='WHERE o.cost ='.$SumV;
    }
    else
        $where.=' AND o.cost ='.$SumV;
}


    $source = 'SELECT o.id, o.id AS number, o.created,o.cost,o.region,o.city ,o.street, o.house, o.flat,o.npoffice,o.phone_number, o.fio,'
            . ' o.id AS linkPhotos '
            . ' FROM  ' . $table . ' o '
            . $where;

$title_fields["number"] = "Номер заказа";
$title_fields["created"] = "Дата создания";
$title_fields["cost"] = "Сумма";
$title_fields["region"] = "Область";
$title_fields["city"] = "Город";
$title_fields["street"] = "Улица";
$title_fields["house"] = "Дом";
$title_fields["flat"] = "Квартира";
$title_fields["npoffice"] = "Офис Н.П.";
$title_fields["phone_number"] = "Телефон";
$title_fields["fio"] = "ФИО";
$title_fields["userid"] = "Id пользователя";
$title_fields["status"] = "Статус";

$title_fields["order_description"] = "Описание к заказу";
$title_fields["ordermake"] = "Заказ создан в";
$title_fields["orderdone"] = "Заказ выполнен в";
$title_fields["ordersend"] = "Заказ отправлен в";
$title_fields["orderget"] = "Заказ получен в";
$title_fields["orderpay"] = "Заказ оплачен в";



$title_fields["novaposhta"] = "Новая почта";
$controls["novaposhta"] = new Control("novaposhta","checkbox","Новая почта",NULL);

$title_fields["courier"] = "Доставка курьером";
$controls["courier"] = new Control("courier","checkbox","Доставка курьером",NULL);

$title_fields["linkPhotos"] = "Посмотреть фото";
$unsorted_fields[] = 'linkPhotos';
$eval_fields["linkPhotos"] = "showOrdersPhotosLink(\$row);";
function showOrdersPhotosLink($row)
{
    ?><a href="/direct/index.php?t=ordersphotos&idorder=<?php echo $row["id"]; ?>">Посмотреть</a> <?php
}



