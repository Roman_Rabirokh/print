<?php
$title = 'Пользователи';

$table = '#__user_info';

$where='';
if(!empty($_GET['user']))
    $where='WHERE o.userid ='.$_GET['user'];
else
    $where='';
//filters
$filters=array();

$fioF= new Control('fio','text','ФИО');

$filters[] = $fioF;

$fioV = filters_get_value($fioF);



if(!empty($fioV))
{
    if(empty($where))
    {
        $where=' WHERE u.fio LIKE "%'.$fioV.'%"';
    }
    else
        $where.=' AND u.fio LIKE "%'.$fioV.'%"';
}




$emailF = new Control('email','text','Email');

$filters[] = $emailF;

$emailV = filters_get_value($emailF);



if(!empty($emailV))
{
    if(empty($where))
    {
        $where=' WHERE u.email LIKE "%'.$emailV.'%"';
    }
    else
        $where.=' AND u.email LIKE "%'.$emailV.'%"';
}


$idUserF = new Control('idUser','number','Id пользователя');

$filters[] = $idUserF;

$idUserV = filters_get_value($idUserF);



if(!empty($idUserV))
{
    if(empty($where))
    {
        $where=' WHERE u.email LIKE "%'.$emailV.'%"';
    }
    else
        $where.=' AND u.email LIKE "%'.$emailV.'%"';
}


$telephoneF= new Control('telephone','text','Телефон');

$filters[] = $telephoneF;

$telephoneV = filters_get_value($telephoneF);



if(!empty($telephoneV))
{
    if(empty($where))
    {
        $where=' WHERE u.phone LIKE "%'.$telephoneV.'%"';
    }
    else
        $where.=' AND u.phone LIKE "%'.$telephoneV.'%"';
}


$oblF= new Control('region','text','Область');

$filters[] = $oblF;

$oblV = filters_get_value($oblF);



if(!empty($oblV))
{
    if(empty($where))
    {
        $where=' WHERE u.region LIKE "%'.$oblV.'%"';
    }
    else
        $where.=' AND u.region LIKE "%'.$oblV.'%"';
}


$cityF= new Control('city','text','Город');

$filters[] = $cityF;

$cityV = filters_get_value($cityF);



if(!empty($cityV))
{
    if(empty($where))
    {
        $where=' WHERE u.city LIKE "%'.$cityV.'%"';
    }
    else
        $where.=' AND u.city LIKE "%'.$cityV.'%"';
}


$streetF= new Control('street','text','Улица');

$filters[] = $streetF;

$streetV = filters_get_value($streetF);



if(!empty($streetV))
{
    if(empty($where))
    {
        $where=' WHERE u.street LIKE "%'.$streetV.'%"';
    }
    else
        $where.=' AND u.street LIKE "%'.$streetV.'%"';
}


$houseF= new Control('house','number','Дом');

$filters[] = $houseF;

$houseV = filters_get_value($houseF);



if(!empty($houseV))
{
    if(empty($where))
    {
        $where=' WHERE u.house='.$houseV;
    }
    else
        $where.=' AND u.house ='.$houseV;
}

$flatF= new Control('house','number','Квартира');

$filters[] = $flatF;

$flatV = filters_get_value($flatF);



if(!empty($flatV))
{
    if(empty($where))
    {
        $where=' WHERE u.flat='.$flatV;
    }
    else
        $where.=' AND u.flat ='.$flatV;
}

$source = 'SELECT u.id,u.fio,u.phone,u.email, u.region, u.city, u.street, u.house, u.flat, u.userid,'
        . 'u.id AS linkOrders '
	
        . 'FROM  ' . $table . ' u ' 
        .$where;



//$sort_changes['price'] = 'price';
//$sort_changes['quests'] = 'quests';

$title_fields["fio"] = "ФИО";
$title_fields["phone"] = "Телефон";
$title_fields["email"] = "E-mail";
$title_fields["region"] = "Область";
$title_fields["city"] = "Город";
$title_fields["street"] = "Улица";
$title_fields["house"] = "Дом";
$title_fields["flat"] = "Квартира";
$title_fields["userid"] = "Id пользователя";
$title_fields["adress"] = "Адрес";
$title_fields["user_status"] = "Статус";

$exclude_fields_edit[]='order_in_work';
$exclude_fields_edit[]='order_done';
$exclude_fields_edit[]='client_get_order';
$exclude_fields_edit[]='order_in_transport';
$exclude_fields_edit[]='drop_files_notify';
$exclude_fields_edit[]='order_paid';
$exclude_fields_edit[]='download_done';


$title_fields["linkOrders"] = "Перейти к заказам";
$unsorted_fields[] = 'linkOrders';
$eval_fields["linkOrders"] = "showPersonalOrdersLink(\$row);";
function showPersonalOrdersLink($row)
{
    ?><a href="/direct/index.php?t=orders&user=<?php echo $row["userid"]; ?>">Перейти</a> <?php
}

//$title_fields["personal"] = "Войти в личный кабинет";
//
//$edit_title_fields["name"] = "Имя";
//$edit_title_fields["email"] = "E-mail";
//$edit_title_fields["passw"] = "Пароль";
//$edit_title_fields["surname"] = "Фамилия";
//$edit_title_fields["created"] = "Создан";
//$edit_title_fields["active"] = "Активность";
//$edit_title_fields["blocked"] = "Заблокирован";
//$edit_title_fields["telephone"] = "Телефон";
//$edit_title_fields["city"] = "Город";
//$edit_title_fields["last_login"] = "Дата последнего входа";
//$edit_title_fields["last_ip"] = "IP последнего входа";
//$edit_title_fields["login"] = "Логин";
//$edit_title_fields["percent"] = "Процент вознаграждения";
//
//
//$controls["city"] = new Control("city","list","Город",getListCity());
//
//$controls['name'] = new Control('name','text','Имя');
//$controls['name']->required = FALSE;
//
//$eval_fields['price_created'] = "showPriceCreated(\$row);";
//
//$eval_fields['sum_percent'] = "showSumPercent(\$row);";
//

//
//
//$unsorted_fields[] = 'price_created';
//
//$unsorted_fields[] = 'sum_percent';
//
//$unsorted_fields[] = 'personal';
//
//
//function showPriceCreated($row)
//{
//    $sum= dbGetOne('SELECT SUM(i.price) FROM #__invoices AS i '
//	    . 'INNER JOIN #__content AS c ON(c.id=i.quest_id)'
//	    . 'INNER JOIN #__users AS u ON(u.id=c.userid)'
//	    . 'WHERE u.id= :id', array(':id' => $row['id']));
//    if(empty($sum))
//	$sum=0;
//    print_r ($sum);
//}
//
//function showSumPercent($row)
//{
//    $sum= dbGetOne('SELECT SUM(i.sum_paid_user) FROM #__invoices AS i '
//	    . 'INNER JOIN #__content AS c ON(c.id=i.quest_id)'
//	    . 'INNER JOIN #__users AS u ON(u.id=c.userid)'
//	    . 'WHERE u.id= :id', array(':id' => $row['id']));
//    if(empty($sum))
//	$sum=0;
//    print_r ($sum);
//}
//
//function getListCity()
//{
//     $cities= dbQuery('SELECT name, id FROM #__content WHERE content_type = 3');
//     $new_cities=array();
//     foreach($cities as $city)
//     {
//	 $new_cities[$city['id']]=$city['name'];
//     }
//     return $new_cities;
//}
//
