<?php

$title = 'Фото';



$table = '#__photo_orders_rows';
$where='';


$filters=array();

$nameF= new Control('name','text','Название');

$filters[] = $nameF;

$nameV = filters_get_value($nameF);



if(!empty($nameV))
{
    if(empty($where))
    {
        $where=' WHERE u.fio LIKE "%'.$nameV.'%"';
    }
    else
        $where.=' AND u.fio LIKE "%'.$nameV.'%"';
}

$source = 'SELECT i.id,i.imageid, i.paper_size, info.width, info.height, '
        . ' i.id AS imgthumb, '
        . ' i.id AS dpi, '
        . ' (SELECT t1.originalname FROM #__images as t1 WHERE t1.id=i.imageid) AS originalname, '
        . ' i.id AS del, '
        . ' (SELECT t2.filename FROM #__images as t2 WHERE t2.id=i.imageid) AS filename '
        . ' FROM  ' . $table . ' i '
        . ' JOIN #__photo_orders_rows_info AS info ON(info.id=i.id) '
        . ' WHERE i.orderid ='.$_GET['idorder'];

$title_fields["originalname"] = "Название";
$unsorted_fields[] = 'originalname';

$exclude_fields[] = 'filename';
$exclude_fields[] = 'imageid';
$exclude_fields[] = 'paper_size';

$title_fields["width"] = "Ширина";
$title_fields["height"] = "Высота";
$title_fields["orderid"] = "Номер заказа";
$title_fields["imageid"] = "Id фото";
$title_fields["count"] = "Количество копий";
$title_fields["filds"] = "Поля";

$title_fields["description"] = "Подпись к фотографии";
$controls["description"] = new Control("back_side_text","longtext","Подпись к фотографии",NULL);

$title_fields["is_print"] = "Печатать фото";
$title_fields["color_text"] = "Цвет текста";



$title_fields["back_side_text"] = "Текст на обратной стороне";
$controls["back_side_text"] = new Control("back_side_text","checkbox","Текст на обратной стороне",NULL);

$title_fields["autocorrect"] = "Автокоррекция";
$controls["autocorrect"] = new Control("autocorrect","checkbox","Автокоррекция",NULL);

$title_fields["write_coordinates"] = "Печатать адрес";
$controls["write_coordinates"] = new Control("write_coordinates","checkbox","Печатать адрес",NULL);

$title_fields["write_date"] = "Печатать дату";
$controls["write_date"] = new Control("write_date","checkbox","Печатать дату",NULL);

$title_fields["font_u"] = "Подчеркивание";
$controls["font_u"] = new Control("font_u","checkbox","Подчеркивание",NULL);

$title_fields["font_i"] = "Курсив";
$controls["font_i"] = new Control("font_i","checkbox","Курсив",NULL);

$title_fields["font_b"] = "Жырный шрифт";
$controls["font_b"] = new Control("font_b","checkbox","Жырный шрифт",NULL);


$title_fields["field_width"] = "Ширина полей";
$controls["field_width"] = new Control("field_width","list","Ширина полей",getListFieldWidth());
function getListFieldWidth()
{
     $text=array(2=>2, 4=>4, 6=>6, 8=>8, 10=>10);
     return $text;
}


$title_fields["paper_size"] = "Размер бумаги";
$controls["paper_size"] = new Control("paper_size","list","Размер бумаги",getListPaperSize());
function getListPaperSize()
{
     $papers= dbQuery('SELECT paper_size AS name, id FROM #__photo_parer_properties WHERE paper_content_type ="size"');
     $papers_new=array();
     foreach($papers as $paper)
     {
	 $papers_new[$paper['id']]=$paper['name'];
     }
     return $papers_new;
}

$title_fields["paper_type"] = "Тип бумаги";
$controls["paper_type"] = new Control("paper_type","list","Тип бумаги",getListPaperTypes());
function getListPaperTypes()
{
     $papers= dbQuery('SELECT paper_type AS name, id FROM #__photo_parer_properties WHERE paper_content_type ="type"');
     $papers_new=array();
     foreach($papers as $paper)
     {
	 $papers_new[$paper['id']]=$paper['name'];
     }
     return $papers_new;
}

$title_fields["font"] = "Тип бумаги";
$controls["font"] = new Control("font","list","Шрифт",getListFonts());
function getListFonts()
{
     $fonts= dbQuery('SELECT paper_font AS name, id FROM #__photo_parer_properties WHERE paper_content_type ="font"');
     $fonts_new=array();
     foreach($fonts as $font)
     {
	 $fonts_new[$font['id']]=$font['name'];
     }
     return $fonts_new;
}

$title_fields["font_size"] = "Размер";
$controls["font_size"] = new Control("font_size","list","Размер шрифта",getListFontsSize());
function getListFontsSize()
{
     $fonts= dbQuery('SELECT paper_font_size AS name, id FROM #__photo_parer_properties WHERE paper_content_type ="font_size"');
     $fonts_new=array();
     foreach($fonts as $font)
     {
	 $fonts_new[$font['id']]=$font['name'];
     }
     return $fonts_new;
}

$title_fields["date_stam"] = "Формат даты";
$controls["date_stam"] = new Control("date_stam","list","Формат даты",getListFormatsDate());
function getListFormatsDate()
{
     $formats= dbQuery('SELECT paper_date AS name, id FROM #__photo_parer_properties WHERE paper_content_type ="date"');
     $formats_new=array();
     foreach($formats as $format)
     {
	 $formats_new[$format['id']]=$format['name'];
     }
     return $formats_new;
}

$title_fields["text_orientation"] = "Положение текста";
$controls["text_orientation"] = new Control("text_orientation","list","Положение текста",getListTextOrientation());
function getListTextOrientation()
{
    $text=array('Левый верхний'=>'Левый верхний','Левый нижний'=>'Левый нижний','Правый верхний'=>'Правый верхний','Правый нижний'=>'Правый нижний','Центр'=>'Центр');
     return $text;
}

$title_fields["framing"] = "Кадрирование";
$controls["framing"] = new Control("framing","list","Кадрирование",getListFraming());
function getListFraming()
{
    $text=array(0=> 'уменьшение с белыми полями', 1=> 'обрезка по меньшей границе', );
    return $text;
}

$title_fields["photo_orientation"] = "Ориентация фото";
$controls["photo_orientation"] = new Control("photo_orientation","list","Ориентация фото",getListPhotoOrientation());
function getListPhotoOrientation()
{
    $text=array(0=> 'портрет', 1=> 'ландшафт', );
    return $text;
}


$title_fields["del"] = "Удалить";
$unsorted_fields[] = 'del';
$eval_fields["del"] = "getShowLinkDelete(\$row);";

function getShowLinkDelete($row)
{
    $status=(int)dbGetOne('SELECT status FROM #__photo_orders WHERE id= :id', [':id'=> $_GET['idorder']]);
    if(!empty($status))
    {
        $status=(int)$status;
    }
    else
    {
        $status=FALSE;   
    }
    if($status==2 && $status!=FALSE)
    {
       $html='<div id="delete_image" idi="'.$row['id'].'" style="cursor:pointer;color:green" onclick="deleteImageFromOrder(this);" >Удалить</div>';
    }
    else
    {
        $html='<div style="color:red">Удалить нельзя </div>';
    }
    echo $html;
}


$title_fields["dpi"] = "Dpi";
$unsorted_fields[] = 'dpi';
$eval_fields["dpi"] = "getDpi(\$row);";

function getDpi($row)
{
     include_once _DIR.'api/images.php';
     $size= dbGetOne('SELECT paper_size FROM #__photo_parer_properties WHERE id= :id', [':id'=>$row['paper_size']]);
     $size=explode('x',$size);
     $dpi=Images::DPI($row['width'], $row['height'], $size[0], $size[0]);
     echo $dpi;
    
}

$title_fields["imgthumb"] = "Картинка";
$unsorted_fields[] = 'imgthumb';
$eval_fields["imgthumb"] = "getImgThumb(\$row);";
function getImgThumb($row)
{
    include_once _DIR.'api/image.php';
    $thumb= getImageById($row['imageid'],array("k"=>80));
    ?>
        <a target="_blank" href="<?php echo _IMAGES_URL . $row["filename"] ; ?>"><img class="img-thumbnail" src="<?php echo $thumb; ?>" /></a></a> 
    <?php
}

