<?php
include_once(_DIR . "api/image.php");
if($this->value > 0)
{
	$img = dbGetRow("SELECT * FROM #__images WHERE id = :id",array(':id'=>$this->value));
	if($img)
	{

			$thumb = getImageById($img["id"],array("k"=>150));
		?>
		<a target="_blank" href="<?php echo _IMAGES_URL . $img["filename"] ; ?>"><img class="img-thumbnail" src="<?php echo $thumb; ?>" /></a>
		&nbsp;<a onclick="rmImage(<?php echo $this->value; ?>)" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
		<?php
	}
}
else {
	?>
<input class="form-control" type="file" name="mainimage"  />
	<?php
}
?>
