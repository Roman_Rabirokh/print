<?php 

$title = 'Дополниетльные поля';

$table = '#__content_fields';


$source = 'SELECT id,name,code,caption,fieldtype,showorder FROM #__content_fields f WHERE content_type = ' . intval($_GET['parent']);

$exclude_fields = array();

$controls['content_type'] = new Control('content_type','hidden');
$controls['content_type']->default_value = intval($_GET['parent']);


$controls['caption'] = new Control('caption','text','Заголовок');

$controls['multiple'] = new Control('multiple','checkbox','Множественное');

$controls['source'] = new Control('source','text','Источник');

$controls['fieldtype'] = new Control('fieldtype','list','Тип поля',
	array(
		'1'=>'Текст',
		'2'=>'Число',
		'3'=>'Дата',
		'4'=>'Список',
		'5'=>'Длинный текст'
	)
);

$controls['params'] = new Control('params','template','Параметры');
$controls['params']->template_mode = 'standart';
$controls['params']->template = 'config/templates/field_params.php';

function after_insert($id,$data)
{
	
}

function after_update($id,$data)
{
	
}

function before_delete($id)
{
	
}

function after_delete($id)
{
	
}


