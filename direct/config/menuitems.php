<?php 

$title = "Пункты меню";

$table = '#__menu_items';

$source = 'SELECT id,title,link,showorder FROM ' . $table . ' WHERE menuid = ' . intval($_GET['parent']);

$title_fields['title'] = 'Текст';
$title_fields['link'] = 'Ссылка';

$controls['menuid'] = new Control('menuid','hidden');
$controls['menuid']->default_value = $_GET['parent'];