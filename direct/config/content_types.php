<?php 

$title = "Типы контента";

$source = "SELECT id,name,pluralname,genitivename,urlname,description FROM #__content_types";

$table =  "#__content_types";


$exclude_fields = array();

$title_fields["urlname"] = "Символьный код";
$title_fields["pluralname"] = "Множественное наименование";
$title_fields["genitivename"] = "(Добавить ..., Удалить ..)";
$title_fields["description"] = "Описание";


$controls["description"] = new Control("description","longtext","Описание");

$controls["readonly"] = new Control("readonly","checkbox","Только чтение");
$controls["url404"] = new Control("url404","checkbox","Статус 404");
$controls["sitemap"] = new Control("sitemap","checkbox","Включить в Sitemap");

if(!empty($_GET['id']) && $_GET['id'] > 0)
{
	
	global $engine_path;
	$edit_content_bottom = $engine_path. '/config/templates/content_fields.php';
}


function after_insert($id)
{
	$sql = 'CREATE TABLE `#__content_data_' . $id . '` ( `id` INT NOT NULL AUTO_INCREMENT , PRIMARY KEY (`id`)) ENGINE = MyISAM;';
	dbNonQuery($sql);
}

function after_delete($id)
{
	$sql = 'DROP TABLE #__content_data_' . $id . ';';
	dbNonQuery($sql);
}

