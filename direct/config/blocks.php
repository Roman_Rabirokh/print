<?php

$title = 'Блоки';

$table = '#__blocks';

$source = 'SELECT id,name,code,showorder FROM #__blocks';

$controls['content'] = new Control('content','longtext','Содержимое');
$controls['content']->rows = 15;

$eval_fields['name'] = "showEditLink(\$row);";

function showEditLink($row)
{
	?><a href="http://<?php echo get_url(1,null,array(),array('id'=>$row['id'])); ?>"><?php echo $row['name']; ?></a><?php
}
