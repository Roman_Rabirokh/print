<?php if(!defined("_APP_START")) { exit(); } ?>
<div class="error-page" itemscope itemtype="http://schema.org/WebPage">
  <div class="error-page-content" itemscope itemtype="http://schema.org/Thing">
    <div class="error-page-message" itemprop="description">
      Извините!<br>
      Но страницы не существует
    </div>
    <a href="#" class="error-page-btn btn" itemprop="url">Главная</a>
    <img src="<?php echo $page->getTemplateUrl(); ?>images/error-page-img.png" alt="" itemprop="image">
  </div>
  <!-- /.error-page-content -->
</div>
