	<?php if(!defined("_APP_START")) { exit(); }

	if(!isset($_GET['id'])) {  redirect('/404'); exit(); }

	?>


			<div class="spanning">
				<div class="cost-count-content" itemscope itemtype="http://schema.org/WebPage">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb" itemprop="breadcrumb">
									<li><a href="/" itemprop="url">Главная</a></li>
									<li><a href="/personal/" itemprop="url">Мой кабинет</a></li>
									<li class="active">Новый заказ
										<?php
												if (isset($_GET['id'])&&!empty($_GET['id']))
													{
														echo "№ ".$_GET['id'];
													}
										?>

									</li>
								</ol>
							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
				    <div class="row">
				      <div class="col-md-12">
				      <?php include('includes/profile-menu.php'); ?>
				      </div>
				      <!-- /.col -->
				    </div>
						<!-- /.row -->
						<div class="row">
							<div class="col-md-12 clearfix">
								<ul class="step-nav page-menu">
									<li class="done"><a href="/personal/new-order/<?php if (isset($_GET['edit'])&&($_GET['edit']==1)) echo"?id=".$_GET['id']."&edit=1"; ?>">Загрузка фото</a></li>
									<li class="done"><a href="/personal/order/?id=<?php print_r($_GET['id']) ?><?php if (isset($_GET['edit'])&&($_GET['edit']==1)) echo"&edit=1"; ?>">Параметры печати</a></li>
<!--									<li class="active"><a href="/personal/order-cost/?id=<?php //print_r($_GET['id']) ?><?php //if (isset($_GET['edit'])&&($_GET['edit']==1)) echo"&edit=1"; ?>">Расчет стоимости</a></li>-->
                                                                        <li class="active"><a>Расчет стоимости</a></li>
									<li class="disabled"> <a href="/personal/delivery/?id=<?php print_r($_GET['id']) ?><?php if (isset($_GET['edit'])&&($_GET['edit']==1)) echo"&edit=1"; ?>">Оплата / Доставка</a></li>
								</ul>
								<a href="/personal/order/?id=<?php print_r($_GET['id'])?><?php if (isset($_GET['edit'])&&($_GET['edit']==1)) echo"&edit=1"; ?>" class="back-step-btn btn">На шаг назад</a>
							</div>
							<!-- /.col -->
						</div>



						<?php $this->includeComponent('photosite/ordercost.list');?>


					</div>
					<!-- /.container -->
				</div>
				<!-- /.cost-count-content -->
                    </div>
				<!-- /.section-contact -->
			
		</div>
			<!-- /.spanning-columns -->
		</div>
