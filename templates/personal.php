<?php if(!defined("_APP_START")) { exit(); }

global $user;
global $app;
?>

<div class="profile-content" itemscope itemtype="http://schema.org/WebPage">
  <div class="container">

    	<?php $app->includeComponent('photosite/personal.block');?>

  </div>
  <!-- /.container -->
</div>
