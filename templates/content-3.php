<?php if(!defined("_APP_START")) { exit(); } ?>
<div class="faq-content" itemscope itemtype="http://schema.org/WebPage">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <?php echo $page->getPath(); ?>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <div class="row">
      <div class="col-md-12">
        <h3 class="faq-title title-3"><?php echo $page->getTitle(); ?></h3>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <div class="row">
      <div class="col-md-12">
        <div class="accordion" itemscope itemtype="http://schema.org/Thing">

          <?php $this->includeComponent('photosite/faq.list');?>
          

        </div>
        <!-- /.accordion -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
</div>
