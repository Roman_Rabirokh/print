<?php if(!defined("_APP_START")) { exit(); } ?>
<div class="text-page-content" itemscope itemtype="http://schema.org/WebPage">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      <?php $page->getPath(); ?>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
  <div class="articles">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md12 col-sm-12">
          <article class="article" itemscope itemtype="http://schema.org/Article">
            <div itemprop="articleBody">
              <h3 class="article-title title-3" itemprop="about"><?php echo $page->getTitle(); ?></h3>
              <div itemprop="text">
                  <?php echo htmlspecialchars_decode($data['detail_text']); ?>
              </div>

            </div>
          </article>

        </div>

        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.articles -->
</div>
