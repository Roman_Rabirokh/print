<?php if(!defined("_APP_START")) { exit(); }
global $user;
global $app;
//зануляем параметры пагинации для страницы параметров
$_SESSION['paging']="";
$_SESSION['page']="";

//jquery file uploader
$page->addCSS('/templates/js/uploader/css/jquery.fileupload.css');
$page->addCSS('/templates/js/uploader/css/jquery.fileupload-ui.css');
$page->addJS('//code.jquery.com/ui/1.11.4/jquery-ui.js');
$page->addJS('/templates/js/tinymce/tinymce.min.js');
$page->addJS('//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js');
$page->addJS('//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js');
$page->addJS('//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js');
$page->addJS('/templates/js/uploader/jquery.iframe-transport.js');
$page->addJS('/templates/js/uploader/jquery.fileupload.js');
$page->addJS('/templates/js/uploader/jquery.fileupload-process.js');
$page->addJS('/templates/js/uploader/jquery.fileupload-image.js');
$page->addJS('/templates/js/uploader/jquery.fileupload-audio.js');
$page->addJS('/templates/js/uploader/jquery.fileupload-video.js');
$page->addJS('/templates/js/uploader/jquery.fileupload-validate.js');
$page->addJS('/templates/js/uploader/jquery.fileupload-ui.js');

$token=App::createFormToken('eslibmishkibylipchelami137');

// перезаписать сессию если в ней не открытый заказ
if((!empty($_GET['id']) && !empty($_SESSION['order_id']) && $_GET['id']!=$_SESSION['order_id'] ) || (empty($_SESSION['order_id']) && !empty($_GET['id'])))
{
    $_SESSION['order_id']=$_GET['id'];
}

//запись данных по соцсетям в базу данных
if (isset($_SESSION['send_status'])&&(!empty($_SESSION['send_status'])))
{

  //вносим фото в очередь


  //Вконтакте,Facebook, Google, Urllink
  if ($_SESSION['sourceid']=='1' or $_SESSION['sourceid']=='2' or $_SESSION['sourceid']=='3'  or $_SESSION['sourceid']=='5')
  {

    foreach ($_SESSION['new_images'] as $new_image) {
      Orders::save_to_db($_SESSION['userid'],$_SESSION['created'], $_SESSION['order_id'], $_SESSION['sourceid'], $new_image, '');
    }
    if(!empty($_SESSION['new_images']))
        Orders::set_status_order ($_SESSION['order_id'], 10); // 10 статус говорит о том, что есть очередь загрузки по заказу
  }
  //Yandex, DropBox
if ($_SESSION['sourceid']=='4' or $_SESSION['sourceid']=='7'){


    Orders::save_to_db_token($user->getid(),$_SESSION['created'], $_SESSION['order_id'], $_SESSION['sourceid'], $_SESSION['url'], $_SESSION['token']);
    Orders::set_status_order ($_SESSION['order_id'], 10); // 10 статус говорит о том, что есть очередь загрузки по заказу

  }

  $_SESSION['send_status']='0';
  unset($_SESSION['new_images']);
  unset($_SESSION['url']);
  unset($_SESSION['album']);
  $_POST = array();
}
else{
  //все фото внесены в очередь

  $_POST = array();
}

//получение номера заказа

if (isset($_GET['edit'])&&!empty($_GET['edit']))
{
  if ($order_id=Orders::order_exist_edit($_GET['id']))
  {
    $images_old=Orders::get_images($order_id['0']['id']);
    $myorderid=$order_id['0']['id'];
    $order_id=$order_id['0']['id'];
  }
}
else{

  if ($order_id=Orders::order_exist())
  {

    $images_old=Orders::get_images($order_id['0']['id']);

    $myorderid=$order_id['0']['id'];
  }
}



//получаем order_id
//если у пользователя есть order c status=0
if (empty($_GET['edit']))
{
    if ($order = Orders::cron_order_exist($user->getid()))
    {
        $order_id = $order['0']['id'];
    } else
    {

//  echo 'Сообщите в техподдержку о случившемся'; die;
        Orders::create_new_order();
        $order = Orders::cron_order_exist($user->getid());

        if (!empty($order['0']['id']))
        {
            $order_id = $order['0']['id'];
        }
    }
}

// статус заказа 0- можно редактировать. 10- есть файлы на загрузку, редактировать нельзя. 1- оплачен тоже нельзя редактировать
$status= Orders::get_info_oreder($order_id)['status'];
if($status==10)
{    
    $info_counter= new CounterLoad();
    $info_counter= $info_counter->getInfo($order_id);
    $count_upload_image= $info_counter['allImage'];
    $count_download_image= $info_counter['downloaded'];

}
//print_r($order);
    //print_r($_SESSION);
if(!empty($images_old))
{
    foreach($images_old as &$image)
    {
        $image['img']=getImageById($image['id'], array('height'=>170,'crop'=>array(0,0,200,155)));
        $image['trumb']= getImageById($image['id'], array('height'=>550 ,/*'crop'=>array(0,0,550,550)*/));
    }
}
unset($image);
if( (!empty($_SESSION['order_id']) && $_SESSION['order_id']!=$order_id) || empty($_SESSION['order_id']) )
{
    $_SESSION['order_id']=$order_id;
}

?>


<!--token to script.js-->
<script>var token = '<?php echo   $token; ?>';</script>
<!--upload with jquery file uploader -->
<script type="text/javascript" src="/templates/js/uploader/script.js"></script>

<!-- FANCYBOX -->
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="/templates/js/fancyBox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox -->
<link rel="stylesheet" href="/templates/js/fancyBox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="/templates/js/fancyBox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="/templates/js/fancyBox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="/templates/js/fancyBox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="/templates/js/fancyBox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<link rel="stylesheet" href="/templates/js/fancyBox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="/templates/js/fancyBox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<!-- end FANCYBOX -->


<script type="text/javascript" >
//added one more link in url upload
$( document ).ready(function() {
    $( ".add" ).click(function() {
      $('.html').append('<input required type="url" name="album_id[]"   value="" placeholder="введите ссылку на файл" class="social_input" ></br></br>');
    });
});

$( document ).ready(function() {
  function readURL(input,id,i,name) {

    if (input.files && input.files[i]) {
        var reader = new FileReader();

        reader.onload = function (e) {

            //добавляем превью картинок
            $('.html').append('<div  id="div'+id+i+'" class="uploaded-photo upload-photo-loading"><div class="uploaded-photo-inner"><div class="uploaded-photo"><div class="uploaded-photo-inner"><img id="'+id+i+'" src="" alt="'+name+'"><a  href="javascript:void(0)" class="uploaded-photo-del" >&times;</a><div class="upload-photo-preloader"><img src="<?php echo $page-> getTemplateUrl(); ?>/images/icons/icon-preloader.png" alt=""></div></div></div>');

            $('#'+id+i).attr('src', encodeURI(e.target.result));
        }

        reader.readAsDataURL(input.files[i]);
    }
}

$("#csend").on("keydown", function (e) {
  return e.which !== 32;
});




//вешаем клик на кнопку "Загрузить с компьютера"
  $("#bt1").click(function() {
      $("#csend").click();
  })

// убрать фото из очереди (база +image folder)
$('.upload-btn-wrap').on('click', '.uploaded-photo-del', function() {

  imageid=$(this).attr('id');
  filename=$(this).attr('alt');
  token='<?php echo ($token.'/'.$user->getID().'/'.$order_id); ?>';
  if (imageid){
    $.ajax({
      type: 'POST',
      url: '/del/previewmy/',
      data: { imageid: imageid, filename: filename, token:token },
      success: function(response) {
                $.fancybox(response, {
                type: "html",  wrapCSS: 'alertmes',autoCenter:true,minWidth:300,minHeight:150,overlay: {
      locked: false
    }
            });
      }
    });
  }
  //с темповой папки убираем просто (tmp folder)
  else {
    {

      $.ajax({
        type: 'POST',
        url: '/del/preview/',
        data: { imageid: imageid, filename: filename, token:token },
        success: function(response) {
                   $.fancybox(response, {
                type: "html",  wrapCSS: 'alertmes',autoCenter:true,minWidth:300,minHeight:150,overlay: {
      locked: false
    }
            });
        }
      });
    }
  }
  $(this).parent().parent().remove();
});

//show social form

    $( ".social-link" ).click(function() {
      $(".social-form").hide();
      $("."+$(this).attr("id")).show();
    });


});
//add fancybox
$(".fancybox").fancybox({
    openEffect  : 'none',
    closeEffect : 'none',
	minHeight: 500,

    afterLoad   : function() {

    }
});




</script>


<link rel="stylesheet" href="/templates/css/social.css" type="text/css" media="screen" />


<div class="photo-upload-content" itemscope itemtype="http://schema.org/WebPage">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ol class="breadcrumb" itemprop="breadcrumb">
          <li><a href="/" itemprop="url">Главная</a></li>
          <li><a href="/personal/" itemprop="url">Мой кабинет</a></li>
          <li class="active"><a href="/personal/new-order/">
            Новый заказ
            <?php
                if (isset($myorderid)&&!empty($myorderid))
                  {
                    echo "№ ".$myorderid;
                  }
            ?>
          </a></li>
        </ol>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <div class="row">
      <div class="col-md-12">
      <?php include('includes/profile-menu.php'); ?>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <div class="row">
      <div class="col-md-12 clearfix">
        <ul class="step-nav page-menu">
<!--          <li class="active"><a href="/personal/new-order/">Загрузка фото</a></li>-->
         <li class="active"><a>Загрузка фото</a></li>
          <li>
              <?php if($status!=10) { ?>
              <a href="/personal/order/<?php if (isset($_GET['edit'])&&($_GET['edit']==1)) {echo '?id='.$_GET['id'].'&edit=1';} else {echo '?id='.$order_id;} ?>">Параметры печати</a>
             <?php } else { ?>
             Параметры печати
                 <?php }?>
          </li>
          <li>Расчет стоимости</li>
          <li class="disabled">Оплата / Доставка</li>
        </ul>
        <a href="/" class="back-step-btn btn">На шаг назад</a>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <div class="row">
      <div class="col-md-12">
        <h3 class="photo-upload-title title-3 title-center">Загрузите Ваши фото удобным для Вас способом</h3>
        <div class="upload-btn-wrap">

          <form id="fileupload" action="/item/image/upload/<?php echo   $token; ?>/" method="POST" enctype="multipart/form-data">
                <!-- Redirect browsers with JavaScript disabled to the origin page -->
                <!--
                <noscript><input type="hidden" name="redirect" value="/noscript/"></noscript>
              -->
                <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                <div class="row fileupload-buttonbar">
                    <div class="col-lg-12 ">
                        <!-- The fileinput-button span is used to style the file input field as button -->
                        <span class="btn btn-success fileinput-button upload-btn " id="bt1">
                            <i class="glyphicon glyphicon-plus"></i>
                            <span >Загрузить с компьютера</span>
                            <input type="file" name="files[]" multiple id="csend"  accept="image" required="required" class="newimage">
                        </span>
                         <a href="#social" class="fancybox"><button class="upload-btn btn ">Загрузить по ссылке</button></a>
                         
                         
                         <?php if ($status != 10)
            { ?>
                <a href="/personal/order/<?php if (isset($_GET['edit']) && ($_GET['edit'] == 1))
                {
                    echo '?id=' . $_GET['id'] . '&edit=1';
                } else
                {
                    echo '?id=' . $order_id;
                } ?>" class="print-parameters-btn btn" style="margin-top:0px;" >Указать параметры печати</a>
<?php } ?>

                    </div>
                    <!-- The global progress state -->
                    <div class="col-lg-5 fileupload-progress fade ">
                        <!-- The global progress bar -->
                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                            <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                        </div>
                        <!-- The extended global progress state
                         <?/*  <span class="fileupload-process"></span>*/?>
                        <?/*<div class="progress-extended">&nbsp;</div>*/?>
                        -->
                    </div>
                </div>
                <!-- The table listing the files available for upload/download -->
              <div class="files">

                  <?php
                      if (isset($images_old)&&(!empty($images_old))){
                        foreach ($images_old as $image) { ?>

                          <div class="uploaded-photo">
                            <div class="uploaded-photo-inner">
                              <img src="<?php print_r( $image['img'])?>" alt="">
                              <a href="javascript:void(0);" class="uploaded-photo-del" id="<?php echo $image['id']?>" alt="<?php echo $image['filename']?>">&times;</a>
<!--                              <a href="<?php //print_r($image['trumb']) ?>" class="img-modal-btn bottom-image" >Предпросмотр</a>-->
                            </div>
                          </div>
                          <?php
                        }
                      }
                  ?>

                </div>
            </form>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="uploaded-photos">
          <div class="uploaded-photos-container">




        </div>
        <!-- /.uploaded-photos -->
            <div class="row fileupload-buttonbar" style='margin-bottom: 60px;text-align: center;'>
        <div class="col-lg-12 ">
            <!-- The fileinput-button span is used to style the file input field as button -->
            <span class="btn btn-success fileinput-button upload-btn " id="bt1">
                <i class="glyphicon glyphicon-plus"></i>
                <span >Загрузить с компьютера</span>
                <input type="file" name="files[]" multiple id="csend"  accept="image" required="required" class="newimage">
            </span>
            <a href="#social" class="fancybox"><button class="upload-btn btn ">Загрузить по ссылке</button></a>

            <?php if ($status != 10)
            { ?>
                <a href="/personal/order/<?php if (isset($_GET['edit']) && ($_GET['edit'] == 1))
                {
                    echo '?id=' . $_GET['id'] . '&edit=1';
                } else
                {
                    echo '?id=' . $order_id;
                } ?>" class="print-parameters-btn btn" >Указать параметры печати</a>
<?php
} else
{
    if ($count_upload_image == 0)
    {
        ?>

                    <h2 class="center-info-load"> Ваши файлы поставлены в очередь для загрузки </h2>

    <?php } if ($count_upload_image != 0)
    { ?>
                    <div class="center-info-load-load">
                        <h2>  Идет загрузка...</h2>
                        Загружено: <?= $count_download_image ?>  из  <?= $count_upload_image ?>
                    </div>
    <?php } ?>
<?php } ?>
        </div>
        </div>
        
        
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
</div>


<!--fancybox  download-->
<div id="social" >
  <h2>Выберите источник загрузки изображений</h2>
   <div id="yandex" class="social-link"><img src="/templates/images/Yandex.jpg"><span>Yandex Disk</span> </div>
   <div id="google" class="social-link"><img src="/templates/images/Google_drive.png"><span> Google Disk</span> </div>
   <div id="dropbox" class="social-link"><img src="/templates/images/DropBox.png"><span>DropBox</span> </div>
  <div id="vkontakte" class="social-link"><img src="/templates/images/Vkontakte.png"><span>Vkontakte </span></div>
  <div id="facebook" class="social-link"><img src="/templates/images/Facebook.png"><span>Facebook</span> </div>
   <div id="urllink" class="social-link"><img src="/templates/images/Url.png"><span>Url ссылками</span> </div>
  <!-- <p>Какоето-то сообщение или подсказка, например, что надо
авторизироваться в ВК</p>-->
  <!--
  <div id="cloud-mail-ru" class="social-link">Cloud.Mail.ru </div>
-->

<div class="social-form google">
  <h2 >Загрузка изображений из своей папки в Google Disk</h2>
  <h3>будет требоваться авторизация в google </h3>
  <form action="/modules/photosite/social-api/index.php" method="post">
    <input  type="hidden" name="social-type"   value="google" >
    <input  type="hidden" name="sourceid"   value="3" >
    <input  type="hidden" name="user_id"   value="<?php print_r($user->getid()) ?>" >
    <input  type="hidden" name="order_id"   value="<?php print_r( $order_id)?>" >

    <button type="submit" class="btn sub">Перейти к загрузке </button>
  </form>
</div >


  <div class="social-form vkontakte">
  	<h2>Загрузка изображений из своего альбома в Вконтакте</h2>
  	<h3>будет требоваться авторизация в vk.com </h3>
  	<form action="/modules/photosite/social-api/download.php" method="post">
      <input  type="hidden" name="social-type"   value="vkontakte" >
      <input  type="hidden" name="sourceid"   value="1" >
      <input  type="hidden" name="user_id"   value="<?php print_r($user->getid()) ?>" >
      <input  type="hidden" name="order_id"   value="<?php if(isset($order_id)&&!empty($order_id)) print_r( $order_id); ?>" >

  		<input required type="text" name="album_id"   value="" placeholder="введите ссылку на альбом"  class="social_input">
                 <p title='например https://vk.com/album48691518_235407090' style="display:inline;">?</p>
                </br>
  		<button type="submit" class="btn sub">Закачать фото на сервер </button>
  	</form>
  </div >

  <div class="social-form facebook">
  	<h2 >Загрузка изображений из своего альбома в Фейсбук</h2>
  	<h3>будет требоваться авторизация в www.facebook.com </h3>
  	<form action="/modules/photosite/social-api/download_facebook.php" method="post">
      <input  type="hidden" name="social-type"   value="facebook" >
      <input  type="hidden" name="sourceid"   value="2" >
      <input  type="hidden" name="user_id"   value="<?php print_r($user->getid()) ?>" >
      <input  type="hidden" name="order_id"   value="<?php print_r( $order_id )?>" >
  		<input required type="text" name="album_id"   value="" placeholder="введите ID альбома" class="social_input">
                <p title='например тут https://www.facebook.com/media/set/?set=a.383279938403091.90967.383141721750246&type=3, ID = 383279938403091' style="display:inline;">?</p><br/>
<!--  			например тут https://www.facebook.com/media/set/?set=a.383279938403091.90967.383141721750246&type=3, ID = 383279938403091 </br></br>-->
  		<button type="submit" class="btn sub">Закачать фото на сервер </button>
  	</form>
  </div>

  <div class="social-form urllink">
  	<h2 >Загрузка изображений с URL по прямой ссылке</h2>
  		<form action="/modules/photosite/social-api/download_link.php" method="post">
        <input  type="hidden" name="social-type"   value="urllinkx" >
        <input  type="hidden" name="sourceid"   value="5" >
        <input  type="hidden" name="user_id"   value="<?php print_r($user->getid()) ?>" >
        <input  type="hidden" name="order_id"   value="<?php print_r( $order_id)?>" >
      <input  type="hidden" name="social-type"   value="urllink" >
  		<input required type="url" name="album_id[]"   value="" placeholder="введите ссылку на файл" class="social_input">
                <p title='например http://www.cruzo.net/user/images/k/prv/d760fbd8f50d9b92dc054ee8390df166_617.jpg' style="display:inline;">?</p>
                <br/><br/>
  		<div class="html" class="btn sub" > </div>

  		<br/> <button class="add"> + Добавить еще одну ссылку</button><br/><br/>
  		<button type="submit" class="btn sub">ЗАКАЧАТЬ </button>
  	</form>
  </div>


  <div class="social-form yandex">
  	<h2 class="social_text">Загрузка изображений из своей папки в Yandex Disc</h2>
  	<h3>будет требоваться авторизация в Yandex </h3>
  	<form action="/modules/photosite/social-api/download_yandex.php" method="post">
      <input  type="hidden" name="social-type"   value="yandex" >
      <input  type="hidden" name="sourceid"   value="4" >
      <input  type="hidden" name="user_id"   value="<?php print_r($user->getid()) ?>" >
      <input  type="hidden" name="order_id"   value="<?php print_r($order_id) ?>" >
  		<input required type="text" name="album_id"   value="" placeholder="введите название папки" class="social_input"> <p title='например "album2016" (в браузерe выглядит так https://disk.yandex.ua/client/disk/album2016)' style="display:inline;">?</p></br>
                
<!--      <span style="font-size: 12px; line-height: 1.0;">
  			например "album2016" (в браузерe выглядит так https://disk.yandex.ua/client/disk/album2016)
                        
      </span>-->
      </br>
  		<button type="submit" class="btn sub">Закачать фото на сервер </button>
  	</form>
  </div>






  <div class="social-form dropbox">
  	<h2 class="social_text">Загрузка изображений из своей папки в DropBox</h2>
  	<h3>будет требоваться авторизация в DropBox </h3>
  	<form action="/modules/photosite/social-api/download_dropbox.php" method="post">
      <input  type="hidden" name="social-type"   value="dropbox" >
      <input  type="hidden" name="sourceid"   value="7" >
      <input  type="hidden" name="user_id"   value="<?php print_r($user->getid()) ?>" >
      <input  type="hidden" name="order_id"   value="<?php print_r( $order_id)?>" >
  		<input required type="text" name="album_id"   value="" placeholder="введите название альбома (папки)" class="social_input">
                <p title='например album (полный путь https://www.dropbox.com/home/album)' style="display:inline;">?</p>
                </br>
<!--  			например album (полный путь https://www.dropbox.com/home/album)</br></br>-->
  		<button type="submit" class="btn sub">Закачать фото на сервер </button>
  	</form>
  </div>

<!--
  <div class="social-form cloud-mail-ru"  >
  	<h2>Загрузка изображений из своей папки в cloud.mail.ru</h2>
  	<h3>будет требоваться авторизация в cloud.mail.ru </h3>
  	<form action="/modules/photosite/social-api/download_mailru.php" method="post">
      <input  type="hidden" name="social-type"   value="mailru" >
      <input  type="hidden" name="sourceid"   value="6" >
      <input  type="hidden" name="user_id"   value="<?php print_r($user->getid()) ?>" >
      <input  type="hidden" name="order_id"   value="<?php print_r( $order_id['0']['id'])?>" >
  		<input required type="text" name="album_id"   value="" placeholder="введите ссылку на альбом" class="social_input"></br>
  			например https://cloud.mail.ru/home/albom/</br>
        папке должен быть дан общий доступ для serhiofirst@mail.ru. После загрузки Вам придет уведомление и тогда можно будет убрать доступ и удалить папку.
      </br>
  		<button type="submit">Закачать фото на сервер </button>
  	</form>
  </div>
</div>

-->



</div>
</div>
<!--end fancybox download -->

<!--uploader -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>

            <strong class="error text-danger"></strong>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>  {% if (!i) { %}

         {% } %}</td>
    </tr>
{% } %}
/*
{% for (var i=0, file; file=o.files[i]; i++) { %}

  <div class="uploaded-photo upload-photo-loading">
    <div class="uploaded-photo-inner">
    {% if (file.url) { %}
    {% if (file.error) { %}
        <div><span class="label label-danger">Ошибка</span> {%=file.error%}</div>
    {% } else { %}
        <img src="{%=file.url%}" style="margin:7px 0 7px 7px;max-width:500px;width:80px;" class="img-thumbnail"  >
   {% } %}

    {% } %}
    <a href="#" class="uploaded-photo-del" id="" alt="{%=file.name%}">&times;</a>
    </div>
  </div>


{% } %}
*/
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}

<div class="uploaded-photo">
  <div class="uploaded-photo-inner">
  {% if (file.url) { %}
  {% if (file.error) { %}
      <div><span class="label label-danger">Ошибка</span> {%=file.error%}</div>
  {% } else { %}
<!--      <img src="/files/tmp/<?php //echo $token?>/{%=file.name%}" style="margin:0px 0 0px 0px;max-width:500px;" class="img-thumbnail"  >-->
 <img src="{%=file.url%} " style="margin:0px 0 0px 0px;max-width:500px;" class="img-thumbnail"  >
 {% } %}

  {% } %}
  <a href="javascript:void(0);" class="uploaded-photo-del" id="" alt="{%=file.name%}">&times;</a>
  </div>
</div>










{% } %}
</script>
</body>
