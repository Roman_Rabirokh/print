	<?php if(!defined("_APP_START")) { exit(); }



	?>

	<div class="spanning">
		<div class="delivery-content" itemscope itemtype="http://schema.org/WebPage">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ol class="breadcrumb" itemprop="breadcrumb">
							<li><a href="/" itemprop="url">Главная</a></li>
							<li><a href="/personal/" itemprop="url">Мой кабинет</a></li>
							<li class="active">Написать отзыв</li>
						</ol>
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
		    <div class="row">
		      <div class="col-md-12">
		      <?php include('includes/profile-menu.php'); ?>
		      </div>
		      <!-- /.col -->
		    </div>
						<?php $this->includeComponent('photosite/callback.block');?>
			</div>
			<!-- /.container -->
		</div>
		<!-- /.delivery-content -->

	</div>
	</div>
	<!-- /.spanning-columns -->
</div>
<!-- /.main -->
