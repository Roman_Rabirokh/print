	<?php if(!defined("_APP_START")) { exit(); }

	if(!isset($_GET['id'])) {  redirect('/404'); exit(); }

	?>

	<div class="spanning">
		<div class="delivery-content" itemscope itemtype="http://schema.org/WebPage">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ol class="breadcrumb" itemprop="breadcrumb">
							<li><a href="/" itemprop="url">Главная</a></li>
							<li><a href="/presonal/" itemprop="url">Мой кабинет</a></li>
							<li class="active">Новый заказ
								<?php
										if (isset($_GET['id'])&&!empty($_GET['id']))
											{
												echo "№ ".$_GET['id'];
											}
								?>
							</li>
						</ol>
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
		    <div class="row">
		      <div class="col-md-12">
		      <?php include('includes/profile-menu.php'); ?>
		      </div>
		      <!-- /.col -->
		    </div>
						<?php $this->includeComponent('photosite/delivery.block');?>
			</div>
			<!-- /.container -->
		</div>
		<!-- /.delivery-content -->

	</div>
	</div>
	<!-- /.spanning-columns -->
</div>
<!-- /.main -->
