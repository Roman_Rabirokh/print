<?php if(!defined("_APP_START")) { exit(); } ?>
<?php if(!$page->notFound) {?>
<div class="section-contact<?php if(!$page->isIndex) {  echo ' hide-before';  } ?>">
  <div class="container">
    <div class="row">
      <div class="col-xl-5 col-lg-6 col-md-12">
        <div class="section-contact-info" itemscope itemtype="http://schema.org/Organization">
          <div class="section-contact-icon"><img src="<?php echo $page-> getTemplateUrl(); ?>images/icons/icon-section-contact.png" alt="" itemprop="logo"></div>
          <div class="section-contact-phones" itemprop="name">
            <div class="section-contact-phone" itemprop="telephone">+38 (050) 728 76 54</div>
            <div class="section-contact-phone" itemprop="telephone">+38 (067) 938 71 09</div>
          </div>
          <div class="section-contact-address" itemprop="address">г. Одесса, ул. Мечникова, д.108</div>
              <?php if ($user->Authorized())
              {
                  ?>
                  <a href="/personal/new-order" class="section-contact-btn btn" itemprop="/personal/new-order">Заказать печать</a>
              <?php } else
              {
                  ?>
                  <a href="#" class="section-contact-btn btn PrintNoReg">Заказать печать</a>
    <?php } ?>


            </div>
      </div>

      <!-- /.col   КАРТА ГУГЛ-->
      <div class="col-xl-7 col-lg-6 col-md-12 clearfix">
        <div id="map" class="map" itemscope itemtype="http://schema.org/Map"></div>
      </div>
      <!-- /.col -->



    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
</div>
<!-- /.section-contact -->
</div>
<!-- /.spanning-columns -->
</div>
<footer class="page-footer" itemscope itemtype="http://schema.org/WPFooter">>
  <div class="page-footer-content">
    <div class="container">
      <div class="row">
        <div class="col-lg-offset-4 col-lg-4 col-md-8 col-sm-7 col-xs-12">
          <div class="page-footer-copyright" itemprop="text">&copy; Все права защищены | <span itemprop="copyrightYear">2017</span></div>
        </div>
        <!-- /.col -->
        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
          <div class="scl-block clearfix">
            <ul>
              <li><a href="#" class="scl-icon scl-icon-inst" itemprop="url">inst</a></li>
              <li><a href="#" class="scl-icon scl-icon-vk" itemprop="url">vk</a></li>
              <li><a href="#" class="scl-icon scl-icon-fb" itemprop="url">fb</a></li>
              <li><a href="#" class="scl-icon scl-icon-gp" itemprop="url">gp</a></li>
              <li><a href="#" class="scl-icon scl-icon-tw" itemprop="url">tw</a></li>
            </ul>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.page-footer-content -->
</footer>
<!-- /.page-footer -->
<?php } ?>
<!--MASK plugin -->
<script type="text/javascript" src="<?php echo $page-> getTemplateUrl(); ?>js/jquery.maskedinput.min"></script>

<script src="<?php echo $page-> getTemplateUrl(); ?>js/library.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCH7oNMIDrv8AETiAkVBXny1XTmPoCZ-3s" defer></script>
<script src="<?php echo $page-> getTemplateUrl(); ?>js/script.js"></script>
<!--FancyBox -->
<script type="text/javascript" src="<?php echo $page-> getTemplateUrl(); ?>js/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?php echo $page-> getTemplateUrl(); ?>js/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?php echo $page-> getTemplateUrl(); ?>js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript" src="<?php echo $page-> getTemplateUrl(); ?>js/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script type="text/javascript" src="<?php echo $page-> getTemplateUrl(); ?>js/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>




<?php $page->getFooter(); ?>
</body>
</html>
