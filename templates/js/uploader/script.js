  $(document).ready(function()
  {

/*
    tinymce.init({
    selector: '.text-edit',  // change this value according to your HTML
    plugins: "emoticons",
    toolbar: "emoticons",
    toolbar: 'undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | removeformat | emoticons ',
    menu: {
      // file: {title: 'File', items: 'newdocument'},
      // edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
      // insert: {title: 'Insert', items: 'link media | template hr'},
      // view: {title: 'View', items: 'visualaid'},
      // format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
      // table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
      // tools: {title: 'Tools', items: 'spellchecker code'}
    }
  });
*/

    $('.box__dragndrop').append('.<br><span class="box__dragndrop"> До 10 фото</span>');

    document.count_img = '';
    //Drag&Drop
    var isAdvancedUpload = function()
    {
      var div = document.createElement('div');
      return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
    }();

    var $form = $('.box');

    if(isAdvancedUpload)
    {
      $form.addClass('has-advanced-upload');
    }

    if (isAdvancedUpload)
    {
      var droppedFiles = false;

      $form.on('dragover dragenter', function() {
        $form.addClass('is-dragover');
      })
      .on('dragleave dragend drop', function() {
        $form.removeClass('is-dragover');
      })
      .on('drop', function(e) {
        droppedFiles = e.originalEvent.dataTransfer.files;
      });
    }

    var $input    = $form.find('input[type="file"]'),
        $label    = $form.find('label'),
        showFiles = function(files) {

          $label.text(files.length > 1 ? ($input.attr('data-multiple-caption') || '').replace( '{count}', files.length ) : files[ 0 ].name);
        };

    $input.on('change', function(e) {

      document.count_img = e.target.files.length;

      count = 0;
      var arr = [];
      $(e.target.files).each(function(){

       if(count < 10)
       {
         arr.push(e.target.files[count]);
       }
       count++;
      });


    showFiles(arr);


      if(document.count_img > 1)
      {
          //  $('.has-advanced-upload').find('label').text(document.count_img + ' Файла выбрано');
          $('.has-advanced-upload').find('label').text('');
      }
    });

     $('#brand').keyup(function()
     {
       $('#brand').attr('name', 'brand');
       $('#brandId').removeAttr('name');
       $('#brandId').val('');
     });
/*
    $( "#brand" ).autocomplete({
          source: function( request, response ) {
            $.ajax({
              type: "POST",
              url: "/personal/brands/",
              dataType: "json",
              data: {
                q: request.term
              },
              success : function (data) {
                 response($.map(data, function (item) {
                  $('#ui-id-1').css('background-color', '#fff');
                  $('#ui-id-1').css('height', '100%');
                   return {
                    label : item.name,
                    value : item.name,
                    id : item.id
                   }
                  }));
                }
            });
          },
          minLength: 1,
          select: function( event, ui ) {
              $('#brand').removeAttr('name');
              $('#brandId').val(ui.item.id);
              $('#brandId').attr('name', 'brand_id')
          },
    });

*/

    $('#add_link').on('click', function()
    {
      $('#links_youtube').append('<tr><td style="width:413px;"><input style="width:413px;" class="input-text" onblur="showLink(this)" name="links[]" value="" /></td><td><button style="margin-left:-9px;margin-top:5px;" class="button del-link" onclick="delInput(this)" type="button">Удалить</button></td><td></td></tr>');
    });

    $('#action').on('click', function()
    {
      $('#action_info').toggle();
    });
/*
    initAutocomplete();
    var autocomplete;
*/

    $("#file").on("change", handleFileSelect);

        selDiv = $("#imgPreview");
        $("#form-ad").on("submit", handleForm);

        $("body").on("click", ".selFile", removeFile);

		 $("#imgPreview").css('position','absolute');
		$("#imgPreview").css('left','60px');
		 $("#imgPreview").css('top','25px');
		 $("#imgPreview").css('z-index','2500');

  });

  function setAttrName(obj)
  {
    $('[name="category"]').removeAttr('name');
    $(obj).attr('name', 'category');
  }

  function setNameAttr(obj)
  {
    $(obj).attr('name', 'ad_images[]');
  }



  function delInput(obj)
  {
    var parentTD = $(obj).parent();
    parentTD.parent().remove();
  }

  function adInputFile()
  {
    $('#ad_images').append('<tr><td><input type="file" onclick="setNameAttr(this);" class="input-text" /></td><td><button class="button del-link" onclick="delInput(this)" type="button">Удалить</button></td></tr>');
  }

/*

  function initAutocomplete()
  {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    var options = {
   	              types: ['(cities)'],
   	              componentRestrictions: {country: countryLocation}
  	             };

    autocomplete = new google.maps.places.Autocomplete((document.getElementById('cityName')),options,{types: ['geocode']});

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
  }
*/
  function fillInAddress()
  {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();
    var cityId = place.id;
    var cityName = place.name;

    $.post('/cities/',{regionId: countryId, city: cityName, city_id: cityId },function(data)
    {
      $('#cityId').val(data);
    },'text');

    document.getElementById('cityName').value = cityName;
  }

  $('#save').on('click', function()
  {
    var description = tinyMCE.get('description').getContent();
    if(description != '')
    {
      $('#description-error').text('');
      document.forms.new-ad.submit();
    }
    else
    {
      $('#description-error').text('Заполните поле.');
      $('html,body').animate({scrollTop: $('#description-error').offset().top-300});
    }
  });

  //Load previews uploades images
  // function readURL(input)
  // {
  //   var images = input.files;
  //   var n = 0;
  //   for(var i = 0; i < images.length; i++)
  //   {
  //     var reader = new FileReader();
  //
  //     reader.onload = function (e)
  //     {
  //
  //
  //       $("#imgPreview").append('<div><img style="height:120px;width:200px;" src="'+ e.target.result +'" />&nbsp;<a href="javascript:void(0);" onclick="delUploadImage('+ n +');" >x</a></div>');
  //       n++;
  //     }
  //
  //     reader.readAsDataURL(input.files[i]);
  //
  //   }
  // }
  //
  //  $('#file').change(function () {
  //      readURL(this);
  //  });
  //
  //  function delUploadImage(n)
  //  {
  //    var FilesUpload = $('#file');
  //    FilesUpload.get(0).files;
  //  }

  var selDiv = "";
  var storedFiles = [];


    function handleFileSelect(e) {
        var files = e.target.files;
        var filesArr = Array.prototype.slice.call(files);
        var count = 0;
        filesArr.forEach(function(f) {

          count++;
           if(count > 10)
           {
              return false;
           }

            if(!f.type.match("image.*")) {
                return;
            }
            storedFiles.push(f);

            var reader = new FileReader();
            reader.onload = function (e) {
                var html = "<div style='float:left;position:relative;margin-right:15px;margin-bottom:15px;'><a class='selFile' data-file='"+f.name+"' href='javascript:void(0);' style='position:absolute;right:-10px;top:-10px;color:#fff;background-color:red;line-height:20px;border-radius:50%;width:20px;height:20px;text-align:center;'><i class='fa fa-times' aria-hidden='true'></i></a><img style='height:70px;' src=\"" + e.target.result + "\"   title='Click to remove'></div>";
                selDiv.append(html);

            }
            reader.readAsDataURL(f);
        });

        for (key in storedFiles){
    e.target.files[key] = storedFiles[key];

  }
  console.log(e.target.files);

    }

    function handleForm(e)
    {
      // e.preventDefault();
    /*  var data = new FormData();


      for(var i=0, len=storedFiles.length; i<len; i++)
      {
        data.append('files', storedFiles[i]);
      }

      var xhr = new XMLHttpRequest();
      xhr.open('POST', '/new/ad/', true);
      xhr.onload = function(e)
      {
        if(this.status == 200)
        {
          //console.log(e.currentTarget.responseText);
          //alert(e.currentTarget.responseText + ' items uploaded.');
        }
      }

      xhr.send(data);*/
    }

    function removeFile(e)
    {
      var file = $(this).data("file");

      for(var i=0;i<storedFiles.length;i++)
      {
        if(storedFiles[i].name === file)
        {
          storedFiles.splice(i,1);
          break;
        }
      }

      $(this).parent().remove();

    }

    $(function () {
        'use strict';

        // Initialize the jQuery File Upload widget:
        $('#fileupload').fileupload({




            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
          //  url: '/item/image/upload/eslibmishkibylipchelami137_57e2267936551/',
            url: '/item/image/upload/' + token + '/',

            autoUpload: true
        });

        // Enable iframe cross-domain access via redirect option:
        $('#fileupload').fileupload(
            'option',
            'redirect',
            window.location.href.replace(
                /\/[^\/]*$/,
                '/cors/result.html?%s'
            )
        );

        if (window.location.hostname === 'https://'+document.location.host+'/') {
            // Demo settings:
            $('#fileupload').fileupload('option', {
                url: 'https://'+document.location.host+'/personal/new-order/',
                // Enable image resizing, except for Android and Opera,
                // which actually support image resizing, but fail to
                // send Blob objects via XHR requests:
                disableImageResize: /Android(?!.*Chrome)|Opera/
                    .test(window.navigator.userAgent),
                maxFileSize: 999000,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
            });
            // Upload server status check for browsers with CORS support:
            if ($.support.cors) {
                $.ajax({
                    url: 'https://'+document.location.host+'/personal/new-order/',
                    type: 'HEAD'
                }).fail(function () {
                    $('<div class="alert alert-danger"/>')
                        .text('Upload server currently unavailable - ' +
                                new Date())
                        .appendTo('#fileupload');
                });
            }
        } else {
              // загрузка файлов из временной папки на страничку загрузки фото
            // Load existing files:
            $('#fileupload').addClass('fileupload-processing');
            $.ajax({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: $('#fileupload').fileupload('option', 'url'),
                dataType: 'json',
                context: $('#fileupload')[0]
            }).always(function () {
                $(this).removeClass('fileupload-processing');
            }).done(function (result) {


                $(this).fileupload('option', 'done')
                    .call(this, $.Event('done'), {result: result});
            });
        }

    });
