  <?php if(!defined("_APP_START")) { exit(); }
  $formID = uniqid();
  $token = $this->createFormToken("na");
  ?>
  <script>var token = '<?php echo   $token; ?>';</script>
<div  class="container"><div class="row"><div class="col-md-12" >
  <iframe style="width:100%;height:150px;display:none;" name="<?php echo $formID; ?>" ></iframe>
  <?/* <form enctype="multipart/form-data" method="post" id="form-ad" name="new-ad" target="<?php echo $formID; ?>" action="/new/ad/" >*/?>
     <fieldset>
       <input type="hidden" name="token" value="<?php echo   $token; ?>" />
       <ul class="form-list field-list" >
         <li>
          <div class="input-box" >
                 <div id="registerMessages"></div>
           <table style="width:100%;">
            <tr>
              <td>
                <label for="name" class="required" >Заголовок<em>*</em></label>
              </td>
              <td>
                <input type="text" required name="name" id="name" class="input-text" value="" />
              </td>
             </tr>
            </table>
           </div>
         </li>
         <li>
           <div class="input-box" >
             <table style="width:100%;">
               <tr>
                 <td>
                   <label for="category" class="required" >Рубрика<em>*</em></label>
                 </td>
                 <td>
                   <div id="categoryNew" >
                       <select name="category" onclick="setAttrName(this);" required onchange="getSubCategory(this);" >
                         <option value="0" >Выберите рубрику</option>
                         <?php
                         foreach($data['CATEGORIES_ADS'] as $category_ad)
                         { ?>
                           <option value="<?php echo $category_ad['id'] ?>"><?php echo $category_ad['name']; ?></option><?php
                         } ?>
                       </select>
                   </div>
                 </td>
               </tr>
             </table>
           </div>
         </li>
         <li>
           <div class="input-box" >
             <table style="width:100%;">
               <tr>
                 <td>
                   <label for="brand" >Бренд</label>
                 </td>
                 <td>
                   <input value="" id="brand" name="brand" class="input-text" placeholder="Введите первые буквы" /><br />
                   <input id="brandId" type="hidden" value="" />
                 </td>
               </tr>
             </table>
           </div>
         </li>
         <li>
           <div class="input-box" >
             <table style="width:100%;">
               <tr>
                 <td>
                   <label for="option_price" class="required" >Цена<em>*</em></label>
                 </td>
                 <td>
                   <select id="price_type" required name ="option_price">
                     <option value="0">Выберите вариант</option><?php
                     foreach ($data['OPTION_PRICE'] as $value)
                     { ?>
                        <option value="<?php echo $value['id']; ?>" ><?php echo $value['option']; ?></option><?php
                      } ?>
                   </select>
                    <input type="text" name="price" class="input-text" value="" />
                    <select name="currency" style="width:110px !important;">
                      <?php foreach($data['CURRENCY'] as $currency)
                      { ?>
                      <option value="<?php echo $currency['id']; ?>" ><?php echo $currency['name'] ?></option><?php
                      } ?>
                    </select>
                 </td>
               </tr>
             </table>
           </div>
         </li>

         <li>
           <div class="input-box" >
             <table style="width:100%;">
               <tr>
                 <td>
                   <label for="ad_images[]" class="required" >Фото</label>
                 </td>
                 <td>
                   <form id="fileupload" action="/item/image/upload/<?php echo   $token; ?>/" method="POST" enctype="multipart/form-data">
                         <!-- Redirect browsers with JavaScript disabled to the origin page -->
                         <noscript><input type="hidden" name="redirect" value="/noscript/"></noscript>
                         <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                         <div class="row fileupload-buttonbar">
                             <div class="col-lg-7 ">
                                 <!-- The fileinput-button span is used to style the file input field as button -->
                                 <span class="btn btn-success fileinput-button">
                                     <i class="glyphicon glyphicon-plus"></i>
                                     <span>Добавить файлы...</span>
                                     <input type="file" name="files[]" multiple>
                                 </span>

                             </div>
                             <!-- The global progress state -->
                             <div class="col-lg-5 fileupload-progress fade ">
                                 <!-- The global progress bar -->
                                 <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                     <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                 </div>
                                 <!-- The extended global progress state -->
                                  <?/*  <span class="fileupload-process"></span>*/?>
                                 <?/*<div class="progress-extended">&nbsp;</div>*/?>
                             </div>
                         </div>
                         <!-- The table listing the files available for upload/download -->
                         <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
                     </form>
                 </td>
               </tr>
             </table>
           </div>
         </li>
         <!-- <li>
           <div class="input-box" >
             <table style="width:100%;">
               <tr>
                 <td>
                   <label for="ad_images[]" class="required" >Fotos</label>
                 </td>
                 <td>
                   <div id="load_img">
                    <table id="ad_images" >
                     <tr>
                        <td>
                          <input type="hidden" name="fotos" value="" />
                          <div class="dropzone" id="manyFotos" style="text-align:left;padding:15px 0 15px 15px;width:643px;min-height:200px;height:100%;border:2px dotted #f8c53c;border-radius:47px;text-aligh:center !important;vertical-align:center !important;" ></div>
                        </td>
                        <td>
                        </td>
                     </tr>
                   </table>
                 </td>
               </tr>
             </table>
           </div>
         </li> -->
         <li>
           <div class="input-box" >
             <table style="width:100%;">
               <tr>
                 <td>
                   <label for="link" class="required" >Видео Youtube</label>
                 </td>
                 <td>
                   <table id="links_youtube">
                  </table>
                   <button id="add_link" class="button" type="button">Добавить ссылку</button>
                 </td>
               </tr>
             </table>
           </div>
         </li>
         <li>
           <div class="input-box" >
             <table style="width:100%;">
               <tr>
                 <td>
                   <label for="name" class="required" >Состояние<em>*</em></label>
                 </td>
                 <td>
                   <select required name="state" >
                     <option value="0">Выберите</option><?php
                     foreach($data['PRODUCT_STATE'] as $item)
                     { ?>
                       <option value="<?php echo $item['id'] ?>" ><?php echo $item['state']; ?></option><?php
                     } ?>
                   </select>
                 </td>
               </tr>
             </table>
           </div>
         </li>
         <li>
           <div class="input-box" >
             <table style="width:100%;">
               <tr>
                 <td>
                   <label>Срок размещения<em>*</em></label>
                 </td>
                 <td>
                   <select name="period" required >
                     <option value="0">Выберите</option><?php
                     foreach($data['AD_TIME'] as $item)
                     { ?>
                       <option value="<?php echo $item['id'] ?>"  ><?php echo $item['time']; ?></option><?php
                     } ?>
                   </select>
                 </td>
               </tr>
             </table>
           </div>
         </li>
         <li id="detailText">
           <div class="input-box" >
             <table style="width:100%;">
               <tr>
                 <td>
                   <label for="detail_text" class="required" >Описание<em>*</em></label>
                 </td>
                 <td>
                   <textarea name="detail_text" class="text-edit" id="description"><?php echo (!empty($data['ITEM']['detail_text']) ? ($data['ITEM']['detail_text']) : '' ); ?></textarea>
                   <p style="color: red" id="description-error" ></p>
                 </td>

               </tr>
             </table>
           </div>
         </li>
         <li>
            <div class="input-box" >
              <table style="width:100%;">
                <tr>
                  <td>
                    <label  class="required" >Оплата&nbsp;&sol; Доставка</label>
                  </td>
                  <td>
                   <p onclick="showDelivery(this);" style="padding-top:18px; cursor:pointer;">Редактировать</p>
                  </td>
                </tr>
              </table>
            </div>
         </li>
         <li style="display: none;" id="delivery">
            <div class="input-box" >
              <table style="width:100%;">
                <tr>
                  <td>
                    <label  class="required" >Оплата&nbsp;&sol; Доставка</label>
                  </td>
                  <td>
                    <textarea name="pay_delivery" class="text-edit" ></textarea>
                  </td>
                </tr>
              </table>
            </div>
         </li>
         <li>
            <div class="input-box" >
              <table style="width:100%;">
                <tr>
                  <td>
                    <label  class="required" >Возврат</label>
                  </td>
                  <td>
                   <p onclick="showReturn(this);" style="padding-top:10px; cursor:pointer;">Редактировать</p>
                  </td>
                </tr>
              </table>
            </div>
         </li>
         <li style="display:none;" id="return">
            <div class="input-box" >
              <table style="width:100%;">
                <tr>
                  <td>
                    <label class="required" >Возврат</label>
                  </td>
                  <td>
                    <textarea name="return" class="text-edit" ></textarea>
                  </td>
                </tr>
             </table>
            </div>
         </li>
         <li>
            <div class="input-box" >
              <table style="width:100%;">
                <tr>
                  <td>
                    <label  class="required" >Гарантия</label>
                  </td>
                  <td>
                   <p onclick="showGuarantee(this);" style="padding-top:10px; cursor:pointer;">Редактировать</p>
                  </td>
                </tr>
              </table>
            </div>
         </li>
         <li style="display: none;" id="guarantee">
           <div class="input-box" >
             <table style="width:100%;">
               <tr>
                 <td>
                   <label class="required" >Гарантия</label>
                 </td>
                 <td>
                   <textarea name="guarantee" class="text-edit" ><?php echo (!empty($data['ITEM']['guarante']) ? ($data['ITEM']['guarante']) : '' ); ?></textarea>
                 </td>
               </tr>
             </table>
           </div>
         </li>
         <li>
           <div class="input-box" >
             <table style="width:100%;">
               <tr>
                 <td>
                   <label for="country" class="required" >Страна<em>*</em></label>
                 </td>
                 <td>
                   <input type="hidden" class="input-text required-entry" name="countryid" id="countryid" value="<?php if(isset($_SESSION['SITE_COUNTRY']['ID'])) echo $_SESSION['SITE_COUNTRY']['ID']; else echo '0'; ?>" disabled/>
                   <input type="text" class="input-text required-entry" name="country" id="country" value="<?php echo $data['COUNTRY']; ?>" disabled/>
                 </td>
               </tr>
             </table>
           </div>
         </li>
         <li>
           <div class="input-box" >
             <table style="width:100%;">
               <tr>
                 <td>
                   <label for="place" class="required" >Город<em>*</em></label>
                 </td>
                 <td>
                   <input type="hidden" name="city_id" id="cityId" class="input-text" value="" />
                   <input type="text" name="city_name" id="cityName" class="input-text" value="" />
                 </td>
               </tr>
             </table>
           </div>
         </li>
         <!-- <li>
           <div class="input-box" >
             <table style="width:100%;">
               <tr>
                 <td>
                   <label for="address" class="required" >Адрес<em>*</em></label>
                 </td>
                 <td>
                   <input type="text" name="address" id="address" class="input-text" value="" />
                 </td>
               </tr>
             </table>
           </div>
         </li> -->
         <li>
           <div class="input-box" >
             <table style="width:100%;">
               <tr>
                 <td>
                    <label for="contact" class="required" >Контактное лицо<em>*</em></label>
                  </td>
                  <td>
                    <input type="text" name="contact" id="contact" class="input-text" value="" />
                  </td>
                </tr>
              </table>
            </div>
         </li>
         <li>
           <div class="input-box" >
             <table style="width:100%;">
               <tr>
                 <td>
                   <label for="email" class="required" >E-mail<em>*</em></label>
                 </td>
                 <td>
                    <input type="email" name="email" id="email" class="input-text" value="" />
                    <div id="email-exist" style="color:red;" ></div>
                 </td>
                </tr>
              </table>
            </div>
          </li>
          <li>
            <div class="input-box" >
              <table style="width:100%;">
                <tr>
                  <td>
                    <label for="phone" class="required" >Телефон<em>*</em></label>
                  </td>
                  <td>
                    <input type="tel" name="phone" id="phone" class="input-text" value="" />
                    <input type="checkbox" name="viber" />&nbsp;Viber
                  </td>
                </tr>
              </table>
            </div>
         </li>
         <li>
           <div class="input-box" >
             <table style="width:100%;">
               <tr>
                 <td>
                   <label for="skype" >Skype</label>
                 </td>
                 <td>
                   <input type="text" name="skype" id="skype" class="input-text" value="" />
                   <div id="fotos"></div>
                 </td>
               </tr>
             </table>
           </div>
         </li>
         <li>
           <div class="input-box" >
             <table style="width:100%;">
               <tr>
                 <td>
                   <label for="place" class="required" ></label>
                 </td>
                 <td>
                  <div class="field">
                    <div style="width:100px;" id="captcha2" class="g-recaptcha" ></div>
                    <?php //$this->includeComponent("system/captcha",array("SHOW"=>TRUE));?><br />
                  </div>
                  <!-- <div class="field">
                  <label style="padding-left: 4px;" class="required" >Ведите код<em style="color: red;">*</em></label><br />
                  <input style="margin-left: 19px;" type="number" maxlength="6" name="code"  class="input-text required-entry"   id="tbSRCode"  />
                  <div id="wrong-captcha" style="color:red; padding-left:18px;"></div>
                  </div> -->
                 </td>
               </tr>
             </table>
           </div>
         </li>
         <li>
           <div class="input-box" >
             <table style="width:100%;">
               <tr>
                 <td>
&nbsp;
                 </td>
                 <td>
                   <div class="buttons-set">
                    <button id="sendNewAd"   class="button" >
                      <span>
                        <span>Сохранить</span>
                      </span>
                    </button>
                  </div>

                 </td>
               </tr>
             </table>
           </div>

         </li>
       </ul>
     </fieldset>
   <?/*</form>*/?>
 </div> </div> </div>
   <script id="template-upload" type="text/x-tmpl">
   {% for (var i=0, file; file=o.files[i]; i++) { %}
       <tr class="template-upload fade">
           <td>
               <span class="preview"></span>
           </td>
           <td>
                 <p class="size">Загрузка...</p>
               <strong class="error text-danger"></strong>
               <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
           </td>
           <td>  {% if (!i) { %}
                <button class="btn btn-warning cancel">
                                    <span>Отмена</span>
                </button>
            {% } %}</td>
       </tr>
   {% } %}
   </script>
   <!-- The template to display files available for download -->
   <script id="template-download" type="text/x-tmpl">
   {% for (var i=0, file; file=o.files[i]; i++) { %}
       <tr class="template-download fade">
           <td style="text-align:left;">
               <span class="preview">
                   {% if (file.thumbnailUrl) { %}
                       <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}" style="margin:7px 0 7px 7px;max-width:500px;width:80px;" class="img-thumbnail"  ></a>
                   {% } %}
               </span>
           </td>
           <td>
           <span class="size">{%=o.formatFileSize(file.size)%}</span>
               {% if (file.error) { %}
                   <div><span class="label label-danger">Ошибка</span> {%=file.error%}</div>
               {% } else { %}
              {% } %}
           </td>

           <td>
               {% if (file.deleteUrl) { %}
                   <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                       <span>Удалить</span>
                   </button>
               {% } else { %}
                   <button class="btn btn-warning cancel">
                       <i class="glyphicon glyphicon-ban-circle"></i>
                       <span>Отмена</span>
                   </button>
               {% } %}
           </td>
       </tr>
   {% } %}
   </script>
