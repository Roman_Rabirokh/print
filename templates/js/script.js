$(document).ready(function () {
    /* параметры для всех фотографий */
    /* Кнопка и textarea*/
    $("#font_b_all").click(function () {
        if ($(this).hasClass("active")) {
            $(".photo-parameter-textarea").css("font-weight", "normal");
        } else {
            $(".photo-parameter-textarea").css("font-weight", "bolder");
        }
    });
    $("#font_i_all").click(function () {
        if ($(this).hasClass("active")) {
            $(".photo-parameter-textarea").css("font-style", "normal");
        } else {
            $(".photo-parameter-textarea").css("font-style", "italic");
        }
    });
    $("#font_u_all").click(function () {
        if ($(this).hasClass("active")) {
            $(".photo-parameter-textarea").css("text-decoration", "none");

        } else {
            $(".photo-parameter-textarea").css("text-decoration", "underline");
        }
    });
    /* конец */
    /* изменения размера */


    $('.paper_size_select').change(function()
    {
        var size=$(this).find('option:selected').text();
        var id= $(this).attr('for');
        height=$('#height-'+id).html();
        height=height.split('px');
        width=$('#width-'+id).html();
        width=width.split('px');
        size=size.split('x');
        var dpi=DPI(width[0],height[0],size[0],size[1]);
        $('#image-dpi-'+id).html(dpi);
        if(dpi<150)
        {
            $('#yellow-block-'+id).show();
            $('#yellow-block-'+id).find('span').html('Не гарантируем качество фотографии');
        }
        else if(dpi<290)
        {
            $('#yellow-block-'+id).show();
            $('#yellow-block-'+id).find('span').html('Фото не высокого качества');
        }
        else
        {
            $('#yellow-block-'+id).hide();
        }
    });
    $("#font_size_all").change(function () {
        var sizeVal = parseInt($(this).find("option:selected").text(), 10);
        var alexScript = $(this).closest(".photo-col.photo-col-2").find("#font_all");
        if (alexScript.val() == "8")
        {
            sizeVal = sizeVal * 2;
            $(".photo-parameter-textarea").css("font-size", sizeVal + "px");
        } else
        {
            $(".photo-parameter-textarea").css("font-size", sizeVal + "px");
        }
    });
    /* конец */

    /*селект со шрифтом */
    $("#font_all").change(function () {
        var sizeVal = $(this).val();
        var fontSize = $(this).closest(".photo-col.photo-col-2").find("#font_size_all :selected").text();
        fontSize = parseInt(fontSize, 10);
        if (sizeVal == "6") {
            $(".photo-parameter-textarea").css("font-family", "Arial");
            fontSize = fontSize;
            $(".photo-parameter-textarea").css("font-size", "" + fontSize + "px");
        } else if (sizeVal == "7") {
            $(".photo-parameter-textarea").css("font-family", "Times New Roman");
            fontSize = fontSize;
            $(".photo-parameter-textarea").css("font-size", "" + fontSize + "px");
        } else if (sizeVal == "8") {
             $(".photo-parameter-textarea").css("font-family", "alexandra-script");
            fontSize = fontSize * 2;
             $(".photo-parameter-textareal").css("font-size", "" + fontSize + "px");

        }
    })
        $("#input_color_text_all").change(function () {
        var sizeVal = $(this).val();
      //  console.log(sizeVal);
         $(".photo-parameter-textarea").css("color", sizeVal);
    })
     $("#paper_text_orientation_all").change(function () {
        var sizeVal = $(this).val();
        if (sizeVal == "Левый верхний") {
           $(".photo-parameter-textarea").css("text-align", "left");

        } else if (sizeVal == "Левый нижний") {
            $(".photo-parameter-textarea").css("font-family", "Times New Roman");

        } else if (sizeVal == "Правый верхний") {
            $(".photo-parameter-textarea").css("text-align", "right");
        } else if (sizeVal == "Правый нижний") {
            $(".photo-parameter-textarea").css("font-family", "Brush Script MT");
        } else if (sizeVal == "Центр") {
            $(".photo-parameter-textarea").css("text-align", "center");
        }
    });
 /*конец параметров для всех фото */
    /*тест-драйв*/
    /*  размер шрифта */
    $(".select_font_size").change(function () {
        var sizeVal = $(this).val();
        // console.log(sizeVal);
        if (sizeVal == "1") {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-size", "14px");
        } else if (sizeVal == "2") {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-size", "16px");
        } else if (sizeVal == "3") {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-size", "22px");
        }
    })
    /* конец */
    /*селект со шрифтом */
    $(".selectfontsize").change(function () {
        var sizeVal = $(this).val();
        console.log(sizeVal);
        if (sizeVal == "1") {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-family", "Arial");

        } else if (sizeVal == "2") {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-family", "Times New Roman");

        } else if (sizeVal == "3") {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-family", "alexandra-script");

        }
    })

    /*конец тест-драйва*/
    /* Кнопка и textarea*/
    $(".font-bold-btn").click(function () {
        if ($(this).hasClass("active")) {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-weight", "normal");
        } else {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-weight", "bolder");
        }
    });
    $(".font-italic-btn").click(function () {
        if ($(this).hasClass("active")) {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-style", "normal");
        } else {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-style", "italic");
        }
    });
    $(".font-underline-btn").click(function () {
        if ($(this).hasClass("active")) {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("text-decoration", "none");

        } else {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("text-decoration", "underline");
        }
    });

    /* конец */
    /* изменения размера */

    $(".select_font_size").change(function () {
        var sizeVal = parseInt($(this).find("option:selected").text(), 10);
        var alexScript = $(this).closest(".photo-col.photo-col-2").find(".select_font");
        if (alexScript.val() == "8")
        {
            sizeVal = sizeVal * 2;
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-size", sizeVal + "px");
        } else
        {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-size", sizeVal + "px");
        }
    });
    /* конец */

    /*селект со шрифтом */
    $(".select_font").change(function () {
        var sizeVal = $(this).val();
        var fontSize = $(this).closest(".photo-col.photo-col-2").find(".select_font_size :selected").text();
        fontSize = parseInt(fontSize, 10);
        if (sizeVal == "6") {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-family", "Arial");
            fontSize = fontSize;
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-size", "" + fontSize + "px");
        } else if (sizeVal == "7") {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-family", "Times New Roman");
            fontSize = fontSize;
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-size", "" + fontSize + "px");
        } else if (sizeVal == "8") {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-family", "alexandra-script");
            fontSize = fontSize * 2;
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-size", "" + fontSize + "px");

        }
    })
    /*конец*/
    /* инпут с цветом */

    $(".oval-elem.color-input").change(function () {
        var sizeVal = $(this).val();
        console.log(sizeVal);
        $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("color", sizeVal);
    })

    /*конец */
    /* позиция текста */
    $(".text_orientation_select").change(function () {
        var sizeVal = $(this).val();
        if (sizeVal == "Левый верхний") {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("text-align", "left");

        } else if (sizeVal == "Левый нижний") {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-family", "Times New Roman");

        } else if (sizeVal == "Правый верхний") {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("text-align", "right");
        } else if (sizeVal == "Правый нижний") {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("font-family", "Brush Script MT");
        } else if (sizeVal == "Центр") {
            $(this).closest(".photo-col.photo-col-2").find(".photo-parameter-textarea").css("text-align", "center");
        }
    });
    /*конец */
    function e() {
        var e = document.getElementById("map"),
                t = {
                    center: new google.maps.LatLng(46.4699, 30.72718),
                    zoom: 14,
                    scrollwheel: !1,
                    /*disableDefaultUI: !0,*/
                    disableDefaultUI: true,
                    zoomControl: true,
                    styles: [{
                            "featureType": "administrative",
                            "elementType": "labels.text.fill",
                            "stylers": [{
                                    "color": "#444444"
                                }]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "all",
                            "stylers": [{
                                    "color": "#f2f2f2"
                                }]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "all",
                            "stylers": [{
                                    "visibility": "off"
                                }]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "geometry.fill",
                            "stylers": [{
                                    "visibility": "on"
                                },
                                {
                                    "hue": "#00ff1c"
                                },
                                {
                                    "saturation": "-52"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "all",
                            "stylers": [{
                                    "saturation": -100
                                },
                                {
                                    "lightness": 45
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "all",
                            "stylers": [{
                                    "visibility": "simplified"
                                }]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "labels.icon",
                            "stylers": [{
                                    "visibility": "off"
                                }]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "all",
                            "stylers": [{
                                    "visibility": "off"
                                }]
                        },
                        {
                            "featureType": "water",
                            "elementType": "all",
                            "stylers": [{
                                    "color": "#c2c2c3"
                                },
                                {
                                    "visibility": "on"
                                }
                            ]
                        }
                    ],
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                },
                n = new google.maps.Map(e, t),
                l = new google.maps.LatLng(46.4699, 30.72718);
        new google.maps.Marker({position: l, map: n, title: "вул. Мечникова, 108", icon: {
                url: "/templates/images/placeholder-point_1.png",
                //scaledSize: new google.maps.Size(34, 44)
            }
        })
    }
    $("body").removeClass("no-js"), $('a[href="#"]').on("click", function (e) {
        e.preventDefault()
    }), $("input[placeholder], textarea[placeholder]").placeholder(), $(".menu-toggle").on("click", function (e) {
        e.preventDefault(), $(".navigation").toggleClass("open")
    }), $(".reviews-slider").slick({infinite: !0, speed: 300, slidesToShow: 3, slidesToScroll: 1, responsive: [{breakpoint: 992, settings: {slidesToShow: 2, slidesToScroll: 1}}, {breakpoint: 768, settings: {slidesToShow: 1, slidesToScroll: 1}}]}), google.maps.event.addDomListener(window, "load", e);
    for (var t = document.getElementsByClassName("accordion-btn"), n = 0; n < t.length; n++)
        t[n].onclick = function () {
            this.classList.toggle("active"), this.nextElementSibling.classList.toggle("show")
        };
    for (var l = document.getElementsByClassName("tooltip-btn"), n = 0; n < l.length; n++)
        l[n].onclick = function (e) {
            e.preventDefault(), this.classList.toggle("active"), this.nextElementSibling.classList.toggle("active")
        };
    for (var o = document.getElementsByClassName("font-style-btn"), n = 0; n < o.length; n++)
        o[n].onclick = function (e) {
            e.preventDefault(), this.classList.toggle("active")
        };
    var s = (document.getElementsByClassName("counter-btn"), $(".copies-counter-input")),
            a = $(".fields-counter-input");
    $(".copies-minus-btn").on("click", function (e) {
        e.preventDefault();
        var t = parseInt(s.val());
        t > 1 && t--, s.val(t)
    }), $(".copies-plus-btn").on("click", function (e) {
        e.preventDefault();
        var t = parseInt(s.val());
        t < 1e3 && t++, s.val(t)
    }), $(".fields-minus-btn").on("click", function (e) {
        e.preventDefault();
        var t = parseInt(a.val());
        t > 0 && t--, a.val(t)
    }), $(".fields-plus-btn").on("click", function (e) {
        e.preventDefault();
        var t = parseInt(a.val());
        t < 1e3 && t++, a.val(t)
    })
}), $(window).load(function () {});

$(function () {

    $(".img-modal-btn").fancybox({
        afterLoad: function () {
            var id = this.element.attr('idImage');
            var isBorder = $('#filds_' + id).val();
            if (isBorder > 0) {
                $('.fancybox-skin').css('padding', '15px');
            }
        },

        openEffect: 'elastic',
        closeEffect: 'elastic',
        aspectRatio: false,
        autoSize: false,
        fitToView: false,
        scrolling: 'visible',
        scrollOutside: true,
        width: 'auto',
        height: 'auto',
        leftRatio: 0,
        padding: 0,
        //    	  helpers : {
        //    		title : {
        //    			type : 'inside'
        //    		}
        //    	},
    });

    $("#enter").click(function (e) {
        //e.preventDefault();
        $("#loginForm").css("display", "block");
        $("#registerForm").fadeOut(500, function () {
            $("#registerForm").css("display", "none");
        });


    });
    $(".close_x").click(function () {
        $("#loginForm").fadeOut(500, function () {
            $("#loginForm").css("display", "none");
        });

    });
    $("#registration1").click(function () {
        $("#registerForm").css("display", "block");
        $("#loginForm").fadeOut(500, function () {
            $("#loginForm").css("display", "none");
        });
    });
    $(".close_x_reg").click(function () {
        $("#registerForm").fadeOut(500, function () {
            $("#registerForm").css("display", "none");
        });
    });
    $(document).keydown(function (e) {
        if (e.keyCode === 27) {
            $("#registerForm").fadeOut(500, function () {
                $("#registerForm").css("display", "none");
            });
            $("#loginForm").fadeOut(500, function () {
                $("#loginForm").css("display", "none");
            });

        }

    });

    $('#delete_all_images').click(function () {
        $.fancybox({type: "html", content: 'Вы уверены что хотите удалить все фото?!</ br> <p style="text-align:center;margin-top:20px;"><a id="delete_all_images_confirm" style="color:red;" href="">Да, уверен.</a></p>', wrapCSS: 'alertmes', autoCenter: true, minWidth: 300, minHeight: 150, overlay: {locked: false}});
        $('#delete_all_images_confirm').click(function () {
            if (confirm) {
                delete_all_images();
            }
        });

    });
    $('#paper_size_all').change(function () {
        if ($(this).val() != 0) {
            var clone = $(this).clone();
            clone.val($(this).val());
            $('#all-params-form').append(clone);

            save_all_parameters_images();

            $('#all-params-form').find('#paper_size_all').remove();
            var select = $(this);
            $('.paper_size_select').each(function () {
                $(this).val(select.val());
//                name = $(this).attr("name");
//                tid = $(this).attr("id");
//                val = $("#" + tid + " option:selected").text();
//                $('#top_checked_' + tid).text(val);
            });
        }
    });

    $('#paper_type_all').change(function () {
        if ($(this).val() != 0) {
            var clone = $(this).clone();
            clone.val($(this).val());
            $('#all-params-form').append(clone);
            save_all_parameters_images();

            $('#all-params-form').find('#paper_type_all').remove();
            var select = $(this);
            $('.select_paper_type').each(function () {
                $(this).val(select.val());

//                name = $(this).attr("name");
//                tid = $(this).attr("id");
//                val = $("#" + tid + " option:selected").text();
//                $('#top_checked_' + tid).text(val);

            });
        }

    });


    $('#photo-date-checkbox-all').click(function () {
        $(".photo-container").toggleClass("active");
        var checkbox = $(this).prev('#checkbox_photo-date-checkbox-all');
        if (checkbox.prop('checked') != false) {
            $('#date_all').hide();
            var input = '<input id="checkbox_photo-container-checkbox" class="isprint" type="hidden" name="write_date" value="0">';
            var check = false;
        } else if (checkbox.prop('checked') != true) {
            $('#date_all').show();
            var input = '<input id="checkbox_photo-container-checkbox" class="isprint" type="hidden" name="write_date" value="1">';
            var check = true;
        }
        $('#all-params-form').append(input);

        var clone = $('#date_stam_all').clone();
        clone.val($('#date_stam_all').val());
        $('#all-params-form').append(clone);
        save_all_parameters_images();

        $('#all-params-form').find('#checkbox_photo-container-checkbox').remove();
        $('#all-params-form').find('#date_stam_all').remove();

        $('.print-date').each(function () {
            if (check == true) {
                this.checked = true;
                $('#' + $(this).attr('id')).attr('checked', 'checked');
                $(this).parent().parent().next('div').show();
            }

            if (check == false) {
                this.checked = false;
                $('#' + $(this).attr('id')).removeAttr('checked');
                $(this).parent().parent().next('div').hide();
            }

        });

    });



    $('#paper_field_width_all').change(function () {
        if ($(this).val() != 0) {
            var clone = $(this).clone();
            clone.hide();
            clone.val($(this).val());
            $('#all-params-form').append(clone);
            save_all_parameters_images();
            var select = $(this);
            $('.field_width_select').each(function () {
                $(this).val(select.val());
            });
        }

    });

    $('#paper_framing_all').change(function () {
        if ($(this).val() != 3) {
            var clone = $(this).clone();
            clone.hide();
            clone.val($(this).val());
            $('#all-params-form').append(clone);
            save_all_parameters_images();
            var select = $(this);
            $('.framing_select').each(function () {
                $(this).val(select.val());
            });
        }

    });

    $('#paper_photo_orientation_all').change(function () {
        if ($(this).val() != 3) {
            var clone = $(this).clone();
            clone.hide();
            clone.val($(this).val());
            $('#all-params-form').append(clone);
            save_all_parameters_images();
            var select = $(this);
            $('.photo_orientation_select').each(function () {
                $(this).val(select.val());
            });
        }

    });


    $('#font_all').change(function () {
        if ($(this).val() != 0) {
            var clone = $(this).clone();
            clone.hide();
            clone.val($(this).val());
            $('#all-params-form').append(clone);
            save_all_parameters_images();
            var select = $(this);
            $('.select_font').each(function () {
                $(this).val(select.val());
            });
        }

    });

    $('#font_size_all').change(function () {
        if ($(this).val() != 0) {
            var clone = $(this).clone();
            clone.hide();
            clone.val($(this).val());
            $('#all-params-form').append(clone);
            save_all_parameters_images();
            var select = $(this);
            $('.select_font_size').each(function () {
                $(this).val(select.val());
            });
        }

    });
    $('#paper_text_orientation_all').change(function () {
        if ($(this).val() != 0) {
            var clone = $(this).clone();
            clone.hide();
            clone.val($(this).val());
            $('#all-params-form').append(clone);
            save_all_parameters_images();
            var select = $(this);
            $('.text_orientation_select').each(function () {
                $(this).val(select.val());
            });
        }

    });

    $('#font_b_all').click(function ()
    {

        input = $('#input_font_b_all').clone();
        input.val($('#input_font_b_all').val());
        val = $('#input_font_b_all').val();
        $('#all-params-form').append(input);
        save_all_parameters_images();
        if (val == 1)
        {
            $('.input_font_b').each(function () {
                $(this).val(val);
                $(this).next().addClass('active');
            });
        } else if (val == 0)
        {
            $('.input_font_b').each(function () {
                $(this).val(val);
                $(this).next().removeClass('active');
            });
        }

    });
    $('#font_i_all').click(function ()
    {

        input = $('#input_font_i_all').clone();
        input.val($('#input_font_i_all').val());
        $('#all-params-form').append(input);
        save_all_parameters_images();
        val = $('#input_font_i_all').val();
        if (val == 1)
        {
            $('.input_font_i').each(function () {
                $(this).val(val);
                $(this).next().addClass('active');
            });
        } else if (val == 0)
        {
            $('.input_font_i').each(function () {
                $(this).val(val);
                $(this).next().removeClass('active');
            });
        }
    });
    $('#font_u_all').click(function ()
    {

        input = $('#input_font_u_all').clone();
        input.val($('#input_font_u_all').val());
        $('#all-params-form').append(input);
        save_all_parameters_images();
        val = $('#input_font_u_all').val();
        if (val == 1)
        {
            $('.input_font_u').each(function () {
                $(this).val(val);
                $(this).next().addClass('active');
            });
        } else if (val == 0)
        {
            $('.input_font_u').each(function () {
                $(this).val(val);
                $(this).next().removeClass('active');
            });
        }
    });
    $('#description_all').change(function ()
    {
        val = $(this).val();
        $('.photo-parameter-textarea').each(function () {
            $(this).val(val);
        });
        clone = $(this).clone();
        clone.hide();
        $('#all-params-form').append(clone);
        save_all_parameters_images();
    });
    $('#input_color_text_all').change(function ()
    {
        val = $(this).val();
        clone = $(this).clone();
        clone.hide();
        $('#all-params-form').append(clone);
        save_all_parameters_images();
        $('.color-input').each(function () {
            $(this).val(val);
        });
    });


    $('#photo-autocorrect-checkbox-all').click(function (e)
    {
        //alert($('#checkbox_photo-autocorrect-checkbox-all').is(':checked'));
        var checkbox = $(this).prev('#checkbox_photo-autocorrect-checkbox-all');
        check = !checkbox.prop('checked');
        if (check == true)
        {
            value_input = 1;
        } else
        {
            value_input = 0
        }
        var input = '<input type="hidden" name="autocorrect" value="' + value_input + '">';
        $('#all-params-form').append(input);
        save_all_parameters_images();


        $('.autocorrect-inputs').each(function () {
            if (check == true) {
                this.checked = true;
                $('#' + $(this).attr('id')).attr('checked', 'checked');
            }

            if (check == false) {
                this.checked = false;
                $('#' + $(this).attr('id')).removeAttr('checked');
            }

        });
    });


    $('#photo-back_side_text-checkbox-all').click(function (e)
    {
        //alert($('#checkbox_photo-autocorrect-checkbox-all').is(':checked'));
        var checkbox = $(this).prev('#checkbox_photo-back_side_text-checkbox-all');
        check = !checkbox.prop('checked');
        if (check == true)
        {
            value_input = 1;
        } else
        {
            value_input = 0
        }
        var input = '<input type="hidden" name="back_side_text" value="' + value_input + '">';
        $('#all-params-form').append(input);
        save_all_parameters_images();


        $('.photo-back_side_inputs').each(function () {
            if (check == true) {
                this.checked = true;
                $('#' + $(this).attr('id')).attr('checked', 'checked');
            }

            if (check == false) {
                this.checked = false;
                $('#' + $(this).attr('id')).removeAttr('checked');
            }

        });
    });

    $('#date_stam_all').change(function () {
        var clone = $(this).clone();
        clone.val($(this).val());
        $('#all-params-form').append(clone);
        var input = '<input id="checkbox_photo-container-checkbox" class="isprint" type="hidden" name="write_date" value="1">';
        $('#all-params-form').append(input);
        save_all_parameters_images();

        $('#all-params-form').find('#date_stam_all').remove();
        $('#all-params-form').find('#checkbox_photo-container-checkbox').remove();

        var select = $(this);
        $('.date-checkbox').each(function () {
            $(this).val(select.val());
        });
    });


    $('#selectall').click(function () {
        var input = '<input id="checkbox_photo-container-checkbox" class="isprint" type="hidden" name="is_print" value="1">';
        $('#all-params-form').append(input);
        save_all_parameters_images();
        $('#all-params-form').find('#checkbox_photo-container-checkbox').remove();
        $('.isprint').each(function () {
            this.checked = true;
            $('#' + $(this).attr('id')).attr('checked', 'checked');
            //обновляем количество в топе
            $('#checked_images').text('Все');
        });

    });

    $('#deselectall').click(function () {
        var input = '<input id="checkbox_photo-container-checkbox" class="isprint" type="hidden" name="is_print" value="0">';
        $('#all-params-form').append(input);
        save_all_parameters_images();
        $('#all-params-form').find('#checkbox_photo-container-checkbox').remove();
        $('.isprint').each(function () {
            this.checked = false;
            $('#' + $(this).attr('id')).removeAttr('checked');
            //обновляем количество в топе
            $('#checked_images').text('0');
        });
    });



});


$(function () {

    $("#btnRegister").click(function () {
        $("#btnRegister").attr("disabled", "disabled");
        var customFields = $("#registerForm").serialize();
        $.post("/component/load/system/register?action=register" + (customFields != "" ? "&" + customFields : ""), {
            email: $("#tbREmail").val(),
            password: $("#tbRPassw").val(),
            repassword: $("#tbRRePassw").val(),
            code: $("#tbRCode").val(),
            token: $("#tbRToken").val()
        }, function (data) {
            $("#btnRegister").removeAttr("disabled");
            if (!data.RESULT) {
                $("#registerMessages").html(data.MESSAGE);
                $("#registerMessages").removeClass('alert alert-success');
                $("#registerMessages").addClass('alert alert-danger');
                //alert(1);
                $("#new_captcha").click();
            } else {
                $("#registerMessages").html(data.MESSAGE);
                $("#registerMessages").addClass('alert alert-success');
                $("#registerMessages").removeClass('alert alert-danger');
                $("#registerForm").remove();
                $("body").animate({scrollTop: 0}, '500', 'swing');
                //alert(2);
                location.reload();
            }

        }, "json");
        return false;
    });


});



$('html').keydown(function (eventObject) { //отлавливаем нажатие клавиш

    if ($('#loginForm').is(':visible')) {
        if (event.keyCode == 13) { //если нажали Enter, то true
            $('#btnLogin').click();
        }
    } else if ($('#registerForm').is(':visible')) {
        if (event.keyCode == 13) { //если нажали Enter, то true
            $('#btnRegister').click();
        }
    }
    if ($('#social').is(':visible')) {
        if (event.keyCode == 27) { //если нажали escape, то true
            $('html').find('.fancybox-item.fancybox-close').click();
        }
    }

});

//$("#val_phone").mask("+38(099) 999-99-99");

//$("#phone_c").mask("+38(099) 999-99-99");

//$('#val_phone').change(function ()
//    {
//        if (checkTelephoneNumber($(this).val()) == false)
//        {
//            $(this).next().attr('disabled','disabled');
//
//        } else
//        {
//            $(this).next().attr('disabled',false);
//        }
//    });
$('#val_phone').click(function () {
    //if($(this).val()=="+__(___) ___-__-__")
    if ($(this).val() == "") {
        $(this).val('+380');
    }
});
$('#phone_c').click(function () {
    //if($(this).val()=="+__(___) ___-__-__")
    if ($(this).val() == "") {
        $(this).val('+380');
    }
});
$('#val_phone').change(function () {
    var rez = checkTelephoneNumber($(this).val());
    if (rez == false || $(this).val().length > 13)
        $(this).val('');

});
$('#phone_c').change(function () {
    var rez = checkTelephoneNumber($(this).val());
    if (rez == false || $(this).val().length > 13)
        $(this).val('');

});

$('#val_email').change(function () {
    if (validateEmail($(this).val()) == false) {
        $(this).next().attr('disabled', 'disabled');

    } else {
        $(this).next().attr('disabled', false);
    }
});

$('.btnShowRegister').click(function () {
    $("#registration1").click();
});

$('.btnShowLogin').click(function () {
    $("#enter").click();
});

$('.PrintNoReg').click(function () {
    $('html, body').animate({scrollTop: 0}, 500);
    $("#enter").click();

});

$('#profile-photo-change').click(function () {
    $('#input_photo').click();
});

$('#csend').change(function () {
    //var ext = this.value.match(/\.(.+)$/)[1];
    var ext=this.value;
    ext=ext.split('.');
    ext=ext[ext.length-1];
    switch (ext) {
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'tiff':
        case 'bmp':
            break;
        default:

            $.fancybox({type: "html", content: 'Тип файла не поддерживается!', wrapCSS: 'alertmes', autoCenter: true, minWidth: 300, minHeight: 150, overlay: {locked: false}});

            this.val('');
            return false;
    }
});

function doLogin() {
    $("#btnLogin").attr("disabled", "disabled");

    $.post("/component/load/system/auth", {
        email: $("#tbEmail").val(),
        passw: $("#tbPassw").val(),
        action: 'login'
    }, function (data) {
        res = data.RESULT;

        if (res) {

            //	document.location.reload();
            window.location.href = document.location.origin;

        } else {
            $("#loginmessage").html('Ошибка авторизации!').fadeIn(300);


        }
        $("#btnLogin").removeAttr("disabled");
    }, "json");
}

$(function () {
    $("#btnLogin").click(function () {

        doLogin();
        return false;
    });
    //	$("#tbPassw").keypress(function(event){
    //		var keycode = (event.keyCode ? event.keyCode : event.which);
    //		if(keycode == '13'){
    //			doLogin();
    //		}
    //	});
    $("#tbEmail").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            $("#tbPassw").focus();
        }
    });
    $("#rPassword").click(function () {
        $("#rPassword_block").slideToggle();
        return false;
    });
    $("#btnRestorePassword").click(function () {


        if ($("#tbRestoreEmail").val() == "") {
            return false;
        }
        $(this).attr("disabled", "disabled");

        $.post("/component/load/system/auth", {
            email: $("#tbRestoreEmail").val(),
            action: 'restore'
        }, function (data) {
            if (data.RESULT) {
                $.fancybox({type: "html", content: 'Данные для восстановления пароля отправлены на указанный Вами E-mail.', wrapCSS: 'alertmes', autoCenter: true, minWidth: 300, minHeight: 150, overlay: {locked: false}});


            } else {
                alert(data.MESSAGE);
            }

            $("#btnRestorePassword").removeAttr("disabled");
        }, "json");



    });
});

// валидация телефоного номера на страничке персональных данных
function checkTelephoneNumber(str) {
    var exp = /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/;
    return exp.test(str);
}
// валидация почты на страничке персональных данных
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function save_all_parameters_images(input = false) {
    new_input = false;
    if (input)
    {
        new_input = input.clone();
        val = input.val();
        new_input.val(val)
        new_input.hide();
        $('#all-params-form').append(new_input);
    }

    var form = $('#all-params-form').serialize();
    $.ajax({
        url: '/save/all/properties/',
        type: 'POST',
        data: form,
        success: function (data) {
            if (new_input)
                new_input.remove();
            idOrder = $('input[name=idorder]').val()
            input = '<input type="hidden" name="idorder" value="' + idOrder + '">';
            $("#all-params-form").empty();
            $("#all-params-form").append(input);

        },
        error: function (jqXHR, textStatus, errorThrown) {

            var errorone = 'статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ";
            $.fancybox({type: "html", content: errorone, wrapCSS: 'alertmes', autoCenter: true, minWidth: 300, minHeight: 150, overlay: {locked: false}});
            var errortwo = "Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR;
            $.fancybox({type: "html", content: errortwo, wrapCSS: 'alertmes', autoCenter: true, minWidth: 300, minHeight: 150, overlay: {locked: false}});
            //alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
        }
    });
}

function delete_all_images() {
    var id = $('#all-params-form').find('input[name=idorder]').val();
    $.ajax({
        url: '/delete/all/images/',
        type: 'POST',
        data: {id: id},
        success: function (data) {
            $.fancybox({type: "html", content: 'Все фото удалены!', wrapCSS: 'alertmes', autoCenter: true, minWidth: 300, minHeight: 150, overlay: {locked: false}});

            window.location.href = '/personal/orders/';


        },
        error: function (jqXHR, textStatus, errorThrown) {


            var errorone = 'статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ";
            $.fancybox({type: "html", content: errorone, wrapCSS: 'alertmes', autoCenter: true, minWidth: 300, minHeight: 150, overlay: {locked: false}});
            var errortwo = "Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR;
            $.fancybox({type: "html", content: errortwo, wrapCSS: 'alertmes', autoCenter: true, minWidth: 300, minHeight: 150, overlay: {locked: false}});
            // alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
            // alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
        }
    });
}
function DPI(widthI,heightI,widthP,heightP)
{
    widthI= parseInt(widthI,10);
    heightI= parseInt(heightI,10);
    widthP= parseInt(widthP,10);
    heightP= parseInt(heightP,10);
    dI=Math.pow(widthI,2)+Math.pow(heightI, 2);
    dI=Math.sqrt(dI);
    dP=Math.pow(widthP, 2)+Math.pow(heightP, 2);
    dP=Math.sqrt(dP);
    dP=dP/2.54;
    return Math.ceil(dI/dP);
}