<?php if(!defined("_APP_START")) { exit(); }  global $user;
$avatar = '';
$avatar = '';
if(isset($_SESSION['CURRENT_USER']['avatar']) && !empty($_SESSION['CURRENT_USER']['avatar']))
{
 $avatar = $_SESSION['CURRENT_USER']['avatar'];
}
$name= dbGetRow('SELECT u.email, i.fio FROM #__users AS u '
        . ' INNER JOIN #__user_info AS i ON( u.id=i.userid ) '
        . ' WHERE u.id = :id', array(':id'=> $user->getID()));
$name= !empty($name['fio']) ? $name['fio']  : $name['email'];
?>
<!DOCTYPE HTML>
<html lang="ru">
<head prefix="og: http://ogp.me/ns#">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
	<link rel="shortcut icon" href="<?php echo $page-> getTemplateUrl(); ?>favicon.ico">
<?php $page->getHeader(); ?>
	<meta name="description" content="description">
	<meta name="keywords" content="keywords">
        <!--fancyBox -->
        <link rel="stylesheet" href="<?php echo $page-> getTemplateUrl(); ?>js/fancybox/source/jquery.fancybox.css?v=1" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo $page-> getTemplateUrl(); ?>js/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo $page-> getTemplateUrl(); ?>js/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
        <!--fancyBox -->
        
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $page-> getTemplateUrl(); ?>apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $page-> getTemplateUrl(); ?>apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $page-> getTemplateUrl(); ?>apple-touch-icon-144x144.png">
	<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700" rel="stylesheet">
	<meta name="msapplication-TileImage" content="<?php echo $page-> getTemplateUrl(); ?>og-logo.png">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="application-name" content="">
	<meta property="og:title" content="">
	<meta property="og:description" content="">
	<meta property="og:url" content="">
	<meta property="og:image" content="og-logo.png">
	<meta property="og:type" content="website">
	<meta property="og:site_name" content="">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<link rel="stylesheet" href="<?php echo $page-> getTemplateUrl(); ?>css/style.css">
	<!--[if lt IE 9]>
			<script src="<?php echo $page-> getTemplateUrl(); ?>js/html5.js"></script>
			<script src="<?php echo $page-> getTemplateUrl(); ?>js/respond.js"></script>
	<![endif]-->
	<script src="<?php echo $page-> getTemplateUrl(); ?>js/jquery.js"></script>
        

        
</head>
<body class="no-js <?php echo $page->getBodyClass(); ?>">
	<?php if(!$page->notFound) {?>
  <div class="main">
    <header class="page-header" itemscope itemtype="http://schema.org/WPHeader">
      <div class="container">
        <div class="row">
          <div class="<?php 	if(!$user->Authorized()) {?>col-md-4 col-sm-5<?php } else { ?> col-xl-4 col-lg-3 col-md-3 col-sm-4 <?php  } ?>">
            <div class="logo">
              <a href="/" itemprop="url"><img src="<?php echo $page-> getTemplateUrl(); ?>images/logo.png" alt="" itemprop="image"></a>
            </div>
          </div>
					<?php 	if($user->Authorized()) {?>
					<div class="col-xl-1 col-lg-2 col-md-2 col-sm-3">
						<div class="photo-counter" itemscope itemtype="http://schema.org/Thing">
							<a href="/personal/orders/" itemprop="url">
								<span class="photo-counter-img"><img src="<?php echo $page-> getTemplateUrl(); ?>images/icons/icon-photo-counter.png" alt="" itemprop="image"></span>
								<span class="photo-counter-info" itemprop="description">Загружено <?php if($dcount=Orders::get_download_count($user->getid())) {echo $dcount; } else { echo '0';}  ?> фото</span>
							</a>
						</div>
					</div>  <div class="col-xl-7 col-lg-7 col-md-7 col-sm-5" id="auth_block_1">
					<?php } else { ?>
<div class="col-md-8 col-sm-7">
					<?php } ?>

            <div 	<?php 	if(!$user->Authorized()) {?> class="page-header-info" <?php } ?>>
              <div class="page-header-top clearfix">
                <div class="header-phones" itemscope itemtype="http://schema.org/Thing">
                  <div itemprop="description">+38 (050) 728 76 54</div>
                  <div itemprop="description">+38 (067) 938 71 09</div>
                </div>
                
                    <?php if($user->Authorized()) { ?>
                  <div class="print-order" itemscope itemtype="http://schema.org/Offer">
                  <a href="/personal/new-order/" class="btn-print-order btn" itemprop="url">Заказать печать</a>
                 
                 </div>
                                                                <?php } else { ?>
                   <div class="print-order PrintNoReg" itemscope itemtype="http://schema.org/Offer"  >
                  <a href="#" class="btn-print-order btn" itemprop="url"'>Заказать печать</a>
                   </div>
                                                                <?php }?>
             
								<?php
								if($user->Authorized()) {
									?>
                  <div class="info-user-header">
                      <a href="/logout/" class="logout-new" itemprop="url"><img src="<?php echo $page->getTemplateUrl(); ?>images/icons/icon-logout.png" alt="" itemprop="image"> <span>Выйти</span></a><?php
                              if (!empty($avatar))
                              {
                                  ?>
                                  
        <?php } ?>



                              <div class="logout" itemscope itemtype="http://schema.org/Thing">
                                  <div class="image">
                                      <img  src="<?php echo $avatar; ?>"  style = "border-radius:60px;padding:0;margin-right:0px; width:40px; height:40px; " alt="" />
                                  </div> 

                                  <div class="text">
        <?php echo $name; ?>
                                  </div>


                              </div> 
                          </div>
										<?php
								}
								else {?>
                <div class="authorization" itemscope itemtype="http://schema.org/Thing">
                  <a href="javascript:void(0);" id="enter" itemprop="url">Вход</a> / <a href="javascript:void(0);" itemprop="url" id="registration1">Регистрация</a>




<form style="text-align: center;" method="post" id="loginForm">
    <h1 style="">Вход</h1><span class="close_x">X</span>
	<div class="form-group" style="margin-top: 35px;">
	<div id="loginmessage"></div>
	<!--<label><?= $MSG["TITLE_EMAIL"] ?>:</label>-->
	<input style="" class="form-control" required type="email" id="tbEmail" name="email" placeholder="e-mail:" />
	</div>
	 <div class="form-group" style="">
	<!--<label><?= $MSG["TITLE_PASSWORD"] ?>:</label>-->
	<input style="" class="form-control" required type="password" id="tbPassw" name="passw" placeholder="пароль:" />
	</div>
	<input style="" id="btnLogin" class="btn btn-default btnEnter" type="button" value="Войти" />
    <!--<a style="display: inline-block; margin-left: 11px;margin-top: 20px;" href="#" id="rPassword"><?= $MSG["TITLE_RESTORE_PASSWORD"] ?></a>-->
   <!-- <div style="margin-top: 40px; display:none;" class="form-inline" id="rPassword_block" style="display:none;">
	 <div  class="form-group"><input style="border: 1px solid #685D5D;margin-top:0;margin-left:0" class="form-control" id="tbRestoreEmail" type="email" placeholder="<?= $MSG["RESTORE_EMAIL"] ?>" />&nbsp;&nbsp;<input style="margin-top:-5px" class="btn btn-default" type="button" value="Восстановить" id="btnRestorePassword" /></div>
</div>-->
<input type="button" class="btn btn-primary btnShowRegister" value="Регистрация" style='margin-top:10px;' />
<p class="reg_text">Или войдите используя аккаунт:</p>
	<p style="margin-top: 20px;">
		<?php
    $client_id = '923569619710-3uaqskanfuakgk0h8hk14vosbegp8utd.apps.googleusercontent.com'; // Client ID
    $client_secret = 'ZbvX7-JLVKn6MTA2-FY7bKoh'; // Client secret
    $redirect_uri = 'https://www.yashaprint.com/modules/social/google.php'; // Redirect URIs

    $url = 'https://accounts.google.com/o/oauth2/auth';

    $params = array(
        'redirect_uri'  => $redirect_uri,
        'response_type' => 'code',
        'client_id'     => $client_id,
        'scope'         => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'
    );

   $google_link = 'href="' . $url . '?' . urldecode(http_build_query($params)) . '"';
?>

<?php
$client_id = "1827876734102416";
$client_secret = "3bf6faa0032bd42ea9a55744014f8ecf";
$redirect_uri = "https://www.yashaprint.com/modules/social/facebook.php";

$url = 'https://www.facebook.com/dialog/oauth';

$params = array(
  'client_id'     => $client_id,
  'redirect_uri'  => $redirect_uri,
  'response_type' => 'code',
  'scope'         => 'email,user_birthday'
);

 $facebook_link = 'href="' . $url . '?' . urldecode(http_build_query($params)) . '"';


 ?>

 <?php

 $client_id = '751324'; // ID
 $client_secret = '16069db25dcc5a8f80f6310ab674ff8b'; // Секретный ключ
 $redirect_uri = 'https://www.yashaprint.com/modules/social/mailru.php'; // Ссылка на приложение }

 $url = 'https://connect.mail.ru/oauth/authorize';

 $params = array(
     'client_id'     => $client_id,
     'response_type' => 'code',
     'redirect_uri'  => $redirect_uri
 );

$mail_link = 'href="' . $url . '?' . urldecode(http_build_query($params)) . '"';

  ?>


<!--
		<a class="insta hover" style="" href="/modules/social/vk.php"><img class="insta not_hover" src="/templates/images/insta.png" style=""/>
        <img class="vk hover" src="/templates/images/insta_hover.png" style=""/>
        </a>
      -->
		<a class="vk1" style="" href="/modules/social/vk.php" title="Vkontakte"><img class="vk not_hover" src="/templates/images/vk.png" style=""/>
        <img class="vk hover" src="/templates/images/vk_hover.png" style=""/>
        </a>
		<a style="" <?php echo $facebook_link;  ?> title="Facebook"><img src="/templates/images/fs.png" style=""/><img class="vk hover" src="/templates/images/fs_hover.png" style=""/>
		</a>
		<a style="" <?php echo $google_link; ?> title="Google"><img src="/templates/images/google.png" style=""/><img class="vk hover" src="/templates/images/google_hover.png" style=""/>
		</a>
		<a style="" href="/modules/social/twitter.php" title="Twitter"><img src="/templates/images/twitter.png" style=""/><img class="vk hover" src="/templates/images/twitter_hover.png" style=""/></a>
		<a style="" href="/modules/social/odnoklassniki.php" title="Odnoklassniki"><img src="/templates/images/odnokl.png" style=""/><img class="vk hover" src="/templates/images/odnokl_hover.png" style=""/></a>

		<!-- <a href="http://ads.ratio.tech/modules/social/odnoklassniki.php"><i class="fa fa-odnoklassniki-square" style="font-size:24px"></i></a> -->
		<a style="margin-right: 10px;" <?php echo $mail_link; ?>  title="Mail.ru"><img src="/templates/images/mail_ru.png" style=" "/><img class="vk hover" src="/templates/images/mail_ru_hover.png" style=""/></a>
	</p>
        <br/>
</form>
<br />
<?php $tokenreg=App::createFormToken('rf'); ?>
<!--token to script.js-->
<script>var token = '<?php echo   $tokenreg; ?>';</script>

<form style="text-align: center;" method="post" id="registerForm">
<div  id="registerMessages"></div>
<h1 style="text-align: center;">Регистрация</h1><span class="close_x_reg">X</span>
	<input type="hidden"  id="tbRToken" value="<?php echo  $tokenreg; ?>" />
	<div class="form-group">
		<!--<label><?php echo $MSG["CAPTION_R_EMAIL"]; ?>:</label>-->
		<input style="" class="form-control" maxlength="100" type="email" required id="tbREmail" placeholder="e-mail:"  />
	</div>
	<div id="customFields">
		<?php echo $data["CUSTOM_FIELDS"]; ?>
	</div>
	<div class="row">
	<div class="form-group "><!--<label><?php echo $MSG["CAPTION_R_PASSWORD"]; ?>:</label>-->
	<input style="" type="password" class="form-control"  maxlength="100" required  id="tbRPassw" placeholder="пароль:"  />
	</div>
	<div class="form-group "><!--<label><?php echo $MSG["CAPTION_R_REPASSWORD"]; ?>:</label>-->
	<input  style="" class="form-control" type="password"  maxlength="100" required  id="tbRRePassw" placeholder="повторите пароль:" /></div>
	<div class="form-group ">
	<?php $this->includeComponent("system/captcha",array("SHOW"=>TRUE));?><br />
	</div>
	<div class="form-group ">
	<!--<label><?php echo $MSG["CAPTION_R_IMAGECODE"]; ?>:</label>-->
	<input style="" type="number" maxlength="6"  class="form-control" required  id="tbRCode"  /></div>
	</div>

	<input type="button" class="btn btn-primary" id="btnRegister" value="Регистрация" />
        
        <input style=""  class="btn btn-default btnShowLogin" type="button" value="Войти" />
        <p class="reg_text">Или войдите используя аккаунт:</p>
	<p style="margin-top: 20px;">
            <!--
		<a class="insta hover" style="" href="/modules/social/vk.php"><img class="insta not_hover" src="/templates/images/insta.png" style=""/>
        <img class="vk hover" src="/templates/images/insta_hover.png" style=""/>
        </a>
      -->
		<a class="vk1" style="" href="/modules/social/vk.php" title="Vkontakte"><img class="vk not_hover" src="/templates/images/vk.png" style=""/>
        <img class="vk hover" src="/templates/images/vk_hover.png" style=""/>
        </a>
		<a style="" <?php echo $facebook_link; ?> title="Facebook" ><img src="/templates/images/fs.png" style=""/><img class="vk hover" src="/templates/images/fs_hover.png" style=""/>
		</a>
		<a style="" <?php echo $google_link; ?> title="Google"><img src="/templates/images/google.png" style=""/><img class="vk hover" src="/templates/images/google_hover.png" style=""/>
		</a>
		<a style="" href="/modules/social/twitter.php" title="Twitter"><img src="/templates/images/twitter.png" style=""/><img class="vk hover" src="/templates/images/twitter_hover.png" style=""/></a>
		<a style="" href="/modules/social/odnoklassniki.php" title="Odnoklassniki"><img src="/templates/images/odnokl.png" style=""/><img class="vk hover" src="/templates/images/odnokl_hover.png" style=""/></a>

		<!-- <a href="http://ads.ratio.tech/modules/social/odnoklassniki.php"><i class="fa fa-odnoklassniki-square" style="font-size:24px"></i></a> -->
		<a style="margin-right: 10px;" <?php echo $mail_link; ?>  title="Mail.ru"><img src="/templates/images/mail_ru.png" style=" "/><img class="vk hover" src="/templates/images/mail_ru_hover.png" style=""/></a>
	</p>
        <br/>
</form>




</div>
								<?php } ?>

             <div class="page-header-menu">
                <nav class="navigation clearfix" itemscope itemtype="http://schema.org/SiteNavigationElement">
                  <a href="#" class="menu-toggle" itemprop="url">
                    <span class="menu-toggle-icon"></span>
                  </a>
                  <div class="navigation-drop">
                    <div class="menu-wrapper clearfix">
                      <ul class="menu clearfix">
												<?php $menu = Content::getMenuByCode('main'); ?>
												<?php foreach($menu as $link) {?>
												<li <?php 
                                                                                                if($link['title']=='Мой кабинет' && !$user->Authorized())
                                                                                                    continue;
                                                                                                if($link['active']) {  ?>class="active" <?php } ?>><a href="<?php echo $link['link']; ?>" itemprop="url"><?php echo $link['title']; ?></a></li>
												<?php } ?>

                                                                                                    <?php if ($user->Authorized())
                                                                                                    { ?>
                                                                                                        <li class="hidden-lg-up"><a href="#" class="btn-print-order btn" itemprop="url">Заказать печать</a></li>


                                                                                                    <?php } else
                                                                                                    { ?>
                                                                                                        <li class="hidden-lg-up PrintNoReg" ><a href="#" class="btn-print-order btn" itemprop="url">Заказать печать</a></li>
                                                                                                    <?php } ?>

                        <li class="hidden-md-up">
                          <div class="header-phones-mobile" itemscope itemtype="http://schema.org/Thing">
                            <div itemprop="description">+38 (050) 728 76 54</div>
                            <div itemprop="description">+38 (067) 938 71 09</div>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <!-- /.navigation-drop -->
                </nav>
                <!-- /.navigation -->
              </div>
            </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container -->
    </header>
		<!-- /.page-header -->
    	<div class="spanning">
				<?php } ?>
