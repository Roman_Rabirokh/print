<?php if(!defined("_APP_START")) { exit(); } ?>
<?php ini_set("display_errors","off"); ?>
<div class="section-product">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-5 col-sm-12">
        <div class="section-product-info" itemscope itemtype="http://schema.org/Product">
          <h3 class="section-product-title title-3" itemprop="name">Название продукта</h3>
          <div class="section-product-subtitle" itemprop="offers">Описание в двух словах, что это такое и как работает</div>
          <p class="section-product-descr" itemprop="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
        </div>
      </div>
      <!-- /.col -->
      <div class="col-lg-6 col-md-7 col-sm-12">
        <div class="section-product-video" itemscope itemtype="http://schema.org/Thing">
          <iframe width="520" height="285" src="https://www.youtube.com/embed/Q0nDQOrBx4A" class="product-video-item" allowfullscreen></iframe>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
</div>
<!-- /.section-product -->
<div class="section-test" itemscope itemtype="http://schema.org/Offer">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-lg-6 col-md-12">
        <h3 class="section-test-title title-3" itemprop="name">Хотите просто распечатать свои фотографии, не выходя из дома?</h3>
        <p class="section-test-descr" itemprop="description">Наш сервис даёт Вам возможность заказать печать Ваших фото, не вставая с дивана. <br>Это займет всего несколько минут и совершенно бесплатно!</p>
        <?php
        global $user;
        if($user->Authorized())
        { ?>
            <a href="/personal/new-order/" class="section-test-btn btn" itemprop="url">Пройдите тест-драйв</a> 
        <?php }
        else {
        ?>  <a href="/test-drive/" class="section-test-btn btn" itemprop="url">Пройдите тест-драйв</a>    
       <?php }
        ?>
       
      </div>
      <!-- /.col -->
      <div class="col-xl-6 col-lg-6 col-md-12">
        <div class="section-test-logo hidden-md-down">
          <img src="<?php echo $page-> getTemplateUrl(); ?>images/logo.png" alt="" itemprop="image">
        </div>
        <div class="section-test-photos clearfix">
          <div class="test-photos-camera">
            <img src="<?php echo $page-> getTemplateUrl(); ?>images/camera.png" alt="" itemprop="image">
          </div>
          <div class="test-photos-items">
            <img src="<?php echo $page-> getTemplateUrl(); ?>images/test-photos.png" alt="" itemprop="image">
          </div>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
</div>
<!-- /.section-test -->
<div class="section-review">
  <div class="bg-element"></div>
  <div class="title-center">
    <h2 class="section-review-title title-2"><span class="review-title-inner">Отзывы</span></h2>
  </div>
  <div class="reviews-slider">

        <?php $this->includeComponent('photosite/comments.block',array('ROWS'=>4));?>

    <!-- /.reviews-item -->

  </div>
  <!-- /.reviews-slider -->
</div>
<!-- /.section-review -->
<div class="section-services">
  <div class="title-center">
    <h2 class="section-services-title title-2">
      <span class="services-title-inner">Наш сервис - это</span>
    </h2>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-6">
        <div class="service" itemscope itemtype="http://schema.org/Service">
          <div class="service-img service-img-circle">
            <img src="<?php echo $page-> getTemplateUrl(); ?>images/service-icon-1.jpg" alt="" itemprop="image">
          </div>
          <div class="service-item-descr" itemprop="description">Качественная печать фото</div>
        </div>
      </div>
      <!-- /.col -->
      <div class="col-lg-3 col-md-6">
        <div class="service" itemscope itemtype="http://schema.org/Service">
          <div class="service-img">
            <img src="<?php echo $page-> getTemplateUrl(); ?>images/service-icon-2.jpg" class="service-img-circle" alt="" itemprop="image">
          </div>
          <div class="service-item-descr" itemprop="description">Быстрое обслуживание</div>
        </div>
      </div>
      <!-- /.col -->
      <div class="col-lg-3 col-md-6">
        <div class="service" itemscope itemtype="http://schema.org/Service">
          <div class="service-img">
            <img src="<?php echo $page-> getTemplateUrl(); ?>images/service-icon-3.jpg" alt="" itemprop="image">
          </div>
          <div class="service-item-descr" itemprop="description">Удобный заказ</div>
        </div>
      </div>
      <!-- /.col -->
      <div class="col-lg-3 col-md-6">
        <div class="service">
          <div class="service-img service-img-circle">
            <img src="<?php echo $page-> getTemplateUrl(); ?>images/service-icon-4.jpg" class="service-img-circle" alt="">
          </div>
          <div class="service-item-descr two-line">Самостоятельный выбор формата и размера фото</div>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
</div>
<!-- /.section-service -->
<div class="section-delivery">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="section-delivery-item" itemscope itemtype="http://schema.org/Service">
          <h2 class="section-delivery-title title-2" itemprop="name"><span class="delivery-title-inner">Доставка</span></h2>
          <p class="delivery-descr" itemprop="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
        </div>
      </div>
      <!-- /.col -->
      <div class="col-md-6">
        <div class="section-delivery-item" itemscope itemtype="http://schema.org/Service">
          <h2 class="section-delivery-title title-2" itemprop="name"><span class="delivery-title-inner">Оплата</span></h2>
          <p class="delivery-descr" itemprop="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.ror -->
  </div>
  <!-- /.container -->
</div>
<!-- /.section-delivery -->
<div class="section-blog">
  <div class="container">
    <div class="row">


      <?php $this->includeComponent('photosite/news.block',array('ROWS'=>3));?>

      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
</div>
<!-- /.section-blog -->
