	<?php if(!defined("_APP_START")) { exit(); }	?>

	<div class="spanning">
		<div class="account-content" itemscope itemtype="http://schema.org/WebPage">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ol class="breadcrumb" itemprop="breadcrumb">
							<li><a href="#" itemprop="url">Главная</a></li>
							<li class="active">Мой кабинет</li>
						</ol>
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
				<div class="row">
		      <div class="col-md-12">
		      <?php include('includes/profile-menu.php'); ?>
		      </div>
		      <!-- /.col -->
		    </div>
				<!-- /.row -->
				<?php
						$this->includeComponent('photosite/mypayments.list');
				?>
