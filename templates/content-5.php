<?php if(!defined("_APP_START")) { exit(); } ?>

<div class="spanning">
  <div class="contact-content" itemscope itemtype="http://schema.org/WebPage">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ol class="breadcrumb" itemprop="breadcrumb">
            <li><a href="/" itemprop="url">Главная</a></li>
            <li class="active">Контакты</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-md-12">
          <h3 class="contact-title title-3 title-center">Контакты</h3>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->
    <div class="contact-items">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="contact-item">
              <h4 class="contact-item-title title-4">Телефоны:</h4>
              <ul itemscope itemtype="http://schema.org/Thing">
                <li itemprop="description">+38 (050) 728 76 54</li>
                <li itemprop="description">+38 (067) 938 71 09</li>
                <li itemprop="description">+38 (096) 456 77 77</li>
              </ul>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="contact-item">
              <h4 class="contact-item-title title-4">Мессенджеры:</h4>
              <ul itemscope itemtype="http://schema.org/Thing">
                <li itemprop="description">Skype: name_123</li>
                <li itemprop="description">Telegram: login456</li>
              </ul>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="contact-item" itemscope itemtype="http://schema.org/Thing">
              <h4 class="contact-item-title title-4">Почта для заказов:</h4>
              <a href="mailto:mail@randomail.ru" itemprop="url">mail@randomail.ru</a>
            </div>
            <div class="contact-item" itemscope itemtype="http://schema.org/Thing">
              <h4 class="contact-item-title title-4">Вопросы и предложения:</h4>
              <a href="mailto:allsystems@gmail.com" itemprop="url">allsystems@gmail.com</a>
            </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container -->
    </div>
    <!-- /.contact-items -->
    <div class="scl">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="scl-item" itemscope itemtype="">
              <img src="/templates/images/scl-img.jpg" alt="" itemprop="image">
            </div>
          </div>
          <!-- /.col -->
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="scl-item" itemscope itemtype="">
              <img src="/templates/images/scl-img.jpg" alt="" itemprop="image">
            </div>
          </div>
          <!-- /.col -->
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="scl-item" itemscope itemtype="">
              <img src="/templates/images/scl-img.jpg" alt="" itemprop="image">
            </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container -->
    </div>
    <!-- /.scl -->
  </div>
  <!-- /.contact-content -->

  <!-- /.section-contact -->

<!-- /.spanning-columns -->
</div>
<!-- /.main -->
