<?php
class SimpleImage {

   var $image;
   var $image_type;
   var $original_width = 0;
   var $original_height = 0;
   var $add_watermark = FALSE;
   var $watermak_path = "";

   function SimpleImage($add_watermark = FALSE)
   {
	   $this->add_watermark = $add_watermark;
   }

   function load($filename) {
      $image_info = getimagesize($filename);
      $this->image_type = $image_info[2];
      if( $this->image_type == IMAGETYPE_JPEG ) {
         $this->image = imagecreatefromjpeg($filename);
      } elseif( $this->image_type == IMAGETYPE_GIF ) {
         $this->image = imagecreatefromgif($filename);
      } elseif( $this->image_type == IMAGETYPE_PNG ) {
         $this->image = imagecreatefrompng($filename);
      }
   }
   function save($filename, $image_type=IMAGETYPE_JPEG, $compression=100, $permissions=null) {
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image,$filename,$compression);
      } elseif( $image_type == IMAGETYPE_GIF ) {
         imagegif($this->image,$filename);
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagepng($this->image,$filename);
      }
      if( $permissions != null) {
         chmod($filename,$permissions);
      }
   }
   function output($image_type=IMAGETYPE_JPEG) {
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image);
      } elseif( $image_type == IMAGETYPE_GIF ) {
         imagegif($this->image);
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagepng($this->image);
      }
   }
   function getWidth() {
      return imagesx($this->image);
   }
   function getHeight() {
      return imagesy($this->image);
   }

   function addWatermark()
   {

		if($this->add_watermark)
		{
			$wmpath = _FILES_PATH.'watermark.png';
			if(!empty($this->watermak_path))
			{
				$wmpath = $this->watermak_path;
			}
			if(file_exists($wmpath))
			{
				$watermark = imagecreatefrompng($wmpath);
				$watermark_width = imagesx($watermark);
				$watermark_height = imagesy($watermark);
				$original_width = $this->getWidth();
				$original_height = $this->getHeight();
				if($watermark_width >=  $original_width)
				{
					$old_watermark_width = $watermark_width;
					$old_watermark_height = $watermark_height;

					$watermark_width = $original_width - ($original_width*0.2);

					$k = $watermark_width/$old_watermark_width;
					$watermark_height = $watermark_height*$k;

					imagealphablending($watermark, false);
					imagesavealpha($watermark, true);

					$newImg = imagecreatetruecolor($watermark_width, $watermark_height);
					imagealphablending($newImg, false);
					imagesavealpha($newImg,true);
					$transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
					imagefilledrectangle($newImg, 0, 0, $watermark_width, $watermark_height, $transparent);
					imagecopyresampled($newImg, $watermark, 0, 0, 0, 0, $watermark_width, $watermark_height,$old_watermark_width,$old_watermark_height);
					$watermark = $newImg;
				}

				$dest_x = ceil($original_width - $watermark_width)/2;
				$dest_y = ceil($original_height - $watermark_height)/2;
				imagecopy($this->image, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height);
			}
		}
   }

   function resizeToHeight($height) {
	  $originalHeight = 	$this->getHeight();

	  if($height > $originalHeight)
	  {
		$height = $originalHeight;
	  }
  $ratio = $height / $originalHeight;
      $width = $this->getWidth() * $ratio;
      $this->resize($width,$height);
   }
   function resizeToWidth($width) {
	  $originalWidth = 	$this->getWidth();

	  if($width > $originalWidth )
	  {
		$width = $originalWidth ;
	  }
$ratio = $width / $originalWidth ;
      $height = $this->getheight() * $ratio;
      $this->resize($width,$height);
   }
   function scale($scale) {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100;
      $this->resize($width,$height);
   }
   function resize($width,$height) {

	 $new_image = imagecreatetruecolor($width, $height);
	  $this->addWatermark();
	  //watermarked

	  	if ( $this->image_type == IMAGETYPE_PNG || $this->image_type == IMAGETYPE_GIF )
		{
			$trnprt_indx = imagecolortransparent($this->image);

			// If we have a specific transparent color
			if ($trnprt_indx >= 0) {

			// Get the original image's transparent color's RGB values
			$trnprt_color    = imagecolorsforindex($this->image, $trnprt_indx);

			// Allocate the same color in the new image resource
			$trnprt_indx    = imagecolorallocate($new_image, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);

			// Completely fill the background of the new image with allocated color.
			imagefill($new_image, 0, 0, $trnprt_indx);

			// Set the background color for new image to transparent
			imagecolortransparent($new_image, $trnprt_indx);


			}
			// Always make a transparent background color for PNGs that don't have one allocated already
			elseif ($this->image_type == IMAGETYPE_PNG) {


			// Turn off transparency blending (temporarily)
			imagealphablending($new_image, false);

			// Create a new transparent color for image
			$color = imagecolorallocatealpha($new_image, 0, 0, 0, 127);

			// Completely fill the background of the new image with allocated color.
			imagefill($new_image, 0, 0, $color);

			// Restore transparency blending
			imagesavealpha($new_image, true);
			}
		}
	  $original_width = $this->getWidth();
	$original_height = $this->getHeight();

	if($original_width < $width || $original_height < $height)
	{

	}
	else
	{

		imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $original_width, $original_height);
		$this->image = $new_image;
	}


   }
	function crop($crop = 'square',$percent = false) {

		$w_i = $this->getWidth();
		$h_i = $this->getHeight();
		if ($crop == 'square') {
			$min = $w_i;
			if ($w_i > $h_i) $min = $h_i;
			$w_o = $h_o = $min;
		} else {
			list($x_o, $y_o, $w_o, $h_o) = $crop;
			if ($percent) {
				$w_o *= $w_i / 100;
				$h_o *= $h_i / 100;
				$x_o *= $w_i / 100;
				$y_o *= $h_i / 100;
			}
			if ($w_o < 0) $w_o += $w_i;
				$w_o -= $x_o;
			if ($h_o < 0) $h_o += $h_i;
			$h_o -= $y_o;
		}

		if($w_o > $w_i)
		{
			$w_o =  $w_i;
		}
		if($h_o > $h_i)
		{
			$h_o =  $h_i;
		}
		$new_image = imagecreatetruecolor($w_o, $h_o);

		if($x_o == 0 && $y_o == 0)
		{
			$x_o = ceil($w_i  - $w_o)/2;
			$y_o = ceil($h_i  - $h_o)/2;
		}
		imagecopy($new_image , $this->image, 0, 0, $x_o, $y_o, $w_o, $h_o);
		$this->image = $new_image;
	}

}

	function getImageById($id,$params = array())
	{
		if(count($params) > 0)
		{
			$resizeFormat = "";
			if(isset($params["height"]) && !isset($params["width"]))
			{
				$resizeFormat = "h";
			}
			else if(!isset($params["height"]) && isset($params["width"]))
			{
				$resizeFormat = "w";
			}
			else if(isset($params["height"]) && isset($params["width"]))
			{
				$resizeFormat = "wh";
			}

			if(isset($params["k"]))
			{
				$resizeFormat = "k";
			}



			$filename = FALSE;



			$img = dbGetRow("SELECT id,filename,width,height,mime FROM " . _DB_TABLE_PREFIX . "images WHERE id = :id",array(":id"=>$id));

			$thumbName = '';

      if(!empty($params['name']))
      {
        $thumbName = create_urlname($params['name']);
      }
      else {

        	$thumbName =  $id;
      }

			switch($resizeFormat)
			{
				case "h":
					$thumbName .=  "-0x" . $params["height"];
				break;
				case "w":
					$thumbName .=  "-" . $params["width"] . "x0";
				break;
				case "wh":
					$thumbName .=  "-" . $params["width"] . "x" . $params["height"];
					break;
				case "k":
					$thumbName .=  "-k" . $params["k"];
					break;
			}
			if(isset($params["crop"]))
			{
				$thumbName .= '-' . implode('-',$params["crop"]);
			}
			$thumbName .=  "." . $img["mime"];
			if(file_exists(_THUMBS_PATH . $thumbName))
			{

				$filename =  _THUMBS_URL . $thumbName;
			}
			else
			{
				if(file_exists(_IMAGES_PATH . $img["filename"]))
				{
					$image = new SimpleImage();

					if(isset($params["watermark"]))
					{
						$image->add_watermark = TRUE;
						$image->watermak_path = $params["watermark"];
					}


					$image->load(_IMAGES_PATH . $img["filename"]);

					switch($resizeFormat)
					{
						case "h":
							$image->resizeToHeight($params["height"]);
							//$thumb =  $id . "-0x" . $params["height"] . "." . $img["mime"];
						break;
						case "w":
							$image->resizeToWidth($params["width"]);
							//$thumb =  $id . "-" . $params["width"] . "x0." . $img["mime"];
						break;
						case "wh":
							//надо подумать
							//$thumb =  $id . "-" . $params["width"] . "x" . $params["height"] . "." . $img["mime"];
							break;
						case "k":
							if($image->getWidth() > $image->getHeight())
							{
								$image->resizeToWidth($params["k"]);
							}
							else if($image->getWidth() < $image->getHeight())
							{
								$image->resizeToHeight($params["k"]);
							}
							else
							{
								$image->resize($params["k"],$params["k"]);
							}
							//$thumb =  $id . "-k" . $params["k"] . "." . $img["mime"];
							break;
					}

					if(isset($params["crop"]))
					{
						$image->crop($params["crop"]);
					}

					$image->save(_THUMBS_PATH . $thumbName,$image->image_type);
					$filename =  _THUMBS_URL . $thumbName;
				}
			}

			return $filename;

		}
		else
		{
			$filename = dbGetOne("SELECT filename FROM " . _DB_TABLE_PREFIX . "images WHERE id = :id",array(":id"=>$id));
			if($filename != "")
			{
				return  _IMAGES_URL . $filename;
			}
			else
			{
				return FALSE;
			}
		}
	}

	function getImages($id,$source)
	{
		return dbQuery('SELECT * FROM #__images WHERE source = :source AND parentid = :parentid',array(':source'=>$source,':parentid'=>$id));
	}

	function getImage($id)
	{
		return dbGetRow('SELECT * FROM #__images WHERE id = :id LIMIT 0,1',array(':id'=>$id));
	}

	function rmImageById($id)
	{
		$filename = dbGetOne("SELECT filename FROM " . _DB_TABLE_PREFIX . "images WHERE id = :id",array(":id"=>$id));
		if($filename != "")
		{
			if(file_exists(_IMAGES_PATH . $filename))
			{

				unlink(_IMAGES_PATH . $filename);
			}
		}
		dbNonQuery("DELETE FROM " . _DB_TABLE_PREFIX . "images WHERE id = :id",array(":id"=>$id));
	}

	function getImageUrlById($id)
	{
		$path = dbGetOne('SELECT filename FROM #__images WHERE id = :id LIMIT 0,1',array(':id'=>$id));

		if(!empty($path))
		{
			return _IMAGES_URL . $path;
		}
		else
		{
			return false;
		}
	}
