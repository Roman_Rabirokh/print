<?php

class User
{

	function User()
	{

	}

	function AddNew($email,$password,$login_after = false,$send_mail = false)
	{
		$controlstring = md5($email.$password.rand(0,10000).rand(0,10000));
		$id = dbNonQuery("INSERT INTO ". _DB_TABLE_PREFIX ."users (email,passw,controlstring,created) VALUES(:email,:passw,:controlstring,now())",array(
			":email"=>$email,
			":passw"=>md5($password._PASSWORD_SOLR),
			":controlstring"=>$controlstring
		),TRUE);
		if($id > 0)
		{
			global $app;
			if($send_mail)
			{
				$activate_link = "http://" . _SITE . "/component/load/system/register?action=activate&code=" . $controlstring;
				$app->sendEmailTemplate(_SITE,_DEFAULT_EMAIL_FROM,"",$email,"
				Уведомление о регистрации на сайте " . _SITE,"_register.tpl",array("#EMAIL#"=>$email,"#PASSW#"=>$password,"#LINK#"=>$activate_link,"#SITE#"=>_SITE ));
			}
			if($login_after)
			{
				$this->AuthorizeByID($id);
			}

			$events = $app->getEventHandlers("afterUserCreated");
			foreach($events as $event)
			{
				call_user_func($event, $id, $_REQUEST);
			}

			return true;
		}
		else
		{
			return false;
		}
	}

	function sendActivateMessage()
	{
		global $app;
		$controlstring = dbGetOne("SELECT controlstring FROM ". _DB_TABLE_PREFIX ."users WHERE id = :userid ", array(":userid"=>$this->getID()));
		if($controlstring != "")
		{
			$activate_link = "http://" . _SITE . "/component/load/system/register?action=activate&code=" . $controlstring;

			$app->sendEmailTemplate(_SITE,_DEFAULT_EMAIL_FROM,"",$this->getEmail(),"
				Активация аккаунта на сайте " . _SITE,"_activate.php",array("#LINK#"=>$activate_link,"#SITE#"=>_SITE ));
		}
	}

	function Authorized()
	{
		if(isset($_SESSION["CURRENT_USER"]))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function IsAdmin()
	{
		if(isset($_SESSION["user"]))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function Authorize($email,$password)
	{
		$result = false;
		$data = dbGetRow("SELECT * FROM ". _DB_TABLE_PREFIX ."users WHERE email = :email AND passw = :passw AND blocked = 0",array(
			":email"=>$email,
			":passw"=>md5($password._PASSWORD_SOLR)
		));
		if(!empty($data["id"]))
		{
			dbNonQuery("UPDATE ". _DB_TABLE_PREFIX ."users SET last_login = now(),last_ip = :last_ip WHERE id = :id",array(
				":last_ip"=>$_SERVER["REMOTE_ADDR"],
				":id"=>$data["id"]
			));
			$_SESSION["CURRENT_USER"] = array("ID"=>$data["id"],"EMAIL"=>$data["email"]);
			$result = true;
		}
		return $result;

	}

	function AuthorizeByID($id)
	{
		$result = false;
		$data = dbGetRow("SELECT * FROM ". _DB_TABLE_PREFIX ."users WHERE id = :id",array(
			":id"=>$id));
		if(!empty($data["id"]))
		{
			dbNonQuery("UPDATE ". _DB_TABLE_PREFIX ."users SET last_login = now(),last_ip = :last_ip WHERE id = :id",array(
				":last_ip"=>$_SERVER["REMOTE_ADDR"],
				":id"=>$data["id"]
			));
			$_SESSION["CURRENT_USER"] = array("ID"=>$data["id"],"EMAIL"=>$data["email"]);
			$result = true;
		}
		return $result;
	}

	function AuthorizeBySocial($code)
	{

	}
	function Logout()
	{
		session_destroy();
	}
	function getEmail()
	{
		if($this->Authorized())
		{
			return $_SESSION["CURRENT_USER"]["EMAIL"];
		}
		else
		{
			return "";
		}
	}
	function getID()
	{
		if($this->Authorized())
		{
			return $_SESSION["CURRENT_USER"]["ID"];
		}
		else
		{
			return "";
		}
	}
	function checkPassword($pwd,$send_mail = TRUE)
	{
		$currentPassword = dbGetOne("SELECT passw FROM ". _DB_TABLE_PREFIX ."users WHERE id = :id",array(":id"=>$this->getID()));
		if($currentPassword == md5($pwd))
		{
			if($send_mail)
			{
				global $app;
				$app->sendEmailTemplate(_SITE,_DEFAULT_EMAIL_FROM,"",$this->getEmail(),"
				Вы успешно изменили пароль на сайте " . _SITE,"_new_passw.php",array("#PASSWORD#"=>$pwd ));
			}

			return true;
		}
		else
		{
			return false;
		}
	}
	function updatePasswordByUser($pwd)
	{
		dbNonQuery("UPDATE " . _DB_TABLE_PREFIX . "users SET
		passw = :passw,
		controlstring = :controlstring
		WHERE id = :id
		",array(
		":passw"=>md5($pwd . _PASSWORD_SOLR),
		":controlstring"=>md5($this->getID().$pwd.rand(0,10000).rand(0,10000)),
		":id"=>$this->getID()
		));
	}

	function updatePassword($controlstring)
	{
		$findUser = dbGetOne("SELECT id FROM " . _DB_TABLE_PREFIX . "users WHERE controlstring = :controlstring",array(":controlstring"=>$controlstring));
		if($findUser != "")
		{
			$newpassword =  random_password();
			dbNonQuery("UPDATE " . _DB_TABLE_PREFIX . "users SET
			passw = :passw,
			controlstring = :controlstring
			WHERE id = :id
			",array(
				":passw"=>md5($newpassword . _PASSWORD_SOLR),
				":controlstring"=>md5($findUser.$newpassword.rand(0,10000).rand(0,10000)),
				":id"=>$findUser
			));
			return $newpassword;
		}
		else
		{

			return false;
		}

	}


	function Activate($controlstring)
	{
		$findUser = dbGetOne("SELECT id FROM " . _DB_TABLE_PREFIX . "users WHERE controlstring = :controlstring AND active = 0",array(":controlstring"=>$controlstring));
		if($findUser != "")
		{
			dbNonQuery("UPDATE " . _DB_TABLE_PREFIX . "users
			SET active = 1 ,controlstring = :controlstring
			WHERE id = :id
			",array(":controlstring"=>md5($findUser.rand(0,10000).rand(0,10000)),
			":id"=>$findUser));

			return true;
		}
		else
		{
			return false;
		}
	}

	function isActivated()
	{
		return dbGetOne("SELECT active FROM " . _DB_TABLE_PREFIX . "users WHERE id = :id",array(
		":id"=>$this->getID()
		));
	}

	function getProfileImage($id,$params = array())
	{
		$imid = dbGetOne("SELECT id FROM " . _DB_TABLE_PREFIX . "images WHERE source = 1 AND parentid = :parentid",array(
			":parentid"=>$id
		));
		if($imid != "")
		{
			return getImageById($imid,$params);
		}
		else
		{
			return FALSE;
		}
	}
	function rmProfileImage($id)
	{
		$imid = dbGetOne("SELECT id FROM " . _DB_TABLE_PREFIX . "images WHERE source = 1 AND parentid = :parentid",array(
			":parentid"=>$id
		));
		if($imid != "")
		{
			rmImageById($imid);
		}
	}
}
