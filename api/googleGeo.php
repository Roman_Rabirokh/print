<?php

/**
 * Description of GoogleGeo
 *
 * @author Roman Rabirokh
 */
class GoogleGeo
{
    private $key='AIzaSyA9tDQKMO_OKt1EOslq6Lo80X-MHHZ76rY';
    private $url='https://maps.googleapis.com/maps/api/geocode/json?';
    private $language='ru';
    private $address;
    
    
    
    public function getAddress($address)
    {
        //$address='48.794666,2.286448';
        $params='latlng='.$address.'&language='.$this->language.'&key='.$this->key;
         if( $curl = curl_init() ) {
            
            curl_setopt($curl, CURLOPT_URL, $this->url.$params);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            $out = curl_exec($curl);
            $this->address=$out;
            curl_close($curl);
          }
          
          $result= (array)json_decode($this->address);
          $result=(array)$result['results'];
          $result=(array)$result[0];
          $result=$result['formatted_address'];
          $this->address=$result;
//          foreach($result as $rez)
//          {
//                $rez=(array)$rez;
//                print_r('<pre>');
//                print_r($rez['formatted_address']);
//                print_r('</pre>');
//          }
          return $this->address;
          
          
    }
}
