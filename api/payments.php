<?php
/**
 * Description of privat24
 *
 * @author Roman Rabirokh
 */
class Payments
{
    private static $publicKey='i84598924454';
    private static $privateKey='S4ZKt5cz271cQ6LGDFQQbzObMSxPvXAtL3jLOy3G';
    public  static function getForm($amount,$orderId)
    {
        $data=
        [
            'version'=> '3',
            'action' => 'pay',
            'amount' => $amount,
            'currency'=> 'UAH',
            'description' =>  'Оплата заказа №'.$orderId,
            'order_id' => $orderId,
            'language' => 'ru',
            'server_url' => 'https://www.yashaprint.com/liqpay/pay/',
            'result_url' => 'https://www.yashaprint.com/personal/orders/',
            'sandbox' => '1',
        ];
        $pay= new LiqPay(self::$publicKey, self::$privateKey);
        return $pay->cnb_form($data);
    }
    
    public function setPay()
    {
        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/LOGS.txt', json_encode($_POST));
        $signaturePost=!empty($_POST['signature']) ? $_POST['signature'] : 0;
        $data=!empty($_POST['data']) ? $_POST['data'] : 0;
        if($signaturePost || $data)
            $this->logLiqPay($signaturePost,$data);
        
        if($signaturePost && $data)
        {   
            $sign = base64_encode( sha1( self::$privateKey . $data . self::$privateKey , 1 ));
            
//            if($sign==$signaturePost)
//            {
            
                $data= base64_decode($data);
                $data= json_decode($data);
                $data=(array)$data;
                
                if($data['status']=='sandbox' || $data['status']=='success')
                {
                    if(!empty($data['order_id']))
                    {
                        $order= Orders::get_info_oreder($data['order_id']);
                        if(!empty($order))
                        {
                            if($order['cost']==$data['amount'])
                            {
                                Orders::set_pay_order($data['order_id']);
                            }
                        }
                    }
                    
                }
                
//            }
        }
    }
    
    private function logLiqPay($signature, $data)
    {
        $data= base64_decode($data);
        dbNonQuery('INSERT INTO #__liqpay_logs (cdate,data,signature) VALUES(NOW(), :data, :sig)',
                [
                    ':data' => $data,
                    ':sig' => $signature,
                ]);
    }
}
