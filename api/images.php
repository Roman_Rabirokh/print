<?php

/**
 * Description of Images
 *
 * @author Roman Rabirokh
 */
class Images
{

    private $image;
    private $imagePath;
    private $info;
    private $id;
    private $colorScheme=[0 => 'UNDEFINED', 1 =>'RGB', 2 =>'GRAY', 3 =>'TRANSPARENT', 4=>'OHTA', 5=>'LAB',6=>'XYZ ',7=>'YCBCR',8=>'YCC',9=>'YIQ',10=>'YPBPR',11=>'YUV',12=>'CMYK',13=>'SRGB',14=>'HSB',15=>'HSL', 16=>'HWB',17=>'REC601LUMA',18=>'REC709LUMA', 19=>'LOG', 20=>'CMY'];

    public function __construct($imagePath, $id)
    {
        $this->image = new Imagick($imagePath);
        $this->id = $id;
        $this->imagePath=$imagePath;
        $this->checkDPI();
    }

    public function saveInfo()
    {
        $this->getInfo();
        if(empty($this->info['colorScheme']))
            $this->info['colorScheme']='';
        if(empty($this->info['exif']['exif:DateTimeOriginal']))
            $this->info['exif']['exif:DateTimeOriginal']=0;
        if( empty($this->info['quality']))
             $this->info['quality']=0;
        if( empty($this->info['position']['Longitude']))
             $this->info['position']['Longitude']=0;
        if( empty($this->info['position']['Latitude']))
             $this->info['position']['Latitude']=0;
        if(empty($this->info['position']['address']))
                  $this->info['position']['address']='';
        
        dbNonQuery('INSERT INTO #__photo_orders_rows_info '
                . '(id,color_scheme,create_date,dpi,quality,type,height,width,longitude,latitude,address) '
                . 'VALUES ( :id, :color, :create, :dpi, :quality, :type, :height, :width,:longitude,:latitude,:address)', 
                [
                    ':id' => $this->id, 
                    ':color' => $this->info['colorScheme'],
                    ':create' => $this->info['exif']['exif:DateTimeOriginal'],
                    ':dpi' => $this->info['dpi']['x'],
                    ':quality' => $this->info['quality'],
                    ':type' => $this->info['type'],
                    ':height' => $this->info['resolution']['height'],
                    ':width' => $this->info['resolution']['width'],
                    ':longitude'=>  $this->info['position']['Longitude'],
                    ':latitude'=>   $this->info['position']['Latitude'],
                    ':address' =>   $this->info['position']['address'],
                ]);
    
    }

    public function getInfo()
    {
        $this->info['exif'] = $this->image->getImageProperties("exif:*");
        $this->getLongLat();
        $this->info['colorScheme'] = $this->image->getImageColorspace ();
        $this->info['colorScheme']= $this->colorScheme[$this->info['colorScheme']];
        $this->info['dpi'] = $this->image->getImageResolution();
        //$this->Dpi();
        $this->info['resolution']=$this->image->getImageGeometry();
        $this->info['type']=$this->image->getImageMimeType();
        if($this->info['type']=='image/x-jpeg')
            $this->info['quality']=$this->image->getImageCompressionQuality();       
        if(!empty($this->info['position']['Longitude']) && !empty($this->info['position']['Latitude']))
        {
            $geo=new GoogleGeo();
            $this->info['position']['address']=$geo->getAddress($this->info['position']['Latitude'].','.$this->info['position']['Longitude']);
            
        }
        return $this->info;
    }

//    private function Dpi()
//    {
//        if (!empty($this->info['dpi']['y']))
//        {
//            $this->info['dpi']['y'] = round($this->info['dpi']['y'] * 2.54, 2);
//        }
//
//        if (!empty($this->info['dpi']['x']))
//        {
//            $this->info['dpi']['x'] = round($this->info['dpi']['x'] * 2.54, 2);
//        }
//    }
    private function getLongLat()
    {
        if( !empty($this->info['exif']['exif:GPSLongitude']) && !empty($this->info['exif']['exif:GPSLatitude']) && !empty($this->info['exif']['exif:GPSLongitudeRef']) && !empty($this->info['exif']['exif:GPSLatitudeRef']))
        {
            $egeoLong = explode(',',$this->info['exif']['exif:GPSLongitude']);
            $egeoLat =   explode(',',$this->info['exif']['exif:GPSLatitude']);
            $egeoLongR =  $this->info['exif']['exif:GPSLongitudeRef'];
            $egeoLatR =  $this->info['exif']['exif:GPSLatitudeRef'];
            $this->info['position']['Longitude'] = $this->toDecimal((int)$egeoLong[0], (int)$egeoLong[1], $egeoLong[2], $egeoLongR);
            $this->info['position']['Latitude'] = $this->toDecimal((int)$egeoLat[0], (int)$egeoLat[1], $egeoLat[2], $egeoLatR);
        }
        

    }
    
    private function toDecimal($deg, $min, $sec, $hem) 
{
        @$d = $deg + ((($min/60) + ($sec/3600))/100);
        return ($hem=='S' || $hem=='W') ? $d*=-1 : $d;
}

    public static function DPI($widthI,$heightI,$widthP,$heightP)
    {
        $dI=pow($widthI, 2)+pow($heightI, 2);
        $dI=sqrt($dI);
        $dP=pow($widthP, 2)+pow($heightP, 2);
        $dP=sqrt($dP);
        $dP=$dP/2.54;
        return ceil($dI/$dP);
        
    }
    private function checkDPI()
    {
        $widthP=95;
        $heightP=135;
        $maxDpi=450;
        $DpiNeeded=300;
        $this->getInfo();
        $dpi= self::DPI($this->info['resolution']['width'], $this->info['resolution']['height'], $widthP, $heightP);
        if($dpi>$maxDpi)
        {
            $widthI=$this->info['resolution']['width'];
            $heightI=$this->info['resolution']['height'];
            $step=100;// 100 px
            while($dpi>$DpiNeeded)
            {
                $widthI-=$step;
                $heightI-=$step;
                $dpi= self::DPI($widthI, $heightI, $widthP, $heightP);
            }
            $image = new SimpleImage();
            $image->load($this->imagePath);
            $image->resizeToHeight($heightI);
            $image->save($this->imagePath,$image->image_type);
            $this->image = new Imagick($this->imagePath);
        }
        
    }

}
