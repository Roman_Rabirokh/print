<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/modules/photosite/social-api/vkPhp/classes/VkPhpSdk.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/modules/photosite/social-api/vkPhp/classes/Oauth2Proxy.php';


// Init OAuth 2.0 proxy
$oauth2Proxy = new Oauth2Proxy(
    '5610507', // client id
    'ktwoiQ8zHQNthDUIENsF', // client secret
    'https://oauth.vk.com/access_token', // access token url
    'https://oauth.vk.com/authorize', // dialog uri
    'code', // response type
    'http://photo.devtech.com.ua/download.php', // redirect url
	'notify,friends,photos,audio,video,wall' // scope
);

// Try to authorize client
if($oauth2Proxy->authorize() === true)
{
	// Init vk.com SDK
	$vkPhpSdk = new VkPhpSdk();
	$vkPhpSdk->setAccessToken($oauth2Proxy->getAccessToken());
	$vkPhpSdk->setUserId($oauth2Proxy->getUserId());


	//get user vk id
	$My_id=$vkPhpSdk->getUserId();

//get album id

$album= explode("_", $_POST['album_id']);



	// API call - get photos
	$result = $vkPhpSdk->api('photos.get', array(
		'owner_id' => $My_id,
		'album_id' => $album['1']
	));

$i=0;

if ($result) {
	foreach ($result['response'] as $key => $value) {
		echo('<a href="'.$value['src_big'].'">'.$value['src_big'].'</a></br>');

		//save file
		$url = $value['src_big'];
		$dir = __DIR__ . "/photos"; // Full Path
		$name = 'image'.$i.'.jpg';

		is_dir($dir) || @mkdir($dir) || die("Can't Create folder");
		copy($url, $dir . DIRECTORY_SEPARATOR . $name);
		$i=$i+1;
	}

echo 'Файлы загружены в папку Photos';
}
else {
	echo 'Изображения отсутствуют или ссылка на альбом дана не верно';
}


}
else {
	echo 'Ошибка авторизации';
}
