<?php 
$MSG = array();
$MSG["ERROR_TOKEN"] = "Время сессии истекло. Необходимо обновить страницу.";
$MSG["ERROR_EMPTY_FIELD"] = "Необходимо заполнить все поля !";
$MSG["ERROR_EQUAL_PASSWORD_FIELDS"] = "Поля 'Пароль' и 'Повторите пароль' не совпадают !";
$MSG["PASSWORD_WRONG_LENGTH"] = "Длина пароля меньше 7 символов !";
$MSG["ERROR_CURRENT_PASSWORD"] = "Вы ввели неправильный текущий пароль !";
$MSG["PASSWORD_UPDATE_SUCCESS"] = "Пароль успешно обновлён !";


$MSG["CURRENT_PASSWORD"] = "Текущий пароль";
$MSG["NEW_PASSWORD"] = "Новый пароль";
$MSG["RENEW_PASSWORD"] = "Повторите пароль";
$MSG["SAVE_CHANGES"] = "Сохранить изменения";