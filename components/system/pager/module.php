<?php

$data['ALL'] = $params['ALL'];
$data["PAGECOUNT"] = ceil($params['ALL']/$params["PAGESIZE"]);


$data["CURRENT_PAGE"] = (isset($_GET["page"]) && intval($_GET["page"]) > 0 ? intval($_GET["page"]) : 1 );
$data['PAGER_SIZE'] = 10;
$data["INTERVALS"] = ceil($data["PAGECOUNT"]/$data['PAGER_SIZE']);


include("template.php");
