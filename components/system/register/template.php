<?php if(!defined("_APP_START")) { exit(); }
if(!$user->Authorized())
{
?>


<form style="text-align: center;" method="post" id="registerForm">
<div  id="registerMessages"></div>
<h1 style="text-align: center;">Регистрация</h1><span class="close_x_reg">X</span>
	<input type="hidden"  id="tbRToken" value="<?php echo $data["TOKEN"]; ?>" />	
	<div class="form-group">
		<!--<label><?php echo $MSG["CAPTION_R_EMAIL"]; ?>:</label>-->
		<input style="" class="form-control" maxlength="100" type="email" required id="tbREmail" placeholder="e-mail:"  />
	</div>
	<div id="customFields">
		<?php echo $data["CUSTOM_FIELDS"]; ?>
	</div>
	<div class="row">
	<div class="form-group "><!--<label><?php echo $MSG["CAPTION_R_PASSWORD"]; ?>:</label>-->
	<input style="" type="password" class="form-control"  maxlength="100" required  id="tbRPassw" placeholder="пароль:"  />
	</div>
	<div class="form-group "><!--<label><?php echo $MSG["CAPTION_R_REPASSWORD"]; ?>:</label>-->
	<input  style="" class="form-control" type="password"  maxlength="100" required  id="tbRRePassw" placeholder="повторите пароль:" /></div>
	<div class="form-group ">
	<?php $this->includeComponent("system/captcha",array("SHOW"=>TRUE));?><br />
	</div>
	<div class="form-group ">
	<!--<label><?php echo $MSG["CAPTION_R_IMAGECODE"]; ?>:</label>-->
	<input style="" type="number" maxlength="6"  class="form-control" required  id="tbRCode"  /></div>
	</div>

	<input type="button" class="btn btn-primary" id="btnRegister" value="Регистрация" />
</form>
<?php } else {
	
	redirect("/personal/");
} 