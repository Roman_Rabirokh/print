<?php if(!defined("_APP_START")) { exit(); } 

include("messages.php");
if(isset($params["action"]) && $params["action"] == "register" && isset($params["IS_REQUEST"]))
{
	
	$result = array("RESULT"=>FALSE,"MESSAGE"=>"");
	
	$validateToken = $this->checkToken("rf",$params["token"]);
	if(!$validateToken)
	{
		$result["MESSAGE"] = $MSG["ERROR_TOKEN"];	
	}
	else
	{
		$validateCode = $this->includeComponent("system/captcha",array("CODE"=>$params["code"]));
		
		if(!$validateCode)
		{
			$result["MESSAGE"] = $MSG["ERROR_CODE"];		
		}
		else
		{
			if(!validateEmail($params["email"]))
			{
				$result["MESSAGE"] = $MSG["ERROR_EMAIL"];	
			}
			else
			{
				
				
				
				$findEmail = dbGetOne("SELECT id FROM " . _DB_TABLE_PREFIX . "users WHERE email = :email",array(":email"=>$params["email"]));
				if($findEmail != "")
				{
					$result["MESSAGE"] = $MSG["EMAIL_EXISTS"];
				}
				else
				{
					
					if($params["password"] != $params["repassword"])
					{
						$result["MESSAGE"] = $MSG["PASSWORD_NOT_REPASSWORD"]; 
					}
					else
					{
						if(strlen($params["password"]) < 7)
						{
							$result["MESSAGE"] = $MSG["EMAIL_WRONG_LENGTH"];
						}
						else
						{
							
							$events = $this->getEventHandlers("beforeUserCreated");
							
							$resultData = TRUE;
							foreach($events as $event)
							{
								$resultData = call_user_func($event,$params,$result["MESSAGE"]);	
							}
											
							if($resultData)
							{
								$id=$user->AddNew($params["email"],$params["password"],FALSE,TRUE);
								$result["RESULT"] = TRUE;
								$result["MESSAGE"] = $MSG["SUCCESS_REGISTER"];
                                                                $user->AuthorizeByID($id);
                                                               
							}
						}
					}	
				}
			}
		}
	}
	echo json_encode($result);
}
else if(isset($params["action"]) && $params["action"] == "activate")
{
	$result = $user->Activate($params["code"]);
	if($result)
	{
		$_SESSION["SUCCESS_ACTIVATED"] = $MSG["SUCCESS_ACTIVATED"];
		header("Location: /personal");
		exit();
	}
	else
	{
		echo "Wrong request.";
	}
}
else
{
	$data["TOKEN"] = $this->createFormToken("rf");
	$data["CUSTOM_FIELDS"] = "";
	$page->addJS($this->getComponentPath($name) . "script.js");
	$events = $this->getEventHandlers("registerExtendTemplate");
	foreach($events as $event)
	{
		$data["CUSTOM_FIELDS"] = call_user_func($event);	
	}
	if(file_exists(_TEMPL . 'register.php'))
	{
		include(_TEMPL . 'register.php');
	}
	else
	{
		include("template.php");
	}
}	