function doLogin()
{
	$("#btnLogin").attr("disabled","disabled");

		$.post("/component/load/system/auth",{
			email:$("#tbEmail").val(),
			passw:$("#tbPassw").val(),
			action:'login'
		},function(data){
			res=data.RESULT;

			if(res)
			{

			//	document.location.reload();
			window.location.href = document.location.origin;

			}
			else
			{
			  $.fancybox({type: "html", content:'Ошибка авторизации !',  wrapCSS: 'alertmes',autoCenter:true,minWidth:300,minHeight:150,overlay: {locked: false}});
				
			}
			$("#btnLogin").removeAttr("disabled");
		},"json");
}

$(function(){
	$("#btnLogin").click(function(){

		doLogin();
		return false;
	});
//	$("#tbPassw").keypress(function(event){
//		var keycode = (event.keyCode ? event.keyCode : event.which);
//		if(keycode == '13'){
//			doLogin();
//		}
//	});
	$("#tbEmail").keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			$("#tbPassw").focus();
		}
	});
	$("#rPassword").click(function(){
		$("#rPassword_block").slideToggle();
		return false;
	});
	$("#btnRestorePassword").click(function(){


		if($("#tbRestoreEmail").val() == "")
		{
			return false;
		}
		$(this).attr("disabled","disabled");

		$.post("/component/load/system/auth",{
			email:$("#tbRestoreEmail").val(),
			action:'restore'
			},function(data){
			if(data.RESULT)
			{
			 $.fancybox({type: "html", content:'Данные для восстановления пароля отправлены на указанный Вами E-mail.',  wrapCSS: 'alertmes',autoCenter:true,minWidth:300,minHeight:150,overlay: {locked: false}});
				

			}
			else
			{
				 $.fancybox({type: "html", content: data.MESSAGE,  wrapCSS: 'alertmes',autoCenter:true,minWidth:300,minHeight:150,overlay: {locked: false}});
			
			}

			$("#btnRestorePassword").removeAttr("disabled");
		},"json");



	});
});
