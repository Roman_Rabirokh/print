<?php if(!defined("_APP_START")) { exit();

}
if(isset($params["action"]))
{
	$result = array("RESULT"=>FALSE,"MESSAGE"=>"");
	if($params["action"] == "login")
	{
		if($user->Authorize($params["email"],$params["passw"]))
		{
			$result ["RESULT"] = TRUE;

		}
	}
	if($params["action"] == "restore")
	{
		include("messages.php");
		if(!validateEmail($params["email"]))
		{
			$result["MESSAGE"] = $MSG["RESTORE_EMAIL_ERROR"];
		}
		else
		{
			$findEmail = dbGetRow("SELECT email,controlstring FROM " . _DB_TABLE_PREFIX . "users WHERE email = :email",array(":email"=>$params["email"]));
			if($findEmail["email"] != "")
			{
				$url = "https://"._SITE . "/component/load/system/restore.password?control=" . $findEmail["controlstring"];
				$this->sendEmailTemplate(_SITE,_DEFAULT_EMAIL_FROM,"",$findEmail["email"],$MSG["RESTORE_EMAIL_SUBJECT"] . _SITE,"_restore.php",array("#URL#"=>$url));
				$result["RESULT"] = TRUE;
			}
			else
			{
				$result["MESSAGE"] = $MSG["RESTORE_EMAIL_NOT_FOUND"];
			}
		}
	}
	if ( $params["action"] == "logout")
	{
		$user->Logout();
		redirect("https://" . _SITE);
	}
	echo json_encode($result);
}
else
{
	include("messages.php");
	$page->addJS($this->getComponentPath($name) . "script.js");
	if(file_exists(_TEMPL . 'auth.php'))
	{
		include(_TEMPL . 'auth.php');
	}
	else
	{
		include("template.php");
	}
}
