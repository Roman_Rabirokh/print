<script src="<?php echo $page-> getTemplateUrl(); ?>js/jquery.js"></script>




<script type="text/javascript" >

$( document ).ready(function() {

    $('#delivery-method-3').change(function()
    {
        if ($(this).prop('checked') != false) 
        {
            $('#inputs_cnp').show();
            $('#np_home').val(1);
            getPiceNpDelivery();
        }
        else
        {
            $('#inputs_cnp').hide();
            $('#np_home').val(0);
            getPiceNpDelivery();
        }
        
    });

  //get cities in region for curier
  $("#region_id").change(function() {
      region_id=$(this).val();

      $.ajax({
        type: 'POST',
        url: '/get/cities/',
        data: {
          region_id: region_id
        },
        success: function(response) {
        //  alert(response);

          $('#city_id').attr('disabled', false);
          $('#city_id').html(response);
        }
      });


    });





    //input ratio
    $("form input:radio").click(function() {
      id=$(this).attr('id');
      if (id=="delivery-method-1")
      {
        $("#c_np").show();
        $("#c_c").hide();
        $('#delivery-method-1').attr( 'checked', 'checked' );
        $('#delivery-method-2').removeAttr('checked');
      }
      else{
        $("#c_np").hide();
        $("#c_c").show();
        $('#delivery-method-2').attr( 'checked', 'checked' );
        $('#delivery-method-1').removeAttr('checked');
      }


    });

    //get cities for novaposhta
      $("#region_np").change(function() {

        ref=$(this).val();
        $('#orderdelivery').html(0);
        $.ajax({
          type: 'POST',
          url: '/get/npcities/',
          data: { ref: ref },
          success: function(response) {
            //alert(response);
            rt=response;
            $("#city_np").html(rt);
            $("#office_np").html('<option>Отделение</option></br>');
            //die;
          }
        });

      });

      //get offices for novaposhta
        $("#city_np").change(function() {
          ref=$(this).val();

          $.ajax({
            type: 'POST',
            url: '/get/npoffice/',
            data: { ref: ref },
            success: function(response) {
            //  alert(response); die;
              rt=response;
              $("#office_np").html(rt);
              //die;
            }
          });

        });

        //get offices for novaposhta
          $("#change_curier").change(function() {

            if($('#change_curier').attr('checked')) {


              $('#change_curier').removeAttr('checked');
              $('#curier_profile').hide();
              $('#curier_handle').show();

            } else {

                $('#change_curier').attr( 'checked', 'checked' );
                $('#curier_handle').hide();
                $('#curier_profile').show();
              }


          });

      //make payment
        $(".payment-method-btn").click(function() {
            //alert($(this).attr("id"));
            var id_button=$(this).attr('id');
            //save order & change his status to 1
            fio='<?php if (isset($user_info['fio'])&&!empty($user_info['fio']))  print_r($user_info['fio']) ?>';
            phone='<?php if (isset($user_info['phone'])&&!empty($user_info['phone']))  print_r($user_info['phone'])?>';
            var phone_input=$('#val_phone').val();
            if(phone_input!="" && phone_input!=undefined && phone_input!=null)
                phone=phone_input;
            var fio_input=$('#fio_input').val();
            if(fio_input!="" && fio_input!=undefined && fio_input!=null)
                fio=fio_input;        
            //валидация на наличие данный получателя
            if (fio && phone) {
              //выбираем блок с информацией
              //если новой почтой
                if($('#delivery-method-1').attr('checked')) {
                  region2=$("#region_np").find(":selected").text();
                  city2=$("#city_np").find(":selected").text();
                  npoffice2=$("#office_np").find(":selected").text();
                  cost=$("#ordertotal").text();

                  //если не выбрал куда отправлять
                  if ((region2=="Область")||(city2=="Город")||(npoffice2=="Отделение"))
                  {
					   $.fancybox({type: "html", content:'Заполните, пожалуйста, реквизиты до конца!',  wrapCSS: 'alertmes',autoCenter:true,minWidth:300,minHeight:150,overlay: {locked: false}});
                       return false;
                    
                  }
                  if($('#delivery-method-3').prop('checked') != false)
                  {
                      street_cnp=$('#street_cnp').val();
                      house_cnp=$('#house_cnp').val();
                      flat_cnp=$('#flat_cnp').val();
                      if(street_cnp == '' || street_cnp == false || street_cnp == undefined || 
                              (
                              (house_cnp == '' || house_cnp == false || house_cnp == undefined) && (flat_cnp == '' || flat_cnp == false || flat_cnp == undefined)
                              ))
                      {
                           $.fancybox({type: "html", content:'Заполните, пожалуйста, реквизиты до конца!',  wrapCSS: 'alertmes',autoCenter:true,minWidth:300,minHeight:150,overlay: {locked: false}});
                       return false;
                      }
                      
                  }
                  else
                  {
                       street_cnp=0;
                       house_cnp=0;
                       flat_cnp=0;
                  }

                  //обновляем информацию в ордере
                  $.ajax({
                    type: 'POST',
                    url: '/update/orderadressnp/',
                    data: {
                      region: region2,
                      city:city2,
                      npoffice: npoffice2,
                      orderid: <?php print_R($_GET['id']); ?>,
                      userid:<?php	print_R($user->getid());?>,
                      cost: cost,
                      phone: phone,
                      fio: fio,
                      street:street_cnp,
                      house:house_cnp,
                      flat:flat_cnp


                    },
                    success: function(response) {
                      //alert(response); die;
                      //window.location.replace("/personal/orders/");
                      //die;
                      if(id_button=='privat24')
                           $('input[name=btn_text]').click();

                    }
                  });


                }
              //если курьером
              if($('#delivery-method-2').attr('checked')) {
                //берем данные из Профиля
                if($('#change_curier').attr('checked')) {
                  //обновляем информацию в ордере


                  region='<?php if (isset($user_info['region'])&&!empty($user_info['region'])) print_r($user_info['region']); else echo ''; ?>';

                  city='<?php if  (isset($user_info['city'])&&!empty($user_info['city'])) print_r($user_info['city']); else echo '';?>';
                  street='<?php if  (isset($user_info['street'])&&!empty($user_info['street']))  print_r($user_info['street']);else echo ''; ?>';
                  house='<?php if  (isset($user_info['house'])&&!empty($user_info['house']))  print_r($user_info['house']); else echo ''; ?>';
                  flat='<?php if  (isset($user_info['flat'])&&!empty($user_info['flat'])) print_r($user_info['flat']); else echo ''; ?>';
                  phone='<?php if  (isset($user_info['phone'])&&!empty($user_info['phone']))  print_r($user_info['phone']); else echo ''; ?>';




                }
                //берем заполненные данные (кроме ФИО и телефона, они по-любому должны быть в профайле)
                else{
                  region=$("#region_id").find(":selected").text(),
                  city=$("#city_id").find(":selected").text(),
                  street=$("#street_c").val(),
                  house=$("#house_c").val(),
                  flat=$("#flat_c").val(),
                  phone=$("#phone_c").val()
                }
                //обновляем инфо

                if(region&&city&&street&&house&&flat)
                {}
                else { $.fancybox({type: "html", content:'Заполните данные полностью!',  wrapCSS: 'alertmes',autoCenter:true,minWidth:300,minHeight:150,overlay: {locked: false}});
				//die;
            }

                cost=$("#ordertotal").text();


                $.ajax({
                  type: 'POST',
                  url: '/update/orderadresscurier/',
                  data: {

                    orderid: <?php print_R($_GET['id']); ?>,
                    userid:<?php	print_R($user->getid());?>,
                    cost: cost,
                    region:region,
                    city:city,
                    street:street,
                    house:house,
                    flat:flat,
                    phone:phone


                  },
                  success: function(response) {
                  
                  
                  if(id_button=='privat24')
                           $('input[name=btn_text]').click();
                  //  alert(response); die;
                  
                  //переходим на оплату.... пока сразу на ордера мои

                  //window.location.replace("/personal/orders/");
                  //die;

                  }
                });


              }
            }
            //нет прописанного получателя
            else {
				$.fancybox({type: "html", content:'Заполните, пожалуйста, в Профиле ФИО и контактный телефон',  wrapCSS: 'alertmes',autoCenter:true,minWidth:300,minHeight:150,overlay: {locked: false}});
             
            }



          });

          $('#city_np').change(function()
          {
            getPiceNpDelivery();
          });


});
function getPiceNpDelivery()
{
      var city =$('#city_np').val();
      if (city == '' || city == false || city == undefined || city=='Город')
      { 
          return false;
      }
      $('#city_get_price_np').val(city);
      var form = $('#get_del_price').serialize();
      $.ajax({
            url: '/get/price/np/delivery/',
            type: 'POST',
            data: form,
            success: function(data) {
                $('#orderdelivery').html(data);

            },
            error: function(jqXHR, textStatus, errorThrown) {

                var errorone = 'статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ";
                $.fancybox({ type: "html", content: errorone, wrapCSS: 'alertmes', autoCenter: true, minWidth: 300, minHeight: 150, overlay: { locked: false } });
                var errortwo = "Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR;
                $.fancybox({ type: "html", content: errortwo, wrapCSS: 'alertmes', autoCenter: true, minWidth: 300, minHeight: 150, overlay: { locked: false } });
                //alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
            }
                    });
}

</script>
  <!-- /.row -->
  <div class="row">
    <div class="col-md-12 clearfix">
      <ul class="step-nav page-menu">
        <li class="done"><a href="/personal/new-order/<?php if (isset($_GET['edit'])&&($_GET['edit']==1)) echo"?id=".$_GET['id']."&edit=1"; ?>">Загрузка фото</a></li>
        <li class="done"><a href="/personal/order/?id=<?php print_r($_GET['id']) ?><?php if (isset($_GET['edit'])&&($_GET['edit']==1)) echo"&edit=1"; ?>">Параметры печати</a></li>
        <li class="done"><a href="/personal/order-cost/?id=<?php print_r($_GET['id']) ?><?php if (isset($_GET['edit'])&&($_GET['edit']==1)) echo"&edit=1"; ?>">Расчет стоимости</a></li>
<!--        <li class="active"><a href="/personal/delivery/?id=<?php //print_r($_GET['id']) ?><?php //if (isset($_GET['edit'])&&($_GET['edit']==1)) echo"&edit=1"; ?>">Оплата / Доставка</a></li>-->
        <li class="active"><a>Оплата / Доставка</a></li>
      </ul>
      <a href="/personal/order-cost/?id=<?php print_r($_GET['id']) ?><?php if (isset($_GET['edit'])&&($_GET['edit']==1)) echo"&edit=1"; ?>" class="back-step-btn btn">На шаг назад</a>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  <div class="row new-deliv">
    <div class="col-lg-8 col-md-12">
      <div class="delivery-info">
        <h3 class="delivery-info-title title-3">Способ доставки</h3>
        <div class="delivery-info-form">
          <form>
            <div class="delivery-method-first delivery-method">
              <div class="radio">
                  <input id="delivery-method-1" type="radio" name="delivery-method-radio" value="np" checked="checked" style="display: none">
<!--                <label for="delivery-method-1" class="delivery-method-label">Новой почтой</label>-->
                    <h2 class="new-post"> Новой почтой</h2>
              </div>
                <br>
                <br>
                <div class="radio">
                <input id="delivery-method-3" type="checkbox" name="np_to_house"  value="curier">
                <label for="delivery-method-3" class="delivery-method-label">Курьером</label>
              </div>
              <a href="http://novaposhta.ua" class="delivery-method-link">novaposhta.ua</a>
              <div class="delivery-fields-container" id="c_np">



                <div class="select-wrap select-wrap-50">
                  <select class="select" id="region_np">
                    <option value=">">Область</option>
                    <?php
                      foreach ($rt as $npr) {
                     ?>
                    <option value="<?php print_r($npr['ref']) ?>"><?php print_r($npr['desc']) ?></option>

                    <?php
                      }
                     ?>

                  </select>
                </div>
                <div class="select-wrap select-wrap-50">
                  <select class="select" id="city_np">
                      <option>Город</option>

                  </select>
                </div>
                <div class="select-wrap select-wrap-100">
                  <select class="select" id="office_np">
                    <option value="">Отделение</option>

                  </select>
                </div>
                  <input type="tel" class="input-text delivery-input delivery-input-50" placeholder="+__(___) ___-__-__" name="phone" id="val_phone" value="<?php if(isset($user_info['phone'])) { echo $user_info['phone']; } //else $a=380442297571; echo($a); ?>">
              <span></span> 
              <input type="text" class="input-text delivery-input delivery-input-50" placeholder="ФИО" name="ФИО" id="fio_input" value="<?php if(isset($user_info['fio'])) { echo $user_info['fio']; } ?>">
              <div id="inputs_cnp" style="display: none">
                  <input type="text" name="street" id="street_cnp" accept=""placeholder="Улица">
                  <input type="text" name="house" id="house_cnp" placeholder="Дом" >
                  <input type="text" name="flat" id="flat_cnp" placeholder="Квартира" > 
              </div>
              
              </div>
            </div>
            <div class="delivery-method-second delivery-method">
<!--              <div class="radio">
                <input id="delivery-method-2" type="radio" name="delivery-method-radio"  value="curier">
                <label for="delivery-method-2" class="delivery-method-label">Курьером</label>
              </div>-->



<!--              <div class="delivery-fields-container" id="c_c" style="display:none">
                <div style="margin-bottom:20px; width:100%;">
                    <label class="checkbox-inline"><input type="checkbox" value="" id="change_curier"> Использовать адрес из Профиля</label>
                </div>
                <div id="curier_profile" style="display:none;">
                  <?php
                    if (isset($user_info)&&!empty($user_info)){ ?>
                      <div>ФИО: <?php if (isset($user_info['fio'])&&!empty($user_info['fio']))  print_r($user_info['fio'])?> </div>
                      <div>Область: <?php if (isset($user_info['region'])&&!empty($user_info['region']))  print_r($user_info['region'])?> </div>
                      <div>Город: <?php if (isset($user_info['city'])&&!empty($user_info['city']))  print_r($user_info['city'])?> </div>
                      <div>Улица: <?php if (isset($user_info['street'])&&!empty($user_info['street']))  print_r($user_info['street'])?> </div>
                      <div>Дом: <?php if (isset($user_info['house'])&&!empty($user_info['house']))  print_r($user_info['house'])?> </div>
                      <div>Квартира: <?php if (isset($user_info['flat'])&&!empty($user_info['flat']))  print_r($user_info['flat'])?> </div>
                      <div>Телефон: <?php if (isset($user_info['phone'])&&!empty($user_info['phone']))  print_r($user_info['phone'])?> </div>
                      <div>Email: <?php if (isset($user_info['email'])&&!empty($user_info['email']))  print_r($user_info['email'])?> </div>


                    <?php }
                    else{
                      echo "заполните данные в <a href='/personal/'>Профиле</a>";
                    }
                  ?>
                </div>
                <div id="curier_handle">
                  <div class="select-wrap select-wrap-50" style="margin-left:0px;">
                    <select class="select" name="region_id" id="region_id"  class="StyleSelectBox">
                      <option value="0"> Область </option>
                      <?php foreach ($regions as $region){
                          if ($region['name'] !="Украина") {
                        ?>
                          <option value="<?php print_R($region['region_id']) ?>"> <?php print_R($region['name']) ?> </option>
                      <?php } }  ?>
                    </select>
                  </div>
                  <div class="select-wrap select-wrap-50" style="margin-left:30px;">
                    <select class="select" name="city_id" id="city_id" disabled="disabled" class="StyleSelectBox">
                      <option value="0"> Город </option>
                    </select>
                  </div>
                  <input type="text" class="input-text delivery-input delivery-input-50" placeholder="Улица" name="street" id="street_c">
                  <input type="text" class="input-text delivery-input delivery-input-25" placeholder="Дом" name="house" id="house_c">
                  <input type="text" class="input-text delivery-input delivery-input-25" placeholder="Квартира" name="flat" id="flat_c">
                  
                  <input type="tel" class="input-text delivery-input delivery-input-50" placeholder="+__(___) ___-__-__" name="phone" id="phone_c">
                
                </div>
              </div>-->
            </div>
          </form>
        </div>
      </div>
      <!-- /.delivery-info -->
    </div>
    <!-- /.col-md-8 -->
    <div class="col-lg-4 col-md-12">
      <div class="delivery-sidebar">
        <div class="delivery-cost">
          <div class="delivery-cost-item"><b>Итого:</b></div>
          <div class="delivery-cost-item cost-item1"><span class="for-payment2">Печать фото на сумму: </span><div class="float-in-order"><span id="ordercost"><?php echo $ordercost; ?></span> <span class="amount1">грн.</span></div></div>
          <div class="delivery-cost-item cost-item1"><span class="for-payment2">Стоимость доставки: </span><div class="float-in-order"><span id="orderdelivery">0</span> <span> грн.</span></div></div>
          <div class="delivery-cost-total"><span class="for-payment1">К оплате:</span> <span id="ordertotal"><?php echo $ordercost; ?></span><span class="amount1">грн.</span></div>
        </div>
      </div>
      <!-- /.delivery-sidebar -->
    </div>
    <!-- /.col-md-4 -->
  </div>
  <!-- /.row -->
  <div class="section-payment">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="payment-method">
            <div class="payment-method-logo"><img src="<?php echo $page-> getTemplateUrl(); ?>images/icons/icon-card-1.png" alt=""></div>
            <p class="payment-method-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
            <button type="button" class="payment-method-btn btn" id="privat24">Оплатить</button>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-md-6">
          <div class="payment-method">
            <div class="payment-method-logo"><span>Карта</span><img src="<?php echo $page-> getTemplateUrl(); ?>images/icons/icon-card-2.png" alt=""></div>
            <p class="payment-method-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
            <button type="button" class="payment-method-btn btn" id="card">Оплатить</button>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->
  </div>
  <form id="get_del_price">    
      <input type="hidden" name="idorder" value="<?=$_GET['id']?>">
      <input type="hidden" name="city" id="city_get_price_np">
      <input type="hidden" name="home" id="np_home" value=0>  
  </form>
  <div class='btn_text'>
  <?php echo $data['form']; ?>
  </div>