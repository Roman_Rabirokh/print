<?php
global $user;
global $app;

//jquery file uploader
$page->addCSS('/templates/js/uploader/css/jquery.fileupload.css');
$page->addCSS('/templates/js/uploader/css/jquery.fileupload-ui.css');
$page->addJS('//code.jquery.com/ui/1.11.4/jquery-ui.js');
$page->addJS('/templates/js/tinymce/tinymce.min.js');
$page->addJS('//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js');
$page->addJS('//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js');
$page->addJS('//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js');
$page->addJS('/templates/js/uploader/jquery.iframe-transport.js');
$page->addJS('/templates/js/uploader/jquery.fileupload.js');
$page->addJS('/templates/js/uploader/jquery.fileupload-process.js');
$page->addJS('/templates/js/uploader/jquery.fileupload-image.js');
$page->addJS('/templates/js/uploader/jquery.fileupload-audio.js');
$page->addJS('/templates/js/uploader/jquery.fileupload-video.js');
$page->addJS('/templates/js/uploader/jquery.fileupload-validate.js');
$page->addJS('/templates/js/uploader/jquery.fileupload-ui.js');

$token = App::createFormToken('eslibmishkibylipchelami137');


//удаляем папку с темповыми фото, если есть
if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/files/tmp/' . $token . '/thumbnail'))
    foreach (glob($_SERVER['DOCUMENT_ROOT'] . '/files/tmp/' . $token . '/thumbnail/*') as $filed)
        unlink($filed);
if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/files/tmp/' . $token . '/thumbnail'))
    @rmdir($_SERVER['DOCUMENT_ROOT'] . '/files/tmp/' . $token . '/thumbnail');
if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/files/tmp/' . $token))
    foreach (glob($_SERVER['DOCUMENT_ROOT'] . '/files/tmp/' . $token . '/*') as $filed)
        @unlink($filed);
if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/files/tmp/' . $token))
    @rmdir($_SERVER['DOCUMENT_ROOT'] . '/files/tmp/' . $token);
?>
<script src="<?php echo $page->getTemplateUrl(); ?>js/jquery.js"></script>
<script>
    var token = '<?php echo $token; ?>';

    function calculate() {

        tt = $("#select_type").val();
        if (tt == '1') {
            koef1 = 1;
        } else {
            koef1 = 1.5;
        }

        tsize = $("#select_size").val();
        if (tsize == '1') {
            koef2 = 10;
        }
        if (tsize == '2') {
            koef2 = 15;
        }
        if (tsize == '3') {
            koef2 = 45;
        }


        tcount = $("#count_f").text();


        price = koef1 * koef2 * tcount;

        tcount = $("#cost").text(price);

    }

    function confirm_delete() {
        return confirm;
    }

    function delete_img() {
        location.reload();
        $.fancybox({type: "html", content: 'Изображение удалено', wrapCSS: 'alertmes', autoCenter: true, minWidth: 300, minHeight: 150, overlay: {locked: false}});

        die;
    }

    $(document).ready(function () {

        $('html').on('click', '#label_is_print', function () {
            //фиксим баг клика со второго раза
            $(this).prev('input').prop("checked", function (i, val) {
                return !val;
            });
            event.preventDefault();




            if ($('#photo-container-checkbox-1').attr('checked')) {
                $('#photo-container-checkbox-1').removeAttr('checked');
                $("#price_view").hide();
            } else {
                $('#photo-container-checkbox-1').attr('checked', 'checked');
                $("#price_view").show();
            }



        });

        //вешаем клик на кнопку "Загрузить с компьютера"
        $("#bt1").click(function () {
            $("#csend").click();

        });

        //изменения элементов
        $("#select_size").change(function () {
            size = $("#select_size").val();
            if (size == "1") {
                size_n = "10 x 15";
            }
            if (size == "2") {
                size_n = "15 X 25";
            }
            if (size == "3") {
                size_n = "95 X 135";
            }
            $("#size_f").text(size_n);
            $("#total_size").text(size_n);

            //calculate
            calculate();

        });


        $("#select_type").change(function () {
            type = $("#select_type").val();
            if (type == "1") {
                type_n = "Глянцевая";
            }
            if (type == "2") {
                type_n = "Матовая";
            }
            $("#type_f").text(type_n);
            $("#total_type").text(type_n);

            //calculate
            calculate();

        });

        $("#copy").click(function () {

            $.fancybox({type: "html", content: 'Копия создана', wrapCSS: 'alertmes', autoCenter: true, minWidth: 300, minHeight: 150, overlay: {locked: false}});

            die;
        });
        //вешаем клики на счетчики
        $(".counter-btn").click(function () {

            //-
            if ($(this).hasClass('copies-minus-btn'))
            {

                input_val = parseInt($("#img_count").val());
                if (input_val > 1) {
                    input_val = input_val - 1;
                    $("#img_count").val(input_val);
                    $('#count_f').text(input_val);
                    $('#total_count').text(input_val);
                }
            }
            //+
            if ($(this).hasClass('copies-plus-btn'))
            {

                input_val = parseInt($("#img_count").val());
                input_val = input_val + 1;
                $("#img_count").val(input_val);
                $('#count_f').text(input_val);
                $('#total_count').text(input_val);

            }

            calculate();
        });

//
        $("#csend").change(function () {

            setTimeout(function () {

                //получаем имя файла
                $.ajax({
                    type: 'POST',
                    url: '/get/demoimg/',
                    data: {folder: token},
                    success: function (response) {
                        //alert(response);die;
                        rt = JSON.parse(response);
                        rt=rt[0];
                        $("#mainimg").attr("src", "/files/tmp/" + token + "/" + rt['name']);
                        $('#prev_dem_img').attr("href", "/files/tmp/" + token + "/" + rt['name']);
                        $('#image-width').html(rt['resolution']['width']);
                        $('#image-height').html(rt['resolution']['height']);
                        tmp=rt['colorScheme'];
                        if( tmp=='' || tmp==false || tmp==undefined)
                            $('#image-color').html('-');
                        else
                             $('#image-color').html(tmp);
                        tmp=rt['dpi'];
                        if( tmp=='' || tmp==false || tmp==undefined)
                            $('#image-dpi').html('-');
                        else
                             $('#image-dpi').html(tmp);
                        tmp= rt['quality'];
                        if( tmp=='' || tmp==false || tmp==undefined)
                            $('#image-quality').html('-');
                        else
                             $('#image-quality').html(tmp);
                         
                        tmp=rt['exif']['exif:DateTimeOriginal'];
                        if( tmp=='' || tmp==false || tmp==undefined)
                         {
                             $('#image-date').html('-');
                            $('#image-time').html('-');
                         }
                            
                        else
                        {
                             data=rt['exif']['exif:DateTimeOriginal'];
                             data=data.split(' ');
                             if(data[0])
                             {
                                 $('#image-date').html(data[0]);
                             }
                             else
                                 $('#image-date').html('-');
                             if(data[1])
                             {
                                 $('#image-time').html(data[1]);
                             }
                             else
                                 $('#image-time').html('');
                        }
                        //tmp=rt['position']['address'];
                        if( rt['position']==undefined || rt['position']['address']==undefined || rt['position']['address']=='' || rt['position']['address']==false ||  !rt['position']['address'])
                            $('#image-pos').html('-');
                        else
                             $('#image-pos').html(rt['position']['address']);
//die;
                        //  return rt;
                    }
                });
                //обновляем вью
                $(".photo-container-wrap").show();
                $(".container").show();
                $("#form").hide();


            }, 500);





        });




    });
</script>
<script type="text/javascript" src="/templates/js/uploader/script.js"></script>

<div class="spanning">
    <div class="test-drive-content" itemscope itemtype="http://schema.org/WebPage">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb" itemprop="breadcrumb">
                        <li><a href="#" itemprop="url">Главная</a></li>
                        <li class="active">Тест-драйв</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <div class="test-drive-info">
                        <h3 class="test-drive-title title-3 title-center">Название продукта</h3>
                        <div class="test-drive-subtitle">Описание в двух словах, что это такое и как работает</div>
                        <p class="test-drive-about">Пользователь загружает изображения с локального компьютера или из сетевых источников. полученные фотограции можно кастомизировать.</p>
                    </div>
                    <!-- /.test-drive-info -->
                </div>
                <!-- /.col -->
            </div>
            <div id="form">
                <form id="fileupload" action="/item/image/upload/<?php echo $token; ?>/" method="POST" enctype="multipart/form-data">
                    <div class="row fileupload-buttonbar">

                        <!-- The fileinput-button span is used to style the file input field as button -->
                        <span class="btn btn-success fileinput-button upload-btn " id="bt1">
                            <i class="glyphicon glyphicon-plus"></i>
                            <span >Загрузить с компьютера</span>
                            <input type="file" name="files[]"  id="csend"  accept="image" required="required" class="newimage" >
                        </span>


                        <!-- The global progress state -->
                        <div class="col-lg-5 fileupload-progress fade ">
                            <!-- The global progress bar -->
                            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                            </div>
                            <!-- The extended global progress state -->
                            <? /*  <span class="fileupload-process"></span> */ ?>
                            <? /* <div class="progress-extended">&nbsp;</div> */ ?>
                        </div>
                    </div>
                    <!-- The table listing the files available for upload/download -->
                    <!--
                            <div id="files" class="files" style="display:none;">
                            </div>
                    -->
                </form>

            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
        <div class="photo-container-wrap" style="display:none;">
            <form>
                <div class="photo-container">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="photo-container-top">
                                    <div class="photo-container-checkbox checkbox">
                                        <input id="photo-container-checkbox-1" type="checkbox" name="photo-container-checkbox">
                                        <label for="photo-container-checkbox-1" class="photo-container-label" id="label_is_print"></label>
                                    </div>
                                    <div class="photo-col photo-col-1">
                                        <div class="photo-parameter photo-parameter1">
                                            <span class="photo-parameter-text">Размер</span>
                                            <div class="tooltip-wrap-5 product-tooltip-wrap">
                                                <button class="product-tooltip-btn tooltip-btn">?</button>
                                                <div class="product-tooltip-left product-tooltip tooltip">Выбор размера бумаги для печати</div>
                                            </div>
                                            <div class="select-wrap">
                                                <select class="select" id="select_size">
                                                    <option value="1">10 x 15</option>
                                                    <option value="2">15 x 25</option>
                                                    <option value="3">95 x 135</option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="photo-parameter photo-parameter2">
                                            <span class="photo-parameter-text">Бумагa</span>
                                            <div class="tooltip-wrap-6 product-tooltip-wrap">
                                                <button class="product-tooltip-btn tooltip-btn">?</button>
                                                <div class="product-tooltip-left product-tooltip tooltip">Выбор типа бумаги для печати</div>
                                            </div>
                                            <div class="select-wrap">

                                                <select class="select" id="select_type">
                                                    <option value="1">Глянцевая</option>
                                                    <option value="2">Матовая</option>

                                                </select>
                                            </div>

                                        </div>
                                        <div class="photo-parameter photo-parameter3">

                                            <span class="photo-parameter-text">Количество</span>
                                            <div class="tooltip-wrap-7 product-tooltip-wrap">
                                                <button class="product-tooltip-btn tooltip-btn">?</button>
                                                <div class="product-tooltip-left product-tooltip tooltip">Выбор количества копий изображения</div>
                                            </div>
                                            <div class="counter-input">
                                                <input type="number"  id="img_count" value="1" >
                                                <button for="img_count" type="button" class="copies-minus-btn counter-minus-btn counter-btn">-</button>
                                                <button for="img_count" type="button" class="copies-plus-btn counter-plus-btn counter-btn">+</button>
                                            </div>

                                        </div>
                                        <div class="photo-parameter photo-parameter4">
                                            <span class="photo-parameter-text">Поля</span>
                                            <div class="tooltip-wrap-8 product-tooltip-wrap">
                                                <button class="product-tooltip-btn tooltip-btn">?</button>
                                                <div class="product-tooltip-left product-tooltip tooltip">Выбор полей фотографии</div>
                                            </div>
                                            <div class="counter-input second-counter-input">
                                                <input type="number" class="fields-counter-input" value="0" >
                                                <button class="fields-minus-btn counter-minus-btn counter-btn">-</button>
                                                <button class="fields-plus-btn counter-plus-btn counter-btn">+</button>
                                            </div>

                                        </div>
                                    </div>
                                    <button type="button" onclick="if (confirm_delete())
            delete_img()" class="photo-container-del"></button>
                                    <div class="tooltip-wrap-1 product-tooltip-wrap">
                                        <button class="product-tooltip-btn tooltip-btn">?</button>
                                        <div class="product-tooltip-left product-tooltip tooltip">Печатаем или нет изображение</div>
                                    </div>
                                    <div class="tooltip-wrap-3 product-tooltip-wrap" >
                                        <button type="button" class="product-tooltip-btn tooltip-btn" >?</button>
                                        <div class="product-tooltip-right product-tooltip tooltip" >Удалить изображение из заказа</div>
                                    </div>
                                </div>
                                <!-- /.photo-container-top -->
                                <div class="photo-container-content clearfix">
                                    <div class="photo-container-img">
                                        <a href="#"><img src="/templates/images/sample.jpg" alt="" id="mainimg"></a>
                                        <a href="#" class="img-modal-btn" id="prev_dem_img">Предпросмотр</a>
                                        <div class="tooltip-wrap-4 product-tooltip-wrap">
                                            <button class="product-tooltip-btn tooltip-btn">?</button>
                                            <div class="product-tooltip-left product-tooltip tooltip">Предпросмотр изображения</div>
                                        </div>
                                    </div>
                                    <div class="photo-col photo-col-1 photo-col-11 ">

                                        <div class="photo-autocorrect-checkbox checkbox">
                                            <div class="tooltip-wrap-22 product-tooltip-wrap">
                                                <button class="product-tooltip-btn tooltip-btn">?</button>
                                                <div class="product-tooltip-left product-tooltip tooltip">Автокорекция</div>
                                            </div>
                                            <input class="autocorrect-inputs" id="checkbox_photo-autocorrect-checkbox-66159" type="checkbox" name="photo-autocorrect-checkbox">
                                            <label id="photo-autocorrect-checkbox-66159" for="checkbox_photo-autocorrect-checkbox-66159" class="photo-autocorrect-label 
                                                   checkbox1">Автокоррекция</label>
                                        </div>

                                        <div class="photo-parameter-text">Ширина полей(мм):</div>

                                        <div class="select-wrap">
                                            <div class="tooltip-wrap-22 product-tooltip-wrap">
                                                <button class="product-tooltip-btn tooltip-btn">?</button>
                                                <div class="product-tooltip-left product-tooltip tooltip">Ширина полей</div>
                                            </div>
                                            <select class="select field_width_select" name="paper_size" id="paper_field_width_66159">
                                                <option value="2">2</option>
                                                <option value="4" selected="selected">4</option>
                                                <option value="6">6</option>
                                                <option value="8">8</option>
                                                <option value="10">10</option>

                                            </select>
                                        </div>
                                        <div class="photo-parameter-text">Кадрирование:</div>

                                        <div class="select-wrap">
                                            <div class="tooltip-wrap-22 product-tooltip-wrap">
                                                <button class="product-tooltip-btn tooltip-btn">?</button>
                                                <div class="product-tooltip-left product-tooltip tooltip">Кадрирование</div>
                                            </div>
                                            <select class="select framing_select" name="paper_size" id="paper_framing_66159">
                                                <option value="0" selected="selected">уменьшение с белыми полями</option>
                                                <option value="1">обрезка по меньшей границе</option>

                                            </select>
                                        </div>

                                        <div class="photo-parameter-text">Ориентация
                                            фото:</div>

                                        <div class="select-wrap">
                                            <div class="tooltip-wrap-22 product-tooltip-wrap">
                                                <button class="product-tooltip-btn tooltip-btn">?</button>
                                                <div class="product-tooltip-left product-tooltip tooltip">Ориентация фото</div>
                                            </div>
                                            <select class="select photo_orientation_select" name="paper_size" id="paper_photo_orientation_66159">
                                                <option value="0">портрет</option>
                                                <option value="1" selected="selected">ландшафт</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="photo-container-parameters clearfix">

                                        <!-- /.col-1 -->
                                        <div class="photo-col photo-col-2">
                                            <div class="photo-parameter photo-textarea-wrap">
                                                <div class="photo-parameter-text">Подпись к фотографии</div>
                                                <textarea class="photo-parameter-textarea" placeholder="Введите текст подписи..."></textarea>
                                                <div class="tooltip-wrap-9 product-tooltip-wrap">
                                                    <button class="product-tooltip-btn tooltip-btn">?</button>
                                                    <div class="product-tooltip-right product-tooltip tooltip">Подпись на изображении</div>
                                                </div>
                                            </div>
                                            <div class="photo-parameter param-text">
                                                <div class="select-wrap wrap-fontsize">
                                                    <select class="select selectfontsize">
                                                        <option value="1">Arial</option>
                                                        <option value="2">Times New Roman</option>
                                                        <option value="3">SCRIPT</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="photo-parameter clearfix">
                                                <div class="font-size-select select-wrap">
                                                    <select class="select select_font_size">
                                                        <option value="1">14</option>
                                                        <option value="2">16</option>
                                                        <option value="3">22</option>
                                                    </select>
                                                </div>
                                                <div class="font-style-buttons">
                                                    <button class="font-bold-btn font-style-btn">B</button>
                                                    <button class="font-italic-btn font-style-btn">I</button>
                                                    <button class="font-underline-btn font-style-btn">U</button>
                                                </div>

                                            </div>
                                            <div class="color"> Цвет текста:


                                            </div>
                                            <input class="oval-elem" type="color" id="input_color_text_all" name="color_text" value="black">
                                            <div class="photo-parameter-text margin-param">Позиция текста:</div>
                                            <div class="select-wrap margin-param">
                                                <select class="select text_orientation_select" name="text_orientation" id="paper_text_orientation_all">
                                                    <option value = 0 >Не выбрано</option>
                                                    <option value="Левый верхний">Левый верхний</option>
                                                    <option value="Левый нижний">Левый нижний</option>
                                                    <option value="Правый верхний">Правый верхний</option>
                                                    <option value="Правый нижний" >Правый нижний</option>
                                                    <option value="Центр">Центр</option>

                                                </select>
                                            </div>
                                        </div>
                                        <!-- /.col-2 -->
                                        <div class="photo-col photo-col-3 photo-col-33wr">
                                            <div class="photo-parameter photo-date-wrap">
                                                <div class="photo-date-checkbox photo-dateTestDrive checkbox ">
                                                    <input id="photo-date-checkbox-1" type="checkbox" name="photo-date-checkbox">
                                                    <label for="photo-date-checkbox-1" class="photo-date-label">Печатать дату</label>
                                                </div>
                                                <!--<div class="tooltip-wrap-10 product-tooltip-wrap">
                                                    <button class="product-tooltip-btn tooltip-btn">?</button>
                                                    <div class="product-tooltip-right product-tooltip tooltip">Печать даты</div>
                                                </div>-->
                                            </div>
                                            <div class="photo-parameter photo-dateformat-wrap">
                                                <div class="select-wrap">
                                                    <select class="dateformat-select select">
                                                        <option value="1">День / Месяц / Год</option>
                                                        <option value="2">Год / День / Месяц</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="photo-parameter photo-coords-parametr">
                                                <div class="photo-coords-checkbox checkbox photo-coords-checkbox-testDrive">
                                                    <input id="photo-coords-checkbox-1" type="checkbox" name="photo-coords-checkbox" checked>
                                                    <label for="photo-coords-checkbox-1" class="photo-coords-label">Печатать координаты</label>
                                                </div>
                                                <!--   <div class="tooltip-wrap-11 product-tooltip-wrap">
                                                       <button class="product-tooltip-btn tooltip-btn">?</button>
                                                       <div class="product-tooltip-right product-tooltip tooltip">Печать координат</div>
                                                   </div> -->
                                            </div>
                                            <div class="product-btn-wrap">
                                                <a href="javascript:void(0);" class="photo-container-btn btn test-drive" id="copy">Создать копию <br>с другими параметрами</a>
                                                <div class="tooltip-wrap-12 product-tooltip-wrap">
                                                    <button class="product-tooltip-btn tooltip-btn">?</button>
                                                    <div class="product-tooltip-right product-tooltip tooltip">Создание копии изображения в текущем заказе</div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.col-3 -->
                                    </div>
                                    <div class="inline-info-block">
                                        <table class="infotable" border="1" width="100%" cellpadding="5">
                                            <tr class="not-even-numbered">
                                                <th colspan="2"> Данные фото </th>

                                            </tr>
                                            <tr class="even-numbered">
                                                <td>Высота</td>
                                                <td id="image-height"></td>
                                            </tr>
                                            <tr class="not-even-numbered">
                                                <td>Ширина</td>
                                                <td id="image-width">  </td>
                                            </tr>   <tr class="quality-elem-find" class="even-numbered">
                                            <div class="tooltip-wrap-22 tooltip-dpi product-tooltip-wrap">
                                                <button class="product-tooltip-btn tooltip-btn">?</button>
                                                <div class="product-tooltip-left product-tooltip tooltip">Dpi</div>
                                            </div>   <td>Dpi</td>
                                            <td id="image-dpi"></td>
                                            </tr>    
                                            <tr class="not-even-numbered">
                                            <div class="tooltip-wrap-22 tooltip-color-scheme product-tooltip-wrap">
                                                <button class="product-tooltip-btn tooltip-btn">?</button>
                                                <div class="product-tooltip-left product-tooltip tooltip">Цветовая схема</div>
                                            </div>    <td>Цветовая схема</td>
                                            <td id="image-color"></td>
                                            </tr> 
                                            <tr class="even-numbered">
                                            <div class="tooltip-wrap-22 tooltip-quality product-tooltip-wrap">
                                                <button class="product-tooltip-btn tooltip-btn">?</button>
                                                <div class="product-tooltip-left product-tooltip tooltip">Качество</div>
                                            </div>  <td>Качество</td>
                                            <td id="image-quality"> </td>
                                            </tr> 
                                            <tr class="not-even-numbered">
                                                <td>Дата создания</td>
                                                <td id="image-date">
                                                </td>
                                            <tr class="even-numbered">
                                                <td>Время создания
                                                </td>
                                                <td id='image-time'> </td>
                                            </tr>    
                                            <tr  class="not-even-numbered">
                                                <td>Место сьемки</td>
                                                <td id='image-pos'>  </td>
                                            </tr> 

                                        </table>

                                    </div>
                                    <!-- /.photo-container-parameters -->
                                </div>
                                <!-- /.photo-container-content -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container -->
                </div>
                <!-- /.photo-container -->
            </form>
        </div>
        <!-- /.photo-container-wrap -->
        <div class="container" style="display:none;">
            <div class="row">
                <div class="col-md-6" id="price_view" style="display:none;">
                    <div class="product-cost">
                        <div class="product-cost-title">Стоимость Вашего заказа:</div>
                        <div class="product-cost-table">
                            <table>
                                <tr>
                                    <td class="product-col-1">Количество:</td>
                                    <td class="product-col-2"><span id="total_count">1</span> шт.</td>
                                </tr>
                                <tr>
                                    <td class="product-col-1">Бумага:</td>
                                    <td class="product-col-2" id="total_type">Глянцевая</td>
                                </tr>
                                <tr>
                                    <td class="product-col-1">Размер:</td>
                                    <td class="product-col-2" id="total_size">10 х 15</td>
                                </tr>
                            </table>
                        </div>
                        <!-- /.product-cost-table -->
                        <div class="product-cost-total"><SPAN ID="cost">10</span> грн</div>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-md-12">
                    <div class="register-info">
                        <p class="register-info-text">Для того, что бы заказать печать Ваших фото с выбранными Вами параметрами, необходимо зарегистрироваться.</p>
                        <a href="/registration/" class="register-info-btn btn">Зарегистрироваться</a>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.test-drive-content -->
</div>
<!--uploader -->
<script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
    <td>
    <span class="preview"></span>
    </td>
    <td>

    <strong class="error text-danger"></strong>
    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
    </td>
    <td>  {% if (!i) { %}

    {% } %}</td>
    </tr>
    {% } %}
    /*
    {% for (var i=0, file; file=o.files[i]; i++) { %}

    <div class="uploaded-photo upload-photo-loading">
    <div class="uploaded-photo-inner">
    {% if (file.url) { %}
    {% if (file.error) { %}
    <div><span class="label label-danger">Ошибка</span> {%=file.error%}</div>
    {% } else { %}
    <img src="{%=file.url%}" style="margin:7px 0 7px 7px;max-width:500px;width:80px;" class="img-thumbnail"  >
    {% } %}

    {% } %}
    <a href="#" class="uploaded-photo-del" id="" alt="{%=file.name%}">&times;</a>
    </div>
    </div>


    {% } %}
    */
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}

    <div class="uploaded-photo">
    <div class="uploaded-photo-inner">
    {% if (file.url) { %}
    {% if (file.error) { %}
    <div><span class="label label-danger">Ошибка</span> {%=file.error%}</div>
    {% } else { %}
    <img src="/files/tmp/<?php echo $token ?>/{%=file.name%}" style="margin:0px 0 0px 0px;max-width:500px;" class="img-thumbnail"  >
    {% } %}

    {% } %}
    <a href="javascript:void(0);" class="uploaded-photo-del" id="" alt="{%=file.name%}">&times;</a>
    </div>
    </div>










    {% } %}
</script>
