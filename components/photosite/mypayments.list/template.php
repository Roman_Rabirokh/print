<?php if(!defined("_APP_START")) { exit(); } ?>
<script src="<?php echo $page-> getTemplateUrl(); ?>js/jquery.js"></script>
<script type="text/javascript">
$( document ).ready(function() {
    $( ".cash-in-btn" ).click(function() {
		$.fancybox({type: "html", content:'Оплата',  wrapCSS: 'alertmes',autoCenter:true,minWidth:300,minHeight:150,overlay: {locked: false}});
        
      });


});
</script>

<?php if ($payments) {?>

<div class="row">
  <div class="col-md-12">
    <div class="account-info">
      <div class="account-balance">
        <span>Состояние счёта:</span>
        <span class="account-balance-sum"><?php if ($payments && !empty($payments['0']['saldo'])) print_r($payments['0']['saldo']) ?> грн</span>
        <span>(на <?php echo date("d:m:Y H:i"); ?>)</span>
      </div>
      <a href="javascript:void();" class="cash-in-btn btn">Пополнить счет</a>
      <div class="account-info">
        <span class="account-cash account-cash-up"><?php if (isset($addmoney)&& !empty($addmoney)) echo $addmoney; ?> грн</span>
        <span class="account-cash account-cash-down"><?php if (isset($removemoney)&& !empty($removemoney)) echo $removemoney; ?> грн</span>
      </div>
    </div>
    <div class="account-accordion accordion" itemscope itemtype="http://schema.org/Thing">

    <?php $date=''; $ij=1; ?>

      <?php
      //разбили события по дням
        foreach ($payments as $payment) {
           $date[]=date_create($payment['created'])->Format('Y-m-d');
        }
        $date=array_unique($date);
        foreach ($date as $dateone) { ?>
          <button class="accordion-btn" itemprop="name"><?php echo $dateone;?></button>
          <div class="accordion-content" itemprop="description">
            <div class="account-accordion-table">
              <table>

          <?php
          foreach ($payments as $payment) {
            if ($dateone==date_create($payment['created'])->Format('Y-m-d'))  {
          ?>

                <tr>
                  <td class="col-1"><?php echo date_create($payment['created'])->Format('H:i') ?></td>
                  <td class="col-2"><?php  print_R($payment['description'])?></td>
                  <td class="col-3"><?php  print_R($payment['money'])?> грн</td>
                  <td class="col-4"><?php  print_R($payment['saldo'])?> грн
                    <?php if (isset($payment['orderid'])&&!empty($payment['orderid'])&&($payment['payment_status']==0))
                      {
                        echo "<a href='' style='color:red'>Оплатить</a>";
                      }
                     ?>

                  </td>
                </tr>




          <?php
        }
        }
        ?>
          </table>
        </div>
      </div>
        <?php

        }


      ?>

    </div>
    <!-- /.accordion -->
  </div>
  <!-- /.col -->
  <?php
}
else
{
  echo "Нет данных";
}
   ?>
</div>
<!-- /.row -->
</div>
<!-- /.container -->
</div>
