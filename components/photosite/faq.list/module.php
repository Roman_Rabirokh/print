<?php if(!defined("_APP_START")) { exit(); }
$pagesize = 10;
$currentPage = isset($_REQUEST["page"]) ? intval($_REQUEST["page"]) : 1;
if($currentPage < 1)
{
	$currentPage = 1;
}
$start = ($currentPage - 1) * $pagesize;

$all_count = 0;

$key = 'faq.list-index-' . $pagesize.$currentPage;
/*if(!$this->getCache($data,$key))
{ */

	$allCount = 0;
	$data['ITEMS'] = Content::GetList(4,array('c.name','c.url',"DATE_FORMAT(c.content_date,'%d.%m.%Y') as cdate", 'detail_text'),array(),array('c.content_date'=>'DESC','c.id'=>'DESC'),array('OFFSET'=>$start,'ROWS'=>$pagesize,'COUNT_ALL'=>TRUE),$allCount);

	foreach($data['ITEMS'] as &$current)
	{
		$current['href'] = Content::contentUrl($current['url']);

		$date = explode('.',$current['cdate']);

		$current['itemDate'] = $date[0] . ' ' . getMonthName($date[1],'long') . ' ' . $date[2];
	}
	$data["PAGECOUNT"] = $allCount;
	$data["PAGESIZE"] = $pagesize;

/*	$this->setCache($data,$key);
}*/

include('template.php');
