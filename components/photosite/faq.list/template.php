<?php if(!defined("_APP_START")) { exit(); } ?>

<?php foreach($data['ITEMS'] as $item) { ?>
<button class="accordion-btn" itemprop="name"><?php echo $item['name']; ?></button>
<div class="accordion-content">
  <div class="accordion-content-inner" itemprop="description">
    <?php echo (htmlspecialchars_decode($item['detail_text'])); ?>
  </div>
</div>

<?php }
//$this->includeComponent("system/pager",array("PAGECOUNT"=>$data["PAGECOUNT"],"PAGESIZE"=>$data["PAGESIZE"]));
