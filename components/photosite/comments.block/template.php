<?php if(!defined("_APP_START")) { exit(); }  ?>


<?php foreach($data['ITEMS'] as $item) { ?>




	<div class="reviews-item" itemscope itemtype="http://schema.org/Person">
		<div class="review-author-photo"><img src="<?php echo $item['thumb']; ?>" alt="" itemprop="image"></div>
		<div class="review-author-info" itemprop="name"><?php echo $item['name']; ?> <span class="review-date">(<?php echo $item['cdate'] ?>)</span></div>
		<div class="review-text" itemprop="description">
			<?php echo truncateText($item['detail_text'],150); ?>
		</div>
	</div>







<?php } ?>
