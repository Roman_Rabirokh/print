<?php if(!defined("_APP_START")) { exit(); }  ?>


<?php foreach($data['ITEMS'] as $item) { ?>
	
	<div class="col-md-4">
		<div class="section-blog-item">
			<div class="blog-item-preview">
				<a href="<?php echo $item['href']; ?>"><img src="<?php echo $item['thumb']; ?>" alt=""></a>
			</div>
			<div class="blog-item-info">
				<h4 class="blog-item-title"><a href="<?php echo $item['href']; ?>"><?php echo $item['name']; ?></a></h4>
				<p class="blog-item-descr">
						<?php echo truncateText($item['detail_text'],150); ?>
				</p>
				<div class="blog-item-date"><?php echo $item['cdate'] ?></div>
			</div>
		</div>
	</div>

<?php } ?>
