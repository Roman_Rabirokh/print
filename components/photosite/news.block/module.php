<?php if(!defined("_APP_START")) { exit(); }

$key = 'news.block-index-' . $params['ROWS'];
/*if($this->getCache($data,$key))
{*/
	$data['ITEMS'] = Content::GetList(3,array('c.name','c.url',"DATE_FORMAT(c.content_date,'%d.%m.%Y') as cdate",'mainimage','detail_text','content_date'),array(),array('c.content_date'=>'DESC','c.id'=>'DESC'),array('OFFSET'=>0,'ROWS'=>$params['ROWS']));


	foreach($data['ITEMS'] as &$current)
	{

		$current['href'] = Content::contentUrl($current['url']);
		$current['thumb'] = getImageById($current['mainimage'],array('width'=>400,'crop'=>array(0,0,346,194 )) );



		$date = explode('.',$current['cdate']);

		$current['itemDate'] = $date[0] . ' ' . getMonthName($date[1],'long') . ' ' . $date[2];
	}
//	$this->setCache($data,$key);
//}


include('template.php');
