<script src="<?php echo $page-> getTemplateUrl(); ?>js/jquery.js"></script>
<script>
    function save_form () {
      fio=$("#fio").text();
      //adress=$("#adress").text();
      phone=$("#phone").text();
      phone=phone.replace(/\(/gi, "");
      phone=phone.replace(/\)/gi, "");
      phone=phone.replace(/\-/gi, "");
      phone=phone.replace(/\ /gi, "");
      email=$("#email").text();
      region=$("#region").text();
      city=$("#city").text();
      street=$("#street").text();
      house=$("#house").text();
      flat=$("#flat").text();

      if($('#notification-checkbox-1').attr('checked')) {
        drop_files_notify='1';
      } else {
          drop_files_notify='0';
        }

      if($('#notification-checkbox-2').attr('checked')) {
          order_in_transport='1';
        } else {
            order_in_transport='0';
          }

      if($('#notification-checkbox-3').attr('checked')) {
            client_get_order='1';
          } else {
              client_get_order='0';
            }
      if($('#notification-checkbox-4').attr('checked')) {
          order_done='1';
          } else {
            order_done='0';
          }
      if($('#notification-checkbox-5').attr('checked')) {
              order_in_work='1';
              } else {
                order_in_work='0';
              }
      if($('#notification-checkbox-6').attr('checked')) {
              order_paid='1';
              } else {
                order_paid='0';
              }
      if($('#notification-checkbox-7').attr('checked')) {
              download_done='1';
              } else {
                download_done='0';
              } 

      $.ajax({
        type: 'POST',
        url: '/save/profileinfo/',
        data: {
          fio: fio,
          //adress: adress,
          region:region,
          city:city,
          street:street,
          house:house,
          flat:flat,
          phone: phone,
          email: email,
          drop_files_notify:drop_files_notify,
          order_in_transport:order_in_transport,
          client_get_order:client_get_order,
          order_done:order_done,
          order_in_work:order_in_work,
          order_paid:order_paid,
          download_done:download_done
        },
        success: function(response) {
          //alert(response);
        }
      });
    }

  $( document ).ready(function() {
      //edit profile
      $( ".edit_element" ).click(function() {
        id=$(this).attr( "alt" );
        $("#input_"+id).show();
        $("#"+id).hide();
      });

      
      
      $('input[name=photo-download]').change(function () {

        var file_data = $(this).prop("files")[0];   // Getting the properties of file from file field
        var form_data = new FormData();                  // Creating object of FormData class
        form_data.append("file", file_data);              // Appending parameter named file with properties of file_field to form_data
        $.ajax
                ({
                    url: '/save/imageprofile/',
                    dataType: 'script',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data, // Setting the data attribute of ajax with file_data
                    type: 'post',
                    success: function (data)
                    {
                        if(data!='false')
                        {   
                            var image = JSON.parse(data);
                            $('#val_photo').attr('src',image);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
						
                        alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                        alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                    }
                });
    });
      

      //save form button click
      $( ".save_form" ).click(function() {
        label_id=$(this).attr( "for" );
        val=$("#val_"+label_id).val();
        $("#"+label_id).text(val);
        $("#"+label_id).show();
        $("#input_"+label_id).hide();

        //save form
        save_form ();

      });
      $('#input_photo').change(function()
      {
         $.ajax
                ({
                    url: '/ajax/save/big-image/',
                    dataType: 'script',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data, // Setting the data attribute of ajax with file_data
                    type: 'post',
                    success: function (data)
                    {
                        console.log(data);
                        console.log(JSON.parse(data));
                        var image = JSON.parse(data);
                        $('#big_image_div').css('background', 'url(' + image + ') no-repeat center top');
                        $('#big_image_div').css('z-index', '99');
                        $('#b_big_image').html('ИЗМЕНИТЬ ФОТО');


                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                        alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                    }
                }); 
      });
      //сохраняем допопции
      $( 'html' ).on( 'click', '.checkbox1', function() {
        //фиксим баг клика со второго раза
        $(this).prev('input').prop("checked", function(i, val){
            return !val;
        });
        event.preventDefault();

            id=$(this).attr('for');

            if($('#'+id).attr('checked')) {
              $('#'+id).removeAttr('checked');

            } else {
                $('#'+id).attr( 'checked', 'checked' );

              }

              //save form
              save_form ();

        });
        
 $('html').keydown(function(eventObject){ //отлавливаем нажатие клавиш

    if ($('.save_form').is(':visible')) {
        if (event.keyCode == 13) { //если нажали Enter, то true
        $('.save_form').click();
        }
    }

});
        

  });
</script>
<style>
#input_fio{
  display: none;
}
#input_adress{
  display: none;
}
#input_region{
  display: none;
}
#input_city{
  display: none;
}
#input_street{
  display: none;
}
#input_flat{
  display: none;
}
#input_house{
  display: none;
}
#input_phone{
  display: none;
}
#input_email{
  display: none;
}
</style>

	<?php 	if($user->Authorized()) {?>
    <div class="row">
      <div class="col-md-12">
        <ol class="breadcrumb" itemprop="breadcrumb">
          <li><a href="/" itemprop="url">Главная</a></li>
          <li class="active">Мой кабинет</li>
        </ol>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <div class="row">
      <div class="col-md-12">
        <?php include('./templates/includes/profile-menu.php'); ?>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

<div class="row">
  <div class="col-md-3 col-sm-12">
    <div class="profile-notice">
      <div class="profile-notice-name">Статус:</div>
      <div class="profile-notice-text">
        <?php
          if (isset($user_info)&&(!empty($user_info['user_status'])))
            {
              print_r($user_info['user_status']);
            }
            else {
              echo 'Пользователь';
            }
        ?>
      </div>
      <button class="profile-tooltip-btn tooltip-btn">?
      </button>
      <div class="profile-tooltip tooltip">Статус пользователя в зависимости от оборота заказанного</div>
    </div>
    <div class="profile-notice">
      <div class="profile-notice-name">Скидки:</div>
      <div class="profile-notice-text">Каждый третий заказ скидка 10 %</div>
      <button class="profile-tooltip-btn tooltip-btn">?
      </button>
      <div class="profile-tooltip tooltip">Как я могу получить скидку?</div>
    </div>
  </div>
  <!-- /.col -->
  <form>
  <div class="col-md-9 col-sm-12">
    <div class="profile-info">
      <h3 class="profile-info-title title-3">Контактные данные</h3>
      <div class="profile-info-table">
        <table>
          <tr class="tr">
            <td class="col-1">ФИО:</td>
            <td class="col-2" >
              <span id="input_fio">
                <input type="text" name="fio" id="val_fio" value="<?php
                      if (isset($user_info)&&(!empty($user_info['fio'])))
                        {
                          print_r(trim($user_info['fio'])); }?>"
                    size="35"
                > <button class="save_form" type="button" for="fio">Сохранить </button>
              </span>

              <span id="fio"  class="edit_element" alt="fio">
                <?php
                  if (isset($user_info)&&(!empty($user_info['fio'])))
                    {
                      print_r(trim($user_info['fio']));
                    }
                ?>
              </span>
            </td>
            <td class="col-3"><button class="change-info-btn edit_element" type="button" alt="fio"></button></td>
          </tr>
          <!--
          <tr class="tr">
            <td class="col-1">Адрес:</td>
            <td class="col-2">
              <span id="input_adress">
                <input type="text" name="adress" id="val_adress" value="<?php
                      if (isset($user_info)&&(!empty($user_info['adress'])))
                        {
                          print_r(trim($user_info['adress'])); }?>"
                    size="35"
                > <button class="save_form" type="button" for="adress">Сохранить </button>
              </span>

              <span id="adress">
                <?php
                  if (isset($user_info)&&(!empty($user_info['adress'])))
                    {
                      print_r(trim($user_info['adress']));
                    }
                ?>
              </span>
            </td>
            <td class="col-3"><button class="change-info-btn edit_element" type="button" alt="adress"></button></td>
          </tr>
        -->
        <tr class="tr">
          <td class="col-1">Область:</td>
          <td class="col-2">
            <span id="input_region">
              <input type="text" name="region" id="val_region" value="<?php
                    if (isset($user_info)&&(!empty($user_info['region'])))
                      {
                        print_r(trim($user_info['region'])); }?>"
                  size="35"
              > <button class="save_form" type="button" for="region">Сохранить </button>
            </span>

            <span id="region" class="edit_element" alt="region" >
              <?php
                if (isset($user_info)&&(!empty($user_info['region'])))
                  {
                    print_r(trim($user_info['region']));
                  }
              ?>
            </span>
          </td>
          <td class="col-3"><button class="change-info-btn edit_element" type="button" alt="region"></button></td>
        </tr>
        <tr class="tr">
          <td class="col-1">Город:</td>
          <td class="col-2">
            <span id="input_city">
              <input type="text" name="city" id="val_city" value="<?php
                    if (isset($user_info)&&(!empty($user_info['city'])))
                      {
                        print_r(trim($user_info['city'])); }?>"
                  size="35"
              > <button class="save_form" type="button" for="city">Сохранить </button>
            </span>

            <span id="city" class="edit_element" alt="city" >
              <?php
                if (isset($user_info)&&(!empty($user_info['city'])))
                  {
                    print_r(trim($user_info['city']));
                  }
              ?>
            </span>
          </td>
          <td class="col-3"><button class="change-info-btn edit_element" type="button" alt="city"></button></td>
        </tr>
          <tr class="tr">
            <td class="col-1">Улица:</td>
            <td class="col-2">
              <span id="input_street">
                <input type="text" name="street" id="val_street" value="<?php
                      if (isset($user_info)&&(!empty($user_info['street'])))
                        {
                          print_r(trim($user_info['street'])); }?>"
                    size="35"
                > <button class="save_form" type="button" for="street">Сохранить </button>
              </span>

              <span id="street" class="edit_element" alt="street">
                <?php
                  if (isset($user_info)&&(!empty($user_info['street'])))
                    {
                      print_r(trim($user_info['street']));
                    }
                ?>
              </span>
            </td>
          <td class="col-3"><button class="change-info-btn edit_element" type="button" alt="street"></button></td>
        </tr>
        <tr class="tr">
          <td class="col-1">Дом:</td>
          <td class="col-2">
            <span id="input_house">
              <input type="text" name="house" id="val_house" value="<?php
                    if (isset($user_info)&&(!empty($user_info['house'])))
                      {
                        print_r(trim($user_info['house'])); }?>"
                  size="35"
              > <button class="save_form" type="button" for="house">Сохранить </button>
            </span>

            <span id="house"  class="edit_element" alt="house">
              <?php
                if (isset($user_info)&&(!empty($user_info['house'])))
                  {
                    print_r(trim($user_info['house']));
                  }
              ?>
            </span>
          </td>
        <td class="col-3"><button class="change-info-btn edit_element" type="button" alt="house"></button></td>
      </tr>
      <tr class="tr">
        <td class="col-1">Квартира:</td>
        <td class="col-2">
          <span id="input_flat">
            <input type="text" name="flat" id="val_flat" value="<?php
                  if (isset($user_info)&&(!empty($user_info['flat'])))
                    {
                      print_r(trim($user_info['flat'])); }?>"
                size="35"
            > <button class="save_form" type="button" for="flat">Сохранить </button>
          </span>

          <span id="flat" class="edit_element" alt="flat">
            <?php
              if (isset($user_info)&&(!empty($user_info['flat'])))
                {
                  print_r(trim($user_info['flat']));
                }
            ?>
          </span>
        </td>
      <td class="col-3"><button class="change-info-btn edit_element" type="button" alt="flat"></button></td>
    </tr>
          <tr class="tr">
            <td class="col-1">Телефон:</td>
            <td class="col-2">
              <span id="input_phone">
                  <input type="tel" placeholder="+__(___) ___-__-__"  name="phone" id="val_phone" value="<?php
                      if (isset($user_info)&&(!empty($user_info['phone'])))
                        {
                          print_r($user_info['phone']); }
 //else {  $a=3804422297571; echo($a);}?>"
                    size="35"
                > <button class="save_form" type="button" for="phone">Сохранить </button>
              </span>

              <span id="phone" class="edit_element" alt="phone">
                <?php
                  if (isset($user_info)&&(!empty($user_info['phone'])))
                    {
                      print_r($user_info['phone']);
                    }
                ?>
              </span>
            </td>
            <td class="col-3"><button class="change-info-btn edit_element" type="button" alt="phone"></button></td>
          </tr>
          <tr class="tr">
            <td class="col-1">Email:</td>
            <td class="col-2">
              <span id="input_email">
                <input type="email" name="email" id="val_email" value="<?php
                      if (isset($user_info)&&(!empty($user_info['email'])))
                        {
                          print_r($user_info['email']); }?>"
                    size="35"
                > <button class="save_form" type="button" for="email">Сохранить </button>
              </span>

              <span id="email" class="edit_element" alt="email">
                <?php
                  if (isset($user_info)&&(!empty($user_info['email'])))
                    {
                      print_r($user_info['email']);
                    }
                ?>
              </span>
            </td>
            <td class="col-3"><button class="change-info-btn edit_element" type="button" alt="email"></button></td>
          </tr>
          
          <tr class="tr">
            <td class="col-1">Фото:    </td>
            <td class='col-2'>  <img src="<?php echo $avatar; ?>" id="val_photo" style="border-radius:60px;padding:0;margin-right:0px; width:40px; height:40px;" ></td>
            <td class='col-3'>
                <button class="change-info-btn edit_element" type="button" alt="photo" id='profile-photo-change'></button></td>
             <input type="file" name="photo-download" id="input_photo" value="" style='display: none'>
            </td>


            
          </tr>
        </table>
      </div>
    </div>
    <!-- /.profile-info -->
    <div class="notification-settings">
      <h3 class="notification-settings-title title-3">Настройка уведомлений</h3>
      <div class="notification-settings-content">
        <div class="checkbox">
          <input id="notification-checkbox-1" type="checkbox" name="notification-checkbox"
          <?php
            if (isset($user_info)&&(!empty($user_info['drop_files_notify'])))
              {
                echo 'checked="checked"';
              }
          ?>
          >
          <label for="notification-checkbox-1" class="notification-label checkbox1">Срок хранения загруженных фото подходит к концу</label>
        </div>
        <div class="checkbox">
          <input id="notification-checkbox-2" type="checkbox" name="notification-checkbox"
          <?php
            if (isset($user_info)&&(!empty($user_info['order_in_transport'])))
              {
                echo 'checked="checked"';
              }
          ?>
          >
          <label for="notification-checkbox-2" class="notification-label checkbox1">Заказ отправлен</label>
        </div>
        <div class="checkbox">
          <input id="notification-checkbox-3" type="checkbox" name="notification-checkbox"
          <?php
            if (isset($user_info)&&(!empty($user_info['client_get_order'])))
              {
                echo 'checked="checked"';
              }
          ?>
          >
          <label for="notification-checkbox-3" class="notification-label checkbox1">Заказ получен клиентом</label>
        </div>
        <div class="checkbox">
          <input id="notification-checkbox-4" type="checkbox" name="notification-checkbox"
          <?php
            if (isset($user_info)&&(!empty($user_info['order_done'])))
              {
                echo 'checked="checked"';
              }
          ?>
          >
          <label for="notification-checkbox-4" class="notification-label checkbox1">Заказ выполнен</label>
        </div>
        <div class="checkbox">
          <input id="notification-checkbox-5" type="checkbox" name="notification-checkbox"
          <?php
            if (isset($user_info)&&(!empty($user_info['order_in_work'])))
              {
                echo 'checked="checked"';
              }
          ?>
          >
          <label for="notification-checkbox-5" class="notification-label checkbox1" >Заказ отправлен в работу</label>
        </div>
          
        <div class="checkbox">
          <input id="notification-checkbox-6" type="checkbox" name="notification-checkbox"
        <?php
            if (isset($user_info)&&(!empty($user_info['order_paid'])))
              {
                echo 'checked="checked"';
              }
          ?>>
          
          <label for="notification-checkbox-6" class="notification-label checkbox1" >Заказ оплачен</label>
        </div>
          
          <div class="checkbox">
          <input id="notification-checkbox-7" type="checkbox" name="notification-checkbox"
           <?php
            if (isset($user_info)&&(!empty($user_info['download_done'])))
              {
                echo 'checked="checked"';
              }
          ?>
          >
         
          <label for="notification-checkbox-7" class="notification-label checkbox1" >Загрузка фото завершена</label>
        </div>
          
      </div>
    </div>
    <!-- /.notification-settings -->
  </div>
  <!-- /.col -->
</form>
</div>
<!-- /.row -->
<?php } else {?>
  Авторизируйтесь, для получения доступа к этой странице
  <?php } ?>
