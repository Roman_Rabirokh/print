<?php if(!defined("_APP_START")) { exit(); }  ?>
<script src="<?php echo $page-> getTemplateUrl(); ?>js/jquery.js"></script>
<script>

	$( document ).ready(function() {
		$( "#callback" ).click(function() {
				if($('#detail').val()){
					text=$('#detail').val();
					userid=<?php echo $user->getid(); ?>;
					//отправляем
					$.ajax({
		        type: 'POST',
		        url: '/save/callback/',
		        data: {
		          text: text,
		          userid: userid
		        },
		        success: function(response) {
		          //alert(response);
							$('#detail').val('');
							  $.fancybox({type: "html", content:'Отзыв отправлен, благодарим!',  wrapCSS: 'alertmes',autoCenter:true,minWidth:300,minHeight:150,overlay: {locked: false}});
							  

		        }
		      });
				}
				else{
					  $.fancybox({type: "html", content:'Отзыв не написан.',  wrapCSS: 'alertmes',autoCenter:true,minWidth:300,minHeight:150,overlay: {locked: false}});
				
				}
			});
	});
</script>
<?php
if($user->Authorized()) { ?>

	<form>
		<div>
			<textarea id="detail" cols="50" rows="10"></textarea>
		</div>
		<button type="button" class="btn-print-order btn" id="callback">Отправить отзыв</button>
	</form>

<?php }
else {
	echo'Для отправки отзыва необходимо авторизироваться';
}
?>
