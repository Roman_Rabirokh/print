<script src="<?php echo $page->getTemplateUrl(); ?>js/jquery.js"></script>
<script type="text/javascript">
    function save_form(formid) {
        paper_size = $('#paper_size_' + formid).val();
        paper_type = $('#paper_type_' + formid).val();
        count = $('#count_' + formid).val();
        filds = $('#filds_' + formid).val();
        description = $('#description_' + formid).val();
        font = $('#font_' + formid).val();
        font_size = $('#font_size_' + formid).val();

        input_font_b = $('#input_font_b_' + formid).val();
        input_font_i = $('#input_font_i_' + formid).val();
        input_font_u = $('#input_font_u_' + formid).val();
        color_text = $('#input_color_text_' + formid).val();
        text_orientation = $('#paper_text_orientation_' + formid).val();
        field_width = $('#paper_field_width_' + formid).val();
        framing = $('#paper_framing_' + formid).val();
        photo_orientation = $('#paper_photo_orientation_' + formid).val();



        if ($('#checkbox_photo-date-checkbox-' + formid).attr('checked')) {
            write_date = '1';
        } else {
            write_date = '0';
        }

        if ($('#checkbox_photo-autocorrect-checkbox-' + formid).attr('checked')) {
            autocorrect = '1';
        } else {
            autocorrect = '0';
        }
        if ($('#checkbox_photo-back_side_text-checkbox-' + formid).attr('checked')) {
            back_side_text = '1';
        } else {
            back_side_text = '0';
        }

        date_stam = $('#date_stam_' + formid).val();


        if ($('#checkbox_photo-coords-checkbox-' + formid).attr('checked')) {
            write_coordinates = '1';
        } else {
            write_coordinates = '0';
        }

        if ($('#checkbox_photo-container-checkbox-' + formid).attr('checked')) {
            is_print = '1';
        } else {
            is_print = '0';
        }

        $.ajax({
            type: 'POST',
            url: '/save/properties/',
            data: {
                imageid: formid,
                paper_size: paper_size,
                paper_type: paper_type,
                count: count,
                filds: filds,
                description: description,
                font: font,
                font_size: font_size,
                input_font_b: input_font_b,
                input_font_i: input_font_i,
                input_font_u: input_font_u,
                write_date: write_date,
                date_stam: date_stam,
                write_coordinates: write_coordinates,
                is_print: is_print,
                color_text: color_text,
                text_orientation: text_orientation,
                autocorrect: autocorrect,
                field_width: field_width,
                framing: framing,
                photo_orientation: photo_orientation,
                back_side_text: back_side_text
            },
            success: function (response) {
                //alert(response);
            }
        });
    }
    function confirm_delete() {
        return confirm;
    }

    function delete_img(imageid, filename) {


        if (imageid) {

            $.ajax({
                type: 'POST',
                url: '/del/imgprop/',
                data: {imageid: imageid, filename: filename},
                success: function (response) {
                    $.fancybox(response, {
                        type: "html", wrapCSS: 'alertmes', autoCenter: true, minWidth: 300, minHeight: 150, overlay: {
                            locked: false
                        }
                    });
                }
            });
            //убираем элемент из вью
            $("#" + imageid).hide();
        } else {
            $.fancybox({type: "html", content: 'Изображение отсутствует', wrapCSS: 'alertmes', autoCenter: true, minWidth: 300, minHeight: 150, overlay: {locked: false}});
            die;
        }
    }

    $(document).ready(function () {

        $('.all_input').keyup(function ()
        {
            var count = $(this).val();
            if (count <= 0 || count >= 1000000)
                $(this).val('');

        });

        // изменяем все checkbox

        $('html').on('click', '.checkbox1', function () {
            //фиксим баг клика со второго раза
            $(this).prev('input').prop("checked", function (i, val) {
                return !val;
            });
            event.preventDefault();

            imgid = $(this).attr('id');
            cid = 'checkbox_' + imgid;


            if ($('#' + cid).attr('checked')) {
                $('#' + cid).removeAttr('checked');
                count = -1;
            } else {
                $('#' + cid).attr('checked', 'checked');
                count = 1;
            }

            if ($('#' + cid).attr('name') == 'is_print') {

                //меняем общее количество выбранных фото
                tci = parseInt($('#checked_images').text());
                tci = tci + count;
                $('#checked_images').text(tci);

            }

            if ($('#' + cid).attr('name') == 'photo-date-checkbox') {
                var check_input = $('#' + cid);
                if (check_input.prop('checked'))
                {
                    check_input.parent().parent().next('div').show();
                } else
                {
                    check_input.parent().parent().next('div').hide();
                }
            }

            //сохранение
            formid = $(this).parents("form").attr("id");
            save_form(formid);

        });


        //вешаем клики на счетчики
        $(".counter-btn").click(function () {

            //-
            if ($(this).hasClass('copies-minus-btn'))
            {
                c_id = $(this).attr("for");
                input_val = parseInt($("#" + c_id).val());
                if (input_val > 1) {
                    input_val = input_val - 1;
                    $("#" + c_id).val(input_val);
                    $('#top_checked_' + c_id).text(input_val);
                }
            }
            //+
            if ($(this).hasClass('copies-plus-btn'))
            {
                c_id = $(this).attr("for");
                input_val = parseInt($("#" + c_id).val());
                input_val = input_val + 1;
                $("#" + c_id).val(input_val);
                $('#top_checked_' + c_id).text(input_val);

            }
            // fields-minus-btn
            if ($(this).hasClass('fields-minus-btn'))
            {
                c_id = $(this).attr("for");
                input_val = parseInt($("#" + c_id).val());
                if (input_val > 0) {
                    input_val = input_val - 2;
                    $("#" + c_id).val(input_val);

                }
            }
            // fields-plus-btn
            if ($(this).hasClass('fields-plus-btn'))
            {
                c_id = $(this).attr("for");
                input_val = parseInt($("#" + c_id).val());
                input_val = input_val + 2;
                $("#" + c_id).val(input_val);
            }
            
            if ($(this).hasClass('counter_all'))
            {
                 if ($(this).hasClass('copies-minus-btn'))
                {
                    var val=$('#count_all').val();
                    $('.copies-minus-btn').each(function()
                    {
                        c_id = $(this).attr("for");
                        $("#" + c_id).val(val);
                    });
                }
                if ($(this).hasClass('copies-plus-btn'))
                {
                    var val=$('#count_all').val();
                    $('.copies-minus-btn').each(function()
                    {
                        c_id = $(this).attr("for");
                        $("#" + c_id).val(val);
                    });
                }
             save_all_parameters_images($('#count_all'));
            }
            if ($(this).hasClass('filds_counter_all'))
            {
                 if ($(this).hasClass('fields-plus-btn'))
                {
                    var val=$('#filds_all').val();
                    $('.fields-plus-btn').each(function()
                    {
                        c_id = $(this).attr("for");
                        $("#" + c_id).val(val);
                    });
                }
                if ($(this).hasClass('fields-minus-btn'))
                {
                    var val=$('#filds_all').val();
                    $('.fields-minus-btn').each(function()
                    {
                        c_id = $(this).attr("for");
                        $("#" + c_id).val(val);
                    });
                }
             save_all_parameters_images($('#filds_all'));
            }
            
            
            if(!$(this).hasClass('filds_counter_all') && !$(this).hasClass('counter_all'))
            {
                formid = $(this).parents("form").attr("id");
                save_form(formid);
            }

        });


        //button click
        $(":button").click(function () {
            if ($(this).hasClass("font-style-btn")) {
                if ($(this).hasClass("active")) {
                    $("#input_" + $(this).attr('id')).val('0');
                } else {
                    $("#input_" + $(this).attr('id')).val('1');
                }
            }

            formid = $(this).parents("form").attr("id");

            save_form(formid);


        });

        //textarea save
        $("textarea").keyup(function () {
            formid = $(this).parents("form").attr("id");
            save_form(formid);


        });


        //отправка формы при изменении
        $('form :input').change(function () {


            formid = $(this).parents("form").attr("id");
            save_form(formid);
        });


        //меняем топ размер бумаги или тип бумаги
        $("select").change(function () {
            name = $(this).attr("name");
            if (name == 'paper_size' || name == 'paper_type') {
                tid = $(this).attr("id");
                val = $("#" + tid + " option:selected").text();
                $('#top_checked_' + tid).text(val);
            }
        });


//делаем копию изображения
        $(".make_copy").click(function () {
            filename = $(this).attr("alt");
            orderid = '<?php print_r($_GET['id']) ?>';
            $.ajax({
                type: 'POST',
                url: '/duplicate/image/',
                data: {filename: filename, orderid: orderid},
                success: function (response) {
                    $.fancybox(response, {
                        type: "html", wrapCSS: 'alertmes', autoCenter: true, minWidth: 300, minHeight: 150, overlay: {
                            locked: false
                        }
                    });
                    location.reload();
                }
            });

        });

        $('input[name=color_text]').change(function ()
        {
            formid = $(this).parents("form").attr("id");
            save_form(formid);
        });

////выбрать все
//$("#selectall").click(function () {
//  $('.isprint').each(function(){
//				this.checked = true;
//        $('#'+$(this).attr('id')).attr( 'checked', 'checked' );
//        //save
//        formid=$(this).parents("form").attr("id");
//        save_form(formid);
//        //обновляем количество в топе
//          $('#checked_images').text('<?php //if (isset($images)&&!empty($images)) print_r(count($images));      ?>');
//			});
//
//});
//
////deselect all
//$("#deselectall").click(function () {
//  $('.isprint').each(function(){
//				this.checked = false;
//        //save
//        $('#'+$(this).attr('id')).removeAttr('checked');
//        formid=$(this).parents("form").attr("id");
//        save_form(formid);
//
//        //обновляем количество в топе
//          $('#checked_images').text('0');
//			});
//});



//sorting
        $("#sorting").change(function () {
            orderid =<?php echo $_GET['id']; ?>;
            order = $(this).val();
            $.ajax({
                type: 'POST',
                url: '/sorting/all/',
                data: {
                    order: order
                },
                success: function (response) {
                    //alert(response);
                    location.reload();
                }
            });
        });

        //paging
        $("#paging").change(function () {

            //orderid=<?php echo $_GET['id']; ?>;
            paging = $(this).val();
            $.ajax({
                type: 'POST',
                url: '/paging/all/',
                data: {
                    paging: paging
                },
                success: function (response) {
                    //alert(response);
                    location.reload();
                }
            });



        });

    });

</script>


<?php
if (!defined("_APP_START")) {
    exit();
}
$page->addCSS('/templates/css/properties.css');
?>
<!-- /.container -->
<div class="filter">
    <div class="container filt-conteiner">
        <div class="row">
            <div class="col-xl-3 col-lg-4 col-md-12">
                <div class="filter-select-links">Выбрать:
                    <a href="javascript:void(0);" class="filter-select-link" id="selectall">Все</a>
                    <span class="filter-separator-line">|</span>
                    <a href="javascript:void(0);" class="filter-select-link" id="deselectall">Ни одной</a>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-xl-7 col-lg-5 col-md-12 wrap-inline wrap-sort-ord">
                <div class="filter-sort">

                    <span class="filter-sort-name">Сортировать по:</span>
                    <div class="filter-select-wrap select-wrap min-select">
                        <select class="filter-sort-select select" id="sorting">
                            <option value="1"  <?php
                            if (isset($_SESSION['sorting']) && (!empty($_SESSION['sorting'])) && ($_SESSION['sorting'] == '1')) {
                                echo 'selected="selected"';
                            }
                            ?>>Размеру</option>
                            <option value="2" <?php
                            if (isset($_SESSION['sorting']) && (!empty($_SESSION['sorting'])) && ($_SESSION['sorting'] == '2')) {
                                echo 'selected="selected"';
                            }
                            ?>>Названию</option>
                            <option value="3" <?php
                            if (isset($_SESSION['sorting']) && (!empty($_SESSION['sorting'])) && ($_SESSION['sorting'] == '3')) {
                                echo 'selected="selected"';
                            }
                            ?>>По ID (убывание)</option>
                        </select>
                    </div>
                </div>
                <div class="filter-sort">
                    <span class="filter-sort-name">Показывать по:</span>
                    <div class="filter-select-wrap select-wrap min-select">
                        <select class="filter-sort-select select" id="paging">
                            <option value="10"  <?php
                            if (isset($_SESSION['paging']) && (!empty($_SESSION['paging'])) && ($_SESSION['paging'] == '10')) {
                                echo 'selected="selected"';
                            }
                            ?>>10</option>
                            <option value="25" <?php
                            if (isset($_SESSION['paging']) && (!empty($_SESSION['paging'])) && ($_SESSION['paging'] == '25')) {
                                echo 'selected="selected"';
                            }
                            ?>>25</option>
                            <option value="50" <?php
                            if (isset($_SESSION['paging']) && (!empty($_SESSION['paging'])) && ($_SESSION['paging'] == '50')) {
                                echo 'selected="selected"';
                            }
                            ?>>50</option>
                            <option value="100" <?php
                            if (isset($_SESSION['paging']) && (!empty($_SESSION['paging'])) && ($_SESSION['paging'] == '100')) {
                                echo 'selected="selected"';
                            }
                            ?>>100</option>
                        </select>
                    </div>

                </div>
            </div>
            <!-- /.col -->
            <div class="col-xl-2 col-lg-12 col-md-12 calculate-the-cost">
                <div class="filter-buttons">
                    <!--
                    <a href="#" class="filter-btn btn">Сохранить параметры</a>
                    -->
                    <a href="/personal/order-cost/?id=<?php print_R($_GET['id']); ?><?php if (isset($_GET['edit']) && ($_GET['edit'] == 1)) echo "&edit=1"; ?> " class="filter-btn btn calculate-value">Рассчитать стоимость</a>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /.filter -->
<div class="filter-info">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-5 col-xs-8">
                <div class="filter-info-text">Выбраны <span id="checked_images"><?php
                        if (isset($topcount) && (!empty($topcount))) {
                            echo $topcount;
                        } else {
                            echo '0';
                        }
                        ?></span> из <span id="all_images"><?php if (!empty($allPhotos)) print_r($allPhotos); ?></span></div>
            </div>
            <!-- /.col -->
            <div class="col-xl-9 col-lg-9 col-md-8 col-sm-7 col-xs-4">
                <div class="filter-arrows clearfix">
                    <?php if (isset($currentPage) && $currentPage > 1) { ?>
                        <a href="/new/paging/<?php echo ($currentPage - 1); ?>" class="filter-arrow filter-arrow-left"></a>
                    <?php } ?>
                    <span>Страница
                        <span>
                            <?php
                            if (isset($currentPage)) {
                                echo $currentPage;
                            }
                            ?>
                        </span>
                        из
                        <span>
                            <?php
                            if (isset($allpage)) {
                                echo $allpage;
                            }
                            ?>
                        </span>
                    </span>
                    <?php if (isset($allpage) && isset($currentPage) && $currentPage < $allpage) { ?>
                        <a href="/new/paging/<?php echo ($currentPage + 1); ?>" class="filter-arrow filter-arrow-right"></a>
                    <?php } ?>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /.filter-info -->
<div class="container all-params-images">
    <div class="all-params" style="">
        <p class="text-center"> Параметры для всех фото:</p>
        <div class="photo-container-top allparam-img">

            <div class="photo-col photo-col-1 inline-block top-main-first">
                <div class="same-size">
                    <span class="photo-parameter-text">Размер</span>
                    <div class="photo-parameter">
                        <div class="select-wrap">
                            <select class="select paper_size_select" 
                                    name="paper_size" id="paper_size_all">
                                 <option value="0">Не выбрано</option>
                                        <?php
                                        foreach ($base_pp as $key => $value) {
                                            if ($value['paper_content_type'] == 'size') {
                                                ?>
                                        <option value="<?php echo $value['id'] ?>" ><?php echo $value['paper_size'] ?> </option>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="same-size">
                    <span class="photo-parameter-text">Бумагa</span>
                    <div class="photo-parameter">
                        <div class="select-wrap">
                            <select class="select select_paper_type" 
                                    name="paper_type" id="paper_type_all">
                                <option value="0">Не выбрано</option>
                                        <?php
                                        foreach ($base_pp as $key => $value) {
                                            if ($value['paper_content_type'] == 'type') {
                                                ?>
                                        <option value="<?php echo $value['id'] ?>"><?php echo $value['paper_type'] ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="same-size first">
                    <span class="photo-parameter-text">Количество</span>
                    <div class="photo-parameter">
                        <div class="counter-input">
                            <input id="count_all" 
                                   type="number" name="count" class="input_count" value="1"
                                     min="1" max="5" readonly="" >
                            <button type="button"  for="count_all" class="copies-minus-btn counter-minus-btn counter-btn counter_all" 
                                    data-type="minus" data-field="count" 
                                    >-</button>
                            <button type="button" for="count_all" class="copies-plus-btn counter-plus-btn counter-btn counter_all"  
                                    data-type="plus" data-field="count">
                                +</button>
                        </div>
                    </div>
                </div>
                <div class="same-size last">
                    <span class="photo-parameter-text">Поля</span>
                    <div class="photo-parameter">
                        <div class="counter-input">
                            <input  id="filds_all" 
                                    name="filds" type="number" class="input_filds" value="0" readonly>
                            <button type="button" for="filds_all" class="fields-minus-btn counter-minus-btn counter-btn filds_counter_all">-</button>
                            <button type="button" for="filds_all" step="2" class="fields-plus-btn counter-plus-btn counter-btn filds_counter_all">
                                +</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.photo-container-top -->



        <div class="photo-col photo-col-1">
            <div class="photo-autocorrect-checkbox checkbox">
                <input id="checkbox_photo-autocorrect-checkbox-all" type="checkbox" 
                       name="autocorrect" >
                <label id="photo-autocorrect-checkbox-all" for="checkbox_photo-autocorrect-checkbox-all" class="photo-autocorrect-label 
                       checkbox1">Автокоррекция</label>
            </div>

            <div class="photo-parameter-text">Ширина полей(мм):</div>
            <div class="select-wrap">
                <select class="select field_width_select" name="field_width" id="paper_field_width_all">
                    <option value=0 selected="selected">Не выбрано</option>
                    <option value=2  >2</option>
                    <option value=4>4</option>
                    <option value=6 >6</option>
                    <option value=8 >8</option>
                    <option value=10 >10</option>

                </select>
            </div>
            <div class="photo-parameter-text">Кадрирование:</div>
            <div class="select-wrap">
                <select class="select framing_select"  name="framing" id="paper_framing_all">
                    <option value=3>Не выбрано</option>
                    <option value=0>уменьшение с белыми полями</option>
                    <option value=1 >обрезка по меньшей границе</option>

                </select>
            </div>

            <div class = "photo-parameter-text">Ориентация
                фото:</div>
            <div class = "select-wrap">
                <select class = "select photo_orientation_select"
                        name = "photo_orientation" id = "paper_photo_orientation_all">
                     <option value=3>Не выбрано</option>
                     <option value = 0 >портрет</option>
                     <option value=1>ландшафт</option>

                </select>
            </div>
        </div>
        <div class="photo-col photo-col-2">

            <div class="photo-parameter font-text">
                <div class="select-wrap">
                    <select id="font_all" class="select" name="font">
                        <option value = 0 >Не выбрано</option>
                        <?php
                        foreach ($base_pp as $key => $value) {
                            if ($value['paper_content_type'] == 'font') {
                                ?>
                                <option value="<?php echo $value['id'] ?>" <?php
                                ?>><?php echo $value['paper_font'] ?></option>
                                        <?php
                                    }
                                }
                                ?>
                    </select>
                </div>
            </div>

            <div class="photo-parameter clearfix font-text">
                <div class="font-size-select select-wrap selectfont">
                    <select id="font_size_all" class="select select_font_size" name="font_size">
                        <option value = 0 >Не выбрано</option>
                        <?php
                        foreach ($base_pp as $key => $value) {
                            if ($value['paper_content_type'] == 'font_size') {
                                ?>
                                <option value="<?php echo $value['id'] ?>"> 
                                    <?php echo $value['paper_font_size'] ?></option>
                                        <?php
                                    }
                                }
                                ?>
                    </select>
                </div>
                <div class="font-style-buttons">
                    <input id="input_font_b_all" type="hidden" name="font_b" value="">
                    <button type="button" id="font_b_all" class="font-bold-btn font-style-btn">B</button>
                    <input id="input_font_i_all" type="hidden" name="font_i" value="">
                    <button type="button" id="font_i_all" class="font-italic-btn font-style-btn">I</button>
                    <input id="input_font_u_all" type="hidden" name="font_u" value="">
                    <button type="button" id="font_u_all" class="font-underline-btn font-style-btn">U</button>
                </div>
            </div>
            <div class="color"> Цвет текста:
                <?php
                if (empty($image['color_text'])) {
                    $image['color_text'] = 'black';
                }
                ?>

            </div>
            <input class="oval-elem" type="color" id="input_color_text_all" name="color_text" value="black">
            <div class="photo-parameter-text margin-param">Позиция текста:</div>
            <div class="select-wrap margin-param">
                <select class="select text_orientation_select" name="text_orientation" id="paper_text_orientation_all">
                    <option value = 0 >Не выбрано</option>
                    <option value="Левый верхний">Левый верхний</option>
                    <option value="Левый нижний">Левый нижний</option>
                    <option value="Правый верхний">Правый верхний</option>
                    <option value="Правый нижний" >Правый нижний</option>
                    <option value="Центр">Центр</option>

                </select>
            </div>
            <br/>
        </div>
        <div class="photo-col photo-col-2 block-with-textarea">
            <div class="photo-parameter photo-textarea-wrap">
                <div class="photo-parameter-text">Подпись к фотографии</div>
                <textarea id="description_all" class="photo-parameter-textarea"  name="description"></textarea>
                <div class="photo-back_side_text-checkbox checkbox margin-param">
                    <input id="checkbox_photo-back_side_text-checkbox-all" type="checkbox"  name="photo-back_side_text-checkbox" class="">
                    <label id="photo-back_side_text-checkbox-all" for="checkbox_photo-back_side_text-checkbox-all" class="photo-back_side_text-label checkbox1">Текст на обратной стороне</label>
                </div>
            </div>
        </div>
        <div class="photo-col-check photo-col-1 last-param"> 
            <div class="photo-parameter photo-date-wrap">
                <div class="photo-date-checkbox checkbox">
                    <input id="checkbox_photo-date-checkbox-all" type="checkbox" name="photo-date-checkbox" >
                    <label id="photo-date-checkbox-all" for="checkbox_photo-date-checkbox-all" class="photo-date-label checkbox1">Печатать дату</label>
                </div>
            </div>
            <div class="photo-parameter photo-dateformat-wrap" style="display: none" id="date_all">
                <div class="select-wrap">
                    <select id="date_stam_all" name="date_stam" class="dateformat-select select">
                        <?php
                        foreach ($base_pp as $key => $value) {
                            if ($value['paper_content_type'] == 'date') {
                                ?>
                                <option value="<?php echo $value['id'] ?>"><?php echo $value['paper_date'] ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="delete-photo-col photo-col-1">
            <span id='delete_all_images'>Удалить все фото</span>
            <form id="all-params-form" action="/save/all/properties/" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="idorder" value="<?= $_GET['id'] ?>">
            </form>
        </div>





    </div>
</div>


<!-- /.filter-info all-params -->

<div class="photo-container-wrap">

    <div class="photo-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">




                    <?php
                    if (isset($images) && (!empty($images))) {

                        foreach ($images as $image) {
                        $style='';
                        $style=$style.'color:'.$image['color_text'].';';
                        if($image['font_b'])
                            $style=$style.'font-weight:bolder;';
                        if($image['font_i'])
                            $style=$style.'font-style:italic;';
                        if($image['font_u'])
                            $style=$style.'text-decoration:underline;';
                        foreach($base_pp as $param)
                        {
                            if($param['paper_content_type']=='font')
                            {
                                if($param['id']==$image['font'])
                                {
                                    if($param['id']==6)
                                    {
                                         $style=$style.'font-family:Arial;';
                                    }
                                    if($param['id']==8)
                                    {
                                         $style=$style.'font-family:alexandra-script;';
                                    }
                                }
                            }
                            if($param['paper_content_type']=='font_size')
                            {
                                if($param['id']==$image['font_size'])
                                {
                                    if($image['font']!=8)
                                    {
                                        $style=$style.'font-size:'.$param['paper_font_size'].'px;';
                                    }
                                    else
                                    {
                                        $size=$param['paper_font_size']*2;
                                        $style=$style.'font-size:'.$size.'px;';
                                    }
                                }
                            }
                        }
                            ?>
                            <form id="<?php echo $image['id'] ?>" action="/save/properties/" method="POST" enctype="multipart/form-data" 
                                <div id="allimage+<?php echo $image['id'] ?>">
                                    <div class="photo-container-top">
                                        <div class="photo-container-checkbox checkbox">
                                            <input id="checkbox_photo-container-checkbox-<?php echo $image['id'] ?>" class="isprint" type="checkbox" name="is_print" <?php
                                            if (isset($image['is_print']) && (!empty($image['is_print']))) {
                                                echo 'checked="checked"';
                                            }
                                            ?>>
                                            <label id="photo-container-checkbox-<?php echo $image['id'] ?>"  for="checkbox_photo-container-checkbox-<?php echo $image['id'] ?>" class="photo-container-label checkbox1 first-checkOrder"></label>
                                        </div>
                                        <div class="photo-col photo-col-1 inline-block block-small">
                                            <span class="photo-parameter-text">Размер</span>
                                            <div class="photo-parameter">
                                                <div class="select-wrap">
                                                    <select class="select paper_size_select" 
                                                            name="paper_size" for="<?php echo $image['id'] ?>" id="paper_size_<?php echo $image['id'] ?>">
                                                                <?php
                                                                foreach ($base_pp as $key => $value) {
                                                                    if ($value['paper_content_type'] == 'size') {
                                                                        ?>
                                                                <option value="<?php echo $value['id'] ?>" 
                                                                <?php
                                                                if (isset($image['paper_size']) && (!empty($image
                                                                                ['paper_size'])) && ($image['paper_size'] == $value['id'])) {
                                                                    echo
                                                                    'selected="selected"';
                                                                }
                                                                ?>><?php echo $value['paper_size'] ?> </option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <span class="photo-parameter-text">Бумагa</span>
                                            <div class="photo-parameter">
                                                <div class="select-wrap">
                                                    <select class="select select_paper_type" 
                                                            name="paper_type" id="paper_type_<?php echo $image['id'] ?>">
                                                                <?php
                                                                foreach ($base_pp as $key => $value) {
                                                                    if ($value['paper_content_type'] == 'type') {
                                                                        ?>
                                                                <option value="<?php echo $value['id'] ?>" 
                                                                <?php
                                                                if (isset($image['paper_type']) && (!empty($image
                                                                                ['paper_type'])) && ($image['paper_type'] == $value['id'])) {
                                                                    echo
                                                                    'selected="selected"';
                                                                }
                                                                ?>><?php echo $value['paper_type'] ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <span class="photo-parameter-text other-param-text">Количество</span>
                                            <div class="photo-parameter other-param">
                                                <div class="counter-input">
                                                    <input id="count_<?php echo $image['id'] ?>" 
                                                           type="number" name="count" class="input_count" value="<?php
                                                           if
                                                           (isset($image['count']) && (!empty($image['count']))) {
                                                               echo $image
                                                               ['count'];
                                                           } else {
                                                               echo '1';
                                                           }
                                                           ?>"  min="1" max="5" readonly="" >
                                                    <button type="button"  for="count_<?php
                                                    echo $image
                                                    ['id']
                                                    ?>" class="copies-minus-btn counter-minus-btn counter-btn" 
                                                            data-type="minus" data-field="count<?php echo $image['id'] ?>" 
                                                            >-</button>
                                                    <button type="button" for="count_<?php
                                                    echo $image
                                                    ['id']
                                                    ?>" class="copies-plus-btn counter-plus-btn counter-btn"  
                                                            data-type="plus" data-field="count<?php echo $image['id'] ?>">
                                                        +</button>
                                                </div>
                                            </div>
                                            <span class="photo-parameter-text other-param-text">Поля</span>
                                            <div class="photo-parameter other-param">
                                                <div class="counter-input">
                                                    <input  id="filds_<?php echo $image['id'] ?>" 
                                                            name="filds" type="number" class="input_filds" value="<?php
                                                            if
                                                            (isset($image['filds']) && (!empty($image['filds']))) {
                                                                echo $image
                                                                ['filds'];
                                                            } else {
                                                                echo '0';
                                                            }
                                                            ?>" readonly>
                                                    <button type="button" for="filds_<?php
                                                    echo $image
                                                    ['id']
                                                    ?>" class="fields-minus-btn counter-minus-btn counter-btn">-</button>
                                                    <button type="button" for="filds_<?php
                                                    echo $image
                                                    ['id']
                                                    ?>" step="2" class="fields-plus-btn counter-plus-btn counter-btn">
                                                        +</button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="photo-container-del del-1-img"  onclick="if (confirm_delete('<?php echo $image['id'] ?>'))
                                                            delete_img('<?php echo $image['id'] ?>', '<?php echo $image['filename'] ?>')" style="cursor:pointer"></div>
                                    </div>
                                    <!-- /.photo-container-top -->


                                    <div class="photo-container-content clearfix">
                                        <div class="photo-container-img marge1">
        <!--                                    <a   class="single_2"><img src="/files/images/<?php //print_r($image['filename'])       ?>" alt=""></a>-->
                                            <a class="single_2 single-photo-order"><img src="<?php print_r($image['img']) ?>" alt=""></a>
                                            <a href="<?php print_r($image['trumb']) ?>" class="img-modal-btn" idImage="<?= $image['id'] ?>" >Предпросмотр</a>
                                        </div>
                                        <div class="photo-container-parameters  photo-container-parameters1 clearfix">

                                            <!-- /.col-1 -->
                                            <div class="photo-col photo-col-1">
                                                <div class="photo-autocorrect-checkbox checkbox">
                                                    <input class="autocorrect-inputs" id="checkbox_photo-autocorrect-checkbox-<?php echo $image['id'] ?>" type="checkbox" 
                                                           name="photo-autocorrect-checkbox" <?php
                                                           if (isset($image
                                                                           ['autocorrect']) && (!empty($image['autocorrect']))) {
                                                               echo
                                                               'checked="checked"';
                                                           }
                                                           ?> >
                                                    <label id="photo-autocorrect-checkbox-<?php
                                                    echo
                                                    $image['id']
                                                    ?>" for="checkbox_photo-autocorrect-checkbox-<?php echo $image['id'] ?>" class="photo-autocorrect-label 
                                                           checkbox1">Автокоррекция</label>
                                                </div>

                                                <div class="photo-parameter-text">Ширина полей(мм):</div>
                                                <div class="select-wrap">
                                                    <select class="select field_width_select" 
                                                            name="paper_size" id="paper_field_width_<?php echo $image['id']
                                                    ?>">
                                                        <option value=2 <?php
                                                        if
                                                        ($image['field_width'] == 2) {
                                                            ?> selected="selected" <?php }
                                                        ?> >2</option>
                                                        <option value = 4 <?php if ($image['field_width'] == 4) {
                                                            ?> selected="selected" <?php } ?> >4</option>
                                                        <option value=6  <?php if ($image['field_width'] == 6) { ?> selected="selected" <?php } ?>>6</option>
                                                        <option value=8  <?php if ($image['field_width'] == 8) { ?> selected="selected" <?php } ?>>8</option>
                                                        <option value=10  <?php if ($image['field_width'] == 10) { ?> selected="selected" <?php } ?>>10</option>

                                                    </select>
                                                </div>
                                                <div class="photo-parameter-text">Кадрирование:</div>
                                                <div class="select-wrap">
                                                    <select class="select framing_select" 
                                                            name="paper_size" id="paper_framing_<?php echo $image['id'] ?>">
                                                        <option value=0 <?php if ($image['framing'] == 0) {
                                                            ?> selected="selected" <?php } ?>>уменьшение с белыми полями</option>
                                                        <option value = 1 <?php if ($image['framing'] == 1) { ?> selected="selected" <?php } ?> >обрезка по меньшей границе</option>

                                                    </select>
                                                </div>

                                                <div class = "photo-parameter-text">Ориентация
                                                    фото:</div>
                                                <div class = "select-wrap">
                                                    <select class = "select photo_orientation_select"
                                                            name = "paper_size" id = "paper_photo_orientation_<?php
                                                            echo $image
                                                            ['id']
                                                            ?>">
                                                        <option value = 0 <?php
                                                        if
                                                        ($image['photo_orientation'] == 0) {
                                                            ?> selected="selected" <?php } ?>>портрет</option>
                                                        <option value=1  <?php if ($image['photo_orientation'] == 1) { ?> selected="selected" 
                                                                <?php } ?>>ландшафт</option>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="photo-col photo-col-2 photowprap2 photParWr">
                                                <div class="photo-parameter photo-textarea-wrap block-order-textarea order-textareaTempl ">
                                                    <div class="photo-parameter-text">Подпись к фотографии</div>
                                                    <textarea id="description_<?php echo $image['id'] ?>" class="photo-parameter-textarea" placeholder="Введите текст подписи..." name="description" style="<?=$style?>"><?php
                                                        if (isset($image['description']) && (!empty($image['description']))) {
                                                            echo $image['description'];
                                                        }
                                                        ?></textarea>
                                                </div>
                                                <div class="photo-parameter font-text font-text12">
                                                    <div class="select-wrap">
                                                        <select id="font_<?php echo $image['id'] ?>" class="select select_font" name="font">
                                                            <?php
                                                            foreach ($base_pp as $key => $value) {
                                                                if ($value['paper_content_type'] == 'font') {
                                                                    ?>
                                                                    <option value="<?php echo $value['id'] ?>" <?php
                                                                    if (isset($image['font']) && (!empty($image['font'])) && ($image['font'] == $value['id'])) {
                                                                        echo 'selected="selected"';
                                                                    }
                                                                    ?>><?php echo $value['paper_font'] ?></option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="color"> Цвет текста:
                                                    <?php
                                                    if (empty($image['color_text'])) {
                                                        $image['color_text'] = 'black';
                                                    }
                                                    ?>

                                                </div>
                                                <div class="photo-parameter clearfix font-text">
                                                    <div class="font-size-select select-wrap">
                                                        <select  id="font_size_<?php echo $image['id'] ?>" class="select select_font_size" name="font_size">
                                                            <?php
                                                            foreach ($base_pp as $key => $value) {
                                                                if ($value['paper_content_type'] == 'font_size') {
                                                                    ?>
                                                                    <option value="<?php echo $value['id'] ?>" <?php
                                                                    if (isset($image['font_size']) && (!empty($image['font_size'])) && ($image['font_size'] == $value['id'])) {
                                                                        echo 'selected="selected"';
                                                                    }
                                                                    ?>><?php echo $value['paper_font_size'] ?></option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                        </select>
                                                    </div>
                                                    <div class="font-style-buttons">
                                                        <input id="input_font_b_<?php echo $image['id'] ?>" type="hidden" name="font_b" class="input_font_b" value="<?php
                                                        if (isset($image['font_b']) && (!empty($image['font_b']))) {
                                                            echo '1';
                                                        }
                                                        ?>">
                                                        <button type="button" id="font_b_<?php echo $image['id'] ?>" class="font-bold-btn font-style-btn <?php
                                                        if (isset($image['font_b']) && (!empty($image['font_b']))) {
                                                            echo 'active';
                                                        }
                                                        ?>">B</button>
                                                        <input id="input_font_i_<?php echo $image['id'] ?>" type="hidden" name="font_i" class="input_font_i" value="<?php
                                                        if (isset($image['font_i']) && (!empty($image['font_i']))) {
                                                            echo '1';
                                                        }
                                                        ?>">
                                                        <button type="button" id="font_i_<?php echo $image['id'] ?>" class="font-italic-btn font-style-btn <?php
                                                        if (isset($image['font_i']) && (!empty($image['font_i']))) {
                                                            echo 'active';
                                                        }
                                                        ?>">I</button>
                                                        <input id="input_font_u_<?php echo $image['id'] ?>" type="hidden" name="font_u" class="input_font_u" value="<?php
                                                        if (isset($image['font_u']) && (!empty($image['font_u']))) {
                                                            echo '1';
                                                        }
                                                        ?>">
                                                        <button type="button" id="font_u_<?php echo $image['id'] ?>" class="font-underline-btn font-style-btn <?php
                                                        if (isset($image['font_u']) && (!empty($image['font_u']))) {
                                                            echo 'active';
                                                        }
                                                        ?>">U</button>
                                                    </div>
                                                </div>
                                                <input class="oval-elem color-input" type="color" id="input_color_text_<?php echo $image['id'] ?>" name="color_text" value="<?= $image['color_text'] ?>">
                                                <div class="photo-parameter-text margin-param">Позиция текста:</div>
                                                <div class="select-wrap margin-param">
                                                    <select class="select text_orientation_select" name="paper_size" id="paper_text_orientation_<?php echo $image['id'] ?>">
                                                        <option value="Левый верхний" <?php if ($image['text_orientation'] == 'Левый верхний') { ?> selected="selected" <?php } ?>>Левый верхний</option>
                                                        <option value="Левый нижний"  <?php if ($image['text_orientation'] == 'Левый нижний') { ?> selected="selected" <?php } ?>>Левый нижний</option>
                                                        <option value="Правый верхний"  <?php if ($image['text_orientation'] == 'Правый верхний') { ?> selected="selected" <?php } ?>>Правый верхний</option>
                                                        <option value="Правый нижний"  <?php if ($image['text_orientation'] == 'Правый нижний') { ?> selected="selected" <?php } ?>>Правый нижний</option>
                                                        <option value="Центр"  <?php if ($image['text_orientation'] == 'Центр') { ?> selected="selected" <?php } ?>>Центр</option>

                                                    </select>
                                                </div>
                                                <div class="photo-back_side_text-checkbox checkbox margin-param">
                                                    <input id="checkbox_photo-back_side_text-checkbox-<?php echo $image['id'] ?>" type="checkbox" class="photo-back_side_inputs"  name="photo-back_side_text-checkbox" <?php
                                                    if (isset($image['autocorrect']) && (!empty($image['back_side_text']))) {
                                                        echo 'checked="checked"';
                                                    }
                                                    ?>>
                                                    <label id="photo-back_side_text-checkbox-<?php echo $image['id'] ?>" for="checkbox_photo-back_side_text-checkbox-<?php echo $image['id'] ?>" class="photo-back_side_text-label checkbox1 check-text-downside">Текст на обратной стороне</label>
                                                </div>




                                                <br/>
                                                <br/>
                                                <br/>
                                                <br/>
                                            </div>
                                            <!-- /.col-2 -->
                                            <div class="photo-col photo-col-3 par3">
                                                <div class="photo-parameter photo-date-wrap">
                                                    <div class="photo-date-checkbox checkbox">
                                                        <input id="checkbox_photo-date-checkbox-<?php echo $image['id'] ?>" type="checkbox" class="print-date" name="photo-date-checkbox" <?php
                                                        if (isset($image['write_date']) && (!empty($image['write_date']))) {
                                                            echo 'checked="checked"';
                                                        }
                                                        ?>>
                                                        <label id="photo-date-checkbox-<?php echo $image['id'] ?>" for="checkbox_photo-date-checkbox-<?php echo $image['id'] ?>" class="photo-date-label photo-DatePar checkbox1">Печатать дату</label>
                                                    </div>
                                                </div>
                                                <div class="photo-parameter photo-dateformat-wrap" <?php if (empty($image['write_date'])) { ?> style="display:none" <?php } ?> >
                                                    <div class="select-wrap">
                                                        <select id="date_stam_<?php echo $image['id'] ?>" name="date_stam" class="dateformat-select select date-checkbox">
                                                            <?php
                                                            foreach ($base_pp as $key => $value) {
                                                                if ($value['paper_content_type'] == 'date') {
                                                                    ?>
                                                                    <option value="<?php echo $value['id'] ?>" <?php
                                                                    if (isset($image['date_stam']) && (!empty($image['date_stam'])) && ($image['date_stam'] == $value['id'])) {
                                                                        echo 'selected="selected"';
                                                                    }
                                                                    ?>><?php echo $value['paper_date'] ?></option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="photo-parameter">
                                                    <?php if (isset($image['exif_data'])) { ?>
                                                        <div class="photo-coords-checkbox checkbox">
                                                            <input id="checkbox_photo-coords-checkbox-<?php echo $image['id'] ?>" type="checkbox" name="photo-coords-checkbox" <?php
                                                            if (isset($image['write_coordinates']) && (!empty($image['write_coordinates']))) {
                                                                echo 'checked="checked"';
                                                            }
                                                            ?>>
                                                            <label id="photo-coords-checkbox-<?php echo $image['id'] ?>" for="checkbox_photo-coords-checkbox-<?php echo $image['id'] ?>" class="photo-coords-label checkbox1">Печатать координаты</label>
                                                        </div>
                                                    <?php } ?>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="inline-info-block info-table1 fabl1 tabInf">
                                            <table class="infotable" border="1" width="100%" cellpadding="5">
                                                <tr class="not-even-numbered">
                                                    <th colspan="2"> Данные фото </th>

                                                </tr>
                                                <tr class="even-numbered">
                                                    <td>Высота</td>
                                                    <td id="height-<?php echo $image['id'] ?>"> <?=
                                                        $image['info']
                                                        ['height']
                                                        ?> px </td>
                                                </tr>
                                                <tr class="not-even-numbered">
                                                    <td>Ширина</td>
                                                    <td id="width-<?php echo $image['id'] ?>"> <?=
                                                        $image['info']
                                                        ['width']
                                                        ?> px </td>
                                                </tr>   <tr class="quality-elem-find" class="even-numbered">
                                                    <td>Dpi</td>
                                                    <td id="image-dpi-<?php echo $image['id'] ?>"><?php
                                                        if (!empty($image['info']['dpi'])) {
                                                            echo $image['info']['dpi'];
                                                        } else
                                                            echo '-';
                                                        ?> </td>
                                                </tr>    
                                                <tr class="not-even-numbered">
                                                    <td>Цветовая схема</td>
                                                    <td>  <?=
                                                        $image
                                                        ['info']['color_scheme']
                                                        ?></td>
                                                </tr> 
                                                <tr class="even-numbered">
                                                    <td>Качество</td>
                                                    <td> <?php
                                                        if ($image['info']['quality'] != 0) {
                                                            echo ($image['info']['quality']);
                                                        } else {
                                                            echo '-';
                                                        }
                                                        ?></td>
                                                </tr> 
                                                <tr class="not-even-numbered">
                                                    <td>Дата создания</td>
                                                    <td><?php
                                                        if ($image['info']['create_date'] != '0000-00-00 00:00:00') {
                                                            $image['info']['create_date'] = explode(' ', $image['info']['create_date']);
                                                            echo $image['info']['create_date'][0];
                                                        } else {
                                                            echo '-';
                                                        }
                                                        ?>
                                                    </td>
                                                <tr class="even-numbered">
                                                    <td>Время создания
                                                    </td>
                                                    <td>
                                                        <?
                                                        if (!empty($image['info']['create_date'][1])) {
                                                            echo $image['info']['create_date'][1];
                                                        } else {
                                                            echo '-';
                                                        }
                                                        ?>


                                                    </td>
                                                </tr>    
                                                <tr  class="not-even-numbered">
                                                    <td>Место сьемки</td>
                                                    <td> <?php
                                                        if (!empty($image['info']['address']))
                                                            echo $image['info']['address'];
                                                        else {
                                                            echo '-';
                                                        }
                                                        ?> </td>
                                                </tr> 

                                            </table>

                                        </div>
                                        <div class="quality-photo">
                                            <?php
                                            $id_block='yellow-block-'.$image['id'];
                                            if ($image['info']['dpi'] < 150)
                                                echo '&nbsp;<div id="'.$id_block.'" class="yellow-block"><div><span>Не гарантируем качество фотографии</span></div> </div>';
                                            else if ($image['info']['dpi'] < 290) {
                                                echo '&nbsp<div id="'.$id_block.'" class="yellow-block"><div><span>Фото не высокого качества</span></div></div>';
                                            }
                                            else
                                            {
                                                 echo '&nbsp<div id="'.$id_block.'" class="yellow-block" style="display:none"><div><span></span></div></div>';
                                            }
                                            ?>
                                        </div>
                                        <div> <a href="javascript:void(0);" class="photo-container-btn btn make_copy" id="<?php echo $image['id'] ?>" alt="<?php echo $image['filename'] ?>">Создать копию <br>с другими параметрами</a>
                                            <!-- /.photo-container-parameters -->
                                        </div>
                                    </div>
                            </form>

                            <?php
                        }

//$this->includeComponent("system/pager",array("ALL"=>$data['ALL'],"PAGESIZE"=>$data["PAGESIZE"]));
                    } else {
                        echo 'у Вас нет доступа к этому заказу.';
                    }
                    ?>
                    <!-- /.photo-container-content -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.photo-container -->

</div>
<!-- /.photo-container-wrap -->

</div>

<div class="filter-info">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-5 col-xs-8">
                <div class="filter-info-text"></span></div>
            </div>
            <!-- /.col -->
            <div class="col-xl-9 col-lg-9 col-md-8 col-sm-7 col-xs-4">
                <div class="filter-arrows clearfix">
                    <?php if (isset($currentPage) && $currentPage > 1) { ?>
                        <a href="/new/paging/<?php echo ($currentPage - 1); ?>" class="filter-arrow filter-arrow-left"></a>
                    <?php } ?>
                    <span>Страница
                        <span>
                            <?php
                            if (isset($currentPage)) {
                                echo $currentPage;
                            }
                            ?>
                        </span>
                        из
                        <span>
                            <?php
                            if (isset($allpage)) {
                                echo $allpage;
                            }
                            ?>
                        </span>
                    </span>
                    <?php if (isset($allpage) && isset($currentPage) && $currentPage < $allpage) { ?>
                        <a href="/new/paging/<?php echo ($currentPage + 1); ?>" class="filter-arrow filter-arrow-right"></a>
                    <?php } ?>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /.filter-info -->
</div>
